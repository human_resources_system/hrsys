/**
 * 
 */
package cn.hrsys.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;



/**
 * @author 高巍
 *
 * 时间 ：2018-8-3上午8:44:51
 */
public  class AddUtil {
	
	
	public Map<Integer, String> xingming(){
		Map<Integer, String> xaMap=new HashMap<Integer, String>();
		Map<Integer, String> xingMap = new HashMap<Integer, String>();
		xingMap.put(1, "刘");
		xingMap.put(2, "屈");
		xingMap.put(3, "赵");
		xingMap.put(4, "张");
		xingMap.put(5, "高");
		xingMap.put(6, "张");
		xingMap.put(7, "王");
		xingMap.put(8, "阙");
		xingMap.put(9, "孙");
		Map<Integer, String> mingMap = new HashMap<Integer, String>();
		mingMap.put(1, "雪");
		mingMap.put(2, "军");
		mingMap.put(3, "明");
		mingMap.put(4, "东");
		mingMap.put(5, "巍");
		mingMap.put(6, "天");
		mingMap.put(7, "冬");
		mingMap.put(8, "海");
		mingMap.put(9, "成");
		mingMap.put(10, "杰");
		mingMap.put(11, "俊");
		mingMap.put(12, "伟");
		mingMap.put(13, " ");
		mingMap.put(14, " ");
		mingMap.put(15, " ");
		mingMap.put(16, " ");
		mingMap.put(17, " ");
		mingMap.put(18, " ");
		mingMap.put(19, " ");
		mingMap.put(20, " ");
		for(int i=0;i<10;i++){
			Random rand = new Random();
			int xing=rand.nextInt(9) + 1; 
			int ming=rand.nextInt(12) + 1;
			int mings=rand.nextInt(20) + 1;
			xaMap.put(i, xingMap.get(xing)+mingMap.get(ming)+mingMap.get(mings));
		}
		return xaMap;
	}
	
	
	public Map<Integer, String> email(){
		Map<Integer, String> xbMap = new HashMap<Integer, String>();
		Map<Integer, String> companyMap = new HashMap<Integer, String>();
		companyMap.put(1, "qq");
		companyMap.put(2, "info");
		companyMap.put(3, "126");
		companyMap.put(4, "163");
		companyMap.put(5, "cn-meiya");
		companyMap.put(6, "hotmail");
		companyMap.put(7, "sina");
		companyMap.put(8, "buynow");
		companyMap.put(9, "erene");
		for(int i=0;i<10;i++){
			int a=(int)((Math.random()*9+1)*10);
			int b=(int)((Math.random()*9+1)*100000000);
			int c=(int)((Math.random()*9+1)*1);
			xbMap.put(i, String.valueOf(a)+String.valueOf(b)+"@"+companyMap.get(c)+".com");
		}
		return xbMap;
	}
	
	
	public Map<Integer, String> phone(){
		Map<Integer, String> xcMap = new HashMap<Integer, String>();
		Map<Integer, String> phoneMap = new HashMap<Integer, String>();
		phoneMap.put(1, "139");
		phoneMap.put(2, "138");
		phoneMap.put(3, "137");
		phoneMap.put(4, "136");
		phoneMap.put(5, "135");
		phoneMap.put(6, "134");
		phoneMap.put(7, "159");
		phoneMap.put(8, "158");
		phoneMap.put(9, "157");
		for(int i=0;i<10;i++){
			int a=(int)((Math.random()*9+1)*10000000);
			int b=(int)((Math.random()*9+1)*1);
			xcMap.put(i, phoneMap.get(b)+a);
		}
		return xcMap;
	}
	
	public  Map<Integer, String> address(){
		Map<Integer, String> xdMap = new HashMap<Integer, String>();
		Map<Integer, String> aMap = new HashMap<Integer, String>();
		aMap.put(1, "广州市");
		aMap.put(2, "北京市");
		aMap.put(3, "天津市");
		aMap.put(4, "武汉市");
		aMap.put(5, "长沙市");
		Map<Integer, String> bMap = new HashMap<Integer, String>();
		bMap.put(1, "天河区");
		bMap.put(2, "白云区");
		bMap.put(3, "番禺区");
		bMap.put(4, "越秀区");
		bMap.put(5, "黄埔区");
		Map<Integer, String> cMap = new HashMap<Integer, String>();
		cMap.put(1, "东城区");
		cMap.put(2, "西城区");
		cMap.put(3, "宣武区");
		cMap.put(4, "朝阳区");
		cMap.put(5, "丰台区");
		Map<Integer, String> dMap = new HashMap<Integer, String>();
		dMap.put(1, "和平区");
		dMap.put(2, "河东区");
		dMap.put(3, "大港区");
		dMap.put(4, "东丽区");
		dMap.put(5, "津南区");
		Map<Integer, String> eMap = new HashMap<Integer, String>();
		eMap.put(1, "江岸区");
		eMap.put(2, "乔口区");
		eMap.put(3, "武昌区");
		eMap.put(4, "汉南区");
		eMap.put(5, "江夏区");
		Map<Integer, String> fMap = new HashMap<Integer, String>();
		fMap.put(1, "芙蓉区");
		fMap.put(2, "岳麓区");
		fMap.put(3, "雨花区");
		fMap.put(4, "天心区");
		fMap.put(5, "开福区");
		Map<Integer, String> gMap = new HashMap<Integer, String>();
		gMap.put(1, "张家村");
		gMap.put(2, "赵家屯");
		gMap.put(3, "张家村");
		gMap.put(4, "王屯");
		gMap.put(5, "孙家村");
		
		for(int i=0;i<10;i++){
			int a=(int)((Math.random()*5+1)*1);
			int b=(int)((Math.random()*5+1)*1);
			int c=(int)((Math.random()*5+1)*1);
			if(a==1){
				xdMap.put(i, aMap.get(a)+bMap.get(b)+gMap.get(c));
			}else if(a==2){
				xdMap.put(i,aMap.get(a)+cMap.get(b)+gMap.get(c));
			}else if(a==3){
				xdMap.put(i,aMap.get(a)+dMap.get(b)+gMap.get(c));;
			}else if(a==4){
				xdMap.put(i,aMap.get(a)+eMap.get(b)+gMap.get(c));;
			}else if(a==5){
				xdMap.put(i,aMap.get(a)+fMap.get(b)+gMap.get(c));
			}
		}
		return xdMap;
	}
	
	
	public  Map<Integer, String> idNumber(){
		Map<Integer, String> xeMap = new HashMap<Integer, String>();
		Map<Integer, String> aMap = new HashMap<Integer, String>();
		aMap.put(1, "120101");
		aMap.put(2, "120102");
		aMap.put(3, "120103");
		aMap.put(4, "120104");
		aMap.put(5, "120105");
		aMap.put(6, "120106");
		aMap.put(7, "130481");
		aMap.put(8, "130529");
		aMap.put(9, "130530");
		for(int i=0;i<10;i++){
			Random rand = new Random();
			int a = rand.nextInt(10) % (9 - 1 + 1) +1;
			int b = rand.nextInt(2021) % (2020 - 1900 + 1) + 1900;
			int c = rand.nextInt(13) % (12 - 1 + 1) + 1;
			int d = rand.nextInt(32) % (31 - 1 + 1) + 1;
			int e = rand.nextInt(1000) % (999 - 99 + 1) + 99;
			if(c>=10){
				if(d>=10){
					xeMap.put(i, aMap.get(a)+String.valueOf(b)+String.valueOf(c)+String.valueOf(d)+String.valueOf(e));
				}else{
					xeMap.put(i, aMap.get(a)+String.valueOf(b)+String.valueOf(c)+"0"+String.valueOf(d)+String.valueOf(e));
				}
			}else{
				if(d>=10){
					xeMap.put(i,aMap.get(a)+String.valueOf(b)+"0"+String.valueOf(c)+String.valueOf(d)+String.valueOf(e));
				}else{
					xeMap.put(i, aMap.get(a)+String.valueOf(b)+"0"+String.valueOf(c)+"0"+String.valueOf(d)+String.valueOf(e));
				}
			}
			
		}
		return xeMap;
		
	}
	
	
	public  Map<Integer, String> via(){
		Map<Integer, String> xfMap = new HashMap<Integer, String>();
		Random rand = new Random();
		for(int i=0;i<10;i++){
			int a = rand.nextInt(21) % (20 - 1 + 1) +1;
			if(a>=10){
				xfMap.put(i, "assets/img/avatars/via"+a+".jpg");
			}else{
				xfMap.put(i, "assets/img/avatars/via"+a+".jpg");
			}
		}
		return xfMap;
		
	}
	
	public  Map<Integer, String> graduatedFrom(){
		Map<Integer, String> xgMap = new HashMap<Integer, String>();
		Map<Integer, String> aMap = new HashMap<Integer, String>();
		aMap.put(1, "北京大学");
		aMap.put(2, "清华大学");
		aMap.put(3, "浙江大学");
		aMap.put(4, "复旦大学");
		aMap.put(5, "中国人民大学");
		aMap.put(6, "上海交通大学");
		aMap.put(7, "武汉大学");
		aMap.put(8, "南京大学");
		aMap.put(9, "中山大学");
		for(int i=0;i<10;i++){
			Random rand = new Random();
			int a = rand.nextInt(10) % (9 - 1 + 1) +1;
			xgMap.put(i, aMap.get(a));
		}
		return xgMap;
	}
	
	
	public  Map<Integer, String> major(){
		Map<Integer, String> xhMap = new HashMap<Integer, String>();
		Map<Integer, String> aMap = new HashMap<Integer, String>();
		aMap.put(1, "哲学");
		aMap.put(2, "经济学");
		aMap.put(3, "法学");
		aMap.put(4, "文学");
		aMap.put(5, "理学");
		aMap.put(6, "管理学");
		aMap.put(7, "艺学");
		aMap.put(8, "工学");
		aMap.put(9, "历史学");
		for(int i=0;i<10;i++){
			Random rand = new Random();
			int a = rand.nextInt(10) % (9 - 1 + 1) +1;
			xhMap.put(i, aMap.get(a));
		}
		return xhMap;
	}
	
	public  Map<Integer, String> remuneration(){
		Map<Integer, String> xiMap = new HashMap<Integer, String>();
		Map<Integer, String> aMap = new HashMap<Integer, String>();
		aMap.put(1, "3000");
		aMap.put(2, "4000");
		aMap.put(3, "5000");
		aMap.put(4, "6000");
		aMap.put(5, "7000");
		aMap.put(6, "8000");
		aMap.put(7, "9000");
		aMap.put(8, "10000");
		aMap.put(9, "11000");
		for(int i=0;i<10;i++){
			Random rand = new Random();
			int a = rand.nextInt(10) % (9 - 1 + 1) +1;
			xiMap.put(i, aMap.get(a));
		}
		return xiMap;
	}
	
	public  Map<Integer, String> openingBank(){
		Map<Integer, String> xjMap = new HashMap<Integer, String>();
		Map<Integer, String> aMap = new HashMap<Integer, String>();
		aMap.put(1, "工商银行");
		aMap.put(2, "农业银行");
		aMap.put(3, "建设银行");
		aMap.put(4, "交通银行");
		aMap.put(5, "邮政储蓄银行");
		for(int i=0;i<10;i++){
			Random rand = new Random();
			int a = rand.nextInt(6) % (5 - 1 + 1) +1;
			xjMap.put(i, aMap.get(a));
		}
		return xjMap;
	}
	
	public  Map<Integer, String> bankAccount(){
		Map<Integer, String> xkMap = new HashMap<Integer, String>();
		for(int i=0;i<10;i++){
			Random rand = new Random();
			int a = rand.nextInt(1000000000) % (99999999 - 10000000 + 1) +10000000;
			int b = rand.nextInt(1000000000) % (99999999 - 10000000 + 1) +10000000;
			xkMap.put(i, String.valueOf(a)+String.valueOf(b));
		}
		return xkMap;
	}
	
	public  Map<Integer, String> record(){
		Map<Integer, String> xlMap = new HashMap<Integer, String>();
		Map<Integer, String> aMap = new HashMap<Integer, String>();
		aMap.put(1, "本人性格开朗，为人细心，做事一丝不苟，能吃苦耐劳，工作脚踏实地，有较强的责任心，具有团队合作精神，又具有较强的独立工作能力，思维活跃。能熟练运用Windows Office Word、Excel、XXX等应用软件，能根据XXX的需求，完成面XXXX工作，具备良好的XXXX意识，极强的沟通能力与谈判能力；在XXXX中提出专业的意见和产品及供应商资讯，提出可行性方案建议，跟进解决XXXX问题；具备团队协作精神，有责任心、人品好、思想端正；具备一定的文件管理能力。");
		aMap.put(2, "工作认真负责，不推卸责任；能承受工作中的压力；工作上可以独当一面；具有团队精神，能与同事，其它部门积极配合，公司利益至上；服从性好，能与上司保持良好的沟通，尊重上司的安排；为人诚实，正直；且好学上进，不断提高工作能力；相信您的选择会让您我更加成功；");
		aMap.put(3, "本人热爱学习，工作态度严谨认真，责任心强，有很好的团队合作能力。有良好的分析、解决问题的思维。以创新、解决客户需求、维护公司利益为宗旨。来接受挑战和更大的发展平台。");
		aMap.put(4, "诚实、稳重、勤奋、积极上进，拥有丰富的大中型企业管理经验，有较强的团队管理能力，良好的沟通协调组织能力，敏锐的洞察力，自信是我的魅力。我有着良好的形象和气质，健康的体魄和乐观的精神使我能全身心地投入到工作当中。");
		aMap.put(5, "本人性格活泼开朗，与人相处和睦融洽，有较强的沟通能力。在校期间担任班长一职，多次策划实施几次大型活动，拥有较强的组织能力和协调能力，并具有良好的身体素质。在多次社会实践中，注意向社会吸取丰富的经验，对工作认真负责。");
		aMap.put(6, "本人性格开朗、稳重、有活力，待人热情、真诚；工作认真负责，积极主动，能吃苦耐劳，用于承受压力，勇于创新；有很强的组织能力和团队协作精神，具有较强的适应能力；纪律性强，工作积极配合；意志坚强，具有较强的无私奉献精神。");
		aMap.put(7, "　对待工作认真负责，善于沟通、协调有较强的组织能力与团队精神；活泼开朗、乐观上进、有爱心并善于施教并行；上进心强、勤于学习能不断提高自身的能力与综合素质。在未来的工作中，我将以充沛的精力，刻苦钻研的精神来努力工作，稳定地提高自己的工作能力，与企业同步发展。");
		aMap.put(8, "本人是设计专业毕业生，熟练手绘，熟练cad,3dmax,vray,photoshop等设计软件，熟练办公软件。懂画施工图，懂一定的对装饰材料和施工方法。本人乐观开朗,积极好学,健谈,有自信,具有设计的创新思想;对待工作认真负责,细心,能够吃苦耐劳,敢于挑战,并且能够很快融于集体。我虽刚刚毕业，但我年轻，有朝气，有能力完成任何工作。尽管我还缺乏一定的经验，但我会用时间和汗水去弥补。请领导放心，我一定会保质保量完成各项工作任务。 思想上积极要求上进，团结同学，尊敬师长，乐于助人，能吃苦耐劳，为人诚恳老实，性格开朗善于与人交际，工作上有较强的组织管理和动手能力，集体观念强，具有团队协作精神，创新意识。");
		aMap.put(9, "通过两年的社会生活，我成长了很多。，我对自己这两年来的收获和感受作一个小结，并以此为我今后行动的指南。思想方面，我追求上进，思想觉悟有了很大的提高。在我从事办公室文员工作过程中，感觉到了办公室文员这一职位在企业运转过程起着衔接与协调重要作用。作为一名办公室文员，要热爱本职工作，兢兢业业，要有不怕苦不怕累的精神，也要有甘当无名英雄的气概。办事要公道，忠于职守并在工作中努力掌握各项技能。");
	    aMap.put(10, "三年的军校生活，使我懂得了纪律得重要性，锻炼了我的意志；乐观向上，大方开朗，热情务实；善与人交流，人际关系良好，待人诚恳；工作认真负责，具有吃苦耐劳、艰苦奋斗的精神；遇事沉着冷静，理智稳重，适应能力强，具备良好的组织协调能力，注重团队精神、爱好阅读,上网,打羽毛球,旅游。");
	    aMap.put(11, "本人热爱生活，性格开朗活泼，乐观向上，乐于助人，乐于进取，积极勤奋，有团队精神，拥有充实的专业知识，也有独立的思维能力，工作态度认真，乐于与人交往，对艺术有着浓厚的兴趣，从小热爱绘画，热爱设计，在校期间曾参加过班级和校园的绘画展览，手绘能力强，熟练cad, 3d max, photoshop,coreldraw等设计软件.有一定的相关工作经验，绘画过许多大型室内空间效果图，本人的博客里有自己设计的3d效果图.希望能成为各大企业一份子,今后务必尽自己能力为贵企业出一份力。");
	    for(int i=0;i<10;i++){
	    	Random rand = new Random();
			int a = rand.nextInt(12) % (11 - 1 + 1) +1;
			xlMap.put(i, aMap.get(a));
	    }
	    return xlMap;
	}
	
	public  Map<Integer, String> familyInfo(){
		Map<Integer, String> xmMap = new HashMap<Integer, String>();
		Map<Integer, String> aMap = new HashMap<Integer, String>();
		aMap.put(1, "家居贫困山区，家境贫困，其主要经济来源靠种农田为生，无其它任何经济来源，现母亲因劳累过度，而生病需长期药物疗养，增加了家庭负担，再加上父亲年老体弱母亲身体不好，且有一祖母已70多岁，常年卧病在床。姐妹四人均在上学。全家的生活重担全靠父母种几亩责任田来维持，为了四姐妹上学，其父母借遍了亲友。全家年总收入不超过叁仟元，人均月收入远远低于350元");
		aMap.put(2, "家中本来有四名成员，爸爸妈妈姐姐和我，家里的生活比较困难，以前靠爸爸挣钱养家，但由于一次意外，爸爸在工地中干活，意外死亡。姐姐今年正在上初三，妈妈就靠种地微薄的收入维持家里的生活，妈妈目前的身体也不太好，经常腰疼，背疼。我们是从农村来的，在外面租房子住，由于爸爸的死亡，使我们家里陷入困境。爸爸一直是家庭里的精神支柱也是家里唯一挣钱的人，由于爸爸的事，妈妈的状态也不是很好，我的家庭条件虽然不太好，这更能磨练我的意志，我不能被困难击倒，因为只有这样我们才能克服生活给我们带来的困难。");
		aMap.put(3, "我有一个温馨的家，温暖静谧。父母是平凡的职工，没有高学历，也没有雄厚的背景，是平凡的家庭中的一员，但正是这样的家庭让我养成了积极向上心态，感谢我的家人，一直以来给以我一个温馨和睦的家庭，让我懂得了责任感，在这里，我带着一颗感恩的心，在寻求我事业上同样也有一个幸福美满的“家”！");
		aMap.put(4, "我是一个平凡的人，出生在一个平凡的三口之家，没有显赫的家世；父母踏踏实实工作，吃苦耐劳勤勤垦垦的精神一直影响着我，他们只有一个信念：就是养家糊口为家庭奉献幸福的责任，努力挣钱改善生活水平的目标。我母亲是一位外向大胆的人，非常独立自主和有主见，在她的培养之下，我也有很强的适应能力，直爽大方和勇气过人。");
		aMap.put(5, "我出生在一个普通的农民家庭，父母都是勤劳正直的人。虽然没有父辈的根基，没有雄厚的家庭背景和经济基础。但受他们的影响，我养成了勤劳、正直、上进心强的性格.家庭稳定，欲寻求长期稳定性工作。");
		for(int i=0;i<10;i++){
			Random rand = new Random();
			int a = rand.nextInt(6) % (5 - 1 + 1) +1;
			xmMap.put(i, aMap.get(a));
		}
		return xmMap;
	}
	
	public String getRandomString(int length){
	     String str="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	     Random random=new Random();
	     StringBuffer sb=new StringBuffer();
	     for(int i=0;i<length;i++){
	       int number=random.nextInt(62);
	       sb.append(str.charAt(number));
	     }
	     return sb.toString();
	 }
}
