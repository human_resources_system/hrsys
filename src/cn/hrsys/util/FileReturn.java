package cn.hrsys.util;

public class FileReturn {
	//文件url路径
	private String picPath;
	//服务器根路径
	private String locPath;
	//文件新名称  APK名称
	private String fileName;
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getPicPath() {
		return picPath;
	}
	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}
	public String getLocPath() {
		return locPath;
	}
	public void setLocPath(String locPath) {
		this.locPath = locPath;
	}
	
	
}
