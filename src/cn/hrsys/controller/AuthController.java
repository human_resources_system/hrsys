package cn.hrsys.controller;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;

import cn.hrsys.pojo.TbModel;
import cn.hrsys.service.TbModelService;

@Controller
@RequestMapping("auth")
public class AuthController {
	
	@Resource
	private TbModelService tbModelService;

	
	@RequestMapping("authorityList.html")
	public String authorityList(Model model,HttpServletRequest request){
		int PageNum=1;
		int PageSize=7;
		List<TbModel> modellist=tbModelService.getAllTbModelList(PageNum,PageSize, null);
		int Page=tbModelService.count();
		Page=Page%PageSize==0?Page/PageSize:Page/PageSize+1;
		model.addAttribute("modellist",modellist);
		request.setAttribute("PageNum",PageNum);
		request.setAttribute("Page",Page);
		return "authorityList";
	}
	
	
	
	@RequestMapping("authorityAdd.html")
	public String authorityAddhtml(Model model){
		List<TbModel> FarAuthlist=tbModelService.getAllFatherAuth();
		List<TbModel> AuthModulelist=tbModelService.getAllAuthModule();
		model.addAttribute("FarAuthlist",FarAuthlist);
		model.addAttribute("AuthModulelist",AuthModulelist);
		return "authorityAdd";
	}
	
	@RequestMapping("authorityUpdate.html")
	    public String authorityUpdatehtml(String modelID,Model model){
		TbModel tbModel=tbModelService.getAllmodelID(modelID);
		model.addAttribute("tbModel",tbModel);
		return "authorityUpdate";
	}
	
}
