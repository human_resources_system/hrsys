package cn.hrsys.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.hrsys.pojo.Agent;
import cn.hrsys.pojo.PublicattributeInformation;
import cn.hrsys.service.AgentService;
import cn.hrsys.service.PublicattributeInformationService;
import cn.hrsys.service.TbModelService;

@Controller
@RequestMapping("/agen")
public class agentController {
	
	@Resource
	private AgentService agentService;
	
	@Resource
	private TbModelService tbModelService;
	
	@Resource
	private PublicattributeInformationService publicService;
	
	@RequestMapping("/agencyList.html")
	public String agencyListhtml(String role,Model modle,HttpServletRequest request){
		int type1= tbModelService.getAllUserAuthority3(role, "人事审核");
		int type2= tbModelService.getAllUserAuthority3(role, "薪酬标准审核");
		int type3= tbModelService.getAllUserAuthority3(role, "薪酬发放审核");
		int type4= tbModelService.getAllUserAuthority3(role, "简历审核");
		type1=type1*1;
		type2=type2*2;
		type3=type3*3;
		type4=type4*4;
		int PageNum=1;
		int PageSize=7;
		int Page=agentService.getAllAgentListPage(type1, type2, type3, type4, null, null);
		Page=Page%PageSize==0?Page/PageSize:Page/PageSize+1;
		List<Agent> agentlist=agentService.getAllAgentList(PageNum, PageSize, type1, type2, type3, type4, null, null);
		List<PublicattributeInformation>  publist=publicService.getAllpublic();
		modle.addAttribute("agentlist",agentlist);
		modle.addAttribute("publist",publist);
		request.setAttribute("PageNum",PageNum);
		request.setAttribute("Page",Page);
		return "agentlist";
	}
}
