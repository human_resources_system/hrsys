package cn.hrsys.controller;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import cn.hrsys.util.FileReturn;
import cn.hrsys.util.FileUploadingUtil;
import cn.hrsys.pojo.Agent;
import cn.hrsys.pojo.RoleModel;
import cn.hrsys.pojo.TbModel;
import cn.hrsys.pojo.TbRole;
import cn.hrsys.pojo.TbUser;
import cn.hrsys.service.AgentService;
import cn.hrsys.service.RoleModelService;
import cn.hrsys.service.TbModelService;
import cn.hrsys.service.TbRoleService;
import cn.hrsys.service.TbUserService;
import cn.hrsys.util.AddUtil;
import cn.hrsys.util.AppReturn;

@Controller
public class JsController {
	@Resource
	private TbUserService tbUserService;

	@Resource
	private TbModelService tbModelService;

	@Resource
	private RoleModelService roleModelService;

	@Resource
	private TbRoleService tbRoleService;
	
	@Resource
	private AgentService agentService;

	AddUtil au = new AddUtil();

	Date date = new Date();
	
	
	
	/**
	 * 待办部分
	 */
	
	@RequestMapping(value = "/agentList", method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public Object agentList(@RequestParam(value = "agentname", required = false) String agentname,
			@RequestParam(value = "agenttype", required = false) String agenttype,
			@RequestParam(value = "agentdate", required = false) String agentdate,
			String UserRole,String PageNum) {
		try {
			if (agentname != null) {
				agentname = new String(agentname.getBytes("ISO-8859-1"), "UTF-8");
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int type1= tbModelService.getAllUserAuthority3(UserRole, "人事审核");
		int type2= tbModelService.getAllUserAuthority3(UserRole, "薪酬标准审核");
		int type3= tbModelService.getAllUserAuthority3(UserRole, "薪酬发放审核");
		int type4= tbModelService.getAllUserAuthority3(UserRole, "简历审核");
		type1=type1*1;
		type2=type2*2;
		type3=type3*3;
		type4=type4*4;
		int PageSize = 7;
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
		Date date=null;
		try {
			if(agentdate!=null&&agentdate!=""){
				date = sf.parse(agentdate);
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(agenttype.equals("1")){
			type2=0;
			type3=0;
			type4=0;
		}else if(agenttype.equals("2")){
			type1=0;
			type3=0;
			type4=0;
		}else if(agenttype.equals("3")){
			type1=0;
			type2=0;
			type4=0;
		}else if(agenttype.equals("4")){
			type1=0;
			type2=0;
			type3=0;
		}
		List<Agent> agentlist=agentService.getAllAgentList(Integer.parseInt(PageNum), PageSize, type1, type2, type3, type4, agentname,date);
		
		return JSONArray.toJSONString(agentlist).toString();
	}
	
	
	@RequestMapping(value = "/agentListPage", method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public Object agentListPage(@RequestParam(value = "agentname", required = false) String agentname,
			@RequestParam(value = "agenttype", required = false) String agenttype,
			@RequestParam(value = "agentdate", required = false) String agentdate,
			String UserRole) {
		try {
			if (agentname != null) {
				agentname = new String(agentname.getBytes("ISO-8859-1"), "UTF-8");
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int type1= tbModelService.getAllUserAuthority3(UserRole, "人事审核");
		int type2= tbModelService.getAllUserAuthority3(UserRole, "薪酬标准审核");
		int type3= tbModelService.getAllUserAuthority3(UserRole, "薪酬发放审核");
		int type4= tbModelService.getAllUserAuthority3(UserRole, "简历审核");
		type1=type1*1;
		type2=type2*2;
		type3=type3*3;
		type4=type4*4;
		int PageSize = 7;
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
		Date date=null;
		try {
			if(agentdate!=null&&agentdate!=""){
				date = sf.parse(agentdate);
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(agenttype.equals("1")){
			type2=0;
			type3=0;
			type4=0;
		}else if(agenttype.equals("2")){
			type1=0;
			type3=0;
			type4=0;
		}else if(agenttype.equals("3")){
			type1=0;
			type2=0;
			type4=0;
		}else if(agenttype.equals("4")){
			type1=0;
			type2=0;
			type3=0;
		}
		int Page=agentService.getAllAgentListPage(type1, type2, type3, type4, agentname, date);
		Page=Page%PageSize==0?Page/PageSize:Page/PageSize+1;
		return JSONArray.toJSONString(Page).toString();
	}
	
	
	
	
	
	
	
	
	
	

	/**
	 * 用户部分
	 */

	@RequestMapping(value = "/userList", method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public Object hruserList(@RequestParam(value = "username", required = false) String username,
			@RequestParam(value = "userrole", required = false) String userrole,
			@RequestParam(value = "PageNum", required = false) String PageNum) {

		try {
			if (username != null) {
				username = new String(username.getBytes("ISO-8859-1"), "UTF-8");
			}
			if (userrole != null) {
				userrole = new String(userrole.getBytes("ISO-8859-1"), "UTF-8");
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int PageSize = 7;
		List<TbUser> userlist = tbUserService.getAllUserList(Integer.parseInt(PageNum), PageSize, username, userrole);
		return JSONArray.toJSONString(userlist).toString();
	}

	@RequestMapping(value = "/userListPage", method = RequestMethod.GET, produces = {
			"application/json;charset=UTF-8" })
	@ResponseBody
	public Object userListPage(@RequestParam(value = "username", required = false) String username,
			@RequestParam(value = "userrole", required = false) String userrole) {

		try {
			if (username != null) {
				username = new String(username.getBytes("ISO-8859-1"), "UTF-8");
			}
			if (userrole != null) {
				userrole = new String(userrole.getBytes("ISO-8859-1"), "UTF-8");
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int PageSize = 7;
		int Page = tbUserService.getAllUserListPage(username, userrole);
		Page = Page % PageSize == 0 ? Page / PageSize : Page / PageSize + 1;
		return JSONArray.toJSONString(Page).toString();
	}

	@RequestMapping(value = "/amenduserstatus", method = RequestMethod.GET, produces = {
			"application/json;charset=UTF-8" })
	@ResponseBody
	public Object amenduserstatus(String id, String status) {
		TbUser tbUser = new TbUser();
		tbUser.setId(Integer.parseInt(id));
		if (Integer.parseInt(status) == 1) {
			tbUser.setStatus(2);
		} else {
			tbUser.setStatus(1);
		}
		int i = tbUserService.getAmendUser(tbUser);
		HashMap<String, String> resultMap = new HashMap<String, String>();
		if (i > 0) {
			resultMap.put("amend", "success");
		} else {
			resultMap.put("amend", "fail");
		}
		return JSONArray.toJSONString(resultMap);
	}

	@RequestMapping(value = "/notauthorityList", method = RequestMethod.GET, produces = {
			"application/json;charset=UTF-8" })
	@ResponseBody
	public Object notauthorityList(@RequestParam(value = "notmodelname", required = false) String notmodelname,
			@RequestParam(value = "roleId", required = false) String roleId,
			@RequestParam(value = "PageNum", required = false) String PageNum) {
		try {
			if (notmodelname != null) {
				notmodelname = new String(notmodelname.getBytes("ISO-8859-1"), "UTF-8");
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int PageSize = 7;
		List<TbModel> notmodellist = tbModelService.getAllTbUserNotTbModelList(Integer.parseInt(PageNum), PageSize,
				roleId, notmodelname);
		return JSONArray.toJSONString(notmodellist).toString();
	}

	@RequestMapping(value = "/hadauthorityList", method = RequestMethod.GET, produces = {
			"application/json;charset=UTF-8" })
	@ResponseBody
	public Object hadauthorityList(@RequestParam(value = "hadmodelname", required = false) String hadmodelname,
			@RequestParam(value = "roleId", required = false) String roleId,
			@RequestParam(value = "PageNum", required = false) String PageNum) {
		try {
			if (hadmodelname != null) {
				hadmodelname = new String(hadmodelname.getBytes("ISO-8859-1"), "UTF-8");
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int PageSize = 7;
		List<TbModel> hadmodellist = tbModelService.getAllTbUserHadTbModelList(Integer.parseInt(PageNum), PageSize,
				roleId, hadmodelname);
		return JSONArray.toJSONString(hadmodellist).toString();
	}

	@RequestMapping(value = "/addauthority", method = RequestMethod.GET, produces = {
			"application/json;charset=UTF-8" })
	@ResponseBody
	public Object addauthority(@RequestParam(value = "roleId", required = false) String roleId,
			@RequestParam(value = "modelId", required = false) String modelId) {
		RoleModel rm = new RoleModel();
		rm.setRoleID(roleId);
		rm.setModelID(modelId);
		int i = roleModelService.getAddRoleModel(rm);
		HashMap<String, String> resultMap = new HashMap<String, String>();
		if (i > 0) {
			resultMap.put("add", "success");
		} else {
			resultMap.put("add", "fail");
		}
		return JSONArray.toJSONString(resultMap);
	}

	@RequestMapping(value = "/allExistlogName", method = RequestMethod.GET, produces = {
			"application/json;charset=UTF-8" })
	@ResponseBody
	public Object allExistlogName(String username) {
		int i = tbUserService.getAllExistlogUsername(username);
		HashMap<String, String> resultMap = new HashMap<String, String>();
		if (i > 0) {
			resultMap.put("logName", "exist");
		} else {
			resultMap.put("logName", "noexist");
		}
		return JSONArray.toJSONString(resultMap);
	}

	@RequestMapping(value = "/AddUser", method = RequestMethod.POST)
	@ResponseBody
	public Object AddUser(String showusername, String username, String passwords, String roleId,
			@RequestParam(value = "via", required = false) MultipartFile attach, String remark,
			String yr,HttpServletRequest request) {
		AppReturn appReturn = new AppReturn();
		FileUploadingUtil fuu = new FileUploadingUtil();
		appReturn = fuu.uploadingLogoFile(request, attach, au.getRandomString(12));
		FileReturn fileReturn = (FileReturn) appReturn.getData();
		TbUser tbUser = new TbUser();
		tbUser.setUserID(au.getRandomString(7));
		tbUser.setLogUsername(username);
		tbUser.setLogPassword(passwords);
		tbUser.setShowUsername(showusername);
		tbUser.setUserVia(fileReturn.getPicPath());
		tbUser.setRole(roleId);
		tbUser.setStatus(1);
		tbUser.setDeleted(0);
		tbUser.setModifyStatus(0);
		tbUser.setCreateTime(date);
		tbUser.setCreateBy(Integer.parseInt(yr));
		tbUser.setRemark(remark);
		int i = tbUserService.getAddUser(tbUser);
		HashMap<String, String> resultMap = new HashMap<String, String>();
		if (i > 0) {
			resultMap.put("AddUser", "exist");
		} else {
			resultMap.put("AddUser", "noexist");
		}
		return JSONArray.toJSONString(resultMap);
	}

	@RequestMapping(value = "/UpdateUser", method = RequestMethod.POST)
	@ResponseBody
	public Object UpdateUser(String id, String showusername, String passwords, String roleId,String mod,
			@RequestParam(value = "upvia", required = false) MultipartFile attach, String remark,
			HttpServletRequest request) {
		TbUser tbUser = new TbUser();
		tbUser.setId(Integer.parseInt(id));
		tbUser.setLogPassword(passwords);
		tbUser.setShowUsername(showusername);
		if(attach.getSize()!=0){
			AppReturn appReturn = new AppReturn();
			FileUploadingUtil fuu = new FileUploadingUtil();
			appReturn = fuu.uploadingLogoFile(request, attach, au.getRandomString(12));
			FileReturn fileReturn = (FileReturn) appReturn.getData();
			tbUser.setUserVia(fileReturn.getPicPath());
		}	
		tbUser.setRole(roleId);
		tbUser.setStatus(1);
		tbUser.setModifyTime(date);
		tbUser.setModifyBy(Integer.parseInt(mod));
		tbUser.setRemark(remark);
		int i = tbUserService.getAmendUser(tbUser);
		HashMap<String, String> resultMap = new HashMap<String, String>();
		if (i > 0) {
			resultMap.put("UpdateUser", "exist");
		} else {
			resultMap.put("UpdateUser", "noexist");
		}
		return JSONArray.toJSONString(resultMap);
	}

	@RequestMapping(value = "/UpdateUserMe", method = RequestMethod.POST)
	@ResponseBody
	public Object UpdateUserMe(String id, String showusername, String passwords,
			@RequestParam(value = "upviame", required = false) MultipartFile attach, String remark,
			HttpServletRequest request) {
		TbUser tbUser = new TbUser();
		tbUser.setId(Integer.parseInt(id));
		tbUser.setLogPassword(passwords);
		tbUser.setShowUsername(showusername);
		if(attach.getSize()!=0){
			AppReturn appReturn = new AppReturn();
			FileUploadingUtil fuu = new FileUploadingUtil();
			appReturn = fuu.uploadingLogoFile(request, attach, au.getRandomString(12));
			FileReturn fileReturn = (FileReturn) appReturn.getData();
			tbUser.setUserVia(fileReturn.getPicPath());
		}
		tbUser.setStatus(1);
		tbUser.setDeleted(0);
		tbUser.setModifyStatus(0);
		tbUser.setModifyTime(date);
		tbUser.setRemark(remark);
		int i = tbUserService.getAmendUser(tbUser);
		HashMap<String, String> resultMap = new HashMap<String, String>();
		if (i > 0) {
			resultMap.put("UpdateUser", "exist");
		} else {
			resultMap.put("UpdateUser", "noexist");
		}
		return JSONArray.toJSONString(resultMap);
	}

	@RequestMapping(value = "/minusauthority", method = RequestMethod.GET, produces = {
			"application/json;charset=UTF-8" })
	@ResponseBody
	public Object minusauthority(@RequestParam(value = "roleId", required = false) String roleId,
			@RequestParam(value = "modelId", required = false) String modelId) {
		RoleModel roleModel = new RoleModel();
		roleModel.setRoleID(roleId);
		roleModel.setModelID(modelId);
		int i = roleModelService.getDeleteRoleModel(roleModel);
		HashMap<String, String> resultMap = new HashMap<String, String>();
		if (i > 0) {
			resultMap.put("minus", "success");
		} else {
			resultMap.put("minus", "fail");
		}
		return JSONArray.toJSONString(resultMap);
	}

	@RequestMapping(value = "/deleteUser", method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public Object deleteUser(String id) {
		TbUser tu = new TbUser();
		tu.setId(Integer.parseInt(id));
		tu.setDeleted(1);
		int i = tbUserService.getAmendUser(tu);
		HashMap<String, String> resultMap = new HashMap<String, String>();
		if (i > 0) {
			resultMap.put("deleteUser", "exist");
		} else {
			resultMap.put("deleteUser", "noexist");
		}
		return JSONArray.toJSONString(resultMap);
	}

	@RequestMapping(value = "/hadModelCount", method = RequestMethod.GET, produces = {
			"application/json;charset=UTF-8" })
	@ResponseBody
	public Object hadModelCount(String roleId,
			@RequestParam(value = "notmodelname", required = false) String notmodelname) {
		try {
			if (notmodelname != null) {
				notmodelname = new String(notmodelname.getBytes("ISO-8859-1"), "UTF-8");
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int count = tbModelService.hadModelCount(roleId, notmodelname);
		count = count % 7 == 0 ? count / 7 : count / 7 + 1;
		return JSONArray.toJSONString(count).toString();
	}

	@RequestMapping(value = "/notModelCount", method = RequestMethod.GET, produces = {
			"application/json;charset=UTF-8" })
	@ResponseBody
	public Object notModelCount(String roleId,
			@RequestParam(value = "notmodelname", required = false) String notmodelname) {
		try {
			if (notmodelname != null) {
				notmodelname = new String(notmodelname.getBytes("ISO-8859-1"), "UTF-8");
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int count = tbModelService.notModelCount(roleId, notmodelname);
		count = count % 7 == 0 ? count / 7 : count / 7 + 1;
		return JSONArray.toJSONString(count).toString();
	}

	/**
	 * 权限部分
	 */

	@RequestMapping(value = "/authorityList", method = RequestMethod.GET, produces = {
			"application/json;charset=UTF-8" })
	@ResponseBody
	public Object hrauthorityList(@RequestParam(value = "modelname", required = false) String modelname,
			@RequestParam(value = "PageNum", required = false) String PageNum) {
		try {
			if (modelname != null) {
				modelname = new String(modelname.getBytes("ISO-8859-1"), "UTF-8");
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int PageSize = 7;
		List<TbModel> modellist = tbModelService.getAllTbModelList(Integer.parseInt(PageNum), PageSize, modelname);
		return JSONArray.toJSONString(modellist).toString();
	}

	@RequestMapping(value = "/authorityListPage", method = RequestMethod.GET, produces = {
			"application/json;charset=UTF-8" })
	@ResponseBody
	public Object authorityListPage(@RequestParam(value = "modelname", required = false) String modelname) {
		try {
			if (modelname != null) {
				modelname = new String(modelname.getBytes("ISO-8859-1"), "UTF-8");
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int PageSize = 7;
		int Page = tbModelService.getAllTbModelListPage(modelname);
		Page = Page % PageSize == 0 ? Page / PageSize : Page / PageSize + 1;
		return JSONArray.toJSONString(Page).toString();
	}

	@RequestMapping(value = "/userAuthorityManag", method = RequestMethod.GET, produces = {
			"application/json;charset=UTF-8" })
	@ResponseBody
	public Object userAuthorityManag(String authorityManag, String userRoleId) {
		try {
			if (authorityManag != null) {
				authorityManag = new String(authorityManag.getBytes("ISO-8859-1"), "UTF-8");
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		int i = tbModelService.getAllUserAuthority3(userRoleId, authorityManag);
		HashMap<String, String> resultMap = new HashMap<String, String>();
		if (i > 0) {
			resultMap.put("Authority", "exist");
		} else {
			resultMap.put("Authority", "noexist");
		}
		return JSONArray.toJSONString(resultMap);
	}

	@RequestMapping(value = "/AddModel", method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public Object AddModel(@RequestParam(value = "modelName", required = false) String modelName,
			@RequestParam(value = "modelUrl", required = false) String modelUrl,
			@RequestParam(value = "farModel", required = false) String farModel,
			@RequestParam(value = "Module", required = false) String Module,
			@RequestParam(value = "remark", required = false) String remark,String yr) {
		try {
			if (modelName != null) {
				modelName = new String(modelName.getBytes("ISO-8859-1"), "UTF-8");
			}
			if (remark != null) {
				remark = new String(remark.getBytes("ISO-8859-1"), "UTF-8");
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		TbModel tbModel = new TbModel();
		if (Module.equals("falset")) {
			tbModel.setModelID(au.getRandomString(7));
			tbModel.setModelName(modelName);
			tbModel.setOrderBy(1);
			tbModel.setRelPage(au.getRandomString(5));
			tbModel.setCreateTime(date);
			tbModel.setCreateBy(Integer.parseInt(yr));
		} else {
			tbModel.setModelID(au.getRandomString(7));
			TbModel tm = tbModelService.getAllmodelID(farModel);
			tbModel.setModelName(modelName);
			tbModel.setModelURL(modelUrl);
			tbModel.setParent(farModel);
			tbModel.setAncentor(Module);
			tbModel.setOrderBy(tm.getOrderBy() + 1);
			tbModel.setRelPage(tm.getRelPage());
			tbModel.setDeleted(0);
			tbModel.setMenuType(tm.getMenuType() + 1);
			tbModel.setCreateTime(date);
			tbModel.setCreateBy(Integer.parseInt(yr));
			tbModel.setRemark(remark);
		}
		int i = tbModelService.getAddTbModel(tbModel);
		HashMap<String, String> resultMap = new HashMap<String, String>();
		if (i > 0) {
			resultMap.put("addModel", "exist");
		} else {
			resultMap.put("addModel", "noexist");
		}
		return JSONArray.toJSONString(resultMap);
	}

	@RequestMapping(value = "/UpdateModel", method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public Object UpdateModel(@RequestParam(value = "modelId", required = false) String modelId,
			@RequestParam(value = "modelName", required = false) String modelName,
			@RequestParam(value = "modelUrl", required = false) String modelUrl,
			@RequestParam(value = "remark", required = false) String remark,String mod) {
		try {
			if (modelName != null) {
				modelName = new String(modelName.getBytes("ISO-8859-1"), "UTF-8");
			}
			if (remark != null) {
				remark = new String(remark.getBytes("ISO-8859-1"), "UTF-8");
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		TbModel tbModel = new TbModel();
		tbModel.setModelID(modelId);
		tbModel.setModelName(modelName);
		tbModel.setModelURL(modelUrl);
		tbModel.setUpdateTime(date);
		tbModel.setUpdateBy(Integer.parseInt(mod));
		tbModel.setRemark(remark);
		int i = tbModelService.getAmendTbModel(tbModel);
		HashMap<String, String> resultMap = new HashMap<String, String>();
		if (i > 0) {
			resultMap.put("UpdateModel", "exist");
		} else {
			resultMap.put("UpdateModel", "noexist");
		}
		return JSONArray.toJSONString(resultMap);
	}

	@RequestMapping(value = "/deleteModel", method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public Object deleteModel(String modelID) {
		int i = 0;
		int g = tbModelService.getAllSonAuth(modelID);
		if (g > 0) {
			i = 0;
		} else {
			TbModel tbModel = new TbModel();
			tbModel.setModelID(modelID);
			tbModel.setDeleted(1);
			i = tbModelService.getAmendTbModel(tbModel);
		}
		HashMap<String, String> resultMap = new HashMap<String, String>();
		if (i > 0) {
			resultMap.put("deleteModel", "exist");
		} else {
			resultMap.put("deleteModel", "noexist");
		}
		return JSONArray.toJSONString(resultMap);
	}

	@RequestMapping(value = "/AddAllAuth", method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public Object AddAllAuth(String roleId) {

		RoleModel rm = new RoleModel();
		rm.setRoleID(roleId);
		int i = roleModelService.getDeleteRoleModel(rm);
		List<TbModel> modelList = tbModelService.getAllAuthority();
		for (TbModel tm : modelList) {
			RoleModel roleModel = new RoleModel();
			roleModel.setRoleID(roleId);
			roleModel.setModelID(tm.getModelID());
			i = roleModelService.getAddRoleModel(roleModel);
		}
		HashMap<String, String> resultMap = new HashMap<String, String>();
		if (i > 0) {
			resultMap.put("add", "exist");
		} else {
			resultMap.put("add", "noexist");
		}
		return JSONArray.toJSONString(resultMap);
	}

	@RequestMapping(value = "/DeleteAllAuth", method = RequestMethod.GET, produces = {
			"application/json;charset=UTF-8" })
	@ResponseBody
	public Object DeleteAllAuth(String roleId) {

		RoleModel rm = new RoleModel();
		rm.setRoleID(roleId);
		int i = roleModelService.getDeleteRoleModel(rm);
		HashMap<String, String> resultMap = new HashMap<String, String>();
		if (i > 0) {
			resultMap.put("dele", "exist");
		} else {
			resultMap.put("dele", "noexist");
		}
		return JSONArray.toJSONString(resultMap);
	}

	/**
	 * 角色部分
	 */

	@RequestMapping(value = "/amendStatus", method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public Object amendStatus(String roleId, String Status) {

		TbRole tr = tbRoleService.getAllRoleEmploy(roleId);
		int i = 0;
		if (tr == null) {
			TbRole tbRole = new TbRole();
			tbRole.setRoleID(roleId);
			if (Integer.parseInt(Status) == 1) {
				tbRole.setShowStatus(2);
			} else {
				tbRole.setShowStatus(1);
			}
			i = tbRoleService.getAmendTbRole(tbRole);
		} else {
			i = 0;
		}
		HashMap<String, String> resultMap = new HashMap<String, String>();
		if (i > 0) {
			resultMap.put("amend", "success");
		} else {
			resultMap.put("amend", "fail");
		}
		return JSONArray.toJSONString(resultMap);
	}

	@RequestMapping(value = "/roleList", method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public Object roleList(String rolename, String PageNum) {
		try {
			if (rolename != null) {
				rolename = new String(rolename.getBytes("ISO-8859-1"), "UTF-8");
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int PageSize = 7;
		List<TbRole> rolelist = tbRoleService.getAllTbRoleList(Integer.parseInt(PageNum), PageSize, rolename);
		return JSONArray.toJSONString(rolelist).toString();
	}

	@RequestMapping(value = "/roleListPage", method = RequestMethod.GET, produces = {
			"application/json;charset=UTF-8" })
	@ResponseBody
	public Object roleListPage(String rolename) {
		try {
			if (rolename != null) {
				rolename = new String(rolename.getBytes("ISO-8859-1"), "UTF-8");
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int PageSize = 7;
		int Page = tbRoleService.getAllTbRoleListPage(rolename);
		Page = Page % PageSize == 0 ? Page / PageSize : Page / PageSize + 1;
		return JSONArray.toJSONString(Page).toString();
	}

	@RequestMapping(value = "/addRole", method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public Object addRole(String rolename, String roleremark,String yr) {
		try {
			if (rolename != null) {
				rolename = new String(rolename.getBytes("ISO-8859-1"), "UTF-8");
			}
			if (roleremark != null) {
				roleremark = new String(roleremark.getBytes("ISO-8859-1"), "UTF-8");
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		TbRole tbRole = new TbRole();
		tbRole.setRoleID(au.getRandomString(7));
		tbRole.setRoleName(rolename);
		tbRole.setDeleted(0);
		tbRole.setShowStatus(1);
		tbRole.setCreateTime(date);
		tbRole.setCreateBy(Integer.parseInt(yr));
		tbRole.setRoleRemark(roleremark);
		int i = tbRoleService.getAddTbRole(tbRole);
		HashMap<String, String> resultMap = new HashMap<String, String>();
		if (i > 0) {
			resultMap.put("addRole", "exist");
		} else {
			resultMap.put("addRole", "noexist");
		}
		return JSONArray.toJSONString(resultMap);
	}

	@RequestMapping(value = "/deleteRole", method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public Object deleteRole(String roleId) {
		TbRole tr = tbRoleService.getAllRoleEmploy(roleId);
		int i = 0;
		if (tr == null) {
			TbRole tbRole = new TbRole();
			tbRole.setRoleID(roleId);
			tbRole.setDeleted(1);
			i = tbRoleService.getAmendTbRole(tbRole);
		} else {
			i = 0;
		}
		HashMap<String, String> resultMap = new HashMap<String, String>();
		if (i > 0) {
			resultMap.put("deleteRole", "exist");
		} else {
			resultMap.put("deleteRole", "noexist");
		}
		return JSONArray.toJSONString(resultMap);
	}

}
