package cn.hrsys.controller;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;

import cn.hrsys.pojo.RoleModel;
import cn.hrsys.pojo.TbModel;
import cn.hrsys.pojo.TbRole;
import cn.hrsys.pojo.TbUser;
import cn.hrsys.service.RoleModelService;
import cn.hrsys.service.TbModelService;
import cn.hrsys.service.TbRoleService;
import cn.hrsys.service.TbUserService;

@Controller
@RequestMapping("user")
public class UserController {

	@Resource
	private TbUserService tbUserService;
	
	@Resource
	private TbModelService tbModelService;
	
	@Resource
	private RoleModelService roleModelService;
	
	@Resource
	private TbRoleService tbRoleService;
	
	@RequestMapping("userList.html")
	public String UserList(Model model,HttpServletRequest request){
		int PageNum=1;
		int PageSize=7;
		List<TbUser> userlist=tbUserService.getAllUserList(PageNum,PageSize, null, null);
		int Page=tbUserService.count();
		Page=Page%PageSize==0?Page/PageSize:Page/PageSize+1;
		model.addAttribute("userlist",userlist);
		request.setAttribute("PageNum",PageNum);
		request.setAttribute("Page",Page);
		return "userList";
	}
	
	
	
	
	@RequestMapping("userAuthority.html")
	public String userAuthorityhtml(String roleId,Model model,HttpServletRequest request){
		int notModelPageNum=1;
		int hadModelPageNum=1;
		int PageSize=7;
		List<TbModel> notmodellist=tbModelService.getAllTbUserNotTbModelList(notModelPageNum, PageSize, roleId, null);
		List<TbModel> hadmodellist=tbModelService.getAllTbUserHadTbModelList(notModelPageNum, PageSize, roleId, null);
		int notModelPage=tbModelService.notModelCount(roleId,null);
		int hadModelPage=tbModelService.hadModelCount(roleId,null);
		notModelPage=notModelPage%PageSize==0?notModelPage/PageSize:notModelPage/PageSize+1;
		hadModelPage=hadModelPage%PageSize==0?hadModelPage/PageSize:hadModelPage/PageSize+1;
		model.addAttribute("notmodellist",notmodellist);
		model.addAttribute("hadmodellist",hadmodellist);
		request.setAttribute("notModelPageNum",notModelPageNum);
		request.setAttribute("hadModelPageNum",hadModelPageNum);
		request.setAttribute("notModelPage",notModelPage);
		request.setAttribute("hadModelPage",hadModelPage);
		request.setAttribute("roleId",roleId);
		return "userAuthority";
	}
	
	@RequestMapping("userAdd.html")
	public String userAddhtml(Model model){
		List<TbRole> rolelist=tbRoleService.getAllHadRoleList();
		model.addAttribute("rolelist",rolelist);
		return "userAdd";
	}
	
	@RequestMapping("userUpdate.html")
	public String userUpdatehtml(Model model,int id){
		List<TbRole> rolelist=tbRoleService.getAllHadRoleList();
		TbUser tbUser=tbUserService.getAllUser(id);
		model.addAttribute("rolelist",rolelist);
		model.addAttribute("tbUser",tbUser);
		return "userUpdate";
	}
	
	
	@RequestMapping("userView.html")
	public String userViewhtml(Model model,int id){
		TbUser tbUser=tbUserService.getAllUser(id);
		model.addAttribute("tbUser",tbUser);
		return "userView";
	}
	
	
	
	@RequestMapping("userViewMe.html")
	public String userViewMehtml(Model model,int id){
		TbUser tbUser=tbUserService.getAllUser(id);
		model.addAttribute("tbUser",tbUser);
		return "userViewMe";
	}
	
	
	@RequestMapping("userUpdateMe.html")
	public String userUpdatehtmlMe(Model model,int id){
		TbUser tbUser=tbUserService.getAllUser(id);
		model.addAttribute("tbUser",tbUser);
		return "userUpdateMe";
	}
}
