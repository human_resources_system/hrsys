package cn.hrsys.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import cn.hrsys.pojo.TbModel;
import cn.hrsys.pojo.TbUser;
import cn.hrsys.service.TbModelService;
import cn.hrsys.service.TbUserService;

/**
 * 模块：登陆
 * 编译人:高巍
 */
@Controller
@RequestMapping("main")
public class mainController {
	@Resource
	private TbUserService tbUserService;
	
	@Resource
	private TbModelService tbModelService;
    //测试
	@RequestMapping("index.html")
	public String index(@RequestParam(value="logUsername",required=false)String logUsername,@RequestParam(value="logPassword",required=false)String logPassword,
			HttpSession session,HttpServletRequest request,HttpServletResponse response){
			int i=tbUserService.getInquirylogUsername(logUsername);
			if(i>0){
				TbUser tbUser=tbUserService.getLogin(logUsername, logPassword);
				if(tbUser!=null){
					try {
						String roleName = URLEncoder.encode(tbUser.getRoleName(), "utf-8");
						String showUsername = URLEncoder.encode(tbUser.getShowUsername(), "utf-8");
						tbUser.setRoleName(roleName);
						tbUser.setShowUsername(showUsername);
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}
					Cookie cookie=new Cookie("tbUser",tbUser.toString());
					cookie.setPath("/");
					cookie.setMaxAge(60*60*2);
					response.addCookie(cookie);
					List<TbModel> fatherauthorityList=tbModelService.getAllTbUserTbModelList(tbUser.getRole(), 1);
					List<TbModel> sonauthorityList=tbModelService.getAllTbUserTbModelList(tbUser.getRole(),2);
					session.setAttribute("fatherauthorityList", fatherauthorityList);
					session.setAttribute("sonauthorityList", sonauthorityList);
					return "index";
				}else{
					request.setAttribute("error", "密码错误！");
					return "login";
				}
			}else{
				request.setAttribute("error", "该用户不存在或已被禁用！");
				return "login";
			}
	}
	
	@RequestMapping("Login.html")
	public String Login(HttpServletRequest request){
		request.setAttribute("error", "HR Login");
		return "login";
	}
}
