package cn.hrsys.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.hrsys.pojo.TbRole;
import cn.hrsys.service.TbRoleService;

@Controller
@RequestMapping("role")
public class RoleController {
	
	@Resource
	private TbRoleService tbRoleService;
	
	@RequestMapping("roleList.html")
	public String roleListhtml(Model model,HttpServletRequest request){
		int PageNum=1;
		int PageSize=7;
		List<TbRole> rolelist=tbRoleService.getAllTbRoleList(1, 7, null);
		int Page=tbRoleService.count();
		Page=Page%PageSize==0?Page/PageSize:Page/PageSize+1;
		model.addAttribute("rolelist",rolelist);
		request.setAttribute("PageNum",PageNum);
		request.setAttribute("Page",Page);
		return "roleList";
	}
	
	@RequestMapping("roleAdd.html")
	public String roleAddhtml(){
		return "roleAdd";
	}
	
	@RequestMapping("roleView.html")
	public String roleViewhtml(String roleId,Model model){
		TbRole tbRole=tbRoleService.getAllTbRole(roleId);
		model.addAttribute("tbRole",tbRole);
		return "roleView";
	}
	
}
