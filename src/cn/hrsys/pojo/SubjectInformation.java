package cn.hrsys.pojo;

import java.util.Date;

public class SubjectInformation {
    private Integer id;

    private String headline;

    private Integer subjectGenre;

    private Integer depId;

    private Integer rank;

    private String content;

    private String remark;

    private String subjectlocPath;

    private String subjectpoiPath;

    private String resultlocPath;

    private String resultpoiPath;

    private Integer status;

    private Date enabledDate;

    private Date loseDate;

    private Integer creationBy;

    private Date creationDate;

    private Integer modifyBy;

    private Date modifyDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline == null ? null : headline.trim();
    }

    public Integer getSubjectGenre() {
        return subjectGenre;
    }

    public void setSubjectGenre(Integer subjectGenre) {
        this.subjectGenre = subjectGenre;
    }

    public Integer getDepId() {
        return depId;
    }

    public void setDepId(Integer depId) {
        this.depId = depId;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public String getSubjectlocPath() {
        return subjectlocPath;
    }

    public void setSubjectlocPath(String subjectlocPath) {
        this.subjectlocPath = subjectlocPath == null ? null : subjectlocPath.trim();
    }

    public String getSubjectpoiPath() {
        return subjectpoiPath;
    }

    public void setSubjectpoiPath(String subjectpoiPath) {
        this.subjectpoiPath = subjectpoiPath == null ? null : subjectpoiPath.trim();
    }

    public String getResultlocPath() {
        return resultlocPath;
    }

    public void setResultlocPath(String resultlocPath) {
        this.resultlocPath = resultlocPath == null ? null : resultlocPath.trim();
    }

    public String getResultpoiPath() {
        return resultpoiPath;
    }

    public void setResultpoiPath(String resultpoiPath) {
        this.resultpoiPath = resultpoiPath == null ? null : resultpoiPath.trim();
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getEnabledDate() {
        return enabledDate;
    }

    public void setEnabledDate(Date enabledDate) {
        this.enabledDate = enabledDate;
    }

    public Date getLoseDate() {
        return loseDate;
    }

    public void setLoseDate(Date loseDate) {
        this.loseDate = loseDate;
    }

    public Integer getCreationBy() {
        return creationBy;
    }

    public void setCreationBy(Integer creationBy) {
        this.creationBy = creationBy;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Integer getModifyBy() {
        return modifyBy;
    }

    public void setModifyBy(Integer modifyBy) {
        this.modifyBy = modifyBy;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }
}