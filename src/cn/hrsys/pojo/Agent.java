package cn.hrsys.pojo;

import java.util.Date;

public class Agent {
    private Integer id;

    private Integer type;

    private String name;

    private Date date;

    private Integer agentId;

    private Integer statuc;
    
    private String typeName;
    


	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public Integer getStatuc() {
        return statuc;
    }

    public void setStatuc(Integer statuc) {
        this.statuc = statuc;
    }
}