package cn.hrsys.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class DepartmentInformationExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public DepartmentInformationExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andDepartmentCodeIsNull() {
            addCriterion("departmentCode is null");
            return (Criteria) this;
        }

        public Criteria andDepartmentCodeIsNotNull() {
            addCriterion("departmentCode is not null");
            return (Criteria) this;
        }

        public Criteria andDepartmentCodeEqualTo(String value) {
            addCriterion("departmentCode =", value, "departmentCode");
            return (Criteria) this;
        }

        public Criteria andDepartmentCodeNotEqualTo(String value) {
            addCriterion("departmentCode <>", value, "departmentCode");
            return (Criteria) this;
        }

        public Criteria andDepartmentCodeGreaterThan(String value) {
            addCriterion("departmentCode >", value, "departmentCode");
            return (Criteria) this;
        }

        public Criteria andDepartmentCodeGreaterThanOrEqualTo(String value) {
            addCriterion("departmentCode >=", value, "departmentCode");
            return (Criteria) this;
        }

        public Criteria andDepartmentCodeLessThan(String value) {
            addCriterion("departmentCode <", value, "departmentCode");
            return (Criteria) this;
        }

        public Criteria andDepartmentCodeLessThanOrEqualTo(String value) {
            addCriterion("departmentCode <=", value, "departmentCode");
            return (Criteria) this;
        }

        public Criteria andDepartmentCodeLike(String value) {
            addCriterion("departmentCode like", value, "departmentCode");
            return (Criteria) this;
        }

        public Criteria andDepartmentCodeNotLike(String value) {
            addCriterion("departmentCode not like", value, "departmentCode");
            return (Criteria) this;
        }

        public Criteria andDepartmentCodeIn(List<String> values) {
            addCriterion("departmentCode in", values, "departmentCode");
            return (Criteria) this;
        }

        public Criteria andDepartmentCodeNotIn(List<String> values) {
            addCriterion("departmentCode not in", values, "departmentCode");
            return (Criteria) this;
        }

        public Criteria andDepartmentCodeBetween(String value1, String value2) {
            addCriterion("departmentCode between", value1, value2, "departmentCode");
            return (Criteria) this;
        }

        public Criteria andDepartmentCodeNotBetween(String value1, String value2) {
            addCriterion("departmentCode not between", value1, value2, "departmentCode");
            return (Criteria) this;
        }

        public Criteria andDepartmentShortNameIsNull() {
            addCriterion("departmentShortName is null");
            return (Criteria) this;
        }

        public Criteria andDepartmentShortNameIsNotNull() {
            addCriterion("departmentShortName is not null");
            return (Criteria) this;
        }

        public Criteria andDepartmentShortNameEqualTo(String value) {
            addCriterion("departmentShortName =", value, "departmentShortName");
            return (Criteria) this;
        }

        public Criteria andDepartmentShortNameNotEqualTo(String value) {
            addCriterion("departmentShortName <>", value, "departmentShortName");
            return (Criteria) this;
        }

        public Criteria andDepartmentShortNameGreaterThan(String value) {
            addCriterion("departmentShortName >", value, "departmentShortName");
            return (Criteria) this;
        }

        public Criteria andDepartmentShortNameGreaterThanOrEqualTo(String value) {
            addCriterion("departmentShortName >=", value, "departmentShortName");
            return (Criteria) this;
        }

        public Criteria andDepartmentShortNameLessThan(String value) {
            addCriterion("departmentShortName <", value, "departmentShortName");
            return (Criteria) this;
        }

        public Criteria andDepartmentShortNameLessThanOrEqualTo(String value) {
            addCriterion("departmentShortName <=", value, "departmentShortName");
            return (Criteria) this;
        }

        public Criteria andDepartmentShortNameLike(String value) {
            addCriterion("departmentShortName like", value, "departmentShortName");
            return (Criteria) this;
        }

        public Criteria andDepartmentShortNameNotLike(String value) {
            addCriterion("departmentShortName not like", value, "departmentShortName");
            return (Criteria) this;
        }

        public Criteria andDepartmentShortNameIn(List<String> values) {
            addCriterion("departmentShortName in", values, "departmentShortName");
            return (Criteria) this;
        }

        public Criteria andDepartmentShortNameNotIn(List<String> values) {
            addCriterion("departmentShortName not in", values, "departmentShortName");
            return (Criteria) this;
        }

        public Criteria andDepartmentShortNameBetween(String value1, String value2) {
            addCriterion("departmentShortName between", value1, value2, "departmentShortName");
            return (Criteria) this;
        }

        public Criteria andDepartmentShortNameNotBetween(String value1, String value2) {
            addCriterion("departmentShortName not between", value1, value2, "departmentShortName");
            return (Criteria) this;
        }

        public Criteria andDepartmentNameIsNull() {
            addCriterion("departmentName is null");
            return (Criteria) this;
        }

        public Criteria andDepartmentNameIsNotNull() {
            addCriterion("departmentName is not null");
            return (Criteria) this;
        }

        public Criteria andDepartmentNameEqualTo(String value) {
            addCriterion("departmentName =", value, "departmentName");
            return (Criteria) this;
        }

        public Criteria andDepartmentNameNotEqualTo(String value) {
            addCriterion("departmentName <>", value, "departmentName");
            return (Criteria) this;
        }

        public Criteria andDepartmentNameGreaterThan(String value) {
            addCriterion("departmentName >", value, "departmentName");
            return (Criteria) this;
        }

        public Criteria andDepartmentNameGreaterThanOrEqualTo(String value) {
            addCriterion("departmentName >=", value, "departmentName");
            return (Criteria) this;
        }

        public Criteria andDepartmentNameLessThan(String value) {
            addCriterion("departmentName <", value, "departmentName");
            return (Criteria) this;
        }

        public Criteria andDepartmentNameLessThanOrEqualTo(String value) {
            addCriterion("departmentName <=", value, "departmentName");
            return (Criteria) this;
        }

        public Criteria andDepartmentNameLike(String value) {
            addCriterion("departmentName like", value, "departmentName");
            return (Criteria) this;
        }

        public Criteria andDepartmentNameNotLike(String value) {
            addCriterion("departmentName not like", value, "departmentName");
            return (Criteria) this;
        }

        public Criteria andDepartmentNameIn(List<String> values) {
            addCriterion("departmentName in", values, "departmentName");
            return (Criteria) this;
        }

        public Criteria andDepartmentNameNotIn(List<String> values) {
            addCriterion("departmentName not in", values, "departmentName");
            return (Criteria) this;
        }

        public Criteria andDepartmentNameBetween(String value1, String value2) {
            addCriterion("departmentName between", value1, value2, "departmentName");
            return (Criteria) this;
        }

        public Criteria andDepartmentNameNotBetween(String value1, String value2) {
            addCriterion("departmentName not between", value1, value2, "departmentName");
            return (Criteria) this;
        }

        public Criteria andDepartmentMountIdIsNull() {
            addCriterion("departmentMountId is null");
            return (Criteria) this;
        }

        public Criteria andDepartmentMountIdIsNotNull() {
            addCriterion("departmentMountId is not null");
            return (Criteria) this;
        }

        public Criteria andDepartmentMountIdEqualTo(Integer value) {
            addCriterion("departmentMountId =", value, "departmentMountId");
            return (Criteria) this;
        }

        public Criteria andDepartmentMountIdNotEqualTo(Integer value) {
            addCriterion("departmentMountId <>", value, "departmentMountId");
            return (Criteria) this;
        }

        public Criteria andDepartmentMountIdGreaterThan(Integer value) {
            addCriterion("departmentMountId >", value, "departmentMountId");
            return (Criteria) this;
        }

        public Criteria andDepartmentMountIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("departmentMountId >=", value, "departmentMountId");
            return (Criteria) this;
        }

        public Criteria andDepartmentMountIdLessThan(Integer value) {
            addCriterion("departmentMountId <", value, "departmentMountId");
            return (Criteria) this;
        }

        public Criteria andDepartmentMountIdLessThanOrEqualTo(Integer value) {
            addCriterion("departmentMountId <=", value, "departmentMountId");
            return (Criteria) this;
        }

        public Criteria andDepartmentMountIdIn(List<Integer> values) {
            addCriterion("departmentMountId in", values, "departmentMountId");
            return (Criteria) this;
        }

        public Criteria andDepartmentMountIdNotIn(List<Integer> values) {
            addCriterion("departmentMountId not in", values, "departmentMountId");
            return (Criteria) this;
        }

        public Criteria andDepartmentMountIdBetween(Integer value1, Integer value2) {
            addCriterion("departmentMountId between", value1, value2, "departmentMountId");
            return (Criteria) this;
        }

        public Criteria andDepartmentMountIdNotBetween(Integer value1, Integer value2) {
            addCriterion("departmentMountId not between", value1, value2, "departmentMountId");
            return (Criteria) this;
        }

        public Criteria andDepartmentAddressIsNull() {
            addCriterion("departmentAddress is null");
            return (Criteria) this;
        }

        public Criteria andDepartmentAddressIsNotNull() {
            addCriterion("departmentAddress is not null");
            return (Criteria) this;
        }

        public Criteria andDepartmentAddressEqualTo(String value) {
            addCriterion("departmentAddress =", value, "departmentAddress");
            return (Criteria) this;
        }

        public Criteria andDepartmentAddressNotEqualTo(String value) {
            addCriterion("departmentAddress <>", value, "departmentAddress");
            return (Criteria) this;
        }

        public Criteria andDepartmentAddressGreaterThan(String value) {
            addCriterion("departmentAddress >", value, "departmentAddress");
            return (Criteria) this;
        }

        public Criteria andDepartmentAddressGreaterThanOrEqualTo(String value) {
            addCriterion("departmentAddress >=", value, "departmentAddress");
            return (Criteria) this;
        }

        public Criteria andDepartmentAddressLessThan(String value) {
            addCriterion("departmentAddress <", value, "departmentAddress");
            return (Criteria) this;
        }

        public Criteria andDepartmentAddressLessThanOrEqualTo(String value) {
            addCriterion("departmentAddress <=", value, "departmentAddress");
            return (Criteria) this;
        }

        public Criteria andDepartmentAddressLike(String value) {
            addCriterion("departmentAddress like", value, "departmentAddress");
            return (Criteria) this;
        }

        public Criteria andDepartmentAddressNotLike(String value) {
            addCriterion("departmentAddress not like", value, "departmentAddress");
            return (Criteria) this;
        }

        public Criteria andDepartmentAddressIn(List<String> values) {
            addCriterion("departmentAddress in", values, "departmentAddress");
            return (Criteria) this;
        }

        public Criteria andDepartmentAddressNotIn(List<String> values) {
            addCriterion("departmentAddress not in", values, "departmentAddress");
            return (Criteria) this;
        }

        public Criteria andDepartmentAddressBetween(String value1, String value2) {
            addCriterion("departmentAddress between", value1, value2, "departmentAddress");
            return (Criteria) this;
        }

        public Criteria andDepartmentAddressNotBetween(String value1, String value2) {
            addCriterion("departmentAddress not between", value1, value2, "departmentAddress");
            return (Criteria) this;
        }

        public Criteria andDepartmentAbstractIsNull() {
            addCriterion("departmentAbstract is null");
            return (Criteria) this;
        }

        public Criteria andDepartmentAbstractIsNotNull() {
            addCriterion("departmentAbstract is not null");
            return (Criteria) this;
        }

        public Criteria andDepartmentAbstractEqualTo(String value) {
            addCriterion("departmentAbstract =", value, "departmentAbstract");
            return (Criteria) this;
        }

        public Criteria andDepartmentAbstractNotEqualTo(String value) {
            addCriterion("departmentAbstract <>", value, "departmentAbstract");
            return (Criteria) this;
        }

        public Criteria andDepartmentAbstractGreaterThan(String value) {
            addCriterion("departmentAbstract >", value, "departmentAbstract");
            return (Criteria) this;
        }

        public Criteria andDepartmentAbstractGreaterThanOrEqualTo(String value) {
            addCriterion("departmentAbstract >=", value, "departmentAbstract");
            return (Criteria) this;
        }

        public Criteria andDepartmentAbstractLessThan(String value) {
            addCriterion("departmentAbstract <", value, "departmentAbstract");
            return (Criteria) this;
        }

        public Criteria andDepartmentAbstractLessThanOrEqualTo(String value) {
            addCriterion("departmentAbstract <=", value, "departmentAbstract");
            return (Criteria) this;
        }

        public Criteria andDepartmentAbstractLike(String value) {
            addCriterion("departmentAbstract like", value, "departmentAbstract");
            return (Criteria) this;
        }

        public Criteria andDepartmentAbstractNotLike(String value) {
            addCriterion("departmentAbstract not like", value, "departmentAbstract");
            return (Criteria) this;
        }

        public Criteria andDepartmentAbstractIn(List<String> values) {
            addCriterion("departmentAbstract in", values, "departmentAbstract");
            return (Criteria) this;
        }

        public Criteria andDepartmentAbstractNotIn(List<String> values) {
            addCriterion("departmentAbstract not in", values, "departmentAbstract");
            return (Criteria) this;
        }

        public Criteria andDepartmentAbstractBetween(String value1, String value2) {
            addCriterion("departmentAbstract between", value1, value2, "departmentAbstract");
            return (Criteria) this;
        }

        public Criteria andDepartmentAbstractNotBetween(String value1, String value2) {
            addCriterion("departmentAbstract not between", value1, value2, "departmentAbstract");
            return (Criteria) this;
        }

        public Criteria andDepartmentRemarkIsNull() {
            addCriterion("departmentRemark is null");
            return (Criteria) this;
        }

        public Criteria andDepartmentRemarkIsNotNull() {
            addCriterion("departmentRemark is not null");
            return (Criteria) this;
        }

        public Criteria andDepartmentRemarkEqualTo(String value) {
            addCriterion("departmentRemark =", value, "departmentRemark");
            return (Criteria) this;
        }

        public Criteria andDepartmentRemarkNotEqualTo(String value) {
            addCriterion("departmentRemark <>", value, "departmentRemark");
            return (Criteria) this;
        }

        public Criteria andDepartmentRemarkGreaterThan(String value) {
            addCriterion("departmentRemark >", value, "departmentRemark");
            return (Criteria) this;
        }

        public Criteria andDepartmentRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("departmentRemark >=", value, "departmentRemark");
            return (Criteria) this;
        }

        public Criteria andDepartmentRemarkLessThan(String value) {
            addCriterion("departmentRemark <", value, "departmentRemark");
            return (Criteria) this;
        }

        public Criteria andDepartmentRemarkLessThanOrEqualTo(String value) {
            addCriterion("departmentRemark <=", value, "departmentRemark");
            return (Criteria) this;
        }

        public Criteria andDepartmentRemarkLike(String value) {
            addCriterion("departmentRemark like", value, "departmentRemark");
            return (Criteria) this;
        }

        public Criteria andDepartmentRemarkNotLike(String value) {
            addCriterion("departmentRemark not like", value, "departmentRemark");
            return (Criteria) this;
        }

        public Criteria andDepartmentRemarkIn(List<String> values) {
            addCriterion("departmentRemark in", values, "departmentRemark");
            return (Criteria) this;
        }

        public Criteria andDepartmentRemarkNotIn(List<String> values) {
            addCriterion("departmentRemark not in", values, "departmentRemark");
            return (Criteria) this;
        }

        public Criteria andDepartmentRemarkBetween(String value1, String value2) {
            addCriterion("departmentRemark between", value1, value2, "departmentRemark");
            return (Criteria) this;
        }

        public Criteria andDepartmentRemarkNotBetween(String value1, String value2) {
            addCriterion("departmentRemark not between", value1, value2, "departmentRemark");
            return (Criteria) this;
        }

        public Criteria andDepartmentStatusIsNull() {
            addCriterion("departmentStatus is null");
            return (Criteria) this;
        }

        public Criteria andDepartmentStatusIsNotNull() {
            addCriterion("departmentStatus is not null");
            return (Criteria) this;
        }

        public Criteria andDepartmentStatusEqualTo(Integer value) {
            addCriterion("departmentStatus =", value, "departmentStatus");
            return (Criteria) this;
        }

        public Criteria andDepartmentStatusNotEqualTo(Integer value) {
            addCriterion("departmentStatus <>", value, "departmentStatus");
            return (Criteria) this;
        }

        public Criteria andDepartmentStatusGreaterThan(Integer value) {
            addCriterion("departmentStatus >", value, "departmentStatus");
            return (Criteria) this;
        }

        public Criteria andDepartmentStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("departmentStatus >=", value, "departmentStatus");
            return (Criteria) this;
        }

        public Criteria andDepartmentStatusLessThan(Integer value) {
            addCriterion("departmentStatus <", value, "departmentStatus");
            return (Criteria) this;
        }

        public Criteria andDepartmentStatusLessThanOrEqualTo(Integer value) {
            addCriterion("departmentStatus <=", value, "departmentStatus");
            return (Criteria) this;
        }

        public Criteria andDepartmentStatusIn(List<Integer> values) {
            addCriterion("departmentStatus in", values, "departmentStatus");
            return (Criteria) this;
        }

        public Criteria andDepartmentStatusNotIn(List<Integer> values) {
            addCriterion("departmentStatus not in", values, "departmentStatus");
            return (Criteria) this;
        }

        public Criteria andDepartmentStatusBetween(Integer value1, Integer value2) {
            addCriterion("departmentStatus between", value1, value2, "departmentStatus");
            return (Criteria) this;
        }

        public Criteria andDepartmentStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("departmentStatus not between", value1, value2, "departmentStatus");
            return (Criteria) this;
        }

        public Criteria andCreationByIsNull() {
            addCriterion("creationBy is null");
            return (Criteria) this;
        }

        public Criteria andCreationByIsNotNull() {
            addCriterion("creationBy is not null");
            return (Criteria) this;
        }

        public Criteria andCreationByEqualTo(Integer value) {
            addCriterion("creationBy =", value, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByNotEqualTo(Integer value) {
            addCriterion("creationBy <>", value, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByGreaterThan(Integer value) {
            addCriterion("creationBy >", value, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByGreaterThanOrEqualTo(Integer value) {
            addCriterion("creationBy >=", value, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByLessThan(Integer value) {
            addCriterion("creationBy <", value, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByLessThanOrEqualTo(Integer value) {
            addCriterion("creationBy <=", value, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByIn(List<Integer> values) {
            addCriterion("creationBy in", values, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByNotIn(List<Integer> values) {
            addCriterion("creationBy not in", values, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByBetween(Integer value1, Integer value2) {
            addCriterion("creationBy between", value1, value2, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByNotBetween(Integer value1, Integer value2) {
            addCriterion("creationBy not between", value1, value2, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationDateIsNull() {
            addCriterion("creationDate is null");
            return (Criteria) this;
        }

        public Criteria andCreationDateIsNotNull() {
            addCriterion("creationDate is not null");
            return (Criteria) this;
        }

        public Criteria andCreationDateEqualTo(Date value) {
            addCriterionForJDBCDate("creationDate =", value, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("creationDate <>", value, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateGreaterThan(Date value) {
            addCriterionForJDBCDate("creationDate >", value, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("creationDate >=", value, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateLessThan(Date value) {
            addCriterionForJDBCDate("creationDate <", value, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("creationDate <=", value, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateIn(List<Date> values) {
            addCriterionForJDBCDate("creationDate in", values, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("creationDate not in", values, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("creationDate between", value1, value2, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("creationDate not between", value1, value2, "creationDate");
            return (Criteria) this;
        }

        public Criteria andModifyByIsNull() {
            addCriterion("modifyBy is null");
            return (Criteria) this;
        }

        public Criteria andModifyByIsNotNull() {
            addCriterion("modifyBy is not null");
            return (Criteria) this;
        }

        public Criteria andModifyByEqualTo(Integer value) {
            addCriterion("modifyBy =", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByNotEqualTo(Integer value) {
            addCriterion("modifyBy <>", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByGreaterThan(Integer value) {
            addCriterion("modifyBy >", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByGreaterThanOrEqualTo(Integer value) {
            addCriterion("modifyBy >=", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByLessThan(Integer value) {
            addCriterion("modifyBy <", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByLessThanOrEqualTo(Integer value) {
            addCriterion("modifyBy <=", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByIn(List<Integer> values) {
            addCriterion("modifyBy in", values, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByNotIn(List<Integer> values) {
            addCriterion("modifyBy not in", values, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByBetween(Integer value1, Integer value2) {
            addCriterion("modifyBy between", value1, value2, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByNotBetween(Integer value1, Integer value2) {
            addCriterion("modifyBy not between", value1, value2, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyDateIsNull() {
            addCriterion("modifyDate is null");
            return (Criteria) this;
        }

        public Criteria andModifyDateIsNotNull() {
            addCriterion("modifyDate is not null");
            return (Criteria) this;
        }

        public Criteria andModifyDateEqualTo(Date value) {
            addCriterionForJDBCDate("modifyDate =", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("modifyDate <>", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateGreaterThan(Date value) {
            addCriterionForJDBCDate("modifyDate >", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("modifyDate >=", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateLessThan(Date value) {
            addCriterionForJDBCDate("modifyDate <", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("modifyDate <=", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateIn(List<Date> values) {
            addCriterionForJDBCDate("modifyDate in", values, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("modifyDate not in", values, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("modifyDate between", value1, value2, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("modifyDate not between", value1, value2, "modifyDate");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}