package cn.hrsys.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class ResumeInformationExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ResumeInformationExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andDepartmentIdIsNull() {
            addCriterion("departmentId is null");
            return (Criteria) this;
        }

        public Criteria andDepartmentIdIsNotNull() {
            addCriterion("departmentId is not null");
            return (Criteria) this;
        }

        public Criteria andDepartmentIdEqualTo(Integer value) {
            addCriterion("departmentId =", value, "departmentId");
            return (Criteria) this;
        }

        public Criteria andDepartmentIdNotEqualTo(Integer value) {
            addCriterion("departmentId <>", value, "departmentId");
            return (Criteria) this;
        }

        public Criteria andDepartmentIdGreaterThan(Integer value) {
            addCriterion("departmentId >", value, "departmentId");
            return (Criteria) this;
        }

        public Criteria andDepartmentIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("departmentId >=", value, "departmentId");
            return (Criteria) this;
        }

        public Criteria andDepartmentIdLessThan(Integer value) {
            addCriterion("departmentId <", value, "departmentId");
            return (Criteria) this;
        }

        public Criteria andDepartmentIdLessThanOrEqualTo(Integer value) {
            addCriterion("departmentId <=", value, "departmentId");
            return (Criteria) this;
        }

        public Criteria andDepartmentIdIn(List<Integer> values) {
            addCriterion("departmentId in", values, "departmentId");
            return (Criteria) this;
        }

        public Criteria andDepartmentIdNotIn(List<Integer> values) {
            addCriterion("departmentId not in", values, "departmentId");
            return (Criteria) this;
        }

        public Criteria andDepartmentIdBetween(Integer value1, Integer value2) {
            addCriterion("departmentId between", value1, value2, "departmentId");
            return (Criteria) this;
        }

        public Criteria andDepartmentIdNotBetween(Integer value1, Integer value2) {
            addCriterion("departmentId not between", value1, value2, "departmentId");
            return (Criteria) this;
        }

        public Criteria andPositionIdIsNull() {
            addCriterion("positionId is null");
            return (Criteria) this;
        }

        public Criteria andPositionIdIsNotNull() {
            addCriterion("positionId is not null");
            return (Criteria) this;
        }

        public Criteria andPositionIdEqualTo(Integer value) {
            addCriterion("positionId =", value, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdNotEqualTo(Integer value) {
            addCriterion("positionId <>", value, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdGreaterThan(Integer value) {
            addCriterion("positionId >", value, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("positionId >=", value, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdLessThan(Integer value) {
            addCriterion("positionId <", value, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdLessThanOrEqualTo(Integer value) {
            addCriterion("positionId <=", value, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdIn(List<Integer> values) {
            addCriterion("positionId in", values, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdNotIn(List<Integer> values) {
            addCriterion("positionId not in", values, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdBetween(Integer value1, Integer value2) {
            addCriterion("positionId between", value1, value2, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdNotBetween(Integer value1, Integer value2) {
            addCriterion("positionId not between", value1, value2, "positionId");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("name not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andSexIsNull() {
            addCriterion("sex is null");
            return (Criteria) this;
        }

        public Criteria andSexIsNotNull() {
            addCriterion("sex is not null");
            return (Criteria) this;
        }

        public Criteria andSexEqualTo(Integer value) {
            addCriterion("sex =", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexNotEqualTo(Integer value) {
            addCriterion("sex <>", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexGreaterThan(Integer value) {
            addCriterion("sex >", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexGreaterThanOrEqualTo(Integer value) {
            addCriterion("sex >=", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexLessThan(Integer value) {
            addCriterion("sex <", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexLessThanOrEqualTo(Integer value) {
            addCriterion("sex <=", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexIn(List<Integer> values) {
            addCriterion("sex in", values, "sex");
            return (Criteria) this;
        }

        public Criteria andSexNotIn(List<Integer> values) {
            addCriterion("sex not in", values, "sex");
            return (Criteria) this;
        }

        public Criteria andSexBetween(Integer value1, Integer value2) {
            addCriterion("sex between", value1, value2, "sex");
            return (Criteria) this;
        }

        public Criteria andSexNotBetween(Integer value1, Integer value2) {
            addCriterion("sex not between", value1, value2, "sex");
            return (Criteria) this;
        }

        public Criteria andEmailIsNull() {
            addCriterion("email is null");
            return (Criteria) this;
        }

        public Criteria andEmailIsNotNull() {
            addCriterion("email is not null");
            return (Criteria) this;
        }

        public Criteria andEmailEqualTo(String value) {
            addCriterion("email =", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotEqualTo(String value) {
            addCriterion("email <>", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailGreaterThan(String value) {
            addCriterion("email >", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailGreaterThanOrEqualTo(String value) {
            addCriterion("email >=", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLessThan(String value) {
            addCriterion("email <", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLessThanOrEqualTo(String value) {
            addCriterion("email <=", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLike(String value) {
            addCriterion("email like", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotLike(String value) {
            addCriterion("email not like", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailIn(List<String> values) {
            addCriterion("email in", values, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotIn(List<String> values) {
            addCriterion("email not in", values, "email");
            return (Criteria) this;
        }

        public Criteria andEmailBetween(String value1, String value2) {
            addCriterion("email between", value1, value2, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotBetween(String value1, String value2) {
            addCriterion("email not between", value1, value2, "email");
            return (Criteria) this;
        }

        public Criteria andRecruitTypeIsNull() {
            addCriterion("recruitType is null");
            return (Criteria) this;
        }

        public Criteria andRecruitTypeIsNotNull() {
            addCriterion("recruitType is not null");
            return (Criteria) this;
        }

        public Criteria andRecruitTypeEqualTo(Integer value) {
            addCriterion("recruitType =", value, "recruitType");
            return (Criteria) this;
        }

        public Criteria andRecruitTypeNotEqualTo(Integer value) {
            addCriterion("recruitType <>", value, "recruitType");
            return (Criteria) this;
        }

        public Criteria andRecruitTypeGreaterThan(Integer value) {
            addCriterion("recruitType >", value, "recruitType");
            return (Criteria) this;
        }

        public Criteria andRecruitTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("recruitType >=", value, "recruitType");
            return (Criteria) this;
        }

        public Criteria andRecruitTypeLessThan(Integer value) {
            addCriterion("recruitType <", value, "recruitType");
            return (Criteria) this;
        }

        public Criteria andRecruitTypeLessThanOrEqualTo(Integer value) {
            addCriterion("recruitType <=", value, "recruitType");
            return (Criteria) this;
        }

        public Criteria andRecruitTypeIn(List<Integer> values) {
            addCriterion("recruitType in", values, "recruitType");
            return (Criteria) this;
        }

        public Criteria andRecruitTypeNotIn(List<Integer> values) {
            addCriterion("recruitType not in", values, "recruitType");
            return (Criteria) this;
        }

        public Criteria andRecruitTypeBetween(Integer value1, Integer value2) {
            addCriterion("recruitType between", value1, value2, "recruitType");
            return (Criteria) this;
        }

        public Criteria andRecruitTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("recruitType not between", value1, value2, "recruitType");
            return (Criteria) this;
        }

        public Criteria andPhoneIsNull() {
            addCriterion("phone is null");
            return (Criteria) this;
        }

        public Criteria andPhoneIsNotNull() {
            addCriterion("phone is not null");
            return (Criteria) this;
        }

        public Criteria andPhoneEqualTo(String value) {
            addCriterion("phone =", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotEqualTo(String value) {
            addCriterion("phone <>", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneGreaterThan(String value) {
            addCriterion("phone >", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneGreaterThanOrEqualTo(String value) {
            addCriterion("phone >=", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLessThan(String value) {
            addCriterion("phone <", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLessThanOrEqualTo(String value) {
            addCriterion("phone <=", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLike(String value) {
            addCriterion("phone like", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotLike(String value) {
            addCriterion("phone not like", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneIn(List<String> values) {
            addCriterion("phone in", values, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotIn(List<String> values) {
            addCriterion("phone not in", values, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneBetween(String value1, String value2) {
            addCriterion("phone between", value1, value2, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotBetween(String value1, String value2) {
            addCriterion("phone not between", value1, value2, "phone");
            return (Criteria) this;
        }

        public Criteria andAddressIsNull() {
            addCriterion("address is null");
            return (Criteria) this;
        }

        public Criteria andAddressIsNotNull() {
            addCriterion("address is not null");
            return (Criteria) this;
        }

        public Criteria andAddressEqualTo(String value) {
            addCriterion("address =", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotEqualTo(String value) {
            addCriterion("address <>", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressGreaterThan(String value) {
            addCriterion("address >", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressGreaterThanOrEqualTo(String value) {
            addCriterion("address >=", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLessThan(String value) {
            addCriterion("address <", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLessThanOrEqualTo(String value) {
            addCriterion("address <=", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLike(String value) {
            addCriterion("address like", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotLike(String value) {
            addCriterion("address not like", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressIn(List<String> values) {
            addCriterion("address in", values, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotIn(List<String> values) {
            addCriterion("address not in", values, "address");
            return (Criteria) this;
        }

        public Criteria andAddressBetween(String value1, String value2) {
            addCriterion("address between", value1, value2, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotBetween(String value1, String value2) {
            addCriterion("address not between", value1, value2, "address");
            return (Criteria) this;
        }

        public Criteria andDomicilePlaceIsNull() {
            addCriterion("domicilePlace is null");
            return (Criteria) this;
        }

        public Criteria andDomicilePlaceIsNotNull() {
            addCriterion("domicilePlace is not null");
            return (Criteria) this;
        }

        public Criteria andDomicilePlaceEqualTo(String value) {
            addCriterion("domicilePlace =", value, "domicilePlace");
            return (Criteria) this;
        }

        public Criteria andDomicilePlaceNotEqualTo(String value) {
            addCriterion("domicilePlace <>", value, "domicilePlace");
            return (Criteria) this;
        }

        public Criteria andDomicilePlaceGreaterThan(String value) {
            addCriterion("domicilePlace >", value, "domicilePlace");
            return (Criteria) this;
        }

        public Criteria andDomicilePlaceGreaterThanOrEqualTo(String value) {
            addCriterion("domicilePlace >=", value, "domicilePlace");
            return (Criteria) this;
        }

        public Criteria andDomicilePlaceLessThan(String value) {
            addCriterion("domicilePlace <", value, "domicilePlace");
            return (Criteria) this;
        }

        public Criteria andDomicilePlaceLessThanOrEqualTo(String value) {
            addCriterion("domicilePlace <=", value, "domicilePlace");
            return (Criteria) this;
        }

        public Criteria andDomicilePlaceLike(String value) {
            addCriterion("domicilePlace like", value, "domicilePlace");
            return (Criteria) this;
        }

        public Criteria andDomicilePlaceNotLike(String value) {
            addCriterion("domicilePlace not like", value, "domicilePlace");
            return (Criteria) this;
        }

        public Criteria andDomicilePlaceIn(List<String> values) {
            addCriterion("domicilePlace in", values, "domicilePlace");
            return (Criteria) this;
        }

        public Criteria andDomicilePlaceNotIn(List<String> values) {
            addCriterion("domicilePlace not in", values, "domicilePlace");
            return (Criteria) this;
        }

        public Criteria andDomicilePlaceBetween(String value1, String value2) {
            addCriterion("domicilePlace between", value1, value2, "domicilePlace");
            return (Criteria) this;
        }

        public Criteria andDomicilePlaceNotBetween(String value1, String value2) {
            addCriterion("domicilePlace not between", value1, value2, "domicilePlace");
            return (Criteria) this;
        }

        public Criteria andIdNumIsNull() {
            addCriterion("idNum is null");
            return (Criteria) this;
        }

        public Criteria andIdNumIsNotNull() {
            addCriterion("idNum is not null");
            return (Criteria) this;
        }

        public Criteria andIdNumEqualTo(String value) {
            addCriterion("idNum =", value, "idNum");
            return (Criteria) this;
        }

        public Criteria andIdNumNotEqualTo(String value) {
            addCriterion("idNum <>", value, "idNum");
            return (Criteria) this;
        }

        public Criteria andIdNumGreaterThan(String value) {
            addCriterion("idNum >", value, "idNum");
            return (Criteria) this;
        }

        public Criteria andIdNumGreaterThanOrEqualTo(String value) {
            addCriterion("idNum >=", value, "idNum");
            return (Criteria) this;
        }

        public Criteria andIdNumLessThan(String value) {
            addCriterion("idNum <", value, "idNum");
            return (Criteria) this;
        }

        public Criteria andIdNumLessThanOrEqualTo(String value) {
            addCriterion("idNum <=", value, "idNum");
            return (Criteria) this;
        }

        public Criteria andIdNumLike(String value) {
            addCriterion("idNum like", value, "idNum");
            return (Criteria) this;
        }

        public Criteria andIdNumNotLike(String value) {
            addCriterion("idNum not like", value, "idNum");
            return (Criteria) this;
        }

        public Criteria andIdNumIn(List<String> values) {
            addCriterion("idNum in", values, "idNum");
            return (Criteria) this;
        }

        public Criteria andIdNumNotIn(List<String> values) {
            addCriterion("idNum not in", values, "idNum");
            return (Criteria) this;
        }

        public Criteria andIdNumBetween(String value1, String value2) {
            addCriterion("idNum between", value1, value2, "idNum");
            return (Criteria) this;
        }

        public Criteria andIdNumNotBetween(String value1, String value2) {
            addCriterion("idNum not between", value1, value2, "idNum");
            return (Criteria) this;
        }

        public Criteria andFaceIsNull() {
            addCriterion("face is null");
            return (Criteria) this;
        }

        public Criteria andFaceIsNotNull() {
            addCriterion("face is not null");
            return (Criteria) this;
        }

        public Criteria andFaceEqualTo(Integer value) {
            addCriterion("face =", value, "face");
            return (Criteria) this;
        }

        public Criteria andFaceNotEqualTo(Integer value) {
            addCriterion("face <>", value, "face");
            return (Criteria) this;
        }

        public Criteria andFaceGreaterThan(Integer value) {
            addCriterion("face >", value, "face");
            return (Criteria) this;
        }

        public Criteria andFaceGreaterThanOrEqualTo(Integer value) {
            addCriterion("face >=", value, "face");
            return (Criteria) this;
        }

        public Criteria andFaceLessThan(Integer value) {
            addCriterion("face <", value, "face");
            return (Criteria) this;
        }

        public Criteria andFaceLessThanOrEqualTo(Integer value) {
            addCriterion("face <=", value, "face");
            return (Criteria) this;
        }

        public Criteria andFaceIn(List<Integer> values) {
            addCriterion("face in", values, "face");
            return (Criteria) this;
        }

        public Criteria andFaceNotIn(List<Integer> values) {
            addCriterion("face not in", values, "face");
            return (Criteria) this;
        }

        public Criteria andFaceBetween(Integer value1, Integer value2) {
            addCriterion("face between", value1, value2, "face");
            return (Criteria) this;
        }

        public Criteria andFaceNotBetween(Integer value1, Integer value2) {
            addCriterion("face not between", value1, value2, "face");
            return (Criteria) this;
        }

        public Criteria andGraduatedFromIsNull() {
            addCriterion("graduatedFrom is null");
            return (Criteria) this;
        }

        public Criteria andGraduatedFromIsNotNull() {
            addCriterion("graduatedFrom is not null");
            return (Criteria) this;
        }

        public Criteria andGraduatedFromEqualTo(String value) {
            addCriterion("graduatedFrom =", value, "graduatedFrom");
            return (Criteria) this;
        }

        public Criteria andGraduatedFromNotEqualTo(String value) {
            addCriterion("graduatedFrom <>", value, "graduatedFrom");
            return (Criteria) this;
        }

        public Criteria andGraduatedFromGreaterThan(String value) {
            addCriterion("graduatedFrom >", value, "graduatedFrom");
            return (Criteria) this;
        }

        public Criteria andGraduatedFromGreaterThanOrEqualTo(String value) {
            addCriterion("graduatedFrom >=", value, "graduatedFrom");
            return (Criteria) this;
        }

        public Criteria andGraduatedFromLessThan(String value) {
            addCriterion("graduatedFrom <", value, "graduatedFrom");
            return (Criteria) this;
        }

        public Criteria andGraduatedFromLessThanOrEqualTo(String value) {
            addCriterion("graduatedFrom <=", value, "graduatedFrom");
            return (Criteria) this;
        }

        public Criteria andGraduatedFromLike(String value) {
            addCriterion("graduatedFrom like", value, "graduatedFrom");
            return (Criteria) this;
        }

        public Criteria andGraduatedFromNotLike(String value) {
            addCriterion("graduatedFrom not like", value, "graduatedFrom");
            return (Criteria) this;
        }

        public Criteria andGraduatedFromIn(List<String> values) {
            addCriterion("graduatedFrom in", values, "graduatedFrom");
            return (Criteria) this;
        }

        public Criteria andGraduatedFromNotIn(List<String> values) {
            addCriterion("graduatedFrom not in", values, "graduatedFrom");
            return (Criteria) this;
        }

        public Criteria andGraduatedFromBetween(String value1, String value2) {
            addCriterion("graduatedFrom between", value1, value2, "graduatedFrom");
            return (Criteria) this;
        }

        public Criteria andGraduatedFromNotBetween(String value1, String value2) {
            addCriterion("graduatedFrom not between", value1, value2, "graduatedFrom");
            return (Criteria) this;
        }

        public Criteria andEducationIsNull() {
            addCriterion("education is null");
            return (Criteria) this;
        }

        public Criteria andEducationIsNotNull() {
            addCriterion("education is not null");
            return (Criteria) this;
        }

        public Criteria andEducationEqualTo(Integer value) {
            addCriterion("education =", value, "education");
            return (Criteria) this;
        }

        public Criteria andEducationNotEqualTo(Integer value) {
            addCriterion("education <>", value, "education");
            return (Criteria) this;
        }

        public Criteria andEducationGreaterThan(Integer value) {
            addCriterion("education >", value, "education");
            return (Criteria) this;
        }

        public Criteria andEducationGreaterThanOrEqualTo(Integer value) {
            addCriterion("education >=", value, "education");
            return (Criteria) this;
        }

        public Criteria andEducationLessThan(Integer value) {
            addCriterion("education <", value, "education");
            return (Criteria) this;
        }

        public Criteria andEducationLessThanOrEqualTo(Integer value) {
            addCriterion("education <=", value, "education");
            return (Criteria) this;
        }

        public Criteria andEducationIn(List<Integer> values) {
            addCriterion("education in", values, "education");
            return (Criteria) this;
        }

        public Criteria andEducationNotIn(List<Integer> values) {
            addCriterion("education not in", values, "education");
            return (Criteria) this;
        }

        public Criteria andEducationBetween(Integer value1, Integer value2) {
            addCriterion("education between", value1, value2, "education");
            return (Criteria) this;
        }

        public Criteria andEducationNotBetween(Integer value1, Integer value2) {
            addCriterion("education not between", value1, value2, "education");
            return (Criteria) this;
        }

        public Criteria andMajorIsNull() {
            addCriterion("major is null");
            return (Criteria) this;
        }

        public Criteria andMajorIsNotNull() {
            addCriterion("major is not null");
            return (Criteria) this;
        }

        public Criteria andMajorEqualTo(String value) {
            addCriterion("major =", value, "major");
            return (Criteria) this;
        }

        public Criteria andMajorNotEqualTo(String value) {
            addCriterion("major <>", value, "major");
            return (Criteria) this;
        }

        public Criteria andMajorGreaterThan(String value) {
            addCriterion("major >", value, "major");
            return (Criteria) this;
        }

        public Criteria andMajorGreaterThanOrEqualTo(String value) {
            addCriterion("major >=", value, "major");
            return (Criteria) this;
        }

        public Criteria andMajorLessThan(String value) {
            addCriterion("major <", value, "major");
            return (Criteria) this;
        }

        public Criteria andMajorLessThanOrEqualTo(String value) {
            addCriterion("major <=", value, "major");
            return (Criteria) this;
        }

        public Criteria andMajorLike(String value) {
            addCriterion("major like", value, "major");
            return (Criteria) this;
        }

        public Criteria andMajorNotLike(String value) {
            addCriterion("major not like", value, "major");
            return (Criteria) this;
        }

        public Criteria andMajorIn(List<String> values) {
            addCriterion("major in", values, "major");
            return (Criteria) this;
        }

        public Criteria andMajorNotIn(List<String> values) {
            addCriterion("major not in", values, "major");
            return (Criteria) this;
        }

        public Criteria andMajorBetween(String value1, String value2) {
            addCriterion("major between", value1, value2, "major");
            return (Criteria) this;
        }

        public Criteria andMajorNotBetween(String value1, String value2) {
            addCriterion("major not between", value1, value2, "major");
            return (Criteria) this;
        }

        public Criteria andWorkExperienceIsNull() {
            addCriterion("workExperience is null");
            return (Criteria) this;
        }

        public Criteria andWorkExperienceIsNotNull() {
            addCriterion("workExperience is not null");
            return (Criteria) this;
        }

        public Criteria andWorkExperienceEqualTo(String value) {
            addCriterion("workExperience =", value, "workExperience");
            return (Criteria) this;
        }

        public Criteria andWorkExperienceNotEqualTo(String value) {
            addCriterion("workExperience <>", value, "workExperience");
            return (Criteria) this;
        }

        public Criteria andWorkExperienceGreaterThan(String value) {
            addCriterion("workExperience >", value, "workExperience");
            return (Criteria) this;
        }

        public Criteria andWorkExperienceGreaterThanOrEqualTo(String value) {
            addCriterion("workExperience >=", value, "workExperience");
            return (Criteria) this;
        }

        public Criteria andWorkExperienceLessThan(String value) {
            addCriterion("workExperience <", value, "workExperience");
            return (Criteria) this;
        }

        public Criteria andWorkExperienceLessThanOrEqualTo(String value) {
            addCriterion("workExperience <=", value, "workExperience");
            return (Criteria) this;
        }

        public Criteria andWorkExperienceLike(String value) {
            addCriterion("workExperience like", value, "workExperience");
            return (Criteria) this;
        }

        public Criteria andWorkExperienceNotLike(String value) {
            addCriterion("workExperience not like", value, "workExperience");
            return (Criteria) this;
        }

        public Criteria andWorkExperienceIn(List<String> values) {
            addCriterion("workExperience in", values, "workExperience");
            return (Criteria) this;
        }

        public Criteria andWorkExperienceNotIn(List<String> values) {
            addCriterion("workExperience not in", values, "workExperience");
            return (Criteria) this;
        }

        public Criteria andWorkExperienceBetween(String value1, String value2) {
            addCriterion("workExperience between", value1, value2, "workExperience");
            return (Criteria) this;
        }

        public Criteria andWorkExperienceNotBetween(String value1, String value2) {
            addCriterion("workExperience not between", value1, value2, "workExperience");
            return (Criteria) this;
        }

        public Criteria andJobIsNull() {
            addCriterion("job is null");
            return (Criteria) this;
        }

        public Criteria andJobIsNotNull() {
            addCriterion("job is not null");
            return (Criteria) this;
        }

        public Criteria andJobEqualTo(Integer value) {
            addCriterion("job =", value, "job");
            return (Criteria) this;
        }

        public Criteria andJobNotEqualTo(Integer value) {
            addCriterion("job <>", value, "job");
            return (Criteria) this;
        }

        public Criteria andJobGreaterThan(Integer value) {
            addCriterion("job >", value, "job");
            return (Criteria) this;
        }

        public Criteria andJobGreaterThanOrEqualTo(Integer value) {
            addCriterion("job >=", value, "job");
            return (Criteria) this;
        }

        public Criteria andJobLessThan(Integer value) {
            addCriterion("job <", value, "job");
            return (Criteria) this;
        }

        public Criteria andJobLessThanOrEqualTo(Integer value) {
            addCriterion("job <=", value, "job");
            return (Criteria) this;
        }

        public Criteria andJobIn(List<Integer> values) {
            addCriterion("job in", values, "job");
            return (Criteria) this;
        }

        public Criteria andJobNotIn(List<Integer> values) {
            addCriterion("job not in", values, "job");
            return (Criteria) this;
        }

        public Criteria andJobBetween(Integer value1, Integer value2) {
            addCriterion("job between", value1, value2, "job");
            return (Criteria) this;
        }

        public Criteria andJobNotBetween(Integer value1, Integer value2) {
            addCriterion("job not between", value1, value2, "job");
            return (Criteria) this;
        }

        public Criteria andFreshGraduateIsNull() {
            addCriterion("freshGraduate is null");
            return (Criteria) this;
        }

        public Criteria andFreshGraduateIsNotNull() {
            addCriterion("freshGraduate is not null");
            return (Criteria) this;
        }

        public Criteria andFreshGraduateEqualTo(Integer value) {
            addCriterion("freshGraduate =", value, "freshGraduate");
            return (Criteria) this;
        }

        public Criteria andFreshGraduateNotEqualTo(Integer value) {
            addCriterion("freshGraduate <>", value, "freshGraduate");
            return (Criteria) this;
        }

        public Criteria andFreshGraduateGreaterThan(Integer value) {
            addCriterion("freshGraduate >", value, "freshGraduate");
            return (Criteria) this;
        }

        public Criteria andFreshGraduateGreaterThanOrEqualTo(Integer value) {
            addCriterion("freshGraduate >=", value, "freshGraduate");
            return (Criteria) this;
        }

        public Criteria andFreshGraduateLessThan(Integer value) {
            addCriterion("freshGraduate <", value, "freshGraduate");
            return (Criteria) this;
        }

        public Criteria andFreshGraduateLessThanOrEqualTo(Integer value) {
            addCriterion("freshGraduate <=", value, "freshGraduate");
            return (Criteria) this;
        }

        public Criteria andFreshGraduateIn(List<Integer> values) {
            addCriterion("freshGraduate in", values, "freshGraduate");
            return (Criteria) this;
        }

        public Criteria andFreshGraduateNotIn(List<Integer> values) {
            addCriterion("freshGraduate not in", values, "freshGraduate");
            return (Criteria) this;
        }

        public Criteria andFreshGraduateBetween(Integer value1, Integer value2) {
            addCriterion("freshGraduate between", value1, value2, "freshGraduate");
            return (Criteria) this;
        }

        public Criteria andFreshGraduateNotBetween(Integer value1, Integer value2) {
            addCriterion("freshGraduate not between", value1, value2, "freshGraduate");
            return (Criteria) this;
        }

        public Criteria andRecordIsNull() {
            addCriterion("record is null");
            return (Criteria) this;
        }

        public Criteria andRecordIsNotNull() {
            addCriterion("record is not null");
            return (Criteria) this;
        }

        public Criteria andRecordEqualTo(String value) {
            addCriterion("record =", value, "record");
            return (Criteria) this;
        }

        public Criteria andRecordNotEqualTo(String value) {
            addCriterion("record <>", value, "record");
            return (Criteria) this;
        }

        public Criteria andRecordGreaterThan(String value) {
            addCriterion("record >", value, "record");
            return (Criteria) this;
        }

        public Criteria andRecordGreaterThanOrEqualTo(String value) {
            addCriterion("record >=", value, "record");
            return (Criteria) this;
        }

        public Criteria andRecordLessThan(String value) {
            addCriterion("record <", value, "record");
            return (Criteria) this;
        }

        public Criteria andRecordLessThanOrEqualTo(String value) {
            addCriterion("record <=", value, "record");
            return (Criteria) this;
        }

        public Criteria andRecordLike(String value) {
            addCriterion("record like", value, "record");
            return (Criteria) this;
        }

        public Criteria andRecordNotLike(String value) {
            addCriterion("record not like", value, "record");
            return (Criteria) this;
        }

        public Criteria andRecordIn(List<String> values) {
            addCriterion("record in", values, "record");
            return (Criteria) this;
        }

        public Criteria andRecordNotIn(List<String> values) {
            addCriterion("record not in", values, "record");
            return (Criteria) this;
        }

        public Criteria andRecordBetween(String value1, String value2) {
            addCriterion("record between", value1, value2, "record");
            return (Criteria) this;
        }

        public Criteria andRecordNotBetween(String value1, String value2) {
            addCriterion("record not between", value1, value2, "record");
            return (Criteria) this;
        }

        public Criteria andRecordlocPathIsNull() {
            addCriterion("recordlocPath is null");
            return (Criteria) this;
        }

        public Criteria andRecordlocPathIsNotNull() {
            addCriterion("recordlocPath is not null");
            return (Criteria) this;
        }

        public Criteria andRecordlocPathEqualTo(String value) {
            addCriterion("recordlocPath =", value, "recordlocPath");
            return (Criteria) this;
        }

        public Criteria andRecordlocPathNotEqualTo(String value) {
            addCriterion("recordlocPath <>", value, "recordlocPath");
            return (Criteria) this;
        }

        public Criteria andRecordlocPathGreaterThan(String value) {
            addCriterion("recordlocPath >", value, "recordlocPath");
            return (Criteria) this;
        }

        public Criteria andRecordlocPathGreaterThanOrEqualTo(String value) {
            addCriterion("recordlocPath >=", value, "recordlocPath");
            return (Criteria) this;
        }

        public Criteria andRecordlocPathLessThan(String value) {
            addCriterion("recordlocPath <", value, "recordlocPath");
            return (Criteria) this;
        }

        public Criteria andRecordlocPathLessThanOrEqualTo(String value) {
            addCriterion("recordlocPath <=", value, "recordlocPath");
            return (Criteria) this;
        }

        public Criteria andRecordlocPathLike(String value) {
            addCriterion("recordlocPath like", value, "recordlocPath");
            return (Criteria) this;
        }

        public Criteria andRecordlocPathNotLike(String value) {
            addCriterion("recordlocPath not like", value, "recordlocPath");
            return (Criteria) this;
        }

        public Criteria andRecordlocPathIn(List<String> values) {
            addCriterion("recordlocPath in", values, "recordlocPath");
            return (Criteria) this;
        }

        public Criteria andRecordlocPathNotIn(List<String> values) {
            addCriterion("recordlocPath not in", values, "recordlocPath");
            return (Criteria) this;
        }

        public Criteria andRecordlocPathBetween(String value1, String value2) {
            addCriterion("recordlocPath between", value1, value2, "recordlocPath");
            return (Criteria) this;
        }

        public Criteria andRecordlocPathNotBetween(String value1, String value2) {
            addCriterion("recordlocPath not between", value1, value2, "recordlocPath");
            return (Criteria) this;
        }

        public Criteria andRecordpoiPathIsNull() {
            addCriterion("recordpoiPath is null");
            return (Criteria) this;
        }

        public Criteria andRecordpoiPathIsNotNull() {
            addCriterion("recordpoiPath is not null");
            return (Criteria) this;
        }

        public Criteria andRecordpoiPathEqualTo(String value) {
            addCriterion("recordpoiPath =", value, "recordpoiPath");
            return (Criteria) this;
        }

        public Criteria andRecordpoiPathNotEqualTo(String value) {
            addCriterion("recordpoiPath <>", value, "recordpoiPath");
            return (Criteria) this;
        }

        public Criteria andRecordpoiPathGreaterThan(String value) {
            addCriterion("recordpoiPath >", value, "recordpoiPath");
            return (Criteria) this;
        }

        public Criteria andRecordpoiPathGreaterThanOrEqualTo(String value) {
            addCriterion("recordpoiPath >=", value, "recordpoiPath");
            return (Criteria) this;
        }

        public Criteria andRecordpoiPathLessThan(String value) {
            addCriterion("recordpoiPath <", value, "recordpoiPath");
            return (Criteria) this;
        }

        public Criteria andRecordpoiPathLessThanOrEqualTo(String value) {
            addCriterion("recordpoiPath <=", value, "recordpoiPath");
            return (Criteria) this;
        }

        public Criteria andRecordpoiPathLike(String value) {
            addCriterion("recordpoiPath like", value, "recordpoiPath");
            return (Criteria) this;
        }

        public Criteria andRecordpoiPathNotLike(String value) {
            addCriterion("recordpoiPath not like", value, "recordpoiPath");
            return (Criteria) this;
        }

        public Criteria andRecordpoiPathIn(List<String> values) {
            addCriterion("recordpoiPath in", values, "recordpoiPath");
            return (Criteria) this;
        }

        public Criteria andRecordpoiPathNotIn(List<String> values) {
            addCriterion("recordpoiPath not in", values, "recordpoiPath");
            return (Criteria) this;
        }

        public Criteria andRecordpoiPathBetween(String value1, String value2) {
            addCriterion("recordpoiPath between", value1, value2, "recordpoiPath");
            return (Criteria) this;
        }

        public Criteria andRecordpoiPathNotBetween(String value1, String value2) {
            addCriterion("recordpoiPath not between", value1, value2, "recordpoiPath");
            return (Criteria) this;
        }

        public Criteria andResumeStatusIsNull() {
            addCriterion("resumeStatus is null");
            return (Criteria) this;
        }

        public Criteria andResumeStatusIsNotNull() {
            addCriterion("resumeStatus is not null");
            return (Criteria) this;
        }

        public Criteria andResumeStatusEqualTo(Integer value) {
            addCriterion("resumeStatus =", value, "resumeStatus");
            return (Criteria) this;
        }

        public Criteria andResumeStatusNotEqualTo(Integer value) {
            addCriterion("resumeStatus <>", value, "resumeStatus");
            return (Criteria) this;
        }

        public Criteria andResumeStatusGreaterThan(Integer value) {
            addCriterion("resumeStatus >", value, "resumeStatus");
            return (Criteria) this;
        }

        public Criteria andResumeStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("resumeStatus >=", value, "resumeStatus");
            return (Criteria) this;
        }

        public Criteria andResumeStatusLessThan(Integer value) {
            addCriterion("resumeStatus <", value, "resumeStatus");
            return (Criteria) this;
        }

        public Criteria andResumeStatusLessThanOrEqualTo(Integer value) {
            addCriterion("resumeStatus <=", value, "resumeStatus");
            return (Criteria) this;
        }

        public Criteria andResumeStatusIn(List<Integer> values) {
            addCriterion("resumeStatus in", values, "resumeStatus");
            return (Criteria) this;
        }

        public Criteria andResumeStatusNotIn(List<Integer> values) {
            addCriterion("resumeStatus not in", values, "resumeStatus");
            return (Criteria) this;
        }

        public Criteria andResumeStatusBetween(Integer value1, Integer value2) {
            addCriterion("resumeStatus between", value1, value2, "resumeStatus");
            return (Criteria) this;
        }

        public Criteria andResumeStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("resumeStatus not between", value1, value2, "resumeStatus");
            return (Criteria) this;
        }

        public Criteria andRecommendByIsNull() {
            addCriterion("recommendBy is null");
            return (Criteria) this;
        }

        public Criteria andRecommendByIsNotNull() {
            addCriterion("recommendBy is not null");
            return (Criteria) this;
        }

        public Criteria andRecommendByEqualTo(Integer value) {
            addCriterion("recommendBy =", value, "recommendBy");
            return (Criteria) this;
        }

        public Criteria andRecommendByNotEqualTo(Integer value) {
            addCriterion("recommendBy <>", value, "recommendBy");
            return (Criteria) this;
        }

        public Criteria andRecommendByGreaterThan(Integer value) {
            addCriterion("recommendBy >", value, "recommendBy");
            return (Criteria) this;
        }

        public Criteria andRecommendByGreaterThanOrEqualTo(Integer value) {
            addCriterion("recommendBy >=", value, "recommendBy");
            return (Criteria) this;
        }

        public Criteria andRecommendByLessThan(Integer value) {
            addCriterion("recommendBy <", value, "recommendBy");
            return (Criteria) this;
        }

        public Criteria andRecommendByLessThanOrEqualTo(Integer value) {
            addCriterion("recommendBy <=", value, "recommendBy");
            return (Criteria) this;
        }

        public Criteria andRecommendByIn(List<Integer> values) {
            addCriterion("recommendBy in", values, "recommendBy");
            return (Criteria) this;
        }

        public Criteria andRecommendByNotIn(List<Integer> values) {
            addCriterion("recommendBy not in", values, "recommendBy");
            return (Criteria) this;
        }

        public Criteria andRecommendByBetween(Integer value1, Integer value2) {
            addCriterion("recommendBy between", value1, value2, "recommendBy");
            return (Criteria) this;
        }

        public Criteria andRecommendByNotBetween(Integer value1, Integer value2) {
            addCriterion("recommendBy not between", value1, value2, "recommendBy");
            return (Criteria) this;
        }

        public Criteria andRecommendDateIsNull() {
            addCriterion("recommendDate is null");
            return (Criteria) this;
        }

        public Criteria andRecommendDateIsNotNull() {
            addCriterion("recommendDate is not null");
            return (Criteria) this;
        }

        public Criteria andRecommendDateEqualTo(Date value) {
            addCriterionForJDBCDate("recommendDate =", value, "recommendDate");
            return (Criteria) this;
        }

        public Criteria andRecommendDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("recommendDate <>", value, "recommendDate");
            return (Criteria) this;
        }

        public Criteria andRecommendDateGreaterThan(Date value) {
            addCriterionForJDBCDate("recommendDate >", value, "recommendDate");
            return (Criteria) this;
        }

        public Criteria andRecommendDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("recommendDate >=", value, "recommendDate");
            return (Criteria) this;
        }

        public Criteria andRecommendDateLessThan(Date value) {
            addCriterionForJDBCDate("recommendDate <", value, "recommendDate");
            return (Criteria) this;
        }

        public Criteria andRecommendDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("recommendDate <=", value, "recommendDate");
            return (Criteria) this;
        }

        public Criteria andRecommendDateIn(List<Date> values) {
            addCriterionForJDBCDate("recommendDate in", values, "recommendDate");
            return (Criteria) this;
        }

        public Criteria andRecommendDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("recommendDate not in", values, "recommendDate");
            return (Criteria) this;
        }

        public Criteria andRecommendDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("recommendDate between", value1, value2, "recommendDate");
            return (Criteria) this;
        }

        public Criteria andRecommendDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("recommendDate not between", value1, value2, "recommendDate");
            return (Criteria) this;
        }

        public Criteria andRecommendIdeaIsNull() {
            addCriterion("recommendIdea is null");
            return (Criteria) this;
        }

        public Criteria andRecommendIdeaIsNotNull() {
            addCriterion("recommendIdea is not null");
            return (Criteria) this;
        }

        public Criteria andRecommendIdeaEqualTo(String value) {
            addCriterion("recommendIdea =", value, "recommendIdea");
            return (Criteria) this;
        }

        public Criteria andRecommendIdeaNotEqualTo(String value) {
            addCriterion("recommendIdea <>", value, "recommendIdea");
            return (Criteria) this;
        }

        public Criteria andRecommendIdeaGreaterThan(String value) {
            addCriterion("recommendIdea >", value, "recommendIdea");
            return (Criteria) this;
        }

        public Criteria andRecommendIdeaGreaterThanOrEqualTo(String value) {
            addCriterion("recommendIdea >=", value, "recommendIdea");
            return (Criteria) this;
        }

        public Criteria andRecommendIdeaLessThan(String value) {
            addCriterion("recommendIdea <", value, "recommendIdea");
            return (Criteria) this;
        }

        public Criteria andRecommendIdeaLessThanOrEqualTo(String value) {
            addCriterion("recommendIdea <=", value, "recommendIdea");
            return (Criteria) this;
        }

        public Criteria andRecommendIdeaLike(String value) {
            addCriterion("recommendIdea like", value, "recommendIdea");
            return (Criteria) this;
        }

        public Criteria andRecommendIdeaNotLike(String value) {
            addCriterion("recommendIdea not like", value, "recommendIdea");
            return (Criteria) this;
        }

        public Criteria andRecommendIdeaIn(List<String> values) {
            addCriterion("recommendIdea in", values, "recommendIdea");
            return (Criteria) this;
        }

        public Criteria andRecommendIdeaNotIn(List<String> values) {
            addCriterion("recommendIdea not in", values, "recommendIdea");
            return (Criteria) this;
        }

        public Criteria andRecommendIdeaBetween(String value1, String value2) {
            addCriterion("recommendIdea between", value1, value2, "recommendIdea");
            return (Criteria) this;
        }

        public Criteria andRecommendIdeaNotBetween(String value1, String value2) {
            addCriterion("recommendIdea not between", value1, value2, "recommendIdea");
            return (Criteria) this;
        }

        public Criteria andModifyByIsNull() {
            addCriterion("modifyBy is null");
            return (Criteria) this;
        }

        public Criteria andModifyByIsNotNull() {
            addCriterion("modifyBy is not null");
            return (Criteria) this;
        }

        public Criteria andModifyByEqualTo(Integer value) {
            addCriterion("modifyBy =", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByNotEqualTo(Integer value) {
            addCriterion("modifyBy <>", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByGreaterThan(Integer value) {
            addCriterion("modifyBy >", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByGreaterThanOrEqualTo(Integer value) {
            addCriterion("modifyBy >=", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByLessThan(Integer value) {
            addCriterion("modifyBy <", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByLessThanOrEqualTo(Integer value) {
            addCriterion("modifyBy <=", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByIn(List<Integer> values) {
            addCriterion("modifyBy in", values, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByNotIn(List<Integer> values) {
            addCriterion("modifyBy not in", values, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByBetween(Integer value1, Integer value2) {
            addCriterion("modifyBy between", value1, value2, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByNotBetween(Integer value1, Integer value2) {
            addCriterion("modifyBy not between", value1, value2, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyDateIsNull() {
            addCriterion("modifyDate is null");
            return (Criteria) this;
        }

        public Criteria andModifyDateIsNotNull() {
            addCriterion("modifyDate is not null");
            return (Criteria) this;
        }

        public Criteria andModifyDateEqualTo(Date value) {
            addCriterionForJDBCDate("modifyDate =", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("modifyDate <>", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateGreaterThan(Date value) {
            addCriterionForJDBCDate("modifyDate >", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("modifyDate >=", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateLessThan(Date value) {
            addCriterionForJDBCDate("modifyDate <", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("modifyDate <=", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateIn(List<Date> values) {
            addCriterionForJDBCDate("modifyDate in", values, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("modifyDate not in", values, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("modifyDate between", value1, value2, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("modifyDate not between", value1, value2, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andAuditStatusIsNull() {
            addCriterion("auditStatus is null");
            return (Criteria) this;
        }

        public Criteria andAuditStatusIsNotNull() {
            addCriterion("auditStatus is not null");
            return (Criteria) this;
        }

        public Criteria andAuditStatusEqualTo(Integer value) {
            addCriterion("auditStatus =", value, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusNotEqualTo(Integer value) {
            addCriterion("auditStatus <>", value, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusGreaterThan(Integer value) {
            addCriterion("auditStatus >", value, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("auditStatus >=", value, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusLessThan(Integer value) {
            addCriterion("auditStatus <", value, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusLessThanOrEqualTo(Integer value) {
            addCriterion("auditStatus <=", value, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusIn(List<Integer> values) {
            addCriterion("auditStatus in", values, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusNotIn(List<Integer> values) {
            addCriterion("auditStatus not in", values, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusBetween(Integer value1, Integer value2) {
            addCriterion("auditStatus between", value1, value2, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("auditStatus not between", value1, value2, "auditStatus");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}