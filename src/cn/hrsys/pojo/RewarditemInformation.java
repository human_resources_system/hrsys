package cn.hrsys.pojo;

import java.math.BigDecimal;
import java.util.Date;

public class RewarditemInformation {
    private Integer id;

    private String rewarditemCode;

    private String rewarditemName;

    private String rewarditemAbstract;

    private BigDecimal rewardmoney;

    private Integer creationBy;

    private Date creationDate;

    private Integer modifyBy;

    private Date modifyDate;

    private String modifyCause;

    private Integer status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRewarditemCode() {
        return rewarditemCode;
    }

    public void setRewarditemCode(String rewarditemCode) {
        this.rewarditemCode = rewarditemCode == null ? null : rewarditemCode.trim();
    }

    public String getRewarditemName() {
        return rewarditemName;
    }

    public void setRewarditemName(String rewarditemName) {
        this.rewarditemName = rewarditemName == null ? null : rewarditemName.trim();
    }

    public String getRewarditemAbstract() {
        return rewarditemAbstract;
    }

    public void setRewarditemAbstract(String rewarditemAbstract) {
        this.rewarditemAbstract = rewarditemAbstract == null ? null : rewarditemAbstract.trim();
    }

    public BigDecimal getRewardmoney() {
        return rewardmoney;
    }

    public void setRewardmoney(BigDecimal rewardmoney) {
        this.rewardmoney = rewardmoney;
    }

    public Integer getCreationBy() {
        return creationBy;
    }

    public void setCreationBy(Integer creationBy) {
        this.creationBy = creationBy;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Integer getModifyBy() {
        return modifyBy;
    }

    public void setModifyBy(Integer modifyBy) {
        this.modifyBy = modifyBy;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getModifyCause() {
        return modifyCause;
    }

    public void setModifyCause(String modifyCause) {
        this.modifyCause = modifyCause == null ? null : modifyCause.trim();
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}