package cn.hrsys.pojo;

import java.math.BigDecimal;
import java.util.Date;

public class AttendanceInformation {
    private Integer id;

    private Date outworkDate;

    private Date quitworkDate;

    private Date overbeginTime;

    private Date overendTime;

    private BigDecimal overtimeAllowance;

    private String overtimeReason;

    private Date leavebeginDate;

    private Date leaveendDate;

    private String leaveReason;

    private Date evectionbeginDate;

    private Date evectionendDate;

    private String evectionReason;

    private BigDecimal evectionAllowance;

    private Integer attendanceStatus;

    private Date workingDate;

    private Integer personId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getOutworkDate() {
        return outworkDate;
    }

    public void setOutworkDate(Date outworkDate) {
        this.outworkDate = outworkDate;
    }

    public Date getQuitworkDate() {
        return quitworkDate;
    }

    public void setQuitworkDate(Date quitworkDate) {
        this.quitworkDate = quitworkDate;
    }

    public Date getOverbeginTime() {
        return overbeginTime;
    }

    public void setOverbeginTime(Date overbeginTime) {
        this.overbeginTime = overbeginTime;
    }

    public Date getOverendTime() {
        return overendTime;
    }

    public void setOverendTime(Date overendTime) {
        this.overendTime = overendTime;
    }

    public BigDecimal getOvertimeAllowance() {
        return overtimeAllowance;
    }

    public void setOvertimeAllowance(BigDecimal overtimeAllowance) {
        this.overtimeAllowance = overtimeAllowance;
    }

    public String getOvertimeReason() {
        return overtimeReason;
    }

    public void setOvertimeReason(String overtimeReason) {
        this.overtimeReason = overtimeReason == null ? null : overtimeReason.trim();
    }

    public Date getLeavebeginDate() {
        return leavebeginDate;
    }

    public void setLeavebeginDate(Date leavebeginDate) {
        this.leavebeginDate = leavebeginDate;
    }

    public Date getLeaveendDate() {
        return leaveendDate;
    }

    public void setLeaveendDate(Date leaveendDate) {
        this.leaveendDate = leaveendDate;
    }

    public String getLeaveReason() {
        return leaveReason;
    }

    public void setLeaveReason(String leaveReason) {
        this.leaveReason = leaveReason == null ? null : leaveReason.trim();
    }

    public Date getEvectionbeginDate() {
        return evectionbeginDate;
    }

    public void setEvectionbeginDate(Date evectionbeginDate) {
        this.evectionbeginDate = evectionbeginDate;
    }

    public Date getEvectionendDate() {
        return evectionendDate;
    }

    public void setEvectionendDate(Date evectionendDate) {
        this.evectionendDate = evectionendDate;
    }

    public String getEvectionReason() {
        return evectionReason;
    }

    public void setEvectionReason(String evectionReason) {
        this.evectionReason = evectionReason == null ? null : evectionReason.trim();
    }

    public BigDecimal getEvectionAllowance() {
        return evectionAllowance;
    }

    public void setEvectionAllowance(BigDecimal evectionAllowance) {
        this.evectionAllowance = evectionAllowance;
    }

    public Integer getAttendanceStatus() {
        return attendanceStatus;
    }

    public void setAttendanceStatus(Integer attendanceStatus) {
        this.attendanceStatus = attendanceStatus;
    }

    public Date getWorkingDate() {
        return workingDate;
    }

    public void setWorkingDate(Date workingDate) {
        this.workingDate = workingDate;
    }

    public Integer getPersonId() {
        return personId;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }
}