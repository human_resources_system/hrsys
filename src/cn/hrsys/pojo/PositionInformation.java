package cn.hrsys.pojo;

import java.util.Date;

public class PositionInformation {
    private Integer id;

    private String positionCode;

    private String positionShortName;

    private String positionName;

    private Integer positionDepId;

    private String positionAbstract;

    private Integer positionType;

    private String positionRemark;

    private Integer positionStatus;

    private Integer creationBy;

    private Date creationDate;

    private Integer modifyBy;

    private Date modifyDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPositionCode() {
        return positionCode;
    }

    public void setPositionCode(String positionCode) {
        this.positionCode = positionCode == null ? null : positionCode.trim();
    }

    public String getPositionShortName() {
        return positionShortName;
    }

    public void setPositionShortName(String positionShortName) {
        this.positionShortName = positionShortName == null ? null : positionShortName.trim();
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName == null ? null : positionName.trim();
    }

    public Integer getPositionDepId() {
        return positionDepId;
    }

    public void setPositionDepId(Integer positionDepId) {
        this.positionDepId = positionDepId;
    }

    public String getPositionAbstract() {
        return positionAbstract;
    }

    public void setPositionAbstract(String positionAbstract) {
        this.positionAbstract = positionAbstract == null ? null : positionAbstract.trim();
    }

    public Integer getPositionType() {
        return positionType;
    }

    public void setPositionType(Integer positionType) {
        this.positionType = positionType;
    }

    public String getPositionRemark() {
        return positionRemark;
    }

    public void setPositionRemark(String positionRemark) {
        this.positionRemark = positionRemark == null ? null : positionRemark.trim();
    }

    public Integer getPositionStatus() {
        return positionStatus;
    }

    public void setPositionStatus(Integer positionStatus) {
        this.positionStatus = positionStatus;
    }

    public Integer getCreationBy() {
        return creationBy;
    }

    public void setCreationBy(Integer creationBy) {
        this.creationBy = creationBy;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Integer getModifyBy() {
        return modifyBy;
    }

    public void setModifyBy(Integer modifyBy) {
        this.modifyBy = modifyBy;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }
}