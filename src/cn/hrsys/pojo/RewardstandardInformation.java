package cn.hrsys.pojo;

import java.math.BigDecimal;
import java.util.Date;

public class RewardstandardInformation {
    private Integer id;

    private String rewardCode;

    private String rewardName;

    private BigDecimal rewardTotal;

    private String rewardDesigner;

    private Integer registerBy;

    private Date registerDate;

    private Integer modifyBy;

    private Date modifyDate;

    private Integer auditStatus;

    private String modifyCause;

    private Integer status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRewardCode() {
        return rewardCode;
    }

    public void setRewardCode(String rewardCode) {
        this.rewardCode = rewardCode == null ? null : rewardCode.trim();
    }

    public String getRewardName() {
        return rewardName;
    }

    public void setRewardName(String rewardName) {
        this.rewardName = rewardName == null ? null : rewardName.trim();
    }

    public BigDecimal getRewardTotal() {
        return rewardTotal;
    }

    public void setRewardTotal(BigDecimal rewardTotal) {
        this.rewardTotal = rewardTotal;
    }

    public String getRewardDesigner() {
        return rewardDesigner;
    }

    public void setRewardDesigner(String rewardDesigner) {
        this.rewardDesigner = rewardDesigner == null ? null : rewardDesigner.trim();
    }

    public Integer getRegisterBy() {
        return registerBy;
    }

    public void setRegisterBy(Integer registerBy) {
        this.registerBy = registerBy;
    }

    public Date getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(Date registerDate) {
        this.registerDate = registerDate;
    }

    public Integer getModifyBy() {
        return modifyBy;
    }

    public void setModifyBy(Integer modifyBy) {
        this.modifyBy = modifyBy;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public Integer getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }

    public String getModifyCause() {
        return modifyCause;
    }

    public void setModifyCause(String modifyCause) {
        this.modifyCause = modifyCause == null ? null : modifyCause.trim();
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}