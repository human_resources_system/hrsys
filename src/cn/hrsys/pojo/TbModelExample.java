package cn.hrsys.pojo;

import java.util.ArrayList;
import java.util.List;

public class TbModelExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public TbModelExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andModelIDIsNull() {
            addCriterion("modelID is null");
            return (Criteria) this;
        }

        public Criteria andModelIDIsNotNull() {
            addCriterion("modelID is not null");
            return (Criteria) this;
        }

        public Criteria andModelIDEqualTo(String value) {
            addCriterion("modelID =", value, "modelID");
            return (Criteria) this;
        }

        public Criteria andModelIDNotEqualTo(String value) {
            addCriterion("modelID <>", value, "modelID");
            return (Criteria) this;
        }

        public Criteria andModelIDGreaterThan(String value) {
            addCriterion("modelID >", value, "modelID");
            return (Criteria) this;
        }

        public Criteria andModelIDGreaterThanOrEqualTo(String value) {
            addCriterion("modelID >=", value, "modelID");
            return (Criteria) this;
        }

        public Criteria andModelIDLessThan(String value) {
            addCriterion("modelID <", value, "modelID");
            return (Criteria) this;
        }

        public Criteria andModelIDLessThanOrEqualTo(String value) {
            addCriterion("modelID <=", value, "modelID");
            return (Criteria) this;
        }

        public Criteria andModelIDLike(String value) {
            addCriterion("modelID like", value, "modelID");
            return (Criteria) this;
        }

        public Criteria andModelIDNotLike(String value) {
            addCriterion("modelID not like", value, "modelID");
            return (Criteria) this;
        }

        public Criteria andModelIDIn(List<String> values) {
            addCriterion("modelID in", values, "modelID");
            return (Criteria) this;
        }

        public Criteria andModelIDNotIn(List<String> values) {
            addCriterion("modelID not in", values, "modelID");
            return (Criteria) this;
        }

        public Criteria andModelIDBetween(String value1, String value2) {
            addCriterion("modelID between", value1, value2, "modelID");
            return (Criteria) this;
        }

        public Criteria andModelIDNotBetween(String value1, String value2) {
            addCriterion("modelID not between", value1, value2, "modelID");
            return (Criteria) this;
        }

        public Criteria andModelNameIsNull() {
            addCriterion("modelName is null");
            return (Criteria) this;
        }

        public Criteria andModelNameIsNotNull() {
            addCriterion("modelName is not null");
            return (Criteria) this;
        }

        public Criteria andModelNameEqualTo(String value) {
            addCriterion("modelName =", value, "modelName");
            return (Criteria) this;
        }

        public Criteria andModelNameNotEqualTo(String value) {
            addCriterion("modelName <>", value, "modelName");
            return (Criteria) this;
        }

        public Criteria andModelNameGreaterThan(String value) {
            addCriterion("modelName >", value, "modelName");
            return (Criteria) this;
        }

        public Criteria andModelNameGreaterThanOrEqualTo(String value) {
            addCriterion("modelName >=", value, "modelName");
            return (Criteria) this;
        }

        public Criteria andModelNameLessThan(String value) {
            addCriterion("modelName <", value, "modelName");
            return (Criteria) this;
        }

        public Criteria andModelNameLessThanOrEqualTo(String value) {
            addCriterion("modelName <=", value, "modelName");
            return (Criteria) this;
        }

        public Criteria andModelNameLike(String value) {
            addCriterion("modelName like", value, "modelName");
            return (Criteria) this;
        }

        public Criteria andModelNameNotLike(String value) {
            addCriterion("modelName not like", value, "modelName");
            return (Criteria) this;
        }

        public Criteria andModelNameIn(List<String> values) {
            addCriterion("modelName in", values, "modelName");
            return (Criteria) this;
        }

        public Criteria andModelNameNotIn(List<String> values) {
            addCriterion("modelName not in", values, "modelName");
            return (Criteria) this;
        }

        public Criteria andModelNameBetween(String value1, String value2) {
            addCriterion("modelName between", value1, value2, "modelName");
            return (Criteria) this;
        }

        public Criteria andModelNameNotBetween(String value1, String value2) {
            addCriterion("modelName not between", value1, value2, "modelName");
            return (Criteria) this;
        }

        public Criteria andModelURLIsNull() {
            addCriterion("modelURL is null");
            return (Criteria) this;
        }

        public Criteria andModelURLIsNotNull() {
            addCriterion("modelURL is not null");
            return (Criteria) this;
        }

        public Criteria andModelURLEqualTo(String value) {
            addCriterion("modelURL =", value, "modelURL");
            return (Criteria) this;
        }

        public Criteria andModelURLNotEqualTo(String value) {
            addCriterion("modelURL <>", value, "modelURL");
            return (Criteria) this;
        }

        public Criteria andModelURLGreaterThan(String value) {
            addCriterion("modelURL >", value, "modelURL");
            return (Criteria) this;
        }

        public Criteria andModelURLGreaterThanOrEqualTo(String value) {
            addCriterion("modelURL >=", value, "modelURL");
            return (Criteria) this;
        }

        public Criteria andModelURLLessThan(String value) {
            addCriterion("modelURL <", value, "modelURL");
            return (Criteria) this;
        }

        public Criteria andModelURLLessThanOrEqualTo(String value) {
            addCriterion("modelURL <=", value, "modelURL");
            return (Criteria) this;
        }

        public Criteria andModelURLLike(String value) {
            addCriterion("modelURL like", value, "modelURL");
            return (Criteria) this;
        }

        public Criteria andModelURLNotLike(String value) {
            addCriterion("modelURL not like", value, "modelURL");
            return (Criteria) this;
        }

        public Criteria andModelURLIn(List<String> values) {
            addCriterion("modelURL in", values, "modelURL");
            return (Criteria) this;
        }

        public Criteria andModelURLNotIn(List<String> values) {
            addCriterion("modelURL not in", values, "modelURL");
            return (Criteria) this;
        }

        public Criteria andModelURLBetween(String value1, String value2) {
            addCriterion("modelURL between", value1, value2, "modelURL");
            return (Criteria) this;
        }

        public Criteria andModelURLNotBetween(String value1, String value2) {
            addCriterion("modelURL not between", value1, value2, "modelURL");
            return (Criteria) this;
        }

        public Criteria andParentIsNull() {
            addCriterion("parent is null");
            return (Criteria) this;
        }

        public Criteria andParentIsNotNull() {
            addCriterion("parent is not null");
            return (Criteria) this;
        }

        public Criteria andParentEqualTo(String value) {
            addCriterion("parent =", value, "parent");
            return (Criteria) this;
        }

        public Criteria andParentNotEqualTo(String value) {
            addCriterion("parent <>", value, "parent");
            return (Criteria) this;
        }

        public Criteria andParentGreaterThan(String value) {
            addCriterion("parent >", value, "parent");
            return (Criteria) this;
        }

        public Criteria andParentGreaterThanOrEqualTo(String value) {
            addCriterion("parent >=", value, "parent");
            return (Criteria) this;
        }

        public Criteria andParentLessThan(String value) {
            addCriterion("parent <", value, "parent");
            return (Criteria) this;
        }

        public Criteria andParentLessThanOrEqualTo(String value) {
            addCriterion("parent <=", value, "parent");
            return (Criteria) this;
        }

        public Criteria andParentLike(String value) {
            addCriterion("parent like", value, "parent");
            return (Criteria) this;
        }

        public Criteria andParentNotLike(String value) {
            addCriterion("parent not like", value, "parent");
            return (Criteria) this;
        }

        public Criteria andParentIn(List<String> values) {
            addCriterion("parent in", values, "parent");
            return (Criteria) this;
        }

        public Criteria andParentNotIn(List<String> values) {
            addCriterion("parent not in", values, "parent");
            return (Criteria) this;
        }

        public Criteria andParentBetween(String value1, String value2) {
            addCriterion("parent between", value1, value2, "parent");
            return (Criteria) this;
        }

        public Criteria andParentNotBetween(String value1, String value2) {
            addCriterion("parent not between", value1, value2, "parent");
            return (Criteria) this;
        }

        public Criteria andAncentorIsNull() {
            addCriterion("ancentor is null");
            return (Criteria) this;
        }

        public Criteria andAncentorIsNotNull() {
            addCriterion("ancentor is not null");
            return (Criteria) this;
        }

        public Criteria andAncentorEqualTo(String value) {
            addCriterion("ancentor =", value, "ancentor");
            return (Criteria) this;
        }

        public Criteria andAncentorNotEqualTo(String value) {
            addCriterion("ancentor <>", value, "ancentor");
            return (Criteria) this;
        }

        public Criteria andAncentorGreaterThan(String value) {
            addCriterion("ancentor >", value, "ancentor");
            return (Criteria) this;
        }

        public Criteria andAncentorGreaterThanOrEqualTo(String value) {
            addCriterion("ancentor >=", value, "ancentor");
            return (Criteria) this;
        }

        public Criteria andAncentorLessThan(String value) {
            addCriterion("ancentor <", value, "ancentor");
            return (Criteria) this;
        }

        public Criteria andAncentorLessThanOrEqualTo(String value) {
            addCriterion("ancentor <=", value, "ancentor");
            return (Criteria) this;
        }

        public Criteria andAncentorLike(String value) {
            addCriterion("ancentor like", value, "ancentor");
            return (Criteria) this;
        }

        public Criteria andAncentorNotLike(String value) {
            addCriterion("ancentor not like", value, "ancentor");
            return (Criteria) this;
        }

        public Criteria andAncentorIn(List<String> values) {
            addCriterion("ancentor in", values, "ancentor");
            return (Criteria) this;
        }

        public Criteria andAncentorNotIn(List<String> values) {
            addCriterion("ancentor not in", values, "ancentor");
            return (Criteria) this;
        }

        public Criteria andAncentorBetween(String value1, String value2) {
            addCriterion("ancentor between", value1, value2, "ancentor");
            return (Criteria) this;
        }

        public Criteria andAncentorNotBetween(String value1, String value2) {
            addCriterion("ancentor not between", value1, value2, "ancentor");
            return (Criteria) this;
        }

        public Criteria andOrderByIsNull() {
            addCriterion("orderBy is null");
            return (Criteria) this;
        }

        public Criteria andOrderByIsNotNull() {
            addCriterion("orderBy is not null");
            return (Criteria) this;
        }

        public Criteria andOrderByEqualTo(Integer value) {
            addCriterion("orderBy =", value, "orderBy");
            return (Criteria) this;
        }

        public Criteria andOrderByNotEqualTo(Integer value) {
            addCriterion("orderBy <>", value, "orderBy");
            return (Criteria) this;
        }

        public Criteria andOrderByGreaterThan(Integer value) {
            addCriterion("orderBy >", value, "orderBy");
            return (Criteria) this;
        }

        public Criteria andOrderByGreaterThanOrEqualTo(Integer value) {
            addCriterion("orderBy >=", value, "orderBy");
            return (Criteria) this;
        }

        public Criteria andOrderByLessThan(Integer value) {
            addCriterion("orderBy <", value, "orderBy");
            return (Criteria) this;
        }

        public Criteria andOrderByLessThanOrEqualTo(Integer value) {
            addCriterion("orderBy <=", value, "orderBy");
            return (Criteria) this;
        }

        public Criteria andOrderByIn(List<Integer> values) {
            addCriterion("orderBy in", values, "orderBy");
            return (Criteria) this;
        }

        public Criteria andOrderByNotIn(List<Integer> values) {
            addCriterion("orderBy not in", values, "orderBy");
            return (Criteria) this;
        }

        public Criteria andOrderByBetween(Integer value1, Integer value2) {
            addCriterion("orderBy between", value1, value2, "orderBy");
            return (Criteria) this;
        }

        public Criteria andOrderByNotBetween(Integer value1, Integer value2) {
            addCriterion("orderBy not between", value1, value2, "orderBy");
            return (Criteria) this;
        }

        public Criteria andRelPageIsNull() {
            addCriterion("relPage is null");
            return (Criteria) this;
        }

        public Criteria andRelPageIsNotNull() {
            addCriterion("relPage is not null");
            return (Criteria) this;
        }

        public Criteria andRelPageEqualTo(String value) {
            addCriterion("relPage =", value, "relPage");
            return (Criteria) this;
        }

        public Criteria andRelPageNotEqualTo(String value) {
            addCriterion("relPage <>", value, "relPage");
            return (Criteria) this;
        }

        public Criteria andRelPageGreaterThan(String value) {
            addCriterion("relPage >", value, "relPage");
            return (Criteria) this;
        }

        public Criteria andRelPageGreaterThanOrEqualTo(String value) {
            addCriterion("relPage >=", value, "relPage");
            return (Criteria) this;
        }

        public Criteria andRelPageLessThan(String value) {
            addCriterion("relPage <", value, "relPage");
            return (Criteria) this;
        }

        public Criteria andRelPageLessThanOrEqualTo(String value) {
            addCriterion("relPage <=", value, "relPage");
            return (Criteria) this;
        }

        public Criteria andRelPageLike(String value) {
            addCriterion("relPage like", value, "relPage");
            return (Criteria) this;
        }

        public Criteria andRelPageNotLike(String value) {
            addCriterion("relPage not like", value, "relPage");
            return (Criteria) this;
        }

        public Criteria andRelPageIn(List<String> values) {
            addCriterion("relPage in", values, "relPage");
            return (Criteria) this;
        }

        public Criteria andRelPageNotIn(List<String> values) {
            addCriterion("relPage not in", values, "relPage");
            return (Criteria) this;
        }

        public Criteria andRelPageBetween(String value1, String value2) {
            addCriterion("relPage between", value1, value2, "relPage");
            return (Criteria) this;
        }

        public Criteria andRelPageNotBetween(String value1, String value2) {
            addCriterion("relPage not between", value1, value2, "relPage");
            return (Criteria) this;
        }

        public Criteria andDeletedIsNull() {
            addCriterion("deleted is null");
            return (Criteria) this;
        }

        public Criteria andDeletedIsNotNull() {
            addCriterion("deleted is not null");
            return (Criteria) this;
        }

        public Criteria andDeletedEqualTo(Integer value) {
            addCriterion("deleted =", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedNotEqualTo(Integer value) {
            addCriterion("deleted <>", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedGreaterThan(Integer value) {
            addCriterion("deleted >", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedGreaterThanOrEqualTo(Integer value) {
            addCriterion("deleted >=", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedLessThan(Integer value) {
            addCriterion("deleted <", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedLessThanOrEqualTo(Integer value) {
            addCriterion("deleted <=", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedIn(List<Integer> values) {
            addCriterion("deleted in", values, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedNotIn(List<Integer> values) {
            addCriterion("deleted not in", values, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedBetween(Integer value1, Integer value2) {
            addCriterion("deleted between", value1, value2, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedNotBetween(Integer value1, Integer value2) {
            addCriterion("deleted not between", value1, value2, "deleted");
            return (Criteria) this;
        }

        public Criteria andMenuTypeIsNull() {
            addCriterion("menuType is null");
            return (Criteria) this;
        }

        public Criteria andMenuTypeIsNotNull() {
            addCriterion("menuType is not null");
            return (Criteria) this;
        }

        public Criteria andMenuTypeEqualTo(Integer value) {
            addCriterion("menuType =", value, "menuType");
            return (Criteria) this;
        }

        public Criteria andMenuTypeNotEqualTo(Integer value) {
            addCriterion("menuType <>", value, "menuType");
            return (Criteria) this;
        }

        public Criteria andMenuTypeGreaterThan(Integer value) {
            addCriterion("menuType >", value, "menuType");
            return (Criteria) this;
        }

        public Criteria andMenuTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("menuType >=", value, "menuType");
            return (Criteria) this;
        }

        public Criteria andMenuTypeLessThan(Integer value) {
            addCriterion("menuType <", value, "menuType");
            return (Criteria) this;
        }

        public Criteria andMenuTypeLessThanOrEqualTo(Integer value) {
            addCriterion("menuType <=", value, "menuType");
            return (Criteria) this;
        }

        public Criteria andMenuTypeIn(List<Integer> values) {
            addCriterion("menuType in", values, "menuType");
            return (Criteria) this;
        }

        public Criteria andMenuTypeNotIn(List<Integer> values) {
            addCriterion("menuType not in", values, "menuType");
            return (Criteria) this;
        }

        public Criteria andMenuTypeBetween(Integer value1, Integer value2) {
            addCriterion("menuType between", value1, value2, "menuType");
            return (Criteria) this;
        }

        public Criteria andMenuTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("menuType not between", value1, value2, "menuType");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNull() {
            addCriterion("remark is null");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNotNull() {
            addCriterion("remark is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkEqualTo(String value) {
            addCriterion("remark =", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotEqualTo(String value) {
            addCriterion("remark <>", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThan(String value) {
            addCriterion("remark >", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("remark >=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThan(String value) {
            addCriterion("remark <", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThanOrEqualTo(String value) {
            addCriterion("remark <=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLike(String value) {
            addCriterion("remark like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotLike(String value) {
            addCriterion("remark not like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkIn(List<String> values) {
            addCriterion("remark in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotIn(List<String> values) {
            addCriterion("remark not in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkBetween(String value1, String value2) {
            addCriterion("remark between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotBetween(String value1, String value2) {
            addCriterion("remark not between", value1, value2, "remark");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}