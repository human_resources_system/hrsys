package cn.hrsys.pojo;

import java.math.BigDecimal;

public class MonthStatistics {
    private Integer id;

    private Integer tardinessNumber;

    private BigDecimal tardinessDeductmoney;

    private Integer leaveDays;

    private BigDecimal leaveDeductmoney;

    private Integer overtimeNumber;

    private BigDecimal overtimeAllowance;

    private Integer evectionDays;

    private BigDecimal evectionAllowance;

    private Integer monthtimeNumber;

    private Integer personId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTardinessNumber() {
        return tardinessNumber;
    }

    public void setTardinessNumber(Integer tardinessNumber) {
        this.tardinessNumber = tardinessNumber;
    }

    public BigDecimal getTardinessDeductmoney() {
        return tardinessDeductmoney;
    }

    public void setTardinessDeductmoney(BigDecimal tardinessDeductmoney) {
        this.tardinessDeductmoney = tardinessDeductmoney;
    }

    public Integer getLeaveDays() {
        return leaveDays;
    }

    public void setLeaveDays(Integer leaveDays) {
        this.leaveDays = leaveDays;
    }

    public BigDecimal getLeaveDeductmoney() {
        return leaveDeductmoney;
    }

    public void setLeaveDeductmoney(BigDecimal leaveDeductmoney) {
        this.leaveDeductmoney = leaveDeductmoney;
    }

    public Integer getOvertimeNumber() {
        return overtimeNumber;
    }

    public void setOvertimeNumber(Integer overtimeNumber) {
        this.overtimeNumber = overtimeNumber;
    }

    public BigDecimal getOvertimeAllowance() {
        return overtimeAllowance;
    }

    public void setOvertimeAllowance(BigDecimal overtimeAllowance) {
        this.overtimeAllowance = overtimeAllowance;
    }

    public Integer getEvectionDays() {
        return evectionDays;
    }

    public void setEvectionDays(Integer evectionDays) {
        this.evectionDays = evectionDays;
    }

    public BigDecimal getEvectionAllowance() {
        return evectionAllowance;
    }

    public void setEvectionAllowance(BigDecimal evectionAllowance) {
        this.evectionAllowance = evectionAllowance;
    }

    public Integer getMonthtimeNumber() {
        return monthtimeNumber;
    }

    public void setMonthtimeNumber(Integer monthtimeNumber) {
        this.monthtimeNumber = monthtimeNumber;
    }

    public Integer getPersonId() {
        return personId;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }
}