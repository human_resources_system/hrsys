package cn.hrsys.pojo;

import java.util.Date;

public class InterviewInformation {
    private Integer id;

    private Integer resumeId;

    private String interviewBy;

    private Date interviewDate;

    private String describe;

    private Integer interviewTime;

    private String writtenLevel;

    private String writtenGrade;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getResumeId() {
        return resumeId;
    }

    public void setResumeId(Integer resumeId) {
        this.resumeId = resumeId;
    }

    public String getInterviewBy() {
        return interviewBy;
    }

    public void setInterviewBy(String interviewBy) {
        this.interviewBy = interviewBy == null ? null : interviewBy.trim();
    }

    public Date getInterviewDate() {
        return interviewDate;
    }

    public void setInterviewDate(Date interviewDate) {
        this.interviewDate = interviewDate;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe == null ? null : describe.trim();
    }

    public Integer getInterviewTime() {
        return interviewTime;
    }

    public void setInterviewTime(Integer interviewTime) {
        this.interviewTime = interviewTime;
    }

    public String getWrittenLevel() {
        return writtenLevel;
    }

    public void setWrittenLevel(String writtenLevel) {
        this.writtenLevel = writtenLevel == null ? null : writtenLevel.trim();
    }

    public String getWrittenGrade() {
        return writtenGrade;
    }

    public void setWrittenGrade(String writtenGrade) {
        this.writtenGrade = writtenGrade == null ? null : writtenGrade.trim();
    }
}