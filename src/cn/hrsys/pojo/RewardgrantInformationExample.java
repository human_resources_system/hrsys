package cn.hrsys.pojo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class RewardgrantInformationExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public RewardgrantInformationExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andRewardbillCodeIsNull() {
            addCriterion("rewardbillCode is null");
            return (Criteria) this;
        }

        public Criteria andRewardbillCodeIsNotNull() {
            addCriterion("rewardbillCode is not null");
            return (Criteria) this;
        }

        public Criteria andRewardbillCodeEqualTo(String value) {
            addCriterion("rewardbillCode =", value, "rewardbillCode");
            return (Criteria) this;
        }

        public Criteria andRewardbillCodeNotEqualTo(String value) {
            addCriterion("rewardbillCode <>", value, "rewardbillCode");
            return (Criteria) this;
        }

        public Criteria andRewardbillCodeGreaterThan(String value) {
            addCriterion("rewardbillCode >", value, "rewardbillCode");
            return (Criteria) this;
        }

        public Criteria andRewardbillCodeGreaterThanOrEqualTo(String value) {
            addCriterion("rewardbillCode >=", value, "rewardbillCode");
            return (Criteria) this;
        }

        public Criteria andRewardbillCodeLessThan(String value) {
            addCriterion("rewardbillCode <", value, "rewardbillCode");
            return (Criteria) this;
        }

        public Criteria andRewardbillCodeLessThanOrEqualTo(String value) {
            addCriterion("rewardbillCode <=", value, "rewardbillCode");
            return (Criteria) this;
        }

        public Criteria andRewardbillCodeLike(String value) {
            addCriterion("rewardbillCode like", value, "rewardbillCode");
            return (Criteria) this;
        }

        public Criteria andRewardbillCodeNotLike(String value) {
            addCriterion("rewardbillCode not like", value, "rewardbillCode");
            return (Criteria) this;
        }

        public Criteria andRewardbillCodeIn(List<String> values) {
            addCriterion("rewardbillCode in", values, "rewardbillCode");
            return (Criteria) this;
        }

        public Criteria andRewardbillCodeNotIn(List<String> values) {
            addCriterion("rewardbillCode not in", values, "rewardbillCode");
            return (Criteria) this;
        }

        public Criteria andRewardbillCodeBetween(String value1, String value2) {
            addCriterion("rewardbillCode between", value1, value2, "rewardbillCode");
            return (Criteria) this;
        }

        public Criteria andRewardbillCodeNotBetween(String value1, String value2) {
            addCriterion("rewardbillCode not between", value1, value2, "rewardbillCode");
            return (Criteria) this;
        }

        public Criteria andDepartmentIdIsNull() {
            addCriterion("departmentId is null");
            return (Criteria) this;
        }

        public Criteria andDepartmentIdIsNotNull() {
            addCriterion("departmentId is not null");
            return (Criteria) this;
        }

        public Criteria andDepartmentIdEqualTo(Integer value) {
            addCriterion("departmentId =", value, "departmentId");
            return (Criteria) this;
        }

        public Criteria andDepartmentIdNotEqualTo(Integer value) {
            addCriterion("departmentId <>", value, "departmentId");
            return (Criteria) this;
        }

        public Criteria andDepartmentIdGreaterThan(Integer value) {
            addCriterion("departmentId >", value, "departmentId");
            return (Criteria) this;
        }

        public Criteria andDepartmentIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("departmentId >=", value, "departmentId");
            return (Criteria) this;
        }

        public Criteria andDepartmentIdLessThan(Integer value) {
            addCriterion("departmentId <", value, "departmentId");
            return (Criteria) this;
        }

        public Criteria andDepartmentIdLessThanOrEqualTo(Integer value) {
            addCriterion("departmentId <=", value, "departmentId");
            return (Criteria) this;
        }

        public Criteria andDepartmentIdIn(List<Integer> values) {
            addCriterion("departmentId in", values, "departmentId");
            return (Criteria) this;
        }

        public Criteria andDepartmentIdNotIn(List<Integer> values) {
            addCriterion("departmentId not in", values, "departmentId");
            return (Criteria) this;
        }

        public Criteria andDepartmentIdBetween(Integer value1, Integer value2) {
            addCriterion("departmentId between", value1, value2, "departmentId");
            return (Criteria) this;
        }

        public Criteria andDepartmentIdNotBetween(Integer value1, Integer value2) {
            addCriterion("departmentId not between", value1, value2, "departmentId");
            return (Criteria) this;
        }

        public Criteria andPositionIdIsNull() {
            addCriterion("positionId is null");
            return (Criteria) this;
        }

        public Criteria andPositionIdIsNotNull() {
            addCriterion("positionId is not null");
            return (Criteria) this;
        }

        public Criteria andPositionIdEqualTo(Integer value) {
            addCriterion("positionId =", value, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdNotEqualTo(Integer value) {
            addCriterion("positionId <>", value, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdGreaterThan(Integer value) {
            addCriterion("positionId >", value, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("positionId >=", value, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdLessThan(Integer value) {
            addCriterion("positionId <", value, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdLessThanOrEqualTo(Integer value) {
            addCriterion("positionId <=", value, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdIn(List<Integer> values) {
            addCriterion("positionId in", values, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdNotIn(List<Integer> values) {
            addCriterion("positionId not in", values, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdBetween(Integer value1, Integer value2) {
            addCriterion("positionId between", value1, value2, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdNotBetween(Integer value1, Integer value2) {
            addCriterion("positionId not between", value1, value2, "positionId");
            return (Criteria) this;
        }

        public Criteria andHeadcountIsNull() {
            addCriterion("headcount is null");
            return (Criteria) this;
        }

        public Criteria andHeadcountIsNotNull() {
            addCriterion("headcount is not null");
            return (Criteria) this;
        }

        public Criteria andHeadcountEqualTo(Integer value) {
            addCriterion("headcount =", value, "headcount");
            return (Criteria) this;
        }

        public Criteria andHeadcountNotEqualTo(Integer value) {
            addCriterion("headcount <>", value, "headcount");
            return (Criteria) this;
        }

        public Criteria andHeadcountGreaterThan(Integer value) {
            addCriterion("headcount >", value, "headcount");
            return (Criteria) this;
        }

        public Criteria andHeadcountGreaterThanOrEqualTo(Integer value) {
            addCriterion("headcount >=", value, "headcount");
            return (Criteria) this;
        }

        public Criteria andHeadcountLessThan(Integer value) {
            addCriterion("headcount <", value, "headcount");
            return (Criteria) this;
        }

        public Criteria andHeadcountLessThanOrEqualTo(Integer value) {
            addCriterion("headcount <=", value, "headcount");
            return (Criteria) this;
        }

        public Criteria andHeadcountIn(List<Integer> values) {
            addCriterion("headcount in", values, "headcount");
            return (Criteria) this;
        }

        public Criteria andHeadcountNotIn(List<Integer> values) {
            addCriterion("headcount not in", values, "headcount");
            return (Criteria) this;
        }

        public Criteria andHeadcountBetween(Integer value1, Integer value2) {
            addCriterion("headcount between", value1, value2, "headcount");
            return (Criteria) this;
        }

        public Criteria andHeadcountNotBetween(Integer value1, Integer value2) {
            addCriterion("headcount not between", value1, value2, "headcount");
            return (Criteria) this;
        }

        public Criteria andBasicpayIsNull() {
            addCriterion("basicpay is null");
            return (Criteria) this;
        }

        public Criteria andBasicpayIsNotNull() {
            addCriterion("basicpay is not null");
            return (Criteria) this;
        }

        public Criteria andBasicpayEqualTo(BigDecimal value) {
            addCriterion("basicpay =", value, "basicpay");
            return (Criteria) this;
        }

        public Criteria andBasicpayNotEqualTo(BigDecimal value) {
            addCriterion("basicpay <>", value, "basicpay");
            return (Criteria) this;
        }

        public Criteria andBasicpayGreaterThan(BigDecimal value) {
            addCriterion("basicpay >", value, "basicpay");
            return (Criteria) this;
        }

        public Criteria andBasicpayGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("basicpay >=", value, "basicpay");
            return (Criteria) this;
        }

        public Criteria andBasicpayLessThan(BigDecimal value) {
            addCriterion("basicpay <", value, "basicpay");
            return (Criteria) this;
        }

        public Criteria andBasicpayLessThanOrEqualTo(BigDecimal value) {
            addCriterion("basicpay <=", value, "basicpay");
            return (Criteria) this;
        }

        public Criteria andBasicpayIn(List<BigDecimal> values) {
            addCriterion("basicpay in", values, "basicpay");
            return (Criteria) this;
        }

        public Criteria andBasicpayNotIn(List<BigDecimal> values) {
            addCriterion("basicpay not in", values, "basicpay");
            return (Criteria) this;
        }

        public Criteria andBasicpayBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("basicpay between", value1, value2, "basicpay");
            return (Criteria) this;
        }

        public Criteria andBasicpayNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("basicpay not between", value1, value2, "basicpay");
            return (Criteria) this;
        }

        public Criteria andNetpayIsNull() {
            addCriterion("netpay is null");
            return (Criteria) this;
        }

        public Criteria andNetpayIsNotNull() {
            addCriterion("netpay is not null");
            return (Criteria) this;
        }

        public Criteria andNetpayEqualTo(BigDecimal value) {
            addCriterion("netpay =", value, "netpay");
            return (Criteria) this;
        }

        public Criteria andNetpayNotEqualTo(BigDecimal value) {
            addCriterion("netpay <>", value, "netpay");
            return (Criteria) this;
        }

        public Criteria andNetpayGreaterThan(BigDecimal value) {
            addCriterion("netpay >", value, "netpay");
            return (Criteria) this;
        }

        public Criteria andNetpayGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("netpay >=", value, "netpay");
            return (Criteria) this;
        }

        public Criteria andNetpayLessThan(BigDecimal value) {
            addCriterion("netpay <", value, "netpay");
            return (Criteria) this;
        }

        public Criteria andNetpayLessThanOrEqualTo(BigDecimal value) {
            addCriterion("netpay <=", value, "netpay");
            return (Criteria) this;
        }

        public Criteria andNetpayIn(List<BigDecimal> values) {
            addCriterion("netpay in", values, "netpay");
            return (Criteria) this;
        }

        public Criteria andNetpayNotIn(List<BigDecimal> values) {
            addCriterion("netpay not in", values, "netpay");
            return (Criteria) this;
        }

        public Criteria andNetpayBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("netpay between", value1, value2, "netpay");
            return (Criteria) this;
        }

        public Criteria andNetpayNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("netpay not between", value1, value2, "netpay");
            return (Criteria) this;
        }

        public Criteria andGranttimeIsNull() {
            addCriterion("granttime is null");
            return (Criteria) this;
        }

        public Criteria andGranttimeIsNotNull() {
            addCriterion("granttime is not null");
            return (Criteria) this;
        }

        public Criteria andGranttimeEqualTo(Date value) {
            addCriterionForJDBCDate("granttime =", value, "granttime");
            return (Criteria) this;
        }

        public Criteria andGranttimeNotEqualTo(Date value) {
            addCriterionForJDBCDate("granttime <>", value, "granttime");
            return (Criteria) this;
        }

        public Criteria andGranttimeGreaterThan(Date value) {
            addCriterionForJDBCDate("granttime >", value, "granttime");
            return (Criteria) this;
        }

        public Criteria andGranttimeGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("granttime >=", value, "granttime");
            return (Criteria) this;
        }

        public Criteria andGranttimeLessThan(Date value) {
            addCriterionForJDBCDate("granttime <", value, "granttime");
            return (Criteria) this;
        }

        public Criteria andGranttimeLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("granttime <=", value, "granttime");
            return (Criteria) this;
        }

        public Criteria andGranttimeIn(List<Date> values) {
            addCriterionForJDBCDate("granttime in", values, "granttime");
            return (Criteria) this;
        }

        public Criteria andGranttimeNotIn(List<Date> values) {
            addCriterionForJDBCDate("granttime not in", values, "granttime");
            return (Criteria) this;
        }

        public Criteria andGranttimeBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("granttime between", value1, value2, "granttime");
            return (Criteria) this;
        }

        public Criteria andGranttimeNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("granttime not between", value1, value2, "granttime");
            return (Criteria) this;
        }

        public Criteria andCreationByIsNull() {
            addCriterion("creationBy is null");
            return (Criteria) this;
        }

        public Criteria andCreationByIsNotNull() {
            addCriterion("creationBy is not null");
            return (Criteria) this;
        }

        public Criteria andCreationByEqualTo(Integer value) {
            addCriterion("creationBy =", value, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByNotEqualTo(Integer value) {
            addCriterion("creationBy <>", value, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByGreaterThan(Integer value) {
            addCriterion("creationBy >", value, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByGreaterThanOrEqualTo(Integer value) {
            addCriterion("creationBy >=", value, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByLessThan(Integer value) {
            addCriterion("creationBy <", value, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByLessThanOrEqualTo(Integer value) {
            addCriterion("creationBy <=", value, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByIn(List<Integer> values) {
            addCriterion("creationBy in", values, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByNotIn(List<Integer> values) {
            addCriterion("creationBy not in", values, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByBetween(Integer value1, Integer value2) {
            addCriterion("creationBy between", value1, value2, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByNotBetween(Integer value1, Integer value2) {
            addCriterion("creationBy not between", value1, value2, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationDateIsNull() {
            addCriterion("creationDate is null");
            return (Criteria) this;
        }

        public Criteria andCreationDateIsNotNull() {
            addCriterion("creationDate is not null");
            return (Criteria) this;
        }

        public Criteria andCreationDateEqualTo(Date value) {
            addCriterionForJDBCDate("creationDate =", value, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("creationDate <>", value, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateGreaterThan(Date value) {
            addCriterionForJDBCDate("creationDate >", value, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("creationDate >=", value, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateLessThan(Date value) {
            addCriterionForJDBCDate("creationDate <", value, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("creationDate <=", value, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateIn(List<Date> values) {
            addCriterionForJDBCDate("creationDate in", values, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("creationDate not in", values, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("creationDate between", value1, value2, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("creationDate not between", value1, value2, "creationDate");
            return (Criteria) this;
        }

        public Criteria andModifyByIsNull() {
            addCriterion("modifyBy is null");
            return (Criteria) this;
        }

        public Criteria andModifyByIsNotNull() {
            addCriterion("modifyBy is not null");
            return (Criteria) this;
        }

        public Criteria andModifyByEqualTo(Integer value) {
            addCriterion("modifyBy =", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByNotEqualTo(Integer value) {
            addCriterion("modifyBy <>", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByGreaterThan(Integer value) {
            addCriterion("modifyBy >", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByGreaterThanOrEqualTo(Integer value) {
            addCriterion("modifyBy >=", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByLessThan(Integer value) {
            addCriterion("modifyBy <", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByLessThanOrEqualTo(Integer value) {
            addCriterion("modifyBy <=", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByIn(List<Integer> values) {
            addCriterion("modifyBy in", values, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByNotIn(List<Integer> values) {
            addCriterion("modifyBy not in", values, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByBetween(Integer value1, Integer value2) {
            addCriterion("modifyBy between", value1, value2, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByNotBetween(Integer value1, Integer value2) {
            addCriterion("modifyBy not between", value1, value2, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyDateIsNull() {
            addCriterion("modifyDate is null");
            return (Criteria) this;
        }

        public Criteria andModifyDateIsNotNull() {
            addCriterion("modifyDate is not null");
            return (Criteria) this;
        }

        public Criteria andModifyDateEqualTo(Date value) {
            addCriterionForJDBCDate("modifyDate =", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("modifyDate <>", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateGreaterThan(Date value) {
            addCriterionForJDBCDate("modifyDate >", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("modifyDate >=", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateLessThan(Date value) {
            addCriterionForJDBCDate("modifyDate <", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("modifyDate <=", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateIn(List<Date> values) {
            addCriterionForJDBCDate("modifyDate in", values, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("modifyDate not in", values, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("modifyDate between", value1, value2, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("modifyDate not between", value1, value2, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}