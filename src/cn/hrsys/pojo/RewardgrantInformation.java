package cn.hrsys.pojo;

import java.math.BigDecimal;
import java.util.Date;

public class RewardgrantInformation {
    private Integer id;

    private String rewardbillCode;

    private Integer departmentId;

    private Integer positionId;

    private Integer headcount;

    private BigDecimal basicpay;

    private BigDecimal netpay;

    private Date granttime;

    private Integer creationBy;

    private Date creationDate;

    private Integer modifyBy;

    private Date modifyDate;

    private Integer status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRewardbillCode() {
        return rewardbillCode;
    }

    public void setRewardbillCode(String rewardbillCode) {
        this.rewardbillCode = rewardbillCode == null ? null : rewardbillCode.trim();
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    public Integer getPositionId() {
        return positionId;
    }

    public void setPositionId(Integer positionId) {
        this.positionId = positionId;
    }

    public Integer getHeadcount() {
        return headcount;
    }

    public void setHeadcount(Integer headcount) {
        this.headcount = headcount;
    }

    public BigDecimal getBasicpay() {
        return basicpay;
    }

    public void setBasicpay(BigDecimal basicpay) {
        this.basicpay = basicpay;
    }

    public BigDecimal getNetpay() {
        return netpay;
    }

    public void setNetpay(BigDecimal netpay) {
        this.netpay = netpay;
    }

    public Date getGranttime() {
        return granttime;
    }

    public void setGranttime(Date granttime) {
        this.granttime = granttime;
    }

    public Integer getCreationBy() {
        return creationBy;
    }

    public void setCreationBy(Integer creationBy) {
        this.creationBy = creationBy;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Integer getModifyBy() {
        return modifyBy;
    }

    public void setModifyBy(Integer modifyBy) {
        this.modifyBy = modifyBy;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}