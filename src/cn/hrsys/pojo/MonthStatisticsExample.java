package cn.hrsys.pojo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class MonthStatisticsExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public MonthStatisticsExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andTardinessNumberIsNull() {
            addCriterion("tardinessNumber is null");
            return (Criteria) this;
        }

        public Criteria andTardinessNumberIsNotNull() {
            addCriterion("tardinessNumber is not null");
            return (Criteria) this;
        }

        public Criteria andTardinessNumberEqualTo(Integer value) {
            addCriterion("tardinessNumber =", value, "tardinessNumber");
            return (Criteria) this;
        }

        public Criteria andTardinessNumberNotEqualTo(Integer value) {
            addCriterion("tardinessNumber <>", value, "tardinessNumber");
            return (Criteria) this;
        }

        public Criteria andTardinessNumberGreaterThan(Integer value) {
            addCriterion("tardinessNumber >", value, "tardinessNumber");
            return (Criteria) this;
        }

        public Criteria andTardinessNumberGreaterThanOrEqualTo(Integer value) {
            addCriterion("tardinessNumber >=", value, "tardinessNumber");
            return (Criteria) this;
        }

        public Criteria andTardinessNumberLessThan(Integer value) {
            addCriterion("tardinessNumber <", value, "tardinessNumber");
            return (Criteria) this;
        }

        public Criteria andTardinessNumberLessThanOrEqualTo(Integer value) {
            addCriterion("tardinessNumber <=", value, "tardinessNumber");
            return (Criteria) this;
        }

        public Criteria andTardinessNumberIn(List<Integer> values) {
            addCriterion("tardinessNumber in", values, "tardinessNumber");
            return (Criteria) this;
        }

        public Criteria andTardinessNumberNotIn(List<Integer> values) {
            addCriterion("tardinessNumber not in", values, "tardinessNumber");
            return (Criteria) this;
        }

        public Criteria andTardinessNumberBetween(Integer value1, Integer value2) {
            addCriterion("tardinessNumber between", value1, value2, "tardinessNumber");
            return (Criteria) this;
        }

        public Criteria andTardinessNumberNotBetween(Integer value1, Integer value2) {
            addCriterion("tardinessNumber not between", value1, value2, "tardinessNumber");
            return (Criteria) this;
        }

        public Criteria andTardinessDeductmoneyIsNull() {
            addCriterion("tardinessDeductmoney is null");
            return (Criteria) this;
        }

        public Criteria andTardinessDeductmoneyIsNotNull() {
            addCriterion("tardinessDeductmoney is not null");
            return (Criteria) this;
        }

        public Criteria andTardinessDeductmoneyEqualTo(BigDecimal value) {
            addCriterion("tardinessDeductmoney =", value, "tardinessDeductmoney");
            return (Criteria) this;
        }

        public Criteria andTardinessDeductmoneyNotEqualTo(BigDecimal value) {
            addCriterion("tardinessDeductmoney <>", value, "tardinessDeductmoney");
            return (Criteria) this;
        }

        public Criteria andTardinessDeductmoneyGreaterThan(BigDecimal value) {
            addCriterion("tardinessDeductmoney >", value, "tardinessDeductmoney");
            return (Criteria) this;
        }

        public Criteria andTardinessDeductmoneyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("tardinessDeductmoney >=", value, "tardinessDeductmoney");
            return (Criteria) this;
        }

        public Criteria andTardinessDeductmoneyLessThan(BigDecimal value) {
            addCriterion("tardinessDeductmoney <", value, "tardinessDeductmoney");
            return (Criteria) this;
        }

        public Criteria andTardinessDeductmoneyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("tardinessDeductmoney <=", value, "tardinessDeductmoney");
            return (Criteria) this;
        }

        public Criteria andTardinessDeductmoneyIn(List<BigDecimal> values) {
            addCriterion("tardinessDeductmoney in", values, "tardinessDeductmoney");
            return (Criteria) this;
        }

        public Criteria andTardinessDeductmoneyNotIn(List<BigDecimal> values) {
            addCriterion("tardinessDeductmoney not in", values, "tardinessDeductmoney");
            return (Criteria) this;
        }

        public Criteria andTardinessDeductmoneyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("tardinessDeductmoney between", value1, value2, "tardinessDeductmoney");
            return (Criteria) this;
        }

        public Criteria andTardinessDeductmoneyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("tardinessDeductmoney not between", value1, value2, "tardinessDeductmoney");
            return (Criteria) this;
        }

        public Criteria andLeaveDaysIsNull() {
            addCriterion("leaveDays is null");
            return (Criteria) this;
        }

        public Criteria andLeaveDaysIsNotNull() {
            addCriterion("leaveDays is not null");
            return (Criteria) this;
        }

        public Criteria andLeaveDaysEqualTo(Integer value) {
            addCriterion("leaveDays =", value, "leaveDays");
            return (Criteria) this;
        }

        public Criteria andLeaveDaysNotEqualTo(Integer value) {
            addCriterion("leaveDays <>", value, "leaveDays");
            return (Criteria) this;
        }

        public Criteria andLeaveDaysGreaterThan(Integer value) {
            addCriterion("leaveDays >", value, "leaveDays");
            return (Criteria) this;
        }

        public Criteria andLeaveDaysGreaterThanOrEqualTo(Integer value) {
            addCriterion("leaveDays >=", value, "leaveDays");
            return (Criteria) this;
        }

        public Criteria andLeaveDaysLessThan(Integer value) {
            addCriterion("leaveDays <", value, "leaveDays");
            return (Criteria) this;
        }

        public Criteria andLeaveDaysLessThanOrEqualTo(Integer value) {
            addCriterion("leaveDays <=", value, "leaveDays");
            return (Criteria) this;
        }

        public Criteria andLeaveDaysIn(List<Integer> values) {
            addCriterion("leaveDays in", values, "leaveDays");
            return (Criteria) this;
        }

        public Criteria andLeaveDaysNotIn(List<Integer> values) {
            addCriterion("leaveDays not in", values, "leaveDays");
            return (Criteria) this;
        }

        public Criteria andLeaveDaysBetween(Integer value1, Integer value2) {
            addCriterion("leaveDays between", value1, value2, "leaveDays");
            return (Criteria) this;
        }

        public Criteria andLeaveDaysNotBetween(Integer value1, Integer value2) {
            addCriterion("leaveDays not between", value1, value2, "leaveDays");
            return (Criteria) this;
        }

        public Criteria andLeaveDeductmoneyIsNull() {
            addCriterion("leaveDeductmoney is null");
            return (Criteria) this;
        }

        public Criteria andLeaveDeductmoneyIsNotNull() {
            addCriterion("leaveDeductmoney is not null");
            return (Criteria) this;
        }

        public Criteria andLeaveDeductmoneyEqualTo(BigDecimal value) {
            addCriterion("leaveDeductmoney =", value, "leaveDeductmoney");
            return (Criteria) this;
        }

        public Criteria andLeaveDeductmoneyNotEqualTo(BigDecimal value) {
            addCriterion("leaveDeductmoney <>", value, "leaveDeductmoney");
            return (Criteria) this;
        }

        public Criteria andLeaveDeductmoneyGreaterThan(BigDecimal value) {
            addCriterion("leaveDeductmoney >", value, "leaveDeductmoney");
            return (Criteria) this;
        }

        public Criteria andLeaveDeductmoneyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("leaveDeductmoney >=", value, "leaveDeductmoney");
            return (Criteria) this;
        }

        public Criteria andLeaveDeductmoneyLessThan(BigDecimal value) {
            addCriterion("leaveDeductmoney <", value, "leaveDeductmoney");
            return (Criteria) this;
        }

        public Criteria andLeaveDeductmoneyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("leaveDeductmoney <=", value, "leaveDeductmoney");
            return (Criteria) this;
        }

        public Criteria andLeaveDeductmoneyIn(List<BigDecimal> values) {
            addCriterion("leaveDeductmoney in", values, "leaveDeductmoney");
            return (Criteria) this;
        }

        public Criteria andLeaveDeductmoneyNotIn(List<BigDecimal> values) {
            addCriterion("leaveDeductmoney not in", values, "leaveDeductmoney");
            return (Criteria) this;
        }

        public Criteria andLeaveDeductmoneyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("leaveDeductmoney between", value1, value2, "leaveDeductmoney");
            return (Criteria) this;
        }

        public Criteria andLeaveDeductmoneyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("leaveDeductmoney not between", value1, value2, "leaveDeductmoney");
            return (Criteria) this;
        }

        public Criteria andOvertimeNumberIsNull() {
            addCriterion("overtimeNumber is null");
            return (Criteria) this;
        }

        public Criteria andOvertimeNumberIsNotNull() {
            addCriterion("overtimeNumber is not null");
            return (Criteria) this;
        }

        public Criteria andOvertimeNumberEqualTo(Integer value) {
            addCriterion("overtimeNumber =", value, "overtimeNumber");
            return (Criteria) this;
        }

        public Criteria andOvertimeNumberNotEqualTo(Integer value) {
            addCriterion("overtimeNumber <>", value, "overtimeNumber");
            return (Criteria) this;
        }

        public Criteria andOvertimeNumberGreaterThan(Integer value) {
            addCriterion("overtimeNumber >", value, "overtimeNumber");
            return (Criteria) this;
        }

        public Criteria andOvertimeNumberGreaterThanOrEqualTo(Integer value) {
            addCriterion("overtimeNumber >=", value, "overtimeNumber");
            return (Criteria) this;
        }

        public Criteria andOvertimeNumberLessThan(Integer value) {
            addCriterion("overtimeNumber <", value, "overtimeNumber");
            return (Criteria) this;
        }

        public Criteria andOvertimeNumberLessThanOrEqualTo(Integer value) {
            addCriterion("overtimeNumber <=", value, "overtimeNumber");
            return (Criteria) this;
        }

        public Criteria andOvertimeNumberIn(List<Integer> values) {
            addCriterion("overtimeNumber in", values, "overtimeNumber");
            return (Criteria) this;
        }

        public Criteria andOvertimeNumberNotIn(List<Integer> values) {
            addCriterion("overtimeNumber not in", values, "overtimeNumber");
            return (Criteria) this;
        }

        public Criteria andOvertimeNumberBetween(Integer value1, Integer value2) {
            addCriterion("overtimeNumber between", value1, value2, "overtimeNumber");
            return (Criteria) this;
        }

        public Criteria andOvertimeNumberNotBetween(Integer value1, Integer value2) {
            addCriterion("overtimeNumber not between", value1, value2, "overtimeNumber");
            return (Criteria) this;
        }

        public Criteria andOvertimeAllowanceIsNull() {
            addCriterion("overtimeAllowance is null");
            return (Criteria) this;
        }

        public Criteria andOvertimeAllowanceIsNotNull() {
            addCriterion("overtimeAllowance is not null");
            return (Criteria) this;
        }

        public Criteria andOvertimeAllowanceEqualTo(BigDecimal value) {
            addCriterion("overtimeAllowance =", value, "overtimeAllowance");
            return (Criteria) this;
        }

        public Criteria andOvertimeAllowanceNotEqualTo(BigDecimal value) {
            addCriterion("overtimeAllowance <>", value, "overtimeAllowance");
            return (Criteria) this;
        }

        public Criteria andOvertimeAllowanceGreaterThan(BigDecimal value) {
            addCriterion("overtimeAllowance >", value, "overtimeAllowance");
            return (Criteria) this;
        }

        public Criteria andOvertimeAllowanceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("overtimeAllowance >=", value, "overtimeAllowance");
            return (Criteria) this;
        }

        public Criteria andOvertimeAllowanceLessThan(BigDecimal value) {
            addCriterion("overtimeAllowance <", value, "overtimeAllowance");
            return (Criteria) this;
        }

        public Criteria andOvertimeAllowanceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("overtimeAllowance <=", value, "overtimeAllowance");
            return (Criteria) this;
        }

        public Criteria andOvertimeAllowanceIn(List<BigDecimal> values) {
            addCriterion("overtimeAllowance in", values, "overtimeAllowance");
            return (Criteria) this;
        }

        public Criteria andOvertimeAllowanceNotIn(List<BigDecimal> values) {
            addCriterion("overtimeAllowance not in", values, "overtimeAllowance");
            return (Criteria) this;
        }

        public Criteria andOvertimeAllowanceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("overtimeAllowance between", value1, value2, "overtimeAllowance");
            return (Criteria) this;
        }

        public Criteria andOvertimeAllowanceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("overtimeAllowance not between", value1, value2, "overtimeAllowance");
            return (Criteria) this;
        }

        public Criteria andEvectionDaysIsNull() {
            addCriterion("evectionDays is null");
            return (Criteria) this;
        }

        public Criteria andEvectionDaysIsNotNull() {
            addCriterion("evectionDays is not null");
            return (Criteria) this;
        }

        public Criteria andEvectionDaysEqualTo(Integer value) {
            addCriterion("evectionDays =", value, "evectionDays");
            return (Criteria) this;
        }

        public Criteria andEvectionDaysNotEqualTo(Integer value) {
            addCriterion("evectionDays <>", value, "evectionDays");
            return (Criteria) this;
        }

        public Criteria andEvectionDaysGreaterThan(Integer value) {
            addCriterion("evectionDays >", value, "evectionDays");
            return (Criteria) this;
        }

        public Criteria andEvectionDaysGreaterThanOrEqualTo(Integer value) {
            addCriterion("evectionDays >=", value, "evectionDays");
            return (Criteria) this;
        }

        public Criteria andEvectionDaysLessThan(Integer value) {
            addCriterion("evectionDays <", value, "evectionDays");
            return (Criteria) this;
        }

        public Criteria andEvectionDaysLessThanOrEqualTo(Integer value) {
            addCriterion("evectionDays <=", value, "evectionDays");
            return (Criteria) this;
        }

        public Criteria andEvectionDaysIn(List<Integer> values) {
            addCriterion("evectionDays in", values, "evectionDays");
            return (Criteria) this;
        }

        public Criteria andEvectionDaysNotIn(List<Integer> values) {
            addCriterion("evectionDays not in", values, "evectionDays");
            return (Criteria) this;
        }

        public Criteria andEvectionDaysBetween(Integer value1, Integer value2) {
            addCriterion("evectionDays between", value1, value2, "evectionDays");
            return (Criteria) this;
        }

        public Criteria andEvectionDaysNotBetween(Integer value1, Integer value2) {
            addCriterion("evectionDays not between", value1, value2, "evectionDays");
            return (Criteria) this;
        }

        public Criteria andEvectionAllowanceIsNull() {
            addCriterion("evectionAllowance is null");
            return (Criteria) this;
        }

        public Criteria andEvectionAllowanceIsNotNull() {
            addCriterion("evectionAllowance is not null");
            return (Criteria) this;
        }

        public Criteria andEvectionAllowanceEqualTo(BigDecimal value) {
            addCriterion("evectionAllowance =", value, "evectionAllowance");
            return (Criteria) this;
        }

        public Criteria andEvectionAllowanceNotEqualTo(BigDecimal value) {
            addCriterion("evectionAllowance <>", value, "evectionAllowance");
            return (Criteria) this;
        }

        public Criteria andEvectionAllowanceGreaterThan(BigDecimal value) {
            addCriterion("evectionAllowance >", value, "evectionAllowance");
            return (Criteria) this;
        }

        public Criteria andEvectionAllowanceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("evectionAllowance >=", value, "evectionAllowance");
            return (Criteria) this;
        }

        public Criteria andEvectionAllowanceLessThan(BigDecimal value) {
            addCriterion("evectionAllowance <", value, "evectionAllowance");
            return (Criteria) this;
        }

        public Criteria andEvectionAllowanceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("evectionAllowance <=", value, "evectionAllowance");
            return (Criteria) this;
        }

        public Criteria andEvectionAllowanceIn(List<BigDecimal> values) {
            addCriterion("evectionAllowance in", values, "evectionAllowance");
            return (Criteria) this;
        }

        public Criteria andEvectionAllowanceNotIn(List<BigDecimal> values) {
            addCriterion("evectionAllowance not in", values, "evectionAllowance");
            return (Criteria) this;
        }

        public Criteria andEvectionAllowanceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("evectionAllowance between", value1, value2, "evectionAllowance");
            return (Criteria) this;
        }

        public Criteria andEvectionAllowanceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("evectionAllowance not between", value1, value2, "evectionAllowance");
            return (Criteria) this;
        }

        public Criteria andMonthtimeNumberIsNull() {
            addCriterion("monthtimeNumber is null");
            return (Criteria) this;
        }

        public Criteria andMonthtimeNumberIsNotNull() {
            addCriterion("monthtimeNumber is not null");
            return (Criteria) this;
        }

        public Criteria andMonthtimeNumberEqualTo(Integer value) {
            addCriterion("monthtimeNumber =", value, "monthtimeNumber");
            return (Criteria) this;
        }

        public Criteria andMonthtimeNumberNotEqualTo(Integer value) {
            addCriterion("monthtimeNumber <>", value, "monthtimeNumber");
            return (Criteria) this;
        }

        public Criteria andMonthtimeNumberGreaterThan(Integer value) {
            addCriterion("monthtimeNumber >", value, "monthtimeNumber");
            return (Criteria) this;
        }

        public Criteria andMonthtimeNumberGreaterThanOrEqualTo(Integer value) {
            addCriterion("monthtimeNumber >=", value, "monthtimeNumber");
            return (Criteria) this;
        }

        public Criteria andMonthtimeNumberLessThan(Integer value) {
            addCriterion("monthtimeNumber <", value, "monthtimeNumber");
            return (Criteria) this;
        }

        public Criteria andMonthtimeNumberLessThanOrEqualTo(Integer value) {
            addCriterion("monthtimeNumber <=", value, "monthtimeNumber");
            return (Criteria) this;
        }

        public Criteria andMonthtimeNumberIn(List<Integer> values) {
            addCriterion("monthtimeNumber in", values, "monthtimeNumber");
            return (Criteria) this;
        }

        public Criteria andMonthtimeNumberNotIn(List<Integer> values) {
            addCriterion("monthtimeNumber not in", values, "monthtimeNumber");
            return (Criteria) this;
        }

        public Criteria andMonthtimeNumberBetween(Integer value1, Integer value2) {
            addCriterion("monthtimeNumber between", value1, value2, "monthtimeNumber");
            return (Criteria) this;
        }

        public Criteria andMonthtimeNumberNotBetween(Integer value1, Integer value2) {
            addCriterion("monthtimeNumber not between", value1, value2, "monthtimeNumber");
            return (Criteria) this;
        }

        public Criteria andPersonIdIsNull() {
            addCriterion("personId is null");
            return (Criteria) this;
        }

        public Criteria andPersonIdIsNotNull() {
            addCriterion("personId is not null");
            return (Criteria) this;
        }

        public Criteria andPersonIdEqualTo(Integer value) {
            addCriterion("personId =", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdNotEqualTo(Integer value) {
            addCriterion("personId <>", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdGreaterThan(Integer value) {
            addCriterion("personId >", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("personId >=", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdLessThan(Integer value) {
            addCriterion("personId <", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdLessThanOrEqualTo(Integer value) {
            addCriterion("personId <=", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdIn(List<Integer> values) {
            addCriterion("personId in", values, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdNotIn(List<Integer> values) {
            addCriterion("personId not in", values, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdBetween(Integer value1, Integer value2) {
            addCriterion("personId between", value1, value2, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdNotBetween(Integer value1, Integer value2) {
            addCriterion("personId not between", value1, value2, "personId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}