package cn.hrsys.pojo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class PersonnelfilesInformationExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PersonnelfilesInformationExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andPersonnelCodeIsNull() {
            addCriterion("personnelCode is null");
            return (Criteria) this;
        }

        public Criteria andPersonnelCodeIsNotNull() {
            addCriterion("personnelCode is not null");
            return (Criteria) this;
        }

        public Criteria andPersonnelCodeEqualTo(String value) {
            addCriterion("personnelCode =", value, "personnelCode");
            return (Criteria) this;
        }

        public Criteria andPersonnelCodeNotEqualTo(String value) {
            addCriterion("personnelCode <>", value, "personnelCode");
            return (Criteria) this;
        }

        public Criteria andPersonnelCodeGreaterThan(String value) {
            addCriterion("personnelCode >", value, "personnelCode");
            return (Criteria) this;
        }

        public Criteria andPersonnelCodeGreaterThanOrEqualTo(String value) {
            addCriterion("personnelCode >=", value, "personnelCode");
            return (Criteria) this;
        }

        public Criteria andPersonnelCodeLessThan(String value) {
            addCriterion("personnelCode <", value, "personnelCode");
            return (Criteria) this;
        }

        public Criteria andPersonnelCodeLessThanOrEqualTo(String value) {
            addCriterion("personnelCode <=", value, "personnelCode");
            return (Criteria) this;
        }

        public Criteria andPersonnelCodeLike(String value) {
            addCriterion("personnelCode like", value, "personnelCode");
            return (Criteria) this;
        }

        public Criteria andPersonnelCodeNotLike(String value) {
            addCriterion("personnelCode not like", value, "personnelCode");
            return (Criteria) this;
        }

        public Criteria andPersonnelCodeIn(List<String> values) {
            addCriterion("personnelCode in", values, "personnelCode");
            return (Criteria) this;
        }

        public Criteria andPersonnelCodeNotIn(List<String> values) {
            addCriterion("personnelCode not in", values, "personnelCode");
            return (Criteria) this;
        }

        public Criteria andPersonnelCodeBetween(String value1, String value2) {
            addCriterion("personnelCode between", value1, value2, "personnelCode");
            return (Criteria) this;
        }

        public Criteria andPersonnelCodeNotBetween(String value1, String value2) {
            addCriterion("personnelCode not between", value1, value2, "personnelCode");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("name not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andSexIsNull() {
            addCriterion("sex is null");
            return (Criteria) this;
        }

        public Criteria andSexIsNotNull() {
            addCriterion("sex is not null");
            return (Criteria) this;
        }

        public Criteria andSexEqualTo(Integer value) {
            addCriterion("sex =", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexNotEqualTo(Integer value) {
            addCriterion("sex <>", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexGreaterThan(Integer value) {
            addCriterion("sex >", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexGreaterThanOrEqualTo(Integer value) {
            addCriterion("sex >=", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexLessThan(Integer value) {
            addCriterion("sex <", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexLessThanOrEqualTo(Integer value) {
            addCriterion("sex <=", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexIn(List<Integer> values) {
            addCriterion("sex in", values, "sex");
            return (Criteria) this;
        }

        public Criteria andSexNotIn(List<Integer> values) {
            addCriterion("sex not in", values, "sex");
            return (Criteria) this;
        }

        public Criteria andSexBetween(Integer value1, Integer value2) {
            addCriterion("sex between", value1, value2, "sex");
            return (Criteria) this;
        }

        public Criteria andSexNotBetween(Integer value1, Integer value2) {
            addCriterion("sex not between", value1, value2, "sex");
            return (Criteria) this;
        }

        public Criteria andEmailIsNull() {
            addCriterion("email is null");
            return (Criteria) this;
        }

        public Criteria andEmailIsNotNull() {
            addCriterion("email is not null");
            return (Criteria) this;
        }

        public Criteria andEmailEqualTo(String value) {
            addCriterion("email =", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotEqualTo(String value) {
            addCriterion("email <>", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailGreaterThan(String value) {
            addCriterion("email >", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailGreaterThanOrEqualTo(String value) {
            addCriterion("email >=", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLessThan(String value) {
            addCriterion("email <", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLessThanOrEqualTo(String value) {
            addCriterion("email <=", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLike(String value) {
            addCriterion("email like", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotLike(String value) {
            addCriterion("email not like", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailIn(List<String> values) {
            addCriterion("email in", values, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotIn(List<String> values) {
            addCriterion("email not in", values, "email");
            return (Criteria) this;
        }

        public Criteria andEmailBetween(String value1, String value2) {
            addCriterion("email between", value1, value2, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotBetween(String value1, String value2) {
            addCriterion("email not between", value1, value2, "email");
            return (Criteria) this;
        }

        public Criteria andPhoneIsNull() {
            addCriterion("phone is null");
            return (Criteria) this;
        }

        public Criteria andPhoneIsNotNull() {
            addCriterion("phone is not null");
            return (Criteria) this;
        }

        public Criteria andPhoneEqualTo(String value) {
            addCriterion("phone =", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotEqualTo(String value) {
            addCriterion("phone <>", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneGreaterThan(String value) {
            addCriterion("phone >", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneGreaterThanOrEqualTo(String value) {
            addCriterion("phone >=", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLessThan(String value) {
            addCriterion("phone <", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLessThanOrEqualTo(String value) {
            addCriterion("phone <=", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLike(String value) {
            addCriterion("phone like", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotLike(String value) {
            addCriterion("phone not like", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneIn(List<String> values) {
            addCriterion("phone in", values, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotIn(List<String> values) {
            addCriterion("phone not in", values, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneBetween(String value1, String value2) {
            addCriterion("phone between", value1, value2, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotBetween(String value1, String value2) {
            addCriterion("phone not between", value1, value2, "phone");
            return (Criteria) this;
        }

        public Criteria andAddressIsNull() {
            addCriterion("address is null");
            return (Criteria) this;
        }

        public Criteria andAddressIsNotNull() {
            addCriterion("address is not null");
            return (Criteria) this;
        }

        public Criteria andAddressEqualTo(String value) {
            addCriterion("address =", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotEqualTo(String value) {
            addCriterion("address <>", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressGreaterThan(String value) {
            addCriterion("address >", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressGreaterThanOrEqualTo(String value) {
            addCriterion("address >=", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLessThan(String value) {
            addCriterion("address <", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLessThanOrEqualTo(String value) {
            addCriterion("address <=", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLike(String value) {
            addCriterion("address like", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotLike(String value) {
            addCriterion("address not like", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressIn(List<String> values) {
            addCriterion("address in", values, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotIn(List<String> values) {
            addCriterion("address not in", values, "address");
            return (Criteria) this;
        }

        public Criteria andAddressBetween(String value1, String value2) {
            addCriterion("address between", value1, value2, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotBetween(String value1, String value2) {
            addCriterion("address not between", value1, value2, "address");
            return (Criteria) this;
        }

        public Criteria andIdNumberIsNull() {
            addCriterion("idNumber is null");
            return (Criteria) this;
        }

        public Criteria andIdNumberIsNotNull() {
            addCriterion("idNumber is not null");
            return (Criteria) this;
        }

        public Criteria andIdNumberEqualTo(String value) {
            addCriterion("idNumber =", value, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIdNumberNotEqualTo(String value) {
            addCriterion("idNumber <>", value, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIdNumberGreaterThan(String value) {
            addCriterion("idNumber >", value, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIdNumberGreaterThanOrEqualTo(String value) {
            addCriterion("idNumber >=", value, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIdNumberLessThan(String value) {
            addCriterion("idNumber <", value, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIdNumberLessThanOrEqualTo(String value) {
            addCriterion("idNumber <=", value, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIdNumberLike(String value) {
            addCriterion("idNumber like", value, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIdNumberNotLike(String value) {
            addCriterion("idNumber not like", value, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIdNumberIn(List<String> values) {
            addCriterion("idNumber in", values, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIdNumberNotIn(List<String> values) {
            addCriterion("idNumber not in", values, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIdNumberBetween(String value1, String value2) {
            addCriterion("idNumber between", value1, value2, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIdNumberNotBetween(String value1, String value2) {
            addCriterion("idNumber not between", value1, value2, "idNumber");
            return (Criteria) this;
        }

        public Criteria andViaIsNull() {
            addCriterion("via is null");
            return (Criteria) this;
        }

        public Criteria andViaIsNotNull() {
            addCriterion("via is not null");
            return (Criteria) this;
        }

        public Criteria andViaEqualTo(String value) {
            addCriterion("via =", value, "via");
            return (Criteria) this;
        }

        public Criteria andViaNotEqualTo(String value) {
            addCriterion("via <>", value, "via");
            return (Criteria) this;
        }

        public Criteria andViaGreaterThan(String value) {
            addCriterion("via >", value, "via");
            return (Criteria) this;
        }

        public Criteria andViaGreaterThanOrEqualTo(String value) {
            addCriterion("via >=", value, "via");
            return (Criteria) this;
        }

        public Criteria andViaLessThan(String value) {
            addCriterion("via <", value, "via");
            return (Criteria) this;
        }

        public Criteria andViaLessThanOrEqualTo(String value) {
            addCriterion("via <=", value, "via");
            return (Criteria) this;
        }

        public Criteria andViaLike(String value) {
            addCriterion("via like", value, "via");
            return (Criteria) this;
        }

        public Criteria andViaNotLike(String value) {
            addCriterion("via not like", value, "via");
            return (Criteria) this;
        }

        public Criteria andViaIn(List<String> values) {
            addCriterion("via in", values, "via");
            return (Criteria) this;
        }

        public Criteria andViaNotIn(List<String> values) {
            addCriterion("via not in", values, "via");
            return (Criteria) this;
        }

        public Criteria andViaBetween(String value1, String value2) {
            addCriterion("via between", value1, value2, "via");
            return (Criteria) this;
        }

        public Criteria andViaNotBetween(String value1, String value2) {
            addCriterion("via not between", value1, value2, "via");
            return (Criteria) this;
        }

        public Criteria andRegisterIsNull() {
            addCriterion("register is null");
            return (Criteria) this;
        }

        public Criteria andRegisterIsNotNull() {
            addCriterion("register is not null");
            return (Criteria) this;
        }

        public Criteria andRegisterEqualTo(String value) {
            addCriterion("register =", value, "register");
            return (Criteria) this;
        }

        public Criteria andRegisterNotEqualTo(String value) {
            addCriterion("register <>", value, "register");
            return (Criteria) this;
        }

        public Criteria andRegisterGreaterThan(String value) {
            addCriterion("register >", value, "register");
            return (Criteria) this;
        }

        public Criteria andRegisterGreaterThanOrEqualTo(String value) {
            addCriterion("register >=", value, "register");
            return (Criteria) this;
        }

        public Criteria andRegisterLessThan(String value) {
            addCriterion("register <", value, "register");
            return (Criteria) this;
        }

        public Criteria andRegisterLessThanOrEqualTo(String value) {
            addCriterion("register <=", value, "register");
            return (Criteria) this;
        }

        public Criteria andRegisterLike(String value) {
            addCriterion("register like", value, "register");
            return (Criteria) this;
        }

        public Criteria andRegisterNotLike(String value) {
            addCriterion("register not like", value, "register");
            return (Criteria) this;
        }

        public Criteria andRegisterIn(List<String> values) {
            addCriterion("register in", values, "register");
            return (Criteria) this;
        }

        public Criteria andRegisterNotIn(List<String> values) {
            addCriterion("register not in", values, "register");
            return (Criteria) this;
        }

        public Criteria andRegisterBetween(String value1, String value2) {
            addCriterion("register between", value1, value2, "register");
            return (Criteria) this;
        }

        public Criteria andRegisterNotBetween(String value1, String value2) {
            addCriterion("register not between", value1, value2, "register");
            return (Criteria) this;
        }

        public Criteria andFaceIdIsNull() {
            addCriterion("faceId is null");
            return (Criteria) this;
        }

        public Criteria andFaceIdIsNotNull() {
            addCriterion("faceId is not null");
            return (Criteria) this;
        }

        public Criteria andFaceIdEqualTo(Integer value) {
            addCriterion("faceId =", value, "faceId");
            return (Criteria) this;
        }

        public Criteria andFaceIdNotEqualTo(Integer value) {
            addCriterion("faceId <>", value, "faceId");
            return (Criteria) this;
        }

        public Criteria andFaceIdGreaterThan(Integer value) {
            addCriterion("faceId >", value, "faceId");
            return (Criteria) this;
        }

        public Criteria andFaceIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("faceId >=", value, "faceId");
            return (Criteria) this;
        }

        public Criteria andFaceIdLessThan(Integer value) {
            addCriterion("faceId <", value, "faceId");
            return (Criteria) this;
        }

        public Criteria andFaceIdLessThanOrEqualTo(Integer value) {
            addCriterion("faceId <=", value, "faceId");
            return (Criteria) this;
        }

        public Criteria andFaceIdIn(List<Integer> values) {
            addCriterion("faceId in", values, "faceId");
            return (Criteria) this;
        }

        public Criteria andFaceIdNotIn(List<Integer> values) {
            addCriterion("faceId not in", values, "faceId");
            return (Criteria) this;
        }

        public Criteria andFaceIdBetween(Integer value1, Integer value2) {
            addCriterion("faceId between", value1, value2, "faceId");
            return (Criteria) this;
        }

        public Criteria andFaceIdNotBetween(Integer value1, Integer value2) {
            addCriterion("faceId not between", value1, value2, "faceId");
            return (Criteria) this;
        }

        public Criteria andSecurityNumIsNull() {
            addCriterion("securityNum is null");
            return (Criteria) this;
        }

        public Criteria andSecurityNumIsNotNull() {
            addCriterion("securityNum is not null");
            return (Criteria) this;
        }

        public Criteria andSecurityNumEqualTo(String value) {
            addCriterion("securityNum =", value, "securityNum");
            return (Criteria) this;
        }

        public Criteria andSecurityNumNotEqualTo(String value) {
            addCriterion("securityNum <>", value, "securityNum");
            return (Criteria) this;
        }

        public Criteria andSecurityNumGreaterThan(String value) {
            addCriterion("securityNum >", value, "securityNum");
            return (Criteria) this;
        }

        public Criteria andSecurityNumGreaterThanOrEqualTo(String value) {
            addCriterion("securityNum >=", value, "securityNum");
            return (Criteria) this;
        }

        public Criteria andSecurityNumLessThan(String value) {
            addCriterion("securityNum <", value, "securityNum");
            return (Criteria) this;
        }

        public Criteria andSecurityNumLessThanOrEqualTo(String value) {
            addCriterion("securityNum <=", value, "securityNum");
            return (Criteria) this;
        }

        public Criteria andSecurityNumLike(String value) {
            addCriterion("securityNum like", value, "securityNum");
            return (Criteria) this;
        }

        public Criteria andSecurityNumNotLike(String value) {
            addCriterion("securityNum not like", value, "securityNum");
            return (Criteria) this;
        }

        public Criteria andSecurityNumIn(List<String> values) {
            addCriterion("securityNum in", values, "securityNum");
            return (Criteria) this;
        }

        public Criteria andSecurityNumNotIn(List<String> values) {
            addCriterion("securityNum not in", values, "securityNum");
            return (Criteria) this;
        }

        public Criteria andSecurityNumBetween(String value1, String value2) {
            addCriterion("securityNum between", value1, value2, "securityNum");
            return (Criteria) this;
        }

        public Criteria andSecurityNumNotBetween(String value1, String value2) {
            addCriterion("securityNum not between", value1, value2, "securityNum");
            return (Criteria) this;
        }

        public Criteria andGraduatedFromIsNull() {
            addCriterion("graduatedFrom is null");
            return (Criteria) this;
        }

        public Criteria andGraduatedFromIsNotNull() {
            addCriterion("graduatedFrom is not null");
            return (Criteria) this;
        }

        public Criteria andGraduatedFromEqualTo(String value) {
            addCriterion("graduatedFrom =", value, "graduatedFrom");
            return (Criteria) this;
        }

        public Criteria andGraduatedFromNotEqualTo(String value) {
            addCriterion("graduatedFrom <>", value, "graduatedFrom");
            return (Criteria) this;
        }

        public Criteria andGraduatedFromGreaterThan(String value) {
            addCriterion("graduatedFrom >", value, "graduatedFrom");
            return (Criteria) this;
        }

        public Criteria andGraduatedFromGreaterThanOrEqualTo(String value) {
            addCriterion("graduatedFrom >=", value, "graduatedFrom");
            return (Criteria) this;
        }

        public Criteria andGraduatedFromLessThan(String value) {
            addCriterion("graduatedFrom <", value, "graduatedFrom");
            return (Criteria) this;
        }

        public Criteria andGraduatedFromLessThanOrEqualTo(String value) {
            addCriterion("graduatedFrom <=", value, "graduatedFrom");
            return (Criteria) this;
        }

        public Criteria andGraduatedFromLike(String value) {
            addCriterion("graduatedFrom like", value, "graduatedFrom");
            return (Criteria) this;
        }

        public Criteria andGraduatedFromNotLike(String value) {
            addCriterion("graduatedFrom not like", value, "graduatedFrom");
            return (Criteria) this;
        }

        public Criteria andGraduatedFromIn(List<String> values) {
            addCriterion("graduatedFrom in", values, "graduatedFrom");
            return (Criteria) this;
        }

        public Criteria andGraduatedFromNotIn(List<String> values) {
            addCriterion("graduatedFrom not in", values, "graduatedFrom");
            return (Criteria) this;
        }

        public Criteria andGraduatedFromBetween(String value1, String value2) {
            addCriterion("graduatedFrom between", value1, value2, "graduatedFrom");
            return (Criteria) this;
        }

        public Criteria andGraduatedFromNotBetween(String value1, String value2) {
            addCriterion("graduatedFrom not between", value1, value2, "graduatedFrom");
            return (Criteria) this;
        }

        public Criteria andEducationIsNull() {
            addCriterion("education is null");
            return (Criteria) this;
        }

        public Criteria andEducationIsNotNull() {
            addCriterion("education is not null");
            return (Criteria) this;
        }

        public Criteria andEducationEqualTo(Integer value) {
            addCriterion("education =", value, "education");
            return (Criteria) this;
        }

        public Criteria andEducationNotEqualTo(Integer value) {
            addCriterion("education <>", value, "education");
            return (Criteria) this;
        }

        public Criteria andEducationGreaterThan(Integer value) {
            addCriterion("education >", value, "education");
            return (Criteria) this;
        }

        public Criteria andEducationGreaterThanOrEqualTo(Integer value) {
            addCriterion("education >=", value, "education");
            return (Criteria) this;
        }

        public Criteria andEducationLessThan(Integer value) {
            addCriterion("education <", value, "education");
            return (Criteria) this;
        }

        public Criteria andEducationLessThanOrEqualTo(Integer value) {
            addCriterion("education <=", value, "education");
            return (Criteria) this;
        }

        public Criteria andEducationIn(List<Integer> values) {
            addCriterion("education in", values, "education");
            return (Criteria) this;
        }

        public Criteria andEducationNotIn(List<Integer> values) {
            addCriterion("education not in", values, "education");
            return (Criteria) this;
        }

        public Criteria andEducationBetween(Integer value1, Integer value2) {
            addCriterion("education between", value1, value2, "education");
            return (Criteria) this;
        }

        public Criteria andEducationNotBetween(Integer value1, Integer value2) {
            addCriterion("education not between", value1, value2, "education");
            return (Criteria) this;
        }

        public Criteria andMajorIsNull() {
            addCriterion("major is null");
            return (Criteria) this;
        }

        public Criteria andMajorIsNotNull() {
            addCriterion("major is not null");
            return (Criteria) this;
        }

        public Criteria andMajorEqualTo(String value) {
            addCriterion("major =", value, "major");
            return (Criteria) this;
        }

        public Criteria andMajorNotEqualTo(String value) {
            addCriterion("major <>", value, "major");
            return (Criteria) this;
        }

        public Criteria andMajorGreaterThan(String value) {
            addCriterion("major >", value, "major");
            return (Criteria) this;
        }

        public Criteria andMajorGreaterThanOrEqualTo(String value) {
            addCriterion("major >=", value, "major");
            return (Criteria) this;
        }

        public Criteria andMajorLessThan(String value) {
            addCriterion("major <", value, "major");
            return (Criteria) this;
        }

        public Criteria andMajorLessThanOrEqualTo(String value) {
            addCriterion("major <=", value, "major");
            return (Criteria) this;
        }

        public Criteria andMajorLike(String value) {
            addCriterion("major like", value, "major");
            return (Criteria) this;
        }

        public Criteria andMajorNotLike(String value) {
            addCriterion("major not like", value, "major");
            return (Criteria) this;
        }

        public Criteria andMajorIn(List<String> values) {
            addCriterion("major in", values, "major");
            return (Criteria) this;
        }

        public Criteria andMajorNotIn(List<String> values) {
            addCriterion("major not in", values, "major");
            return (Criteria) this;
        }

        public Criteria andMajorBetween(String value1, String value2) {
            addCriterion("major between", value1, value2, "major");
            return (Criteria) this;
        }

        public Criteria andMajorNotBetween(String value1, String value2) {
            addCriterion("major not between", value1, value2, "major");
            return (Criteria) this;
        }

        public Criteria andRemunerationIsNull() {
            addCriterion("remuneration is null");
            return (Criteria) this;
        }

        public Criteria andRemunerationIsNotNull() {
            addCriterion("remuneration is not null");
            return (Criteria) this;
        }

        public Criteria andRemunerationEqualTo(BigDecimal value) {
            addCriterion("remuneration =", value, "remuneration");
            return (Criteria) this;
        }

        public Criteria andRemunerationNotEqualTo(BigDecimal value) {
            addCriterion("remuneration <>", value, "remuneration");
            return (Criteria) this;
        }

        public Criteria andRemunerationGreaterThan(BigDecimal value) {
            addCriterion("remuneration >", value, "remuneration");
            return (Criteria) this;
        }

        public Criteria andRemunerationGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("remuneration >=", value, "remuneration");
            return (Criteria) this;
        }

        public Criteria andRemunerationLessThan(BigDecimal value) {
            addCriterion("remuneration <", value, "remuneration");
            return (Criteria) this;
        }

        public Criteria andRemunerationLessThanOrEqualTo(BigDecimal value) {
            addCriterion("remuneration <=", value, "remuneration");
            return (Criteria) this;
        }

        public Criteria andRemunerationIn(List<BigDecimal> values) {
            addCriterion("remuneration in", values, "remuneration");
            return (Criteria) this;
        }

        public Criteria andRemunerationNotIn(List<BigDecimal> values) {
            addCriterion("remuneration not in", values, "remuneration");
            return (Criteria) this;
        }

        public Criteria andRemunerationBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("remuneration between", value1, value2, "remuneration");
            return (Criteria) this;
        }

        public Criteria andRemunerationNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("remuneration not between", value1, value2, "remuneration");
            return (Criteria) this;
        }

        public Criteria andOpeningBankIsNull() {
            addCriterion("openingBank is null");
            return (Criteria) this;
        }

        public Criteria andOpeningBankIsNotNull() {
            addCriterion("openingBank is not null");
            return (Criteria) this;
        }

        public Criteria andOpeningBankEqualTo(String value) {
            addCriterion("openingBank =", value, "openingBank");
            return (Criteria) this;
        }

        public Criteria andOpeningBankNotEqualTo(String value) {
            addCriterion("openingBank <>", value, "openingBank");
            return (Criteria) this;
        }

        public Criteria andOpeningBankGreaterThan(String value) {
            addCriterion("openingBank >", value, "openingBank");
            return (Criteria) this;
        }

        public Criteria andOpeningBankGreaterThanOrEqualTo(String value) {
            addCriterion("openingBank >=", value, "openingBank");
            return (Criteria) this;
        }

        public Criteria andOpeningBankLessThan(String value) {
            addCriterion("openingBank <", value, "openingBank");
            return (Criteria) this;
        }

        public Criteria andOpeningBankLessThanOrEqualTo(String value) {
            addCriterion("openingBank <=", value, "openingBank");
            return (Criteria) this;
        }

        public Criteria andOpeningBankLike(String value) {
            addCriterion("openingBank like", value, "openingBank");
            return (Criteria) this;
        }

        public Criteria andOpeningBankNotLike(String value) {
            addCriterion("openingBank not like", value, "openingBank");
            return (Criteria) this;
        }

        public Criteria andOpeningBankIn(List<String> values) {
            addCriterion("openingBank in", values, "openingBank");
            return (Criteria) this;
        }

        public Criteria andOpeningBankNotIn(List<String> values) {
            addCriterion("openingBank not in", values, "openingBank");
            return (Criteria) this;
        }

        public Criteria andOpeningBankBetween(String value1, String value2) {
            addCriterion("openingBank between", value1, value2, "openingBank");
            return (Criteria) this;
        }

        public Criteria andOpeningBankNotBetween(String value1, String value2) {
            addCriterion("openingBank not between", value1, value2, "openingBank");
            return (Criteria) this;
        }

        public Criteria andBankAccountIsNull() {
            addCriterion("bankAccount is null");
            return (Criteria) this;
        }

        public Criteria andBankAccountIsNotNull() {
            addCriterion("bankAccount is not null");
            return (Criteria) this;
        }

        public Criteria andBankAccountEqualTo(String value) {
            addCriterion("bankAccount =", value, "bankAccount");
            return (Criteria) this;
        }

        public Criteria andBankAccountNotEqualTo(String value) {
            addCriterion("bankAccount <>", value, "bankAccount");
            return (Criteria) this;
        }

        public Criteria andBankAccountGreaterThan(String value) {
            addCriterion("bankAccount >", value, "bankAccount");
            return (Criteria) this;
        }

        public Criteria andBankAccountGreaterThanOrEqualTo(String value) {
            addCriterion("bankAccount >=", value, "bankAccount");
            return (Criteria) this;
        }

        public Criteria andBankAccountLessThan(String value) {
            addCriterion("bankAccount <", value, "bankAccount");
            return (Criteria) this;
        }

        public Criteria andBankAccountLessThanOrEqualTo(String value) {
            addCriterion("bankAccount <=", value, "bankAccount");
            return (Criteria) this;
        }

        public Criteria andBankAccountLike(String value) {
            addCriterion("bankAccount like", value, "bankAccount");
            return (Criteria) this;
        }

        public Criteria andBankAccountNotLike(String value) {
            addCriterion("bankAccount not like", value, "bankAccount");
            return (Criteria) this;
        }

        public Criteria andBankAccountIn(List<String> values) {
            addCriterion("bankAccount in", values, "bankAccount");
            return (Criteria) this;
        }

        public Criteria andBankAccountNotIn(List<String> values) {
            addCriterion("bankAccount not in", values, "bankAccount");
            return (Criteria) this;
        }

        public Criteria andBankAccountBetween(String value1, String value2) {
            addCriterion("bankAccount between", value1, value2, "bankAccount");
            return (Criteria) this;
        }

        public Criteria andBankAccountNotBetween(String value1, String value2) {
            addCriterion("bankAccount not between", value1, value2, "bankAccount");
            return (Criteria) this;
        }

        public Criteria andRecordIsNull() {
            addCriterion("record is null");
            return (Criteria) this;
        }

        public Criteria andRecordIsNotNull() {
            addCriterion("record is not null");
            return (Criteria) this;
        }

        public Criteria andRecordEqualTo(String value) {
            addCriterion("record =", value, "record");
            return (Criteria) this;
        }

        public Criteria andRecordNotEqualTo(String value) {
            addCriterion("record <>", value, "record");
            return (Criteria) this;
        }

        public Criteria andRecordGreaterThan(String value) {
            addCriterion("record >", value, "record");
            return (Criteria) this;
        }

        public Criteria andRecordGreaterThanOrEqualTo(String value) {
            addCriterion("record >=", value, "record");
            return (Criteria) this;
        }

        public Criteria andRecordLessThan(String value) {
            addCriterion("record <", value, "record");
            return (Criteria) this;
        }

        public Criteria andRecordLessThanOrEqualTo(String value) {
            addCriterion("record <=", value, "record");
            return (Criteria) this;
        }

        public Criteria andRecordLike(String value) {
            addCriterion("record like", value, "record");
            return (Criteria) this;
        }

        public Criteria andRecordNotLike(String value) {
            addCriterion("record not like", value, "record");
            return (Criteria) this;
        }

        public Criteria andRecordIn(List<String> values) {
            addCriterion("record in", values, "record");
            return (Criteria) this;
        }

        public Criteria andRecordNotIn(List<String> values) {
            addCriterion("record not in", values, "record");
            return (Criteria) this;
        }

        public Criteria andRecordBetween(String value1, String value2) {
            addCriterion("record between", value1, value2, "record");
            return (Criteria) this;
        }

        public Criteria andRecordNotBetween(String value1, String value2) {
            addCriterion("record not between", value1, value2, "record");
            return (Criteria) this;
        }

        public Criteria andFamilyInfoIsNull() {
            addCriterion("familyInfo is null");
            return (Criteria) this;
        }

        public Criteria andFamilyInfoIsNotNull() {
            addCriterion("familyInfo is not null");
            return (Criteria) this;
        }

        public Criteria andFamilyInfoEqualTo(String value) {
            addCriterion("familyInfo =", value, "familyInfo");
            return (Criteria) this;
        }

        public Criteria andFamilyInfoNotEqualTo(String value) {
            addCriterion("familyInfo <>", value, "familyInfo");
            return (Criteria) this;
        }

        public Criteria andFamilyInfoGreaterThan(String value) {
            addCriterion("familyInfo >", value, "familyInfo");
            return (Criteria) this;
        }

        public Criteria andFamilyInfoGreaterThanOrEqualTo(String value) {
            addCriterion("familyInfo >=", value, "familyInfo");
            return (Criteria) this;
        }

        public Criteria andFamilyInfoLessThan(String value) {
            addCriterion("familyInfo <", value, "familyInfo");
            return (Criteria) this;
        }

        public Criteria andFamilyInfoLessThanOrEqualTo(String value) {
            addCriterion("familyInfo <=", value, "familyInfo");
            return (Criteria) this;
        }

        public Criteria andFamilyInfoLike(String value) {
            addCriterion("familyInfo like", value, "familyInfo");
            return (Criteria) this;
        }

        public Criteria andFamilyInfoNotLike(String value) {
            addCriterion("familyInfo not like", value, "familyInfo");
            return (Criteria) this;
        }

        public Criteria andFamilyInfoIn(List<String> values) {
            addCriterion("familyInfo in", values, "familyInfo");
            return (Criteria) this;
        }

        public Criteria andFamilyInfoNotIn(List<String> values) {
            addCriterion("familyInfo not in", values, "familyInfo");
            return (Criteria) this;
        }

        public Criteria andFamilyInfoBetween(String value1, String value2) {
            addCriterion("familyInfo between", value1, value2, "familyInfo");
            return (Criteria) this;
        }

        public Criteria andFamilyInfoNotBetween(String value1, String value2) {
            addCriterion("familyInfo not between", value1, value2, "familyInfo");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNull() {
            addCriterion("remark is null");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNotNull() {
            addCriterion("remark is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkEqualTo(String value) {
            addCriterion("remark =", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotEqualTo(String value) {
            addCriterion("remark <>", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThan(String value) {
            addCriterion("remark >", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("remark >=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThan(String value) {
            addCriterion("remark <", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThanOrEqualTo(String value) {
            addCriterion("remark <=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLike(String value) {
            addCriterion("remark like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotLike(String value) {
            addCriterion("remark not like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkIn(List<String> values) {
            addCriterion("remark in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotIn(List<String> values) {
            addCriterion("remark not in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkBetween(String value1, String value2) {
            addCriterion("remark between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotBetween(String value1, String value2) {
            addCriterion("remark not between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRecordlocPathIsNull() {
            addCriterion("recordlocPath is null");
            return (Criteria) this;
        }

        public Criteria andRecordlocPathIsNotNull() {
            addCriterion("recordlocPath is not null");
            return (Criteria) this;
        }

        public Criteria andRecordlocPathEqualTo(String value) {
            addCriterion("recordlocPath =", value, "recordlocPath");
            return (Criteria) this;
        }

        public Criteria andRecordlocPathNotEqualTo(String value) {
            addCriterion("recordlocPath <>", value, "recordlocPath");
            return (Criteria) this;
        }

        public Criteria andRecordlocPathGreaterThan(String value) {
            addCriterion("recordlocPath >", value, "recordlocPath");
            return (Criteria) this;
        }

        public Criteria andRecordlocPathGreaterThanOrEqualTo(String value) {
            addCriterion("recordlocPath >=", value, "recordlocPath");
            return (Criteria) this;
        }

        public Criteria andRecordlocPathLessThan(String value) {
            addCriterion("recordlocPath <", value, "recordlocPath");
            return (Criteria) this;
        }

        public Criteria andRecordlocPathLessThanOrEqualTo(String value) {
            addCriterion("recordlocPath <=", value, "recordlocPath");
            return (Criteria) this;
        }

        public Criteria andRecordlocPathLike(String value) {
            addCriterion("recordlocPath like", value, "recordlocPath");
            return (Criteria) this;
        }

        public Criteria andRecordlocPathNotLike(String value) {
            addCriterion("recordlocPath not like", value, "recordlocPath");
            return (Criteria) this;
        }

        public Criteria andRecordlocPathIn(List<String> values) {
            addCriterion("recordlocPath in", values, "recordlocPath");
            return (Criteria) this;
        }

        public Criteria andRecordlocPathNotIn(List<String> values) {
            addCriterion("recordlocPath not in", values, "recordlocPath");
            return (Criteria) this;
        }

        public Criteria andRecordlocPathBetween(String value1, String value2) {
            addCriterion("recordlocPath between", value1, value2, "recordlocPath");
            return (Criteria) this;
        }

        public Criteria andRecordlocPathNotBetween(String value1, String value2) {
            addCriterion("recordlocPath not between", value1, value2, "recordlocPath");
            return (Criteria) this;
        }

        public Criteria andRecordpoiPathIsNull() {
            addCriterion("recordpoiPath is null");
            return (Criteria) this;
        }

        public Criteria andRecordpoiPathIsNotNull() {
            addCriterion("recordpoiPath is not null");
            return (Criteria) this;
        }

        public Criteria andRecordpoiPathEqualTo(String value) {
            addCriterion("recordpoiPath =", value, "recordpoiPath");
            return (Criteria) this;
        }

        public Criteria andRecordpoiPathNotEqualTo(String value) {
            addCriterion("recordpoiPath <>", value, "recordpoiPath");
            return (Criteria) this;
        }

        public Criteria andRecordpoiPathGreaterThan(String value) {
            addCriterion("recordpoiPath >", value, "recordpoiPath");
            return (Criteria) this;
        }

        public Criteria andRecordpoiPathGreaterThanOrEqualTo(String value) {
            addCriterion("recordpoiPath >=", value, "recordpoiPath");
            return (Criteria) this;
        }

        public Criteria andRecordpoiPathLessThan(String value) {
            addCriterion("recordpoiPath <", value, "recordpoiPath");
            return (Criteria) this;
        }

        public Criteria andRecordpoiPathLessThanOrEqualTo(String value) {
            addCriterion("recordpoiPath <=", value, "recordpoiPath");
            return (Criteria) this;
        }

        public Criteria andRecordpoiPathLike(String value) {
            addCriterion("recordpoiPath like", value, "recordpoiPath");
            return (Criteria) this;
        }

        public Criteria andRecordpoiPathNotLike(String value) {
            addCriterion("recordpoiPath not like", value, "recordpoiPath");
            return (Criteria) this;
        }

        public Criteria andRecordpoiPathIn(List<String> values) {
            addCriterion("recordpoiPath in", values, "recordpoiPath");
            return (Criteria) this;
        }

        public Criteria andRecordpoiPathNotIn(List<String> values) {
            addCriterion("recordpoiPath not in", values, "recordpoiPath");
            return (Criteria) this;
        }

        public Criteria andRecordpoiPathBetween(String value1, String value2) {
            addCriterion("recordpoiPath between", value1, value2, "recordpoiPath");
            return (Criteria) this;
        }

        public Criteria andRecordpoiPathNotBetween(String value1, String value2) {
            addCriterion("recordpoiPath not between", value1, value2, "recordpoiPath");
            return (Criteria) this;
        }

        public Criteria andDepartmentIdIsNull() {
            addCriterion("departmentId is null");
            return (Criteria) this;
        }

        public Criteria andDepartmentIdIsNotNull() {
            addCriterion("departmentId is not null");
            return (Criteria) this;
        }

        public Criteria andDepartmentIdEqualTo(Integer value) {
            addCriterion("departmentId =", value, "departmentId");
            return (Criteria) this;
        }

        public Criteria andDepartmentIdNotEqualTo(Integer value) {
            addCriterion("departmentId <>", value, "departmentId");
            return (Criteria) this;
        }

        public Criteria andDepartmentIdGreaterThan(Integer value) {
            addCriterion("departmentId >", value, "departmentId");
            return (Criteria) this;
        }

        public Criteria andDepartmentIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("departmentId >=", value, "departmentId");
            return (Criteria) this;
        }

        public Criteria andDepartmentIdLessThan(Integer value) {
            addCriterion("departmentId <", value, "departmentId");
            return (Criteria) this;
        }

        public Criteria andDepartmentIdLessThanOrEqualTo(Integer value) {
            addCriterion("departmentId <=", value, "departmentId");
            return (Criteria) this;
        }

        public Criteria andDepartmentIdIn(List<Integer> values) {
            addCriterion("departmentId in", values, "departmentId");
            return (Criteria) this;
        }

        public Criteria andDepartmentIdNotIn(List<Integer> values) {
            addCriterion("departmentId not in", values, "departmentId");
            return (Criteria) this;
        }

        public Criteria andDepartmentIdBetween(Integer value1, Integer value2) {
            addCriterion("departmentId between", value1, value2, "departmentId");
            return (Criteria) this;
        }

        public Criteria andDepartmentIdNotBetween(Integer value1, Integer value2) {
            addCriterion("departmentId not between", value1, value2, "departmentId");
            return (Criteria) this;
        }

        public Criteria andPositionIdIsNull() {
            addCriterion("positionId is null");
            return (Criteria) this;
        }

        public Criteria andPositionIdIsNotNull() {
            addCriterion("positionId is not null");
            return (Criteria) this;
        }

        public Criteria andPositionIdEqualTo(Integer value) {
            addCriterion("positionId =", value, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdNotEqualTo(Integer value) {
            addCriterion("positionId <>", value, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdGreaterThan(Integer value) {
            addCriterion("positionId >", value, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("positionId >=", value, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdLessThan(Integer value) {
            addCriterion("positionId <", value, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdLessThanOrEqualTo(Integer value) {
            addCriterion("positionId <=", value, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdIn(List<Integer> values) {
            addCriterion("positionId in", values, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdNotIn(List<Integer> values) {
            addCriterion("positionId not in", values, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdBetween(Integer value1, Integer value2) {
            addCriterion("positionId between", value1, value2, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdNotBetween(Integer value1, Integer value2) {
            addCriterion("positionId not between", value1, value2, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionCodeIsNull() {
            addCriterion("positionCode is null");
            return (Criteria) this;
        }

        public Criteria andPositionCodeIsNotNull() {
            addCriterion("positionCode is not null");
            return (Criteria) this;
        }

        public Criteria andPositionCodeEqualTo(String value) {
            addCriterion("positionCode =", value, "positionCode");
            return (Criteria) this;
        }

        public Criteria andPositionCodeNotEqualTo(String value) {
            addCriterion("positionCode <>", value, "positionCode");
            return (Criteria) this;
        }

        public Criteria andPositionCodeGreaterThan(String value) {
            addCriterion("positionCode >", value, "positionCode");
            return (Criteria) this;
        }

        public Criteria andPositionCodeGreaterThanOrEqualTo(String value) {
            addCriterion("positionCode >=", value, "positionCode");
            return (Criteria) this;
        }

        public Criteria andPositionCodeLessThan(String value) {
            addCriterion("positionCode <", value, "positionCode");
            return (Criteria) this;
        }

        public Criteria andPositionCodeLessThanOrEqualTo(String value) {
            addCriterion("positionCode <=", value, "positionCode");
            return (Criteria) this;
        }

        public Criteria andPositionCodeLike(String value) {
            addCriterion("positionCode like", value, "positionCode");
            return (Criteria) this;
        }

        public Criteria andPositionCodeNotLike(String value) {
            addCriterion("positionCode not like", value, "positionCode");
            return (Criteria) this;
        }

        public Criteria andPositionCodeIn(List<String> values) {
            addCriterion("positionCode in", values, "positionCode");
            return (Criteria) this;
        }

        public Criteria andPositionCodeNotIn(List<String> values) {
            addCriterion("positionCode not in", values, "positionCode");
            return (Criteria) this;
        }

        public Criteria andPositionCodeBetween(String value1, String value2) {
            addCriterion("positionCode between", value1, value2, "positionCode");
            return (Criteria) this;
        }

        public Criteria andPositionCodeNotBetween(String value1, String value2) {
            addCriterion("positionCode not between", value1, value2, "positionCode");
            return (Criteria) this;
        }

        public Criteria andRegisterByIsNull() {
            addCriterion("registerBy is null");
            return (Criteria) this;
        }

        public Criteria andRegisterByIsNotNull() {
            addCriterion("registerBy is not null");
            return (Criteria) this;
        }

        public Criteria andRegisterByEqualTo(Integer value) {
            addCriterion("registerBy =", value, "registerBy");
            return (Criteria) this;
        }

        public Criteria andRegisterByNotEqualTo(Integer value) {
            addCriterion("registerBy <>", value, "registerBy");
            return (Criteria) this;
        }

        public Criteria andRegisterByGreaterThan(Integer value) {
            addCriterion("registerBy >", value, "registerBy");
            return (Criteria) this;
        }

        public Criteria andRegisterByGreaterThanOrEqualTo(Integer value) {
            addCriterion("registerBy >=", value, "registerBy");
            return (Criteria) this;
        }

        public Criteria andRegisterByLessThan(Integer value) {
            addCriterion("registerBy <", value, "registerBy");
            return (Criteria) this;
        }

        public Criteria andRegisterByLessThanOrEqualTo(Integer value) {
            addCriterion("registerBy <=", value, "registerBy");
            return (Criteria) this;
        }

        public Criteria andRegisterByIn(List<Integer> values) {
            addCriterion("registerBy in", values, "registerBy");
            return (Criteria) this;
        }

        public Criteria andRegisterByNotIn(List<Integer> values) {
            addCriterion("registerBy not in", values, "registerBy");
            return (Criteria) this;
        }

        public Criteria andRegisterByBetween(Integer value1, Integer value2) {
            addCriterion("registerBy between", value1, value2, "registerBy");
            return (Criteria) this;
        }

        public Criteria andRegisterByNotBetween(Integer value1, Integer value2) {
            addCriterion("registerBy not between", value1, value2, "registerBy");
            return (Criteria) this;
        }

        public Criteria andRegisterDateIsNull() {
            addCriterion("registerDate is null");
            return (Criteria) this;
        }

        public Criteria andRegisterDateIsNotNull() {
            addCriterion("registerDate is not null");
            return (Criteria) this;
        }

        public Criteria andRegisterDateEqualTo(Date value) {
            addCriterionForJDBCDate("registerDate =", value, "registerDate");
            return (Criteria) this;
        }

        public Criteria andRegisterDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("registerDate <>", value, "registerDate");
            return (Criteria) this;
        }

        public Criteria andRegisterDateGreaterThan(Date value) {
            addCriterionForJDBCDate("registerDate >", value, "registerDate");
            return (Criteria) this;
        }

        public Criteria andRegisterDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("registerDate >=", value, "registerDate");
            return (Criteria) this;
        }

        public Criteria andRegisterDateLessThan(Date value) {
            addCriterionForJDBCDate("registerDate <", value, "registerDate");
            return (Criteria) this;
        }

        public Criteria andRegisterDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("registerDate <=", value, "registerDate");
            return (Criteria) this;
        }

        public Criteria andRegisterDateIn(List<Date> values) {
            addCriterionForJDBCDate("registerDate in", values, "registerDate");
            return (Criteria) this;
        }

        public Criteria andRegisterDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("registerDate not in", values, "registerDate");
            return (Criteria) this;
        }

        public Criteria andRegisterDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("registerDate between", value1, value2, "registerDate");
            return (Criteria) this;
        }

        public Criteria andRegisterDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("registerDate not between", value1, value2, "registerDate");
            return (Criteria) this;
        }

        public Criteria andModifyByIsNull() {
            addCriterion("modifyBy is null");
            return (Criteria) this;
        }

        public Criteria andModifyByIsNotNull() {
            addCriterion("modifyBy is not null");
            return (Criteria) this;
        }

        public Criteria andModifyByEqualTo(Integer value) {
            addCriterion("modifyBy =", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByNotEqualTo(Integer value) {
            addCriterion("modifyBy <>", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByGreaterThan(Integer value) {
            addCriterion("modifyBy >", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByGreaterThanOrEqualTo(Integer value) {
            addCriterion("modifyBy >=", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByLessThan(Integer value) {
            addCriterion("modifyBy <", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByLessThanOrEqualTo(Integer value) {
            addCriterion("modifyBy <=", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByIn(List<Integer> values) {
            addCriterion("modifyBy in", values, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByNotIn(List<Integer> values) {
            addCriterion("modifyBy not in", values, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByBetween(Integer value1, Integer value2) {
            addCriterion("modifyBy between", value1, value2, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByNotBetween(Integer value1, Integer value2) {
            addCriterion("modifyBy not between", value1, value2, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyDateIsNull() {
            addCriterion("modifyDate is null");
            return (Criteria) this;
        }

        public Criteria andModifyDateIsNotNull() {
            addCriterion("modifyDate is not null");
            return (Criteria) this;
        }

        public Criteria andModifyDateEqualTo(Date value) {
            addCriterionForJDBCDate("modifyDate =", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("modifyDate <>", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateGreaterThan(Date value) {
            addCriterionForJDBCDate("modifyDate >", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("modifyDate >=", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateLessThan(Date value) {
            addCriterionForJDBCDate("modifyDate <", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("modifyDate <=", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateIn(List<Date> values) {
            addCriterionForJDBCDate("modifyDate in", values, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("modifyDate not in", values, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("modifyDate between", value1, value2, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("modifyDate not between", value1, value2, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andAuditStatusIsNull() {
            addCriterion("auditStatus is null");
            return (Criteria) this;
        }

        public Criteria andAuditStatusIsNotNull() {
            addCriterion("auditStatus is not null");
            return (Criteria) this;
        }

        public Criteria andAuditStatusEqualTo(Integer value) {
            addCriterion("auditStatus =", value, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusNotEqualTo(Integer value) {
            addCriterion("auditStatus <>", value, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusGreaterThan(Integer value) {
            addCriterion("auditStatus >", value, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("auditStatus >=", value, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusLessThan(Integer value) {
            addCriterion("auditStatus <", value, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusLessThanOrEqualTo(Integer value) {
            addCriterion("auditStatus <=", value, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusIn(List<Integer> values) {
            addCriterion("auditStatus in", values, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusNotIn(List<Integer> values) {
            addCriterion("auditStatus not in", values, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusBetween(Integer value1, Integer value2) {
            addCriterion("auditStatus between", value1, value2, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("auditStatus not between", value1, value2, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andFilesStatusIsNull() {
            addCriterion("filesStatus is null");
            return (Criteria) this;
        }

        public Criteria andFilesStatusIsNotNull() {
            addCriterion("filesStatus is not null");
            return (Criteria) this;
        }

        public Criteria andFilesStatusEqualTo(Integer value) {
            addCriterion("filesStatus =", value, "filesStatus");
            return (Criteria) this;
        }

        public Criteria andFilesStatusNotEqualTo(Integer value) {
            addCriterion("filesStatus <>", value, "filesStatus");
            return (Criteria) this;
        }

        public Criteria andFilesStatusGreaterThan(Integer value) {
            addCriterion("filesStatus >", value, "filesStatus");
            return (Criteria) this;
        }

        public Criteria andFilesStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("filesStatus >=", value, "filesStatus");
            return (Criteria) this;
        }

        public Criteria andFilesStatusLessThan(Integer value) {
            addCriterion("filesStatus <", value, "filesStatus");
            return (Criteria) this;
        }

        public Criteria andFilesStatusLessThanOrEqualTo(Integer value) {
            addCriterion("filesStatus <=", value, "filesStatus");
            return (Criteria) this;
        }

        public Criteria andFilesStatusIn(List<Integer> values) {
            addCriterion("filesStatus in", values, "filesStatus");
            return (Criteria) this;
        }

        public Criteria andFilesStatusNotIn(List<Integer> values) {
            addCriterion("filesStatus not in", values, "filesStatus");
            return (Criteria) this;
        }

        public Criteria andFilesStatusBetween(Integer value1, Integer value2) {
            addCriterion("filesStatus between", value1, value2, "filesStatus");
            return (Criteria) this;
        }

        public Criteria andFilesStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("filesStatus not between", value1, value2, "filesStatus");
            return (Criteria) this;
        }

        public Criteria andSystemIsNull() {
            addCriterion("system is null");
            return (Criteria) this;
        }

        public Criteria andSystemIsNotNull() {
            addCriterion("system is not null");
            return (Criteria) this;
        }

        public Criteria andSystemEqualTo(Integer value) {
            addCriterion("system =", value, "system");
            return (Criteria) this;
        }

        public Criteria andSystemNotEqualTo(Integer value) {
            addCriterion("system <>", value, "system");
            return (Criteria) this;
        }

        public Criteria andSystemGreaterThan(Integer value) {
            addCriterion("system >", value, "system");
            return (Criteria) this;
        }

        public Criteria andSystemGreaterThanOrEqualTo(Integer value) {
            addCriterion("system >=", value, "system");
            return (Criteria) this;
        }

        public Criteria andSystemLessThan(Integer value) {
            addCriterion("system <", value, "system");
            return (Criteria) this;
        }

        public Criteria andSystemLessThanOrEqualTo(Integer value) {
            addCriterion("system <=", value, "system");
            return (Criteria) this;
        }

        public Criteria andSystemIn(List<Integer> values) {
            addCriterion("system in", values, "system");
            return (Criteria) this;
        }

        public Criteria andSystemNotIn(List<Integer> values) {
            addCriterion("system not in", values, "system");
            return (Criteria) this;
        }

        public Criteria andSystemBetween(Integer value1, Integer value2) {
            addCriterion("system between", value1, value2, "system");
            return (Criteria) this;
        }

        public Criteria andSystemNotBetween(Integer value1, Integer value2) {
            addCriterion("system not between", value1, value2, "system");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}