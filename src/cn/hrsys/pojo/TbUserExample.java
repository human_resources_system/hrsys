package cn.hrsys.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class TbUserExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public TbUserExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andUserIDIsNull() {
            addCriterion("userID is null");
            return (Criteria) this;
        }

        public Criteria andUserIDIsNotNull() {
            addCriterion("userID is not null");
            return (Criteria) this;
        }

        public Criteria andUserIDEqualTo(String value) {
            addCriterion("userID =", value, "userID");
            return (Criteria) this;
        }

        public Criteria andUserIDNotEqualTo(String value) {
            addCriterion("userID <>", value, "userID");
            return (Criteria) this;
        }

        public Criteria andUserIDGreaterThan(String value) {
            addCriterion("userID >", value, "userID");
            return (Criteria) this;
        }

        public Criteria andUserIDGreaterThanOrEqualTo(String value) {
            addCriterion("userID >=", value, "userID");
            return (Criteria) this;
        }

        public Criteria andUserIDLessThan(String value) {
            addCriterion("userID <", value, "userID");
            return (Criteria) this;
        }

        public Criteria andUserIDLessThanOrEqualTo(String value) {
            addCriterion("userID <=", value, "userID");
            return (Criteria) this;
        }

        public Criteria andUserIDLike(String value) {
            addCriterion("userID like", value, "userID");
            return (Criteria) this;
        }

        public Criteria andUserIDNotLike(String value) {
            addCriterion("userID not like", value, "userID");
            return (Criteria) this;
        }

        public Criteria andUserIDIn(List<String> values) {
            addCriterion("userID in", values, "userID");
            return (Criteria) this;
        }

        public Criteria andUserIDNotIn(List<String> values) {
            addCriterion("userID not in", values, "userID");
            return (Criteria) this;
        }

        public Criteria andUserIDBetween(String value1, String value2) {
            addCriterion("userID between", value1, value2, "userID");
            return (Criteria) this;
        }

        public Criteria andUserIDNotBetween(String value1, String value2) {
            addCriterion("userID not between", value1, value2, "userID");
            return (Criteria) this;
        }

        public Criteria andLogUsernameIsNull() {
            addCriterion("logUsername is null");
            return (Criteria) this;
        }

        public Criteria andLogUsernameIsNotNull() {
            addCriterion("logUsername is not null");
            return (Criteria) this;
        }

        public Criteria andLogUsernameEqualTo(String value) {
            addCriterion("logUsername =", value, "logUsername");
            return (Criteria) this;
        }

        public Criteria andLogUsernameNotEqualTo(String value) {
            addCriterion("logUsername <>", value, "logUsername");
            return (Criteria) this;
        }

        public Criteria andLogUsernameGreaterThan(String value) {
            addCriterion("logUsername >", value, "logUsername");
            return (Criteria) this;
        }

        public Criteria andLogUsernameGreaterThanOrEqualTo(String value) {
            addCriterion("logUsername >=", value, "logUsername");
            return (Criteria) this;
        }

        public Criteria andLogUsernameLessThan(String value) {
            addCriterion("logUsername <", value, "logUsername");
            return (Criteria) this;
        }

        public Criteria andLogUsernameLessThanOrEqualTo(String value) {
            addCriterion("logUsername <=", value, "logUsername");
            return (Criteria) this;
        }

        public Criteria andLogUsernameLike(String value) {
            addCriterion("logUsername like", value, "logUsername");
            return (Criteria) this;
        }

        public Criteria andLogUsernameNotLike(String value) {
            addCriterion("logUsername not like", value, "logUsername");
            return (Criteria) this;
        }

        public Criteria andLogUsernameIn(List<String> values) {
            addCriterion("logUsername in", values, "logUsername");
            return (Criteria) this;
        }

        public Criteria andLogUsernameNotIn(List<String> values) {
            addCriterion("logUsername not in", values, "logUsername");
            return (Criteria) this;
        }

        public Criteria andLogUsernameBetween(String value1, String value2) {
            addCriterion("logUsername between", value1, value2, "logUsername");
            return (Criteria) this;
        }

        public Criteria andLogUsernameNotBetween(String value1, String value2) {
            addCriterion("logUsername not between", value1, value2, "logUsername");
            return (Criteria) this;
        }

        public Criteria andLogPasswordIsNull() {
            addCriterion("logPassword is null");
            return (Criteria) this;
        }

        public Criteria andLogPasswordIsNotNull() {
            addCriterion("logPassword is not null");
            return (Criteria) this;
        }

        public Criteria andLogPasswordEqualTo(String value) {
            addCriterion("logPassword =", value, "logPassword");
            return (Criteria) this;
        }

        public Criteria andLogPasswordNotEqualTo(String value) {
            addCriterion("logPassword <>", value, "logPassword");
            return (Criteria) this;
        }

        public Criteria andLogPasswordGreaterThan(String value) {
            addCriterion("logPassword >", value, "logPassword");
            return (Criteria) this;
        }

        public Criteria andLogPasswordGreaterThanOrEqualTo(String value) {
            addCriterion("logPassword >=", value, "logPassword");
            return (Criteria) this;
        }

        public Criteria andLogPasswordLessThan(String value) {
            addCriterion("logPassword <", value, "logPassword");
            return (Criteria) this;
        }

        public Criteria andLogPasswordLessThanOrEqualTo(String value) {
            addCriterion("logPassword <=", value, "logPassword");
            return (Criteria) this;
        }

        public Criteria andLogPasswordLike(String value) {
            addCriterion("logPassword like", value, "logPassword");
            return (Criteria) this;
        }

        public Criteria andLogPasswordNotLike(String value) {
            addCriterion("logPassword not like", value, "logPassword");
            return (Criteria) this;
        }

        public Criteria andLogPasswordIn(List<String> values) {
            addCriterion("logPassword in", values, "logPassword");
            return (Criteria) this;
        }

        public Criteria andLogPasswordNotIn(List<String> values) {
            addCriterion("logPassword not in", values, "logPassword");
            return (Criteria) this;
        }

        public Criteria andLogPasswordBetween(String value1, String value2) {
            addCriterion("logPassword between", value1, value2, "logPassword");
            return (Criteria) this;
        }

        public Criteria andLogPasswordNotBetween(String value1, String value2) {
            addCriterion("logPassword not between", value1, value2, "logPassword");
            return (Criteria) this;
        }

        public Criteria andShowUsernameIsNull() {
            addCriterion("showUsername is null");
            return (Criteria) this;
        }

        public Criteria andShowUsernameIsNotNull() {
            addCriterion("showUsername is not null");
            return (Criteria) this;
        }

        public Criteria andShowUsernameEqualTo(String value) {
            addCriterion("showUsername =", value, "showUsername");
            return (Criteria) this;
        }

        public Criteria andShowUsernameNotEqualTo(String value) {
            addCriterion("showUsername <>", value, "showUsername");
            return (Criteria) this;
        }

        public Criteria andShowUsernameGreaterThan(String value) {
            addCriterion("showUsername >", value, "showUsername");
            return (Criteria) this;
        }

        public Criteria andShowUsernameGreaterThanOrEqualTo(String value) {
            addCriterion("showUsername >=", value, "showUsername");
            return (Criteria) this;
        }

        public Criteria andShowUsernameLessThan(String value) {
            addCriterion("showUsername <", value, "showUsername");
            return (Criteria) this;
        }

        public Criteria andShowUsernameLessThanOrEqualTo(String value) {
            addCriterion("showUsername <=", value, "showUsername");
            return (Criteria) this;
        }

        public Criteria andShowUsernameLike(String value) {
            addCriterion("showUsername like", value, "showUsername");
            return (Criteria) this;
        }

        public Criteria andShowUsernameNotLike(String value) {
            addCriterion("showUsername not like", value, "showUsername");
            return (Criteria) this;
        }

        public Criteria andShowUsernameIn(List<String> values) {
            addCriterion("showUsername in", values, "showUsername");
            return (Criteria) this;
        }

        public Criteria andShowUsernameNotIn(List<String> values) {
            addCriterion("showUsername not in", values, "showUsername");
            return (Criteria) this;
        }

        public Criteria andShowUsernameBetween(String value1, String value2) {
            addCriterion("showUsername between", value1, value2, "showUsername");
            return (Criteria) this;
        }

        public Criteria andShowUsernameNotBetween(String value1, String value2) {
            addCriterion("showUsername not between", value1, value2, "showUsername");
            return (Criteria) this;
        }

        public Criteria andUserViaIsNull() {
            addCriterion("userVia is null");
            return (Criteria) this;
        }

        public Criteria andUserViaIsNotNull() {
            addCriterion("userVia is not null");
            return (Criteria) this;
        }

        public Criteria andUserViaEqualTo(String value) {
            addCriterion("userVia =", value, "userVia");
            return (Criteria) this;
        }

        public Criteria andUserViaNotEqualTo(String value) {
            addCriterion("userVia <>", value, "userVia");
            return (Criteria) this;
        }

        public Criteria andUserViaGreaterThan(String value) {
            addCriterion("userVia >", value, "userVia");
            return (Criteria) this;
        }

        public Criteria andUserViaGreaterThanOrEqualTo(String value) {
            addCriterion("userVia >=", value, "userVia");
            return (Criteria) this;
        }

        public Criteria andUserViaLessThan(String value) {
            addCriterion("userVia <", value, "userVia");
            return (Criteria) this;
        }

        public Criteria andUserViaLessThanOrEqualTo(String value) {
            addCriterion("userVia <=", value, "userVia");
            return (Criteria) this;
        }

        public Criteria andUserViaLike(String value) {
            addCriterion("userVia like", value, "userVia");
            return (Criteria) this;
        }

        public Criteria andUserViaNotLike(String value) {
            addCriterion("userVia not like", value, "userVia");
            return (Criteria) this;
        }

        public Criteria andUserViaIn(List<String> values) {
            addCriterion("userVia in", values, "userVia");
            return (Criteria) this;
        }

        public Criteria andUserViaNotIn(List<String> values) {
            addCriterion("userVia not in", values, "userVia");
            return (Criteria) this;
        }

        public Criteria andUserViaBetween(String value1, String value2) {
            addCriterion("userVia between", value1, value2, "userVia");
            return (Criteria) this;
        }

        public Criteria andUserViaNotBetween(String value1, String value2) {
            addCriterion("userVia not between", value1, value2, "userVia");
            return (Criteria) this;
        }

        public Criteria andRoleIsNull() {
            addCriterion("role is null");
            return (Criteria) this;
        }

        public Criteria andRoleIsNotNull() {
            addCriterion("role is not null");
            return (Criteria) this;
        }

        public Criteria andRoleEqualTo(String value) {
            addCriterion("role =", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleNotEqualTo(String value) {
            addCriterion("role <>", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleGreaterThan(String value) {
            addCriterion("role >", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleGreaterThanOrEqualTo(String value) {
            addCriterion("role >=", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleLessThan(String value) {
            addCriterion("role <", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleLessThanOrEqualTo(String value) {
            addCriterion("role <=", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleLike(String value) {
            addCriterion("role like", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleNotLike(String value) {
            addCriterion("role not like", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleIn(List<String> values) {
            addCriterion("role in", values, "role");
            return (Criteria) this;
        }

        public Criteria andRoleNotIn(List<String> values) {
            addCriterion("role not in", values, "role");
            return (Criteria) this;
        }

        public Criteria andRoleBetween(String value1, String value2) {
            addCriterion("role between", value1, value2, "role");
            return (Criteria) this;
        }

        public Criteria andRoleNotBetween(String value1, String value2) {
            addCriterion("role not between", value1, value2, "role");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andDeletedIsNull() {
            addCriterion("deleted is null");
            return (Criteria) this;
        }

        public Criteria andDeletedIsNotNull() {
            addCriterion("deleted is not null");
            return (Criteria) this;
        }

        public Criteria andDeletedEqualTo(Integer value) {
            addCriterion("deleted =", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedNotEqualTo(Integer value) {
            addCriterion("deleted <>", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedGreaterThan(Integer value) {
            addCriterion("deleted >", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedGreaterThanOrEqualTo(Integer value) {
            addCriterion("deleted >=", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedLessThan(Integer value) {
            addCriterion("deleted <", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedLessThanOrEqualTo(Integer value) {
            addCriterion("deleted <=", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedIn(List<Integer> values) {
            addCriterion("deleted in", values, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedNotIn(List<Integer> values) {
            addCriterion("deleted not in", values, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedBetween(Integer value1, Integer value2) {
            addCriterion("deleted between", value1, value2, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedNotBetween(Integer value1, Integer value2) {
            addCriterion("deleted not between", value1, value2, "deleted");
            return (Criteria) this;
        }

        public Criteria andModifyStatusIsNull() {
            addCriterion("modifyStatus is null");
            return (Criteria) this;
        }

        public Criteria andModifyStatusIsNotNull() {
            addCriterion("modifyStatus is not null");
            return (Criteria) this;
        }

        public Criteria andModifyStatusEqualTo(Integer value) {
            addCriterion("modifyStatus =", value, "modifyStatus");
            return (Criteria) this;
        }

        public Criteria andModifyStatusNotEqualTo(Integer value) {
            addCriterion("modifyStatus <>", value, "modifyStatus");
            return (Criteria) this;
        }

        public Criteria andModifyStatusGreaterThan(Integer value) {
            addCriterion("modifyStatus >", value, "modifyStatus");
            return (Criteria) this;
        }

        public Criteria andModifyStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("modifyStatus >=", value, "modifyStatus");
            return (Criteria) this;
        }

        public Criteria andModifyStatusLessThan(Integer value) {
            addCriterion("modifyStatus <", value, "modifyStatus");
            return (Criteria) this;
        }

        public Criteria andModifyStatusLessThanOrEqualTo(Integer value) {
            addCriterion("modifyStatus <=", value, "modifyStatus");
            return (Criteria) this;
        }

        public Criteria andModifyStatusIn(List<Integer> values) {
            addCriterion("modifyStatus in", values, "modifyStatus");
            return (Criteria) this;
        }

        public Criteria andModifyStatusNotIn(List<Integer> values) {
            addCriterion("modifyStatus not in", values, "modifyStatus");
            return (Criteria) this;
        }

        public Criteria andModifyStatusBetween(Integer value1, Integer value2) {
            addCriterion("modifyStatus between", value1, value2, "modifyStatus");
            return (Criteria) this;
        }

        public Criteria andModifyStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("modifyStatus not between", value1, value2, "modifyStatus");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("createTime is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("createTime is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterionForJDBCDate("createTime =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterionForJDBCDate("createTime <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterionForJDBCDate("createTime >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("createTime >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterionForJDBCDate("createTime <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("createTime <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterionForJDBCDate("createTime in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterionForJDBCDate("createTime not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("createTime between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("createTime not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIsNull() {
            addCriterion("modifyTime is null");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIsNotNull() {
            addCriterion("modifyTime is not null");
            return (Criteria) this;
        }

        public Criteria andModifyTimeEqualTo(Date value) {
            addCriterionForJDBCDate("modifyTime =", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotEqualTo(Date value) {
            addCriterionForJDBCDate("modifyTime <>", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeGreaterThan(Date value) {
            addCriterionForJDBCDate("modifyTime >", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("modifyTime >=", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeLessThan(Date value) {
            addCriterionForJDBCDate("modifyTime <", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("modifyTime <=", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIn(List<Date> values) {
            addCriterionForJDBCDate("modifyTime in", values, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotIn(List<Date> values) {
            addCriterionForJDBCDate("modifyTime not in", values, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("modifyTime between", value1, value2, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("modifyTime not between", value1, value2, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNull() {
            addCriterion("remark is null");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNotNull() {
            addCriterion("remark is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkEqualTo(String value) {
            addCriterion("remark =", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotEqualTo(String value) {
            addCriterion("remark <>", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThan(String value) {
            addCriterion("remark >", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("remark >=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThan(String value) {
            addCriterion("remark <", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThanOrEqualTo(String value) {
            addCriterion("remark <=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLike(String value) {
            addCriterion("remark like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotLike(String value) {
            addCriterion("remark not like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkIn(List<String> values) {
            addCriterion("remark in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotIn(List<String> values) {
            addCriterion("remark not in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkBetween(String value1, String value2) {
            addCriterion("remark between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotBetween(String value1, String value2) {
            addCriterion("remark not between", value1, value2, "remark");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}