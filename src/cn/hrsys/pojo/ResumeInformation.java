package cn.hrsys.pojo;

import java.util.Date;

public class ResumeInformation {
    private Integer id;

    private Integer departmentId;

    private Integer positionId;

    private String name;

    private Integer sex;

    private String email;

    private Integer recruitType;

    private String phone;

    private String address;

    private String domicilePlace;

    private String idNum;

    private Integer face;

    private String graduatedFrom;

    private Integer education;

    private String major;

    private String workExperience;

    private Integer job;

    private Integer freshGraduate;

    private String record;

    private String recordlocPath;

    private String recordpoiPath;

    private Integer resumeStatus;

    private Integer recommendBy;

    private Date recommendDate;

    private String recommendIdea;

    private Integer modifyBy;

    private Date modifyDate;

    private Integer auditStatus;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    public Integer getPositionId() {
        return positionId;
    }

    public void setPositionId(Integer positionId) {
        this.positionId = positionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public Integer getRecruitType() {
        return recruitType;
    }

    public void setRecruitType(Integer recruitType) {
        this.recruitType = recruitType;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getDomicilePlace() {
        return domicilePlace;
    }

    public void setDomicilePlace(String domicilePlace) {
        this.domicilePlace = domicilePlace == null ? null : domicilePlace.trim();
    }

    public String getIdNum() {
        return idNum;
    }

    public void setIdNum(String idNum) {
        this.idNum = idNum == null ? null : idNum.trim();
    }

    public Integer getFace() {
        return face;
    }

    public void setFace(Integer face) {
        this.face = face;
    }

    public String getGraduatedFrom() {
        return graduatedFrom;
    }

    public void setGraduatedFrom(String graduatedFrom) {
        this.graduatedFrom = graduatedFrom == null ? null : graduatedFrom.trim();
    }

    public Integer getEducation() {
        return education;
    }

    public void setEducation(Integer education) {
        this.education = education;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major == null ? null : major.trim();
    }

    public String getWorkExperience() {
        return workExperience;
    }

    public void setWorkExperience(String workExperience) {
        this.workExperience = workExperience == null ? null : workExperience.trim();
    }

    public Integer getJob() {
        return job;
    }

    public void setJob(Integer job) {
        this.job = job;
    }

    public Integer getFreshGraduate() {
        return freshGraduate;
    }

    public void setFreshGraduate(Integer freshGraduate) {
        this.freshGraduate = freshGraduate;
    }

    public String getRecord() {
        return record;
    }

    public void setRecord(String record) {
        this.record = record == null ? null : record.trim();
    }

    public String getRecordlocPath() {
        return recordlocPath;
    }

    public void setRecordlocPath(String recordlocPath) {
        this.recordlocPath = recordlocPath == null ? null : recordlocPath.trim();
    }

    public String getRecordpoiPath() {
        return recordpoiPath;
    }

    public void setRecordpoiPath(String recordpoiPath) {
        this.recordpoiPath = recordpoiPath == null ? null : recordpoiPath.trim();
    }

    public Integer getResumeStatus() {
        return resumeStatus;
    }

    public void setResumeStatus(Integer resumeStatus) {
        this.resumeStatus = resumeStatus;
    }

    public Integer getRecommendBy() {
        return recommendBy;
    }

    public void setRecommendBy(Integer recommendBy) {
        this.recommendBy = recommendBy;
    }

    public Date getRecommendDate() {
        return recommendDate;
    }

    public void setRecommendDate(Date recommendDate) {
        this.recommendDate = recommendDate;
    }

    public String getRecommendIdea() {
        return recommendIdea;
    }

    public void setRecommendIdea(String recommendIdea) {
        this.recommendIdea = recommendIdea == null ? null : recommendIdea.trim();
    }

    public Integer getModifyBy() {
        return modifyBy;
    }

    public void setModifyBy(Integer modifyBy) {
        this.modifyBy = modifyBy;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public Integer getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }
}