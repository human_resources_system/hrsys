package cn.hrsys.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class SubjectInformationExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public SubjectInformationExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andHeadlineIsNull() {
            addCriterion("headline is null");
            return (Criteria) this;
        }

        public Criteria andHeadlineIsNotNull() {
            addCriterion("headline is not null");
            return (Criteria) this;
        }

        public Criteria andHeadlineEqualTo(String value) {
            addCriterion("headline =", value, "headline");
            return (Criteria) this;
        }

        public Criteria andHeadlineNotEqualTo(String value) {
            addCriterion("headline <>", value, "headline");
            return (Criteria) this;
        }

        public Criteria andHeadlineGreaterThan(String value) {
            addCriterion("headline >", value, "headline");
            return (Criteria) this;
        }

        public Criteria andHeadlineGreaterThanOrEqualTo(String value) {
            addCriterion("headline >=", value, "headline");
            return (Criteria) this;
        }

        public Criteria andHeadlineLessThan(String value) {
            addCriterion("headline <", value, "headline");
            return (Criteria) this;
        }

        public Criteria andHeadlineLessThanOrEqualTo(String value) {
            addCriterion("headline <=", value, "headline");
            return (Criteria) this;
        }

        public Criteria andHeadlineLike(String value) {
            addCriterion("headline like", value, "headline");
            return (Criteria) this;
        }

        public Criteria andHeadlineNotLike(String value) {
            addCriterion("headline not like", value, "headline");
            return (Criteria) this;
        }

        public Criteria andHeadlineIn(List<String> values) {
            addCriterion("headline in", values, "headline");
            return (Criteria) this;
        }

        public Criteria andHeadlineNotIn(List<String> values) {
            addCriterion("headline not in", values, "headline");
            return (Criteria) this;
        }

        public Criteria andHeadlineBetween(String value1, String value2) {
            addCriterion("headline between", value1, value2, "headline");
            return (Criteria) this;
        }

        public Criteria andHeadlineNotBetween(String value1, String value2) {
            addCriterion("headline not between", value1, value2, "headline");
            return (Criteria) this;
        }

        public Criteria andSubjectGenreIsNull() {
            addCriterion("subjectGenre is null");
            return (Criteria) this;
        }

        public Criteria andSubjectGenreIsNotNull() {
            addCriterion("subjectGenre is not null");
            return (Criteria) this;
        }

        public Criteria andSubjectGenreEqualTo(Integer value) {
            addCriterion("subjectGenre =", value, "subjectGenre");
            return (Criteria) this;
        }

        public Criteria andSubjectGenreNotEqualTo(Integer value) {
            addCriterion("subjectGenre <>", value, "subjectGenre");
            return (Criteria) this;
        }

        public Criteria andSubjectGenreGreaterThan(Integer value) {
            addCriterion("subjectGenre >", value, "subjectGenre");
            return (Criteria) this;
        }

        public Criteria andSubjectGenreGreaterThanOrEqualTo(Integer value) {
            addCriterion("subjectGenre >=", value, "subjectGenre");
            return (Criteria) this;
        }

        public Criteria andSubjectGenreLessThan(Integer value) {
            addCriterion("subjectGenre <", value, "subjectGenre");
            return (Criteria) this;
        }

        public Criteria andSubjectGenreLessThanOrEqualTo(Integer value) {
            addCriterion("subjectGenre <=", value, "subjectGenre");
            return (Criteria) this;
        }

        public Criteria andSubjectGenreIn(List<Integer> values) {
            addCriterion("subjectGenre in", values, "subjectGenre");
            return (Criteria) this;
        }

        public Criteria andSubjectGenreNotIn(List<Integer> values) {
            addCriterion("subjectGenre not in", values, "subjectGenre");
            return (Criteria) this;
        }

        public Criteria andSubjectGenreBetween(Integer value1, Integer value2) {
            addCriterion("subjectGenre between", value1, value2, "subjectGenre");
            return (Criteria) this;
        }

        public Criteria andSubjectGenreNotBetween(Integer value1, Integer value2) {
            addCriterion("subjectGenre not between", value1, value2, "subjectGenre");
            return (Criteria) this;
        }

        public Criteria andDepIdIsNull() {
            addCriterion("depId is null");
            return (Criteria) this;
        }

        public Criteria andDepIdIsNotNull() {
            addCriterion("depId is not null");
            return (Criteria) this;
        }

        public Criteria andDepIdEqualTo(Integer value) {
            addCriterion("depId =", value, "depId");
            return (Criteria) this;
        }

        public Criteria andDepIdNotEqualTo(Integer value) {
            addCriterion("depId <>", value, "depId");
            return (Criteria) this;
        }

        public Criteria andDepIdGreaterThan(Integer value) {
            addCriterion("depId >", value, "depId");
            return (Criteria) this;
        }

        public Criteria andDepIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("depId >=", value, "depId");
            return (Criteria) this;
        }

        public Criteria andDepIdLessThan(Integer value) {
            addCriterion("depId <", value, "depId");
            return (Criteria) this;
        }

        public Criteria andDepIdLessThanOrEqualTo(Integer value) {
            addCriterion("depId <=", value, "depId");
            return (Criteria) this;
        }

        public Criteria andDepIdIn(List<Integer> values) {
            addCriterion("depId in", values, "depId");
            return (Criteria) this;
        }

        public Criteria andDepIdNotIn(List<Integer> values) {
            addCriterion("depId not in", values, "depId");
            return (Criteria) this;
        }

        public Criteria andDepIdBetween(Integer value1, Integer value2) {
            addCriterion("depId between", value1, value2, "depId");
            return (Criteria) this;
        }

        public Criteria andDepIdNotBetween(Integer value1, Integer value2) {
            addCriterion("depId not between", value1, value2, "depId");
            return (Criteria) this;
        }

        public Criteria andRankIsNull() {
            addCriterion("rank is null");
            return (Criteria) this;
        }

        public Criteria andRankIsNotNull() {
            addCriterion("rank is not null");
            return (Criteria) this;
        }

        public Criteria andRankEqualTo(Integer value) {
            addCriterion("rank =", value, "rank");
            return (Criteria) this;
        }

        public Criteria andRankNotEqualTo(Integer value) {
            addCriterion("rank <>", value, "rank");
            return (Criteria) this;
        }

        public Criteria andRankGreaterThan(Integer value) {
            addCriterion("rank >", value, "rank");
            return (Criteria) this;
        }

        public Criteria andRankGreaterThanOrEqualTo(Integer value) {
            addCriterion("rank >=", value, "rank");
            return (Criteria) this;
        }

        public Criteria andRankLessThan(Integer value) {
            addCriterion("rank <", value, "rank");
            return (Criteria) this;
        }

        public Criteria andRankLessThanOrEqualTo(Integer value) {
            addCriterion("rank <=", value, "rank");
            return (Criteria) this;
        }

        public Criteria andRankIn(List<Integer> values) {
            addCriterion("rank in", values, "rank");
            return (Criteria) this;
        }

        public Criteria andRankNotIn(List<Integer> values) {
            addCriterion("rank not in", values, "rank");
            return (Criteria) this;
        }

        public Criteria andRankBetween(Integer value1, Integer value2) {
            addCriterion("rank between", value1, value2, "rank");
            return (Criteria) this;
        }

        public Criteria andRankNotBetween(Integer value1, Integer value2) {
            addCriterion("rank not between", value1, value2, "rank");
            return (Criteria) this;
        }

        public Criteria andContentIsNull() {
            addCriterion("content is null");
            return (Criteria) this;
        }

        public Criteria andContentIsNotNull() {
            addCriterion("content is not null");
            return (Criteria) this;
        }

        public Criteria andContentEqualTo(String value) {
            addCriterion("content =", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotEqualTo(String value) {
            addCriterion("content <>", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentGreaterThan(String value) {
            addCriterion("content >", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentGreaterThanOrEqualTo(String value) {
            addCriterion("content >=", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLessThan(String value) {
            addCriterion("content <", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLessThanOrEqualTo(String value) {
            addCriterion("content <=", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLike(String value) {
            addCriterion("content like", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotLike(String value) {
            addCriterion("content not like", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentIn(List<String> values) {
            addCriterion("content in", values, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotIn(List<String> values) {
            addCriterion("content not in", values, "content");
            return (Criteria) this;
        }

        public Criteria andContentBetween(String value1, String value2) {
            addCriterion("content between", value1, value2, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotBetween(String value1, String value2) {
            addCriterion("content not between", value1, value2, "content");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNull() {
            addCriterion("remark is null");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNotNull() {
            addCriterion("remark is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkEqualTo(String value) {
            addCriterion("remark =", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotEqualTo(String value) {
            addCriterion("remark <>", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThan(String value) {
            addCriterion("remark >", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("remark >=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThan(String value) {
            addCriterion("remark <", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThanOrEqualTo(String value) {
            addCriterion("remark <=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLike(String value) {
            addCriterion("remark like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotLike(String value) {
            addCriterion("remark not like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkIn(List<String> values) {
            addCriterion("remark in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotIn(List<String> values) {
            addCriterion("remark not in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkBetween(String value1, String value2) {
            addCriterion("remark between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotBetween(String value1, String value2) {
            addCriterion("remark not between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andSubjectlocPathIsNull() {
            addCriterion("subjectlocPath is null");
            return (Criteria) this;
        }

        public Criteria andSubjectlocPathIsNotNull() {
            addCriterion("subjectlocPath is not null");
            return (Criteria) this;
        }

        public Criteria andSubjectlocPathEqualTo(String value) {
            addCriterion("subjectlocPath =", value, "subjectlocPath");
            return (Criteria) this;
        }

        public Criteria andSubjectlocPathNotEqualTo(String value) {
            addCriterion("subjectlocPath <>", value, "subjectlocPath");
            return (Criteria) this;
        }

        public Criteria andSubjectlocPathGreaterThan(String value) {
            addCriterion("subjectlocPath >", value, "subjectlocPath");
            return (Criteria) this;
        }

        public Criteria andSubjectlocPathGreaterThanOrEqualTo(String value) {
            addCriterion("subjectlocPath >=", value, "subjectlocPath");
            return (Criteria) this;
        }

        public Criteria andSubjectlocPathLessThan(String value) {
            addCriterion("subjectlocPath <", value, "subjectlocPath");
            return (Criteria) this;
        }

        public Criteria andSubjectlocPathLessThanOrEqualTo(String value) {
            addCriterion("subjectlocPath <=", value, "subjectlocPath");
            return (Criteria) this;
        }

        public Criteria andSubjectlocPathLike(String value) {
            addCriterion("subjectlocPath like", value, "subjectlocPath");
            return (Criteria) this;
        }

        public Criteria andSubjectlocPathNotLike(String value) {
            addCriterion("subjectlocPath not like", value, "subjectlocPath");
            return (Criteria) this;
        }

        public Criteria andSubjectlocPathIn(List<String> values) {
            addCriterion("subjectlocPath in", values, "subjectlocPath");
            return (Criteria) this;
        }

        public Criteria andSubjectlocPathNotIn(List<String> values) {
            addCriterion("subjectlocPath not in", values, "subjectlocPath");
            return (Criteria) this;
        }

        public Criteria andSubjectlocPathBetween(String value1, String value2) {
            addCriterion("subjectlocPath between", value1, value2, "subjectlocPath");
            return (Criteria) this;
        }

        public Criteria andSubjectlocPathNotBetween(String value1, String value2) {
            addCriterion("subjectlocPath not between", value1, value2, "subjectlocPath");
            return (Criteria) this;
        }

        public Criteria andSubjectpoiPathIsNull() {
            addCriterion("subjectpoiPath is null");
            return (Criteria) this;
        }

        public Criteria andSubjectpoiPathIsNotNull() {
            addCriterion("subjectpoiPath is not null");
            return (Criteria) this;
        }

        public Criteria andSubjectpoiPathEqualTo(String value) {
            addCriterion("subjectpoiPath =", value, "subjectpoiPath");
            return (Criteria) this;
        }

        public Criteria andSubjectpoiPathNotEqualTo(String value) {
            addCriterion("subjectpoiPath <>", value, "subjectpoiPath");
            return (Criteria) this;
        }

        public Criteria andSubjectpoiPathGreaterThan(String value) {
            addCriterion("subjectpoiPath >", value, "subjectpoiPath");
            return (Criteria) this;
        }

        public Criteria andSubjectpoiPathGreaterThanOrEqualTo(String value) {
            addCriterion("subjectpoiPath >=", value, "subjectpoiPath");
            return (Criteria) this;
        }

        public Criteria andSubjectpoiPathLessThan(String value) {
            addCriterion("subjectpoiPath <", value, "subjectpoiPath");
            return (Criteria) this;
        }

        public Criteria andSubjectpoiPathLessThanOrEqualTo(String value) {
            addCriterion("subjectpoiPath <=", value, "subjectpoiPath");
            return (Criteria) this;
        }

        public Criteria andSubjectpoiPathLike(String value) {
            addCriterion("subjectpoiPath like", value, "subjectpoiPath");
            return (Criteria) this;
        }

        public Criteria andSubjectpoiPathNotLike(String value) {
            addCriterion("subjectpoiPath not like", value, "subjectpoiPath");
            return (Criteria) this;
        }

        public Criteria andSubjectpoiPathIn(List<String> values) {
            addCriterion("subjectpoiPath in", values, "subjectpoiPath");
            return (Criteria) this;
        }

        public Criteria andSubjectpoiPathNotIn(List<String> values) {
            addCriterion("subjectpoiPath not in", values, "subjectpoiPath");
            return (Criteria) this;
        }

        public Criteria andSubjectpoiPathBetween(String value1, String value2) {
            addCriterion("subjectpoiPath between", value1, value2, "subjectpoiPath");
            return (Criteria) this;
        }

        public Criteria andSubjectpoiPathNotBetween(String value1, String value2) {
            addCriterion("subjectpoiPath not between", value1, value2, "subjectpoiPath");
            return (Criteria) this;
        }

        public Criteria andResultlocPathIsNull() {
            addCriterion("resultlocPath is null");
            return (Criteria) this;
        }

        public Criteria andResultlocPathIsNotNull() {
            addCriterion("resultlocPath is not null");
            return (Criteria) this;
        }

        public Criteria andResultlocPathEqualTo(String value) {
            addCriterion("resultlocPath =", value, "resultlocPath");
            return (Criteria) this;
        }

        public Criteria andResultlocPathNotEqualTo(String value) {
            addCriterion("resultlocPath <>", value, "resultlocPath");
            return (Criteria) this;
        }

        public Criteria andResultlocPathGreaterThan(String value) {
            addCriterion("resultlocPath >", value, "resultlocPath");
            return (Criteria) this;
        }

        public Criteria andResultlocPathGreaterThanOrEqualTo(String value) {
            addCriterion("resultlocPath >=", value, "resultlocPath");
            return (Criteria) this;
        }

        public Criteria andResultlocPathLessThan(String value) {
            addCriterion("resultlocPath <", value, "resultlocPath");
            return (Criteria) this;
        }

        public Criteria andResultlocPathLessThanOrEqualTo(String value) {
            addCriterion("resultlocPath <=", value, "resultlocPath");
            return (Criteria) this;
        }

        public Criteria andResultlocPathLike(String value) {
            addCriterion("resultlocPath like", value, "resultlocPath");
            return (Criteria) this;
        }

        public Criteria andResultlocPathNotLike(String value) {
            addCriterion("resultlocPath not like", value, "resultlocPath");
            return (Criteria) this;
        }

        public Criteria andResultlocPathIn(List<String> values) {
            addCriterion("resultlocPath in", values, "resultlocPath");
            return (Criteria) this;
        }

        public Criteria andResultlocPathNotIn(List<String> values) {
            addCriterion("resultlocPath not in", values, "resultlocPath");
            return (Criteria) this;
        }

        public Criteria andResultlocPathBetween(String value1, String value2) {
            addCriterion("resultlocPath between", value1, value2, "resultlocPath");
            return (Criteria) this;
        }

        public Criteria andResultlocPathNotBetween(String value1, String value2) {
            addCriterion("resultlocPath not between", value1, value2, "resultlocPath");
            return (Criteria) this;
        }

        public Criteria andResultpoiPathIsNull() {
            addCriterion("resultpoiPath is null");
            return (Criteria) this;
        }

        public Criteria andResultpoiPathIsNotNull() {
            addCriterion("resultpoiPath is not null");
            return (Criteria) this;
        }

        public Criteria andResultpoiPathEqualTo(String value) {
            addCriterion("resultpoiPath =", value, "resultpoiPath");
            return (Criteria) this;
        }

        public Criteria andResultpoiPathNotEqualTo(String value) {
            addCriterion("resultpoiPath <>", value, "resultpoiPath");
            return (Criteria) this;
        }

        public Criteria andResultpoiPathGreaterThan(String value) {
            addCriterion("resultpoiPath >", value, "resultpoiPath");
            return (Criteria) this;
        }

        public Criteria andResultpoiPathGreaterThanOrEqualTo(String value) {
            addCriterion("resultpoiPath >=", value, "resultpoiPath");
            return (Criteria) this;
        }

        public Criteria andResultpoiPathLessThan(String value) {
            addCriterion("resultpoiPath <", value, "resultpoiPath");
            return (Criteria) this;
        }

        public Criteria andResultpoiPathLessThanOrEqualTo(String value) {
            addCriterion("resultpoiPath <=", value, "resultpoiPath");
            return (Criteria) this;
        }

        public Criteria andResultpoiPathLike(String value) {
            addCriterion("resultpoiPath like", value, "resultpoiPath");
            return (Criteria) this;
        }

        public Criteria andResultpoiPathNotLike(String value) {
            addCriterion("resultpoiPath not like", value, "resultpoiPath");
            return (Criteria) this;
        }

        public Criteria andResultpoiPathIn(List<String> values) {
            addCriterion("resultpoiPath in", values, "resultpoiPath");
            return (Criteria) this;
        }

        public Criteria andResultpoiPathNotIn(List<String> values) {
            addCriterion("resultpoiPath not in", values, "resultpoiPath");
            return (Criteria) this;
        }

        public Criteria andResultpoiPathBetween(String value1, String value2) {
            addCriterion("resultpoiPath between", value1, value2, "resultpoiPath");
            return (Criteria) this;
        }

        public Criteria andResultpoiPathNotBetween(String value1, String value2) {
            addCriterion("resultpoiPath not between", value1, value2, "resultpoiPath");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andEnabledDateIsNull() {
            addCriterion("enabledDate is null");
            return (Criteria) this;
        }

        public Criteria andEnabledDateIsNotNull() {
            addCriterion("enabledDate is not null");
            return (Criteria) this;
        }

        public Criteria andEnabledDateEqualTo(Date value) {
            addCriterionForJDBCDate("enabledDate =", value, "enabledDate");
            return (Criteria) this;
        }

        public Criteria andEnabledDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("enabledDate <>", value, "enabledDate");
            return (Criteria) this;
        }

        public Criteria andEnabledDateGreaterThan(Date value) {
            addCriterionForJDBCDate("enabledDate >", value, "enabledDate");
            return (Criteria) this;
        }

        public Criteria andEnabledDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("enabledDate >=", value, "enabledDate");
            return (Criteria) this;
        }

        public Criteria andEnabledDateLessThan(Date value) {
            addCriterionForJDBCDate("enabledDate <", value, "enabledDate");
            return (Criteria) this;
        }

        public Criteria andEnabledDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("enabledDate <=", value, "enabledDate");
            return (Criteria) this;
        }

        public Criteria andEnabledDateIn(List<Date> values) {
            addCriterionForJDBCDate("enabledDate in", values, "enabledDate");
            return (Criteria) this;
        }

        public Criteria andEnabledDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("enabledDate not in", values, "enabledDate");
            return (Criteria) this;
        }

        public Criteria andEnabledDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("enabledDate between", value1, value2, "enabledDate");
            return (Criteria) this;
        }

        public Criteria andEnabledDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("enabledDate not between", value1, value2, "enabledDate");
            return (Criteria) this;
        }

        public Criteria andLoseDateIsNull() {
            addCriterion("loseDate is null");
            return (Criteria) this;
        }

        public Criteria andLoseDateIsNotNull() {
            addCriterion("loseDate is not null");
            return (Criteria) this;
        }

        public Criteria andLoseDateEqualTo(Date value) {
            addCriterionForJDBCDate("loseDate =", value, "loseDate");
            return (Criteria) this;
        }

        public Criteria andLoseDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("loseDate <>", value, "loseDate");
            return (Criteria) this;
        }

        public Criteria andLoseDateGreaterThan(Date value) {
            addCriterionForJDBCDate("loseDate >", value, "loseDate");
            return (Criteria) this;
        }

        public Criteria andLoseDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("loseDate >=", value, "loseDate");
            return (Criteria) this;
        }

        public Criteria andLoseDateLessThan(Date value) {
            addCriterionForJDBCDate("loseDate <", value, "loseDate");
            return (Criteria) this;
        }

        public Criteria andLoseDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("loseDate <=", value, "loseDate");
            return (Criteria) this;
        }

        public Criteria andLoseDateIn(List<Date> values) {
            addCriterionForJDBCDate("loseDate in", values, "loseDate");
            return (Criteria) this;
        }

        public Criteria andLoseDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("loseDate not in", values, "loseDate");
            return (Criteria) this;
        }

        public Criteria andLoseDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("loseDate between", value1, value2, "loseDate");
            return (Criteria) this;
        }

        public Criteria andLoseDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("loseDate not between", value1, value2, "loseDate");
            return (Criteria) this;
        }

        public Criteria andCreationByIsNull() {
            addCriterion("creationBy is null");
            return (Criteria) this;
        }

        public Criteria andCreationByIsNotNull() {
            addCriterion("creationBy is not null");
            return (Criteria) this;
        }

        public Criteria andCreationByEqualTo(Integer value) {
            addCriterion("creationBy =", value, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByNotEqualTo(Integer value) {
            addCriterion("creationBy <>", value, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByGreaterThan(Integer value) {
            addCriterion("creationBy >", value, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByGreaterThanOrEqualTo(Integer value) {
            addCriterion("creationBy >=", value, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByLessThan(Integer value) {
            addCriterion("creationBy <", value, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByLessThanOrEqualTo(Integer value) {
            addCriterion("creationBy <=", value, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByIn(List<Integer> values) {
            addCriterion("creationBy in", values, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByNotIn(List<Integer> values) {
            addCriterion("creationBy not in", values, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByBetween(Integer value1, Integer value2) {
            addCriterion("creationBy between", value1, value2, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByNotBetween(Integer value1, Integer value2) {
            addCriterion("creationBy not between", value1, value2, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationDateIsNull() {
            addCriterion("creationDate is null");
            return (Criteria) this;
        }

        public Criteria andCreationDateIsNotNull() {
            addCriterion("creationDate is not null");
            return (Criteria) this;
        }

        public Criteria andCreationDateEqualTo(Date value) {
            addCriterionForJDBCDate("creationDate =", value, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("creationDate <>", value, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateGreaterThan(Date value) {
            addCriterionForJDBCDate("creationDate >", value, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("creationDate >=", value, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateLessThan(Date value) {
            addCriterionForJDBCDate("creationDate <", value, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("creationDate <=", value, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateIn(List<Date> values) {
            addCriterionForJDBCDate("creationDate in", values, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("creationDate not in", values, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("creationDate between", value1, value2, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("creationDate not between", value1, value2, "creationDate");
            return (Criteria) this;
        }

        public Criteria andModifyByIsNull() {
            addCriterion("modifyBy is null");
            return (Criteria) this;
        }

        public Criteria andModifyByIsNotNull() {
            addCriterion("modifyBy is not null");
            return (Criteria) this;
        }

        public Criteria andModifyByEqualTo(Integer value) {
            addCriterion("modifyBy =", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByNotEqualTo(Integer value) {
            addCriterion("modifyBy <>", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByGreaterThan(Integer value) {
            addCriterion("modifyBy >", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByGreaterThanOrEqualTo(Integer value) {
            addCriterion("modifyBy >=", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByLessThan(Integer value) {
            addCriterion("modifyBy <", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByLessThanOrEqualTo(Integer value) {
            addCriterion("modifyBy <=", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByIn(List<Integer> values) {
            addCriterion("modifyBy in", values, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByNotIn(List<Integer> values) {
            addCriterion("modifyBy not in", values, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByBetween(Integer value1, Integer value2) {
            addCriterion("modifyBy between", value1, value2, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByNotBetween(Integer value1, Integer value2) {
            addCriterion("modifyBy not between", value1, value2, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyDateIsNull() {
            addCriterion("modifyDate is null");
            return (Criteria) this;
        }

        public Criteria andModifyDateIsNotNull() {
            addCriterion("modifyDate is not null");
            return (Criteria) this;
        }

        public Criteria andModifyDateEqualTo(Date value) {
            addCriterionForJDBCDate("modifyDate =", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("modifyDate <>", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateGreaterThan(Date value) {
            addCriterionForJDBCDate("modifyDate >", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("modifyDate >=", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateLessThan(Date value) {
            addCriterionForJDBCDate("modifyDate <", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("modifyDate <=", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateIn(List<Date> values) {
            addCriterionForJDBCDate("modifyDate in", values, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("modifyDate not in", values, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("modifyDate between", value1, value2, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("modifyDate not between", value1, value2, "modifyDate");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}