package cn.hrsys.pojo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class RewarditemInformationExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public RewarditemInformationExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andRewarditemCodeIsNull() {
            addCriterion("rewarditemCode is null");
            return (Criteria) this;
        }

        public Criteria andRewarditemCodeIsNotNull() {
            addCriterion("rewarditemCode is not null");
            return (Criteria) this;
        }

        public Criteria andRewarditemCodeEqualTo(String value) {
            addCriterion("rewarditemCode =", value, "rewarditemCode");
            return (Criteria) this;
        }

        public Criteria andRewarditemCodeNotEqualTo(String value) {
            addCriterion("rewarditemCode <>", value, "rewarditemCode");
            return (Criteria) this;
        }

        public Criteria andRewarditemCodeGreaterThan(String value) {
            addCriterion("rewarditemCode >", value, "rewarditemCode");
            return (Criteria) this;
        }

        public Criteria andRewarditemCodeGreaterThanOrEqualTo(String value) {
            addCriterion("rewarditemCode >=", value, "rewarditemCode");
            return (Criteria) this;
        }

        public Criteria andRewarditemCodeLessThan(String value) {
            addCriterion("rewarditemCode <", value, "rewarditemCode");
            return (Criteria) this;
        }

        public Criteria andRewarditemCodeLessThanOrEqualTo(String value) {
            addCriterion("rewarditemCode <=", value, "rewarditemCode");
            return (Criteria) this;
        }

        public Criteria andRewarditemCodeLike(String value) {
            addCriterion("rewarditemCode like", value, "rewarditemCode");
            return (Criteria) this;
        }

        public Criteria andRewarditemCodeNotLike(String value) {
            addCriterion("rewarditemCode not like", value, "rewarditemCode");
            return (Criteria) this;
        }

        public Criteria andRewarditemCodeIn(List<String> values) {
            addCriterion("rewarditemCode in", values, "rewarditemCode");
            return (Criteria) this;
        }

        public Criteria andRewarditemCodeNotIn(List<String> values) {
            addCriterion("rewarditemCode not in", values, "rewarditemCode");
            return (Criteria) this;
        }

        public Criteria andRewarditemCodeBetween(String value1, String value2) {
            addCriterion("rewarditemCode between", value1, value2, "rewarditemCode");
            return (Criteria) this;
        }

        public Criteria andRewarditemCodeNotBetween(String value1, String value2) {
            addCriterion("rewarditemCode not between", value1, value2, "rewarditemCode");
            return (Criteria) this;
        }

        public Criteria andRewarditemNameIsNull() {
            addCriterion("rewarditemName is null");
            return (Criteria) this;
        }

        public Criteria andRewarditemNameIsNotNull() {
            addCriterion("rewarditemName is not null");
            return (Criteria) this;
        }

        public Criteria andRewarditemNameEqualTo(String value) {
            addCriterion("rewarditemName =", value, "rewarditemName");
            return (Criteria) this;
        }

        public Criteria andRewarditemNameNotEqualTo(String value) {
            addCriterion("rewarditemName <>", value, "rewarditemName");
            return (Criteria) this;
        }

        public Criteria andRewarditemNameGreaterThan(String value) {
            addCriterion("rewarditemName >", value, "rewarditemName");
            return (Criteria) this;
        }

        public Criteria andRewarditemNameGreaterThanOrEqualTo(String value) {
            addCriterion("rewarditemName >=", value, "rewarditemName");
            return (Criteria) this;
        }

        public Criteria andRewarditemNameLessThan(String value) {
            addCriterion("rewarditemName <", value, "rewarditemName");
            return (Criteria) this;
        }

        public Criteria andRewarditemNameLessThanOrEqualTo(String value) {
            addCriterion("rewarditemName <=", value, "rewarditemName");
            return (Criteria) this;
        }

        public Criteria andRewarditemNameLike(String value) {
            addCriterion("rewarditemName like", value, "rewarditemName");
            return (Criteria) this;
        }

        public Criteria andRewarditemNameNotLike(String value) {
            addCriterion("rewarditemName not like", value, "rewarditemName");
            return (Criteria) this;
        }

        public Criteria andRewarditemNameIn(List<String> values) {
            addCriterion("rewarditemName in", values, "rewarditemName");
            return (Criteria) this;
        }

        public Criteria andRewarditemNameNotIn(List<String> values) {
            addCriterion("rewarditemName not in", values, "rewarditemName");
            return (Criteria) this;
        }

        public Criteria andRewarditemNameBetween(String value1, String value2) {
            addCriterion("rewarditemName between", value1, value2, "rewarditemName");
            return (Criteria) this;
        }

        public Criteria andRewarditemNameNotBetween(String value1, String value2) {
            addCriterion("rewarditemName not between", value1, value2, "rewarditemName");
            return (Criteria) this;
        }

        public Criteria andRewarditemAbstractIsNull() {
            addCriterion("rewarditemAbstract is null");
            return (Criteria) this;
        }

        public Criteria andRewarditemAbstractIsNotNull() {
            addCriterion("rewarditemAbstract is not null");
            return (Criteria) this;
        }

        public Criteria andRewarditemAbstractEqualTo(String value) {
            addCriterion("rewarditemAbstract =", value, "rewarditemAbstract");
            return (Criteria) this;
        }

        public Criteria andRewarditemAbstractNotEqualTo(String value) {
            addCriterion("rewarditemAbstract <>", value, "rewarditemAbstract");
            return (Criteria) this;
        }

        public Criteria andRewarditemAbstractGreaterThan(String value) {
            addCriterion("rewarditemAbstract >", value, "rewarditemAbstract");
            return (Criteria) this;
        }

        public Criteria andRewarditemAbstractGreaterThanOrEqualTo(String value) {
            addCriterion("rewarditemAbstract >=", value, "rewarditemAbstract");
            return (Criteria) this;
        }

        public Criteria andRewarditemAbstractLessThan(String value) {
            addCriterion("rewarditemAbstract <", value, "rewarditemAbstract");
            return (Criteria) this;
        }

        public Criteria andRewarditemAbstractLessThanOrEqualTo(String value) {
            addCriterion("rewarditemAbstract <=", value, "rewarditemAbstract");
            return (Criteria) this;
        }

        public Criteria andRewarditemAbstractLike(String value) {
            addCriterion("rewarditemAbstract like", value, "rewarditemAbstract");
            return (Criteria) this;
        }

        public Criteria andRewarditemAbstractNotLike(String value) {
            addCriterion("rewarditemAbstract not like", value, "rewarditemAbstract");
            return (Criteria) this;
        }

        public Criteria andRewarditemAbstractIn(List<String> values) {
            addCriterion("rewarditemAbstract in", values, "rewarditemAbstract");
            return (Criteria) this;
        }

        public Criteria andRewarditemAbstractNotIn(List<String> values) {
            addCriterion("rewarditemAbstract not in", values, "rewarditemAbstract");
            return (Criteria) this;
        }

        public Criteria andRewarditemAbstractBetween(String value1, String value2) {
            addCriterion("rewarditemAbstract between", value1, value2, "rewarditemAbstract");
            return (Criteria) this;
        }

        public Criteria andRewarditemAbstractNotBetween(String value1, String value2) {
            addCriterion("rewarditemAbstract not between", value1, value2, "rewarditemAbstract");
            return (Criteria) this;
        }

        public Criteria andRewardmoneyIsNull() {
            addCriterion("rewardmoney is null");
            return (Criteria) this;
        }

        public Criteria andRewardmoneyIsNotNull() {
            addCriterion("rewardmoney is not null");
            return (Criteria) this;
        }

        public Criteria andRewardmoneyEqualTo(BigDecimal value) {
            addCriterion("rewardmoney =", value, "rewardmoney");
            return (Criteria) this;
        }

        public Criteria andRewardmoneyNotEqualTo(BigDecimal value) {
            addCriterion("rewardmoney <>", value, "rewardmoney");
            return (Criteria) this;
        }

        public Criteria andRewardmoneyGreaterThan(BigDecimal value) {
            addCriterion("rewardmoney >", value, "rewardmoney");
            return (Criteria) this;
        }

        public Criteria andRewardmoneyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("rewardmoney >=", value, "rewardmoney");
            return (Criteria) this;
        }

        public Criteria andRewardmoneyLessThan(BigDecimal value) {
            addCriterion("rewardmoney <", value, "rewardmoney");
            return (Criteria) this;
        }

        public Criteria andRewardmoneyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("rewardmoney <=", value, "rewardmoney");
            return (Criteria) this;
        }

        public Criteria andRewardmoneyIn(List<BigDecimal> values) {
            addCriterion("rewardmoney in", values, "rewardmoney");
            return (Criteria) this;
        }

        public Criteria andRewardmoneyNotIn(List<BigDecimal> values) {
            addCriterion("rewardmoney not in", values, "rewardmoney");
            return (Criteria) this;
        }

        public Criteria andRewardmoneyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("rewardmoney between", value1, value2, "rewardmoney");
            return (Criteria) this;
        }

        public Criteria andRewardmoneyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("rewardmoney not between", value1, value2, "rewardmoney");
            return (Criteria) this;
        }

        public Criteria andCreationByIsNull() {
            addCriterion("creationBy is null");
            return (Criteria) this;
        }

        public Criteria andCreationByIsNotNull() {
            addCriterion("creationBy is not null");
            return (Criteria) this;
        }

        public Criteria andCreationByEqualTo(Integer value) {
            addCriterion("creationBy =", value, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByNotEqualTo(Integer value) {
            addCriterion("creationBy <>", value, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByGreaterThan(Integer value) {
            addCriterion("creationBy >", value, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByGreaterThanOrEqualTo(Integer value) {
            addCriterion("creationBy >=", value, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByLessThan(Integer value) {
            addCriterion("creationBy <", value, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByLessThanOrEqualTo(Integer value) {
            addCriterion("creationBy <=", value, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByIn(List<Integer> values) {
            addCriterion("creationBy in", values, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByNotIn(List<Integer> values) {
            addCriterion("creationBy not in", values, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByBetween(Integer value1, Integer value2) {
            addCriterion("creationBy between", value1, value2, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByNotBetween(Integer value1, Integer value2) {
            addCriterion("creationBy not between", value1, value2, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationDateIsNull() {
            addCriterion("creationDate is null");
            return (Criteria) this;
        }

        public Criteria andCreationDateIsNotNull() {
            addCriterion("creationDate is not null");
            return (Criteria) this;
        }

        public Criteria andCreationDateEqualTo(Date value) {
            addCriterionForJDBCDate("creationDate =", value, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("creationDate <>", value, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateGreaterThan(Date value) {
            addCriterionForJDBCDate("creationDate >", value, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("creationDate >=", value, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateLessThan(Date value) {
            addCriterionForJDBCDate("creationDate <", value, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("creationDate <=", value, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateIn(List<Date> values) {
            addCriterionForJDBCDate("creationDate in", values, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("creationDate not in", values, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("creationDate between", value1, value2, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("creationDate not between", value1, value2, "creationDate");
            return (Criteria) this;
        }

        public Criteria andModifyByIsNull() {
            addCriterion("modifyBy is null");
            return (Criteria) this;
        }

        public Criteria andModifyByIsNotNull() {
            addCriterion("modifyBy is not null");
            return (Criteria) this;
        }

        public Criteria andModifyByEqualTo(Integer value) {
            addCriterion("modifyBy =", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByNotEqualTo(Integer value) {
            addCriterion("modifyBy <>", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByGreaterThan(Integer value) {
            addCriterion("modifyBy >", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByGreaterThanOrEqualTo(Integer value) {
            addCriterion("modifyBy >=", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByLessThan(Integer value) {
            addCriterion("modifyBy <", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByLessThanOrEqualTo(Integer value) {
            addCriterion("modifyBy <=", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByIn(List<Integer> values) {
            addCriterion("modifyBy in", values, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByNotIn(List<Integer> values) {
            addCriterion("modifyBy not in", values, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByBetween(Integer value1, Integer value2) {
            addCriterion("modifyBy between", value1, value2, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByNotBetween(Integer value1, Integer value2) {
            addCriterion("modifyBy not between", value1, value2, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyDateIsNull() {
            addCriterion("modifyDate is null");
            return (Criteria) this;
        }

        public Criteria andModifyDateIsNotNull() {
            addCriterion("modifyDate is not null");
            return (Criteria) this;
        }

        public Criteria andModifyDateEqualTo(Date value) {
            addCriterionForJDBCDate("modifyDate =", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("modifyDate <>", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateGreaterThan(Date value) {
            addCriterionForJDBCDate("modifyDate >", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("modifyDate >=", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateLessThan(Date value) {
            addCriterionForJDBCDate("modifyDate <", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("modifyDate <=", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateIn(List<Date> values) {
            addCriterionForJDBCDate("modifyDate in", values, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("modifyDate not in", values, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("modifyDate between", value1, value2, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("modifyDate not between", value1, value2, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyCauseIsNull() {
            addCriterion("modifyCause is null");
            return (Criteria) this;
        }

        public Criteria andModifyCauseIsNotNull() {
            addCriterion("modifyCause is not null");
            return (Criteria) this;
        }

        public Criteria andModifyCauseEqualTo(String value) {
            addCriterion("modifyCause =", value, "modifyCause");
            return (Criteria) this;
        }

        public Criteria andModifyCauseNotEqualTo(String value) {
            addCriterion("modifyCause <>", value, "modifyCause");
            return (Criteria) this;
        }

        public Criteria andModifyCauseGreaterThan(String value) {
            addCriterion("modifyCause >", value, "modifyCause");
            return (Criteria) this;
        }

        public Criteria andModifyCauseGreaterThanOrEqualTo(String value) {
            addCriterion("modifyCause >=", value, "modifyCause");
            return (Criteria) this;
        }

        public Criteria andModifyCauseLessThan(String value) {
            addCriterion("modifyCause <", value, "modifyCause");
            return (Criteria) this;
        }

        public Criteria andModifyCauseLessThanOrEqualTo(String value) {
            addCriterion("modifyCause <=", value, "modifyCause");
            return (Criteria) this;
        }

        public Criteria andModifyCauseLike(String value) {
            addCriterion("modifyCause like", value, "modifyCause");
            return (Criteria) this;
        }

        public Criteria andModifyCauseNotLike(String value) {
            addCriterion("modifyCause not like", value, "modifyCause");
            return (Criteria) this;
        }

        public Criteria andModifyCauseIn(List<String> values) {
            addCriterion("modifyCause in", values, "modifyCause");
            return (Criteria) this;
        }

        public Criteria andModifyCauseNotIn(List<String> values) {
            addCriterion("modifyCause not in", values, "modifyCause");
            return (Criteria) this;
        }

        public Criteria andModifyCauseBetween(String value1, String value2) {
            addCriterion("modifyCause between", value1, value2, "modifyCause");
            return (Criteria) this;
        }

        public Criteria andModifyCauseNotBetween(String value1, String value2) {
            addCriterion("modifyCause not between", value1, value2, "modifyCause");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}