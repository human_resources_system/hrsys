package cn.hrsys.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class PositionInformationExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PositionInformationExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andPositionCodeIsNull() {
            addCriterion("positionCode is null");
            return (Criteria) this;
        }

        public Criteria andPositionCodeIsNotNull() {
            addCriterion("positionCode is not null");
            return (Criteria) this;
        }

        public Criteria andPositionCodeEqualTo(String value) {
            addCriterion("positionCode =", value, "positionCode");
            return (Criteria) this;
        }

        public Criteria andPositionCodeNotEqualTo(String value) {
            addCriterion("positionCode <>", value, "positionCode");
            return (Criteria) this;
        }

        public Criteria andPositionCodeGreaterThan(String value) {
            addCriterion("positionCode >", value, "positionCode");
            return (Criteria) this;
        }

        public Criteria andPositionCodeGreaterThanOrEqualTo(String value) {
            addCriterion("positionCode >=", value, "positionCode");
            return (Criteria) this;
        }

        public Criteria andPositionCodeLessThan(String value) {
            addCriterion("positionCode <", value, "positionCode");
            return (Criteria) this;
        }

        public Criteria andPositionCodeLessThanOrEqualTo(String value) {
            addCriterion("positionCode <=", value, "positionCode");
            return (Criteria) this;
        }

        public Criteria andPositionCodeLike(String value) {
            addCriterion("positionCode like", value, "positionCode");
            return (Criteria) this;
        }

        public Criteria andPositionCodeNotLike(String value) {
            addCriterion("positionCode not like", value, "positionCode");
            return (Criteria) this;
        }

        public Criteria andPositionCodeIn(List<String> values) {
            addCriterion("positionCode in", values, "positionCode");
            return (Criteria) this;
        }

        public Criteria andPositionCodeNotIn(List<String> values) {
            addCriterion("positionCode not in", values, "positionCode");
            return (Criteria) this;
        }

        public Criteria andPositionCodeBetween(String value1, String value2) {
            addCriterion("positionCode between", value1, value2, "positionCode");
            return (Criteria) this;
        }

        public Criteria andPositionCodeNotBetween(String value1, String value2) {
            addCriterion("positionCode not between", value1, value2, "positionCode");
            return (Criteria) this;
        }

        public Criteria andPositionShortNameIsNull() {
            addCriterion("positionShortName is null");
            return (Criteria) this;
        }

        public Criteria andPositionShortNameIsNotNull() {
            addCriterion("positionShortName is not null");
            return (Criteria) this;
        }

        public Criteria andPositionShortNameEqualTo(String value) {
            addCriterion("positionShortName =", value, "positionShortName");
            return (Criteria) this;
        }

        public Criteria andPositionShortNameNotEqualTo(String value) {
            addCriterion("positionShortName <>", value, "positionShortName");
            return (Criteria) this;
        }

        public Criteria andPositionShortNameGreaterThan(String value) {
            addCriterion("positionShortName >", value, "positionShortName");
            return (Criteria) this;
        }

        public Criteria andPositionShortNameGreaterThanOrEqualTo(String value) {
            addCriterion("positionShortName >=", value, "positionShortName");
            return (Criteria) this;
        }

        public Criteria andPositionShortNameLessThan(String value) {
            addCriterion("positionShortName <", value, "positionShortName");
            return (Criteria) this;
        }

        public Criteria andPositionShortNameLessThanOrEqualTo(String value) {
            addCriterion("positionShortName <=", value, "positionShortName");
            return (Criteria) this;
        }

        public Criteria andPositionShortNameLike(String value) {
            addCriterion("positionShortName like", value, "positionShortName");
            return (Criteria) this;
        }

        public Criteria andPositionShortNameNotLike(String value) {
            addCriterion("positionShortName not like", value, "positionShortName");
            return (Criteria) this;
        }

        public Criteria andPositionShortNameIn(List<String> values) {
            addCriterion("positionShortName in", values, "positionShortName");
            return (Criteria) this;
        }

        public Criteria andPositionShortNameNotIn(List<String> values) {
            addCriterion("positionShortName not in", values, "positionShortName");
            return (Criteria) this;
        }

        public Criteria andPositionShortNameBetween(String value1, String value2) {
            addCriterion("positionShortName between", value1, value2, "positionShortName");
            return (Criteria) this;
        }

        public Criteria andPositionShortNameNotBetween(String value1, String value2) {
            addCriterion("positionShortName not between", value1, value2, "positionShortName");
            return (Criteria) this;
        }

        public Criteria andPositionNameIsNull() {
            addCriterion("positionName is null");
            return (Criteria) this;
        }

        public Criteria andPositionNameIsNotNull() {
            addCriterion("positionName is not null");
            return (Criteria) this;
        }

        public Criteria andPositionNameEqualTo(String value) {
            addCriterion("positionName =", value, "positionName");
            return (Criteria) this;
        }

        public Criteria andPositionNameNotEqualTo(String value) {
            addCriterion("positionName <>", value, "positionName");
            return (Criteria) this;
        }

        public Criteria andPositionNameGreaterThan(String value) {
            addCriterion("positionName >", value, "positionName");
            return (Criteria) this;
        }

        public Criteria andPositionNameGreaterThanOrEqualTo(String value) {
            addCriterion("positionName >=", value, "positionName");
            return (Criteria) this;
        }

        public Criteria andPositionNameLessThan(String value) {
            addCriterion("positionName <", value, "positionName");
            return (Criteria) this;
        }

        public Criteria andPositionNameLessThanOrEqualTo(String value) {
            addCriterion("positionName <=", value, "positionName");
            return (Criteria) this;
        }

        public Criteria andPositionNameLike(String value) {
            addCriterion("positionName like", value, "positionName");
            return (Criteria) this;
        }

        public Criteria andPositionNameNotLike(String value) {
            addCriterion("positionName not like", value, "positionName");
            return (Criteria) this;
        }

        public Criteria andPositionNameIn(List<String> values) {
            addCriterion("positionName in", values, "positionName");
            return (Criteria) this;
        }

        public Criteria andPositionNameNotIn(List<String> values) {
            addCriterion("positionName not in", values, "positionName");
            return (Criteria) this;
        }

        public Criteria andPositionNameBetween(String value1, String value2) {
            addCriterion("positionName between", value1, value2, "positionName");
            return (Criteria) this;
        }

        public Criteria andPositionNameNotBetween(String value1, String value2) {
            addCriterion("positionName not between", value1, value2, "positionName");
            return (Criteria) this;
        }

        public Criteria andPositionDepIdIsNull() {
            addCriterion("positionDepId is null");
            return (Criteria) this;
        }

        public Criteria andPositionDepIdIsNotNull() {
            addCriterion("positionDepId is not null");
            return (Criteria) this;
        }

        public Criteria andPositionDepIdEqualTo(Integer value) {
            addCriterion("positionDepId =", value, "positionDepId");
            return (Criteria) this;
        }

        public Criteria andPositionDepIdNotEqualTo(Integer value) {
            addCriterion("positionDepId <>", value, "positionDepId");
            return (Criteria) this;
        }

        public Criteria andPositionDepIdGreaterThan(Integer value) {
            addCriterion("positionDepId >", value, "positionDepId");
            return (Criteria) this;
        }

        public Criteria andPositionDepIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("positionDepId >=", value, "positionDepId");
            return (Criteria) this;
        }

        public Criteria andPositionDepIdLessThan(Integer value) {
            addCriterion("positionDepId <", value, "positionDepId");
            return (Criteria) this;
        }

        public Criteria andPositionDepIdLessThanOrEqualTo(Integer value) {
            addCriterion("positionDepId <=", value, "positionDepId");
            return (Criteria) this;
        }

        public Criteria andPositionDepIdIn(List<Integer> values) {
            addCriterion("positionDepId in", values, "positionDepId");
            return (Criteria) this;
        }

        public Criteria andPositionDepIdNotIn(List<Integer> values) {
            addCriterion("positionDepId not in", values, "positionDepId");
            return (Criteria) this;
        }

        public Criteria andPositionDepIdBetween(Integer value1, Integer value2) {
            addCriterion("positionDepId between", value1, value2, "positionDepId");
            return (Criteria) this;
        }

        public Criteria andPositionDepIdNotBetween(Integer value1, Integer value2) {
            addCriterion("positionDepId not between", value1, value2, "positionDepId");
            return (Criteria) this;
        }

        public Criteria andPositionAbstractIsNull() {
            addCriterion("positionAbstract is null");
            return (Criteria) this;
        }

        public Criteria andPositionAbstractIsNotNull() {
            addCriterion("positionAbstract is not null");
            return (Criteria) this;
        }

        public Criteria andPositionAbstractEqualTo(String value) {
            addCriterion("positionAbstract =", value, "positionAbstract");
            return (Criteria) this;
        }

        public Criteria andPositionAbstractNotEqualTo(String value) {
            addCriterion("positionAbstract <>", value, "positionAbstract");
            return (Criteria) this;
        }

        public Criteria andPositionAbstractGreaterThan(String value) {
            addCriterion("positionAbstract >", value, "positionAbstract");
            return (Criteria) this;
        }

        public Criteria andPositionAbstractGreaterThanOrEqualTo(String value) {
            addCriterion("positionAbstract >=", value, "positionAbstract");
            return (Criteria) this;
        }

        public Criteria andPositionAbstractLessThan(String value) {
            addCriterion("positionAbstract <", value, "positionAbstract");
            return (Criteria) this;
        }

        public Criteria andPositionAbstractLessThanOrEqualTo(String value) {
            addCriterion("positionAbstract <=", value, "positionAbstract");
            return (Criteria) this;
        }

        public Criteria andPositionAbstractLike(String value) {
            addCriterion("positionAbstract like", value, "positionAbstract");
            return (Criteria) this;
        }

        public Criteria andPositionAbstractNotLike(String value) {
            addCriterion("positionAbstract not like", value, "positionAbstract");
            return (Criteria) this;
        }

        public Criteria andPositionAbstractIn(List<String> values) {
            addCriterion("positionAbstract in", values, "positionAbstract");
            return (Criteria) this;
        }

        public Criteria andPositionAbstractNotIn(List<String> values) {
            addCriterion("positionAbstract not in", values, "positionAbstract");
            return (Criteria) this;
        }

        public Criteria andPositionAbstractBetween(String value1, String value2) {
            addCriterion("positionAbstract between", value1, value2, "positionAbstract");
            return (Criteria) this;
        }

        public Criteria andPositionAbstractNotBetween(String value1, String value2) {
            addCriterion("positionAbstract not between", value1, value2, "positionAbstract");
            return (Criteria) this;
        }

        public Criteria andPositionTypeIsNull() {
            addCriterion("positionType is null");
            return (Criteria) this;
        }

        public Criteria andPositionTypeIsNotNull() {
            addCriterion("positionType is not null");
            return (Criteria) this;
        }

        public Criteria andPositionTypeEqualTo(Integer value) {
            addCriterion("positionType =", value, "positionType");
            return (Criteria) this;
        }

        public Criteria andPositionTypeNotEqualTo(Integer value) {
            addCriterion("positionType <>", value, "positionType");
            return (Criteria) this;
        }

        public Criteria andPositionTypeGreaterThan(Integer value) {
            addCriterion("positionType >", value, "positionType");
            return (Criteria) this;
        }

        public Criteria andPositionTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("positionType >=", value, "positionType");
            return (Criteria) this;
        }

        public Criteria andPositionTypeLessThan(Integer value) {
            addCriterion("positionType <", value, "positionType");
            return (Criteria) this;
        }

        public Criteria andPositionTypeLessThanOrEqualTo(Integer value) {
            addCriterion("positionType <=", value, "positionType");
            return (Criteria) this;
        }

        public Criteria andPositionTypeIn(List<Integer> values) {
            addCriterion("positionType in", values, "positionType");
            return (Criteria) this;
        }

        public Criteria andPositionTypeNotIn(List<Integer> values) {
            addCriterion("positionType not in", values, "positionType");
            return (Criteria) this;
        }

        public Criteria andPositionTypeBetween(Integer value1, Integer value2) {
            addCriterion("positionType between", value1, value2, "positionType");
            return (Criteria) this;
        }

        public Criteria andPositionTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("positionType not between", value1, value2, "positionType");
            return (Criteria) this;
        }

        public Criteria andPositionRemarkIsNull() {
            addCriterion("positionRemark is null");
            return (Criteria) this;
        }

        public Criteria andPositionRemarkIsNotNull() {
            addCriterion("positionRemark is not null");
            return (Criteria) this;
        }

        public Criteria andPositionRemarkEqualTo(String value) {
            addCriterion("positionRemark =", value, "positionRemark");
            return (Criteria) this;
        }

        public Criteria andPositionRemarkNotEqualTo(String value) {
            addCriterion("positionRemark <>", value, "positionRemark");
            return (Criteria) this;
        }

        public Criteria andPositionRemarkGreaterThan(String value) {
            addCriterion("positionRemark >", value, "positionRemark");
            return (Criteria) this;
        }

        public Criteria andPositionRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("positionRemark >=", value, "positionRemark");
            return (Criteria) this;
        }

        public Criteria andPositionRemarkLessThan(String value) {
            addCriterion("positionRemark <", value, "positionRemark");
            return (Criteria) this;
        }

        public Criteria andPositionRemarkLessThanOrEqualTo(String value) {
            addCriterion("positionRemark <=", value, "positionRemark");
            return (Criteria) this;
        }

        public Criteria andPositionRemarkLike(String value) {
            addCriterion("positionRemark like", value, "positionRemark");
            return (Criteria) this;
        }

        public Criteria andPositionRemarkNotLike(String value) {
            addCriterion("positionRemark not like", value, "positionRemark");
            return (Criteria) this;
        }

        public Criteria andPositionRemarkIn(List<String> values) {
            addCriterion("positionRemark in", values, "positionRemark");
            return (Criteria) this;
        }

        public Criteria andPositionRemarkNotIn(List<String> values) {
            addCriterion("positionRemark not in", values, "positionRemark");
            return (Criteria) this;
        }

        public Criteria andPositionRemarkBetween(String value1, String value2) {
            addCriterion("positionRemark between", value1, value2, "positionRemark");
            return (Criteria) this;
        }

        public Criteria andPositionRemarkNotBetween(String value1, String value2) {
            addCriterion("positionRemark not between", value1, value2, "positionRemark");
            return (Criteria) this;
        }

        public Criteria andPositionStatusIsNull() {
            addCriterion("positionStatus is null");
            return (Criteria) this;
        }

        public Criteria andPositionStatusIsNotNull() {
            addCriterion("positionStatus is not null");
            return (Criteria) this;
        }

        public Criteria andPositionStatusEqualTo(Integer value) {
            addCriterion("positionStatus =", value, "positionStatus");
            return (Criteria) this;
        }

        public Criteria andPositionStatusNotEqualTo(Integer value) {
            addCriterion("positionStatus <>", value, "positionStatus");
            return (Criteria) this;
        }

        public Criteria andPositionStatusGreaterThan(Integer value) {
            addCriterion("positionStatus >", value, "positionStatus");
            return (Criteria) this;
        }

        public Criteria andPositionStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("positionStatus >=", value, "positionStatus");
            return (Criteria) this;
        }

        public Criteria andPositionStatusLessThan(Integer value) {
            addCriterion("positionStatus <", value, "positionStatus");
            return (Criteria) this;
        }

        public Criteria andPositionStatusLessThanOrEqualTo(Integer value) {
            addCriterion("positionStatus <=", value, "positionStatus");
            return (Criteria) this;
        }

        public Criteria andPositionStatusIn(List<Integer> values) {
            addCriterion("positionStatus in", values, "positionStatus");
            return (Criteria) this;
        }

        public Criteria andPositionStatusNotIn(List<Integer> values) {
            addCriterion("positionStatus not in", values, "positionStatus");
            return (Criteria) this;
        }

        public Criteria andPositionStatusBetween(Integer value1, Integer value2) {
            addCriterion("positionStatus between", value1, value2, "positionStatus");
            return (Criteria) this;
        }

        public Criteria andPositionStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("positionStatus not between", value1, value2, "positionStatus");
            return (Criteria) this;
        }

        public Criteria andCreationByIsNull() {
            addCriterion("creationBy is null");
            return (Criteria) this;
        }

        public Criteria andCreationByIsNotNull() {
            addCriterion("creationBy is not null");
            return (Criteria) this;
        }

        public Criteria andCreationByEqualTo(Integer value) {
            addCriterion("creationBy =", value, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByNotEqualTo(Integer value) {
            addCriterion("creationBy <>", value, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByGreaterThan(Integer value) {
            addCriterion("creationBy >", value, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByGreaterThanOrEqualTo(Integer value) {
            addCriterion("creationBy >=", value, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByLessThan(Integer value) {
            addCriterion("creationBy <", value, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByLessThanOrEqualTo(Integer value) {
            addCriterion("creationBy <=", value, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByIn(List<Integer> values) {
            addCriterion("creationBy in", values, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByNotIn(List<Integer> values) {
            addCriterion("creationBy not in", values, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByBetween(Integer value1, Integer value2) {
            addCriterion("creationBy between", value1, value2, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByNotBetween(Integer value1, Integer value2) {
            addCriterion("creationBy not between", value1, value2, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationDateIsNull() {
            addCriterion("creationDate is null");
            return (Criteria) this;
        }

        public Criteria andCreationDateIsNotNull() {
            addCriterion("creationDate is not null");
            return (Criteria) this;
        }

        public Criteria andCreationDateEqualTo(Date value) {
            addCriterionForJDBCDate("creationDate =", value, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("creationDate <>", value, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateGreaterThan(Date value) {
            addCriterionForJDBCDate("creationDate >", value, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("creationDate >=", value, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateLessThan(Date value) {
            addCriterionForJDBCDate("creationDate <", value, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("creationDate <=", value, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateIn(List<Date> values) {
            addCriterionForJDBCDate("creationDate in", values, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("creationDate not in", values, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("creationDate between", value1, value2, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("creationDate not between", value1, value2, "creationDate");
            return (Criteria) this;
        }

        public Criteria andModifyByIsNull() {
            addCriterion("modifyBy is null");
            return (Criteria) this;
        }

        public Criteria andModifyByIsNotNull() {
            addCriterion("modifyBy is not null");
            return (Criteria) this;
        }

        public Criteria andModifyByEqualTo(Integer value) {
            addCriterion("modifyBy =", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByNotEqualTo(Integer value) {
            addCriterion("modifyBy <>", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByGreaterThan(Integer value) {
            addCriterion("modifyBy >", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByGreaterThanOrEqualTo(Integer value) {
            addCriterion("modifyBy >=", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByLessThan(Integer value) {
            addCriterion("modifyBy <", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByLessThanOrEqualTo(Integer value) {
            addCriterion("modifyBy <=", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByIn(List<Integer> values) {
            addCriterion("modifyBy in", values, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByNotIn(List<Integer> values) {
            addCriterion("modifyBy not in", values, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByBetween(Integer value1, Integer value2) {
            addCriterion("modifyBy between", value1, value2, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByNotBetween(Integer value1, Integer value2) {
            addCriterion("modifyBy not between", value1, value2, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyDateIsNull() {
            addCriterion("modifyDate is null");
            return (Criteria) this;
        }

        public Criteria andModifyDateIsNotNull() {
            addCriterion("modifyDate is not null");
            return (Criteria) this;
        }

        public Criteria andModifyDateEqualTo(Date value) {
            addCriterionForJDBCDate("modifyDate =", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("modifyDate <>", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateGreaterThan(Date value) {
            addCriterionForJDBCDate("modifyDate >", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("modifyDate >=", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateLessThan(Date value) {
            addCriterionForJDBCDate("modifyDate <", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("modifyDate <=", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateIn(List<Date> values) {
            addCriterionForJDBCDate("modifyDate in", values, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("modifyDate not in", values, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("modifyDate between", value1, value2, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("modifyDate not between", value1, value2, "modifyDate");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}