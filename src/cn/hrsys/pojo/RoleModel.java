package cn.hrsys.pojo;

public class RoleModel {
    private String roleID;

    private String modelID;

    public String getRoleID() {
        return roleID;
    }

    public void setRoleID(String roleID) {
        this.roleID = roleID == null ? null : roleID.trim();
    }

    public String getModelID() {
        return modelID;
    }

    public void setModelID(String modelID) {
        this.modelID = modelID == null ? null : modelID.trim();
    }
}