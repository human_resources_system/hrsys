package cn.hrsys.pojo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class AttendanceInformationExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public AttendanceInformationExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andOutworkDateIsNull() {
            addCriterion("outworkDate is null");
            return (Criteria) this;
        }

        public Criteria andOutworkDateIsNotNull() {
            addCriterion("outworkDate is not null");
            return (Criteria) this;
        }

        public Criteria andOutworkDateEqualTo(Date value) {
            addCriterionForJDBCDate("outworkDate =", value, "outworkDate");
            return (Criteria) this;
        }

        public Criteria andOutworkDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("outworkDate <>", value, "outworkDate");
            return (Criteria) this;
        }

        public Criteria andOutworkDateGreaterThan(Date value) {
            addCriterionForJDBCDate("outworkDate >", value, "outworkDate");
            return (Criteria) this;
        }

        public Criteria andOutworkDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("outworkDate >=", value, "outworkDate");
            return (Criteria) this;
        }

        public Criteria andOutworkDateLessThan(Date value) {
            addCriterionForJDBCDate("outworkDate <", value, "outworkDate");
            return (Criteria) this;
        }

        public Criteria andOutworkDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("outworkDate <=", value, "outworkDate");
            return (Criteria) this;
        }

        public Criteria andOutworkDateIn(List<Date> values) {
            addCriterionForJDBCDate("outworkDate in", values, "outworkDate");
            return (Criteria) this;
        }

        public Criteria andOutworkDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("outworkDate not in", values, "outworkDate");
            return (Criteria) this;
        }

        public Criteria andOutworkDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("outworkDate between", value1, value2, "outworkDate");
            return (Criteria) this;
        }

        public Criteria andOutworkDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("outworkDate not between", value1, value2, "outworkDate");
            return (Criteria) this;
        }

        public Criteria andQuitworkDateIsNull() {
            addCriterion("quitworkDate is null");
            return (Criteria) this;
        }

        public Criteria andQuitworkDateIsNotNull() {
            addCriterion("quitworkDate is not null");
            return (Criteria) this;
        }

        public Criteria andQuitworkDateEqualTo(Date value) {
            addCriterionForJDBCDate("quitworkDate =", value, "quitworkDate");
            return (Criteria) this;
        }

        public Criteria andQuitworkDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("quitworkDate <>", value, "quitworkDate");
            return (Criteria) this;
        }

        public Criteria andQuitworkDateGreaterThan(Date value) {
            addCriterionForJDBCDate("quitworkDate >", value, "quitworkDate");
            return (Criteria) this;
        }

        public Criteria andQuitworkDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("quitworkDate >=", value, "quitworkDate");
            return (Criteria) this;
        }

        public Criteria andQuitworkDateLessThan(Date value) {
            addCriterionForJDBCDate("quitworkDate <", value, "quitworkDate");
            return (Criteria) this;
        }

        public Criteria andQuitworkDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("quitworkDate <=", value, "quitworkDate");
            return (Criteria) this;
        }

        public Criteria andQuitworkDateIn(List<Date> values) {
            addCriterionForJDBCDate("quitworkDate in", values, "quitworkDate");
            return (Criteria) this;
        }

        public Criteria andQuitworkDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("quitworkDate not in", values, "quitworkDate");
            return (Criteria) this;
        }

        public Criteria andQuitworkDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("quitworkDate between", value1, value2, "quitworkDate");
            return (Criteria) this;
        }

        public Criteria andQuitworkDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("quitworkDate not between", value1, value2, "quitworkDate");
            return (Criteria) this;
        }

        public Criteria andOverbeginTimeIsNull() {
            addCriterion("overbeginTime is null");
            return (Criteria) this;
        }

        public Criteria andOverbeginTimeIsNotNull() {
            addCriterion("overbeginTime is not null");
            return (Criteria) this;
        }

        public Criteria andOverbeginTimeEqualTo(Date value) {
            addCriterionForJDBCDate("overbeginTime =", value, "overbeginTime");
            return (Criteria) this;
        }

        public Criteria andOverbeginTimeNotEqualTo(Date value) {
            addCriterionForJDBCDate("overbeginTime <>", value, "overbeginTime");
            return (Criteria) this;
        }

        public Criteria andOverbeginTimeGreaterThan(Date value) {
            addCriterionForJDBCDate("overbeginTime >", value, "overbeginTime");
            return (Criteria) this;
        }

        public Criteria andOverbeginTimeGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("overbeginTime >=", value, "overbeginTime");
            return (Criteria) this;
        }

        public Criteria andOverbeginTimeLessThan(Date value) {
            addCriterionForJDBCDate("overbeginTime <", value, "overbeginTime");
            return (Criteria) this;
        }

        public Criteria andOverbeginTimeLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("overbeginTime <=", value, "overbeginTime");
            return (Criteria) this;
        }

        public Criteria andOverbeginTimeIn(List<Date> values) {
            addCriterionForJDBCDate("overbeginTime in", values, "overbeginTime");
            return (Criteria) this;
        }

        public Criteria andOverbeginTimeNotIn(List<Date> values) {
            addCriterionForJDBCDate("overbeginTime not in", values, "overbeginTime");
            return (Criteria) this;
        }

        public Criteria andOverbeginTimeBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("overbeginTime between", value1, value2, "overbeginTime");
            return (Criteria) this;
        }

        public Criteria andOverbeginTimeNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("overbeginTime not between", value1, value2, "overbeginTime");
            return (Criteria) this;
        }

        public Criteria andOverendTimeIsNull() {
            addCriterion("overendTime is null");
            return (Criteria) this;
        }

        public Criteria andOverendTimeIsNotNull() {
            addCriterion("overendTime is not null");
            return (Criteria) this;
        }

        public Criteria andOverendTimeEqualTo(Date value) {
            addCriterionForJDBCDate("overendTime =", value, "overendTime");
            return (Criteria) this;
        }

        public Criteria andOverendTimeNotEqualTo(Date value) {
            addCriterionForJDBCDate("overendTime <>", value, "overendTime");
            return (Criteria) this;
        }

        public Criteria andOverendTimeGreaterThan(Date value) {
            addCriterionForJDBCDate("overendTime >", value, "overendTime");
            return (Criteria) this;
        }

        public Criteria andOverendTimeGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("overendTime >=", value, "overendTime");
            return (Criteria) this;
        }

        public Criteria andOverendTimeLessThan(Date value) {
            addCriterionForJDBCDate("overendTime <", value, "overendTime");
            return (Criteria) this;
        }

        public Criteria andOverendTimeLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("overendTime <=", value, "overendTime");
            return (Criteria) this;
        }

        public Criteria andOverendTimeIn(List<Date> values) {
            addCriterionForJDBCDate("overendTime in", values, "overendTime");
            return (Criteria) this;
        }

        public Criteria andOverendTimeNotIn(List<Date> values) {
            addCriterionForJDBCDate("overendTime not in", values, "overendTime");
            return (Criteria) this;
        }

        public Criteria andOverendTimeBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("overendTime between", value1, value2, "overendTime");
            return (Criteria) this;
        }

        public Criteria andOverendTimeNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("overendTime not between", value1, value2, "overendTime");
            return (Criteria) this;
        }

        public Criteria andOvertimeAllowanceIsNull() {
            addCriterion("overtimeAllowance is null");
            return (Criteria) this;
        }

        public Criteria andOvertimeAllowanceIsNotNull() {
            addCriterion("overtimeAllowance is not null");
            return (Criteria) this;
        }

        public Criteria andOvertimeAllowanceEqualTo(BigDecimal value) {
            addCriterion("overtimeAllowance =", value, "overtimeAllowance");
            return (Criteria) this;
        }

        public Criteria andOvertimeAllowanceNotEqualTo(BigDecimal value) {
            addCriterion("overtimeAllowance <>", value, "overtimeAllowance");
            return (Criteria) this;
        }

        public Criteria andOvertimeAllowanceGreaterThan(BigDecimal value) {
            addCriterion("overtimeAllowance >", value, "overtimeAllowance");
            return (Criteria) this;
        }

        public Criteria andOvertimeAllowanceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("overtimeAllowance >=", value, "overtimeAllowance");
            return (Criteria) this;
        }

        public Criteria andOvertimeAllowanceLessThan(BigDecimal value) {
            addCriterion("overtimeAllowance <", value, "overtimeAllowance");
            return (Criteria) this;
        }

        public Criteria andOvertimeAllowanceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("overtimeAllowance <=", value, "overtimeAllowance");
            return (Criteria) this;
        }

        public Criteria andOvertimeAllowanceIn(List<BigDecimal> values) {
            addCriterion("overtimeAllowance in", values, "overtimeAllowance");
            return (Criteria) this;
        }

        public Criteria andOvertimeAllowanceNotIn(List<BigDecimal> values) {
            addCriterion("overtimeAllowance not in", values, "overtimeAllowance");
            return (Criteria) this;
        }

        public Criteria andOvertimeAllowanceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("overtimeAllowance between", value1, value2, "overtimeAllowance");
            return (Criteria) this;
        }

        public Criteria andOvertimeAllowanceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("overtimeAllowance not between", value1, value2, "overtimeAllowance");
            return (Criteria) this;
        }

        public Criteria andOvertimeReasonIsNull() {
            addCriterion("overtimeReason is null");
            return (Criteria) this;
        }

        public Criteria andOvertimeReasonIsNotNull() {
            addCriterion("overtimeReason is not null");
            return (Criteria) this;
        }

        public Criteria andOvertimeReasonEqualTo(String value) {
            addCriterion("overtimeReason =", value, "overtimeReason");
            return (Criteria) this;
        }

        public Criteria andOvertimeReasonNotEqualTo(String value) {
            addCriterion("overtimeReason <>", value, "overtimeReason");
            return (Criteria) this;
        }

        public Criteria andOvertimeReasonGreaterThan(String value) {
            addCriterion("overtimeReason >", value, "overtimeReason");
            return (Criteria) this;
        }

        public Criteria andOvertimeReasonGreaterThanOrEqualTo(String value) {
            addCriterion("overtimeReason >=", value, "overtimeReason");
            return (Criteria) this;
        }

        public Criteria andOvertimeReasonLessThan(String value) {
            addCriterion("overtimeReason <", value, "overtimeReason");
            return (Criteria) this;
        }

        public Criteria andOvertimeReasonLessThanOrEqualTo(String value) {
            addCriterion("overtimeReason <=", value, "overtimeReason");
            return (Criteria) this;
        }

        public Criteria andOvertimeReasonLike(String value) {
            addCriterion("overtimeReason like", value, "overtimeReason");
            return (Criteria) this;
        }

        public Criteria andOvertimeReasonNotLike(String value) {
            addCriterion("overtimeReason not like", value, "overtimeReason");
            return (Criteria) this;
        }

        public Criteria andOvertimeReasonIn(List<String> values) {
            addCriterion("overtimeReason in", values, "overtimeReason");
            return (Criteria) this;
        }

        public Criteria andOvertimeReasonNotIn(List<String> values) {
            addCriterion("overtimeReason not in", values, "overtimeReason");
            return (Criteria) this;
        }

        public Criteria andOvertimeReasonBetween(String value1, String value2) {
            addCriterion("overtimeReason between", value1, value2, "overtimeReason");
            return (Criteria) this;
        }

        public Criteria andOvertimeReasonNotBetween(String value1, String value2) {
            addCriterion("overtimeReason not between", value1, value2, "overtimeReason");
            return (Criteria) this;
        }

        public Criteria andLeavebeginDateIsNull() {
            addCriterion("leavebeginDate is null");
            return (Criteria) this;
        }

        public Criteria andLeavebeginDateIsNotNull() {
            addCriterion("leavebeginDate is not null");
            return (Criteria) this;
        }

        public Criteria andLeavebeginDateEqualTo(Date value) {
            addCriterionForJDBCDate("leavebeginDate =", value, "leavebeginDate");
            return (Criteria) this;
        }

        public Criteria andLeavebeginDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("leavebeginDate <>", value, "leavebeginDate");
            return (Criteria) this;
        }

        public Criteria andLeavebeginDateGreaterThan(Date value) {
            addCriterionForJDBCDate("leavebeginDate >", value, "leavebeginDate");
            return (Criteria) this;
        }

        public Criteria andLeavebeginDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("leavebeginDate >=", value, "leavebeginDate");
            return (Criteria) this;
        }

        public Criteria andLeavebeginDateLessThan(Date value) {
            addCriterionForJDBCDate("leavebeginDate <", value, "leavebeginDate");
            return (Criteria) this;
        }

        public Criteria andLeavebeginDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("leavebeginDate <=", value, "leavebeginDate");
            return (Criteria) this;
        }

        public Criteria andLeavebeginDateIn(List<Date> values) {
            addCriterionForJDBCDate("leavebeginDate in", values, "leavebeginDate");
            return (Criteria) this;
        }

        public Criteria andLeavebeginDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("leavebeginDate not in", values, "leavebeginDate");
            return (Criteria) this;
        }

        public Criteria andLeavebeginDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("leavebeginDate between", value1, value2, "leavebeginDate");
            return (Criteria) this;
        }

        public Criteria andLeavebeginDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("leavebeginDate not between", value1, value2, "leavebeginDate");
            return (Criteria) this;
        }

        public Criteria andLeaveendDateIsNull() {
            addCriterion("leaveendDate is null");
            return (Criteria) this;
        }

        public Criteria andLeaveendDateIsNotNull() {
            addCriterion("leaveendDate is not null");
            return (Criteria) this;
        }

        public Criteria andLeaveendDateEqualTo(Date value) {
            addCriterionForJDBCDate("leaveendDate =", value, "leaveendDate");
            return (Criteria) this;
        }

        public Criteria andLeaveendDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("leaveendDate <>", value, "leaveendDate");
            return (Criteria) this;
        }

        public Criteria andLeaveendDateGreaterThan(Date value) {
            addCriterionForJDBCDate("leaveendDate >", value, "leaveendDate");
            return (Criteria) this;
        }

        public Criteria andLeaveendDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("leaveendDate >=", value, "leaveendDate");
            return (Criteria) this;
        }

        public Criteria andLeaveendDateLessThan(Date value) {
            addCriterionForJDBCDate("leaveendDate <", value, "leaveendDate");
            return (Criteria) this;
        }

        public Criteria andLeaveendDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("leaveendDate <=", value, "leaveendDate");
            return (Criteria) this;
        }

        public Criteria andLeaveendDateIn(List<Date> values) {
            addCriterionForJDBCDate("leaveendDate in", values, "leaveendDate");
            return (Criteria) this;
        }

        public Criteria andLeaveendDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("leaveendDate not in", values, "leaveendDate");
            return (Criteria) this;
        }

        public Criteria andLeaveendDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("leaveendDate between", value1, value2, "leaveendDate");
            return (Criteria) this;
        }

        public Criteria andLeaveendDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("leaveendDate not between", value1, value2, "leaveendDate");
            return (Criteria) this;
        }

        public Criteria andLeaveReasonIsNull() {
            addCriterion("leaveReason is null");
            return (Criteria) this;
        }

        public Criteria andLeaveReasonIsNotNull() {
            addCriterion("leaveReason is not null");
            return (Criteria) this;
        }

        public Criteria andLeaveReasonEqualTo(String value) {
            addCriterion("leaveReason =", value, "leaveReason");
            return (Criteria) this;
        }

        public Criteria andLeaveReasonNotEqualTo(String value) {
            addCriterion("leaveReason <>", value, "leaveReason");
            return (Criteria) this;
        }

        public Criteria andLeaveReasonGreaterThan(String value) {
            addCriterion("leaveReason >", value, "leaveReason");
            return (Criteria) this;
        }

        public Criteria andLeaveReasonGreaterThanOrEqualTo(String value) {
            addCriterion("leaveReason >=", value, "leaveReason");
            return (Criteria) this;
        }

        public Criteria andLeaveReasonLessThan(String value) {
            addCriterion("leaveReason <", value, "leaveReason");
            return (Criteria) this;
        }

        public Criteria andLeaveReasonLessThanOrEqualTo(String value) {
            addCriterion("leaveReason <=", value, "leaveReason");
            return (Criteria) this;
        }

        public Criteria andLeaveReasonLike(String value) {
            addCriterion("leaveReason like", value, "leaveReason");
            return (Criteria) this;
        }

        public Criteria andLeaveReasonNotLike(String value) {
            addCriterion("leaveReason not like", value, "leaveReason");
            return (Criteria) this;
        }

        public Criteria andLeaveReasonIn(List<String> values) {
            addCriterion("leaveReason in", values, "leaveReason");
            return (Criteria) this;
        }

        public Criteria andLeaveReasonNotIn(List<String> values) {
            addCriterion("leaveReason not in", values, "leaveReason");
            return (Criteria) this;
        }

        public Criteria andLeaveReasonBetween(String value1, String value2) {
            addCriterion("leaveReason between", value1, value2, "leaveReason");
            return (Criteria) this;
        }

        public Criteria andLeaveReasonNotBetween(String value1, String value2) {
            addCriterion("leaveReason not between", value1, value2, "leaveReason");
            return (Criteria) this;
        }

        public Criteria andEvectionbeginDateIsNull() {
            addCriterion("evectionbeginDate is null");
            return (Criteria) this;
        }

        public Criteria andEvectionbeginDateIsNotNull() {
            addCriterion("evectionbeginDate is not null");
            return (Criteria) this;
        }

        public Criteria andEvectionbeginDateEqualTo(Date value) {
            addCriterionForJDBCDate("evectionbeginDate =", value, "evectionbeginDate");
            return (Criteria) this;
        }

        public Criteria andEvectionbeginDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("evectionbeginDate <>", value, "evectionbeginDate");
            return (Criteria) this;
        }

        public Criteria andEvectionbeginDateGreaterThan(Date value) {
            addCriterionForJDBCDate("evectionbeginDate >", value, "evectionbeginDate");
            return (Criteria) this;
        }

        public Criteria andEvectionbeginDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("evectionbeginDate >=", value, "evectionbeginDate");
            return (Criteria) this;
        }

        public Criteria andEvectionbeginDateLessThan(Date value) {
            addCriterionForJDBCDate("evectionbeginDate <", value, "evectionbeginDate");
            return (Criteria) this;
        }

        public Criteria andEvectionbeginDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("evectionbeginDate <=", value, "evectionbeginDate");
            return (Criteria) this;
        }

        public Criteria andEvectionbeginDateIn(List<Date> values) {
            addCriterionForJDBCDate("evectionbeginDate in", values, "evectionbeginDate");
            return (Criteria) this;
        }

        public Criteria andEvectionbeginDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("evectionbeginDate not in", values, "evectionbeginDate");
            return (Criteria) this;
        }

        public Criteria andEvectionbeginDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("evectionbeginDate between", value1, value2, "evectionbeginDate");
            return (Criteria) this;
        }

        public Criteria andEvectionbeginDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("evectionbeginDate not between", value1, value2, "evectionbeginDate");
            return (Criteria) this;
        }

        public Criteria andEvectionendDateIsNull() {
            addCriterion("evectionendDate is null");
            return (Criteria) this;
        }

        public Criteria andEvectionendDateIsNotNull() {
            addCriterion("evectionendDate is not null");
            return (Criteria) this;
        }

        public Criteria andEvectionendDateEqualTo(Date value) {
            addCriterionForJDBCDate("evectionendDate =", value, "evectionendDate");
            return (Criteria) this;
        }

        public Criteria andEvectionendDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("evectionendDate <>", value, "evectionendDate");
            return (Criteria) this;
        }

        public Criteria andEvectionendDateGreaterThan(Date value) {
            addCriterionForJDBCDate("evectionendDate >", value, "evectionendDate");
            return (Criteria) this;
        }

        public Criteria andEvectionendDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("evectionendDate >=", value, "evectionendDate");
            return (Criteria) this;
        }

        public Criteria andEvectionendDateLessThan(Date value) {
            addCriterionForJDBCDate("evectionendDate <", value, "evectionendDate");
            return (Criteria) this;
        }

        public Criteria andEvectionendDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("evectionendDate <=", value, "evectionendDate");
            return (Criteria) this;
        }

        public Criteria andEvectionendDateIn(List<Date> values) {
            addCriterionForJDBCDate("evectionendDate in", values, "evectionendDate");
            return (Criteria) this;
        }

        public Criteria andEvectionendDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("evectionendDate not in", values, "evectionendDate");
            return (Criteria) this;
        }

        public Criteria andEvectionendDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("evectionendDate between", value1, value2, "evectionendDate");
            return (Criteria) this;
        }

        public Criteria andEvectionendDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("evectionendDate not between", value1, value2, "evectionendDate");
            return (Criteria) this;
        }

        public Criteria andEvectionReasonIsNull() {
            addCriterion("evectionReason is null");
            return (Criteria) this;
        }

        public Criteria andEvectionReasonIsNotNull() {
            addCriterion("evectionReason is not null");
            return (Criteria) this;
        }

        public Criteria andEvectionReasonEqualTo(String value) {
            addCriterion("evectionReason =", value, "evectionReason");
            return (Criteria) this;
        }

        public Criteria andEvectionReasonNotEqualTo(String value) {
            addCriterion("evectionReason <>", value, "evectionReason");
            return (Criteria) this;
        }

        public Criteria andEvectionReasonGreaterThan(String value) {
            addCriterion("evectionReason >", value, "evectionReason");
            return (Criteria) this;
        }

        public Criteria andEvectionReasonGreaterThanOrEqualTo(String value) {
            addCriterion("evectionReason >=", value, "evectionReason");
            return (Criteria) this;
        }

        public Criteria andEvectionReasonLessThan(String value) {
            addCriterion("evectionReason <", value, "evectionReason");
            return (Criteria) this;
        }

        public Criteria andEvectionReasonLessThanOrEqualTo(String value) {
            addCriterion("evectionReason <=", value, "evectionReason");
            return (Criteria) this;
        }

        public Criteria andEvectionReasonLike(String value) {
            addCriterion("evectionReason like", value, "evectionReason");
            return (Criteria) this;
        }

        public Criteria andEvectionReasonNotLike(String value) {
            addCriterion("evectionReason not like", value, "evectionReason");
            return (Criteria) this;
        }

        public Criteria andEvectionReasonIn(List<String> values) {
            addCriterion("evectionReason in", values, "evectionReason");
            return (Criteria) this;
        }

        public Criteria andEvectionReasonNotIn(List<String> values) {
            addCriterion("evectionReason not in", values, "evectionReason");
            return (Criteria) this;
        }

        public Criteria andEvectionReasonBetween(String value1, String value2) {
            addCriterion("evectionReason between", value1, value2, "evectionReason");
            return (Criteria) this;
        }

        public Criteria andEvectionReasonNotBetween(String value1, String value2) {
            addCriterion("evectionReason not between", value1, value2, "evectionReason");
            return (Criteria) this;
        }

        public Criteria andEvectionAllowanceIsNull() {
            addCriterion("evectionAllowance is null");
            return (Criteria) this;
        }

        public Criteria andEvectionAllowanceIsNotNull() {
            addCriterion("evectionAllowance is not null");
            return (Criteria) this;
        }

        public Criteria andEvectionAllowanceEqualTo(BigDecimal value) {
            addCriterion("evectionAllowance =", value, "evectionAllowance");
            return (Criteria) this;
        }

        public Criteria andEvectionAllowanceNotEqualTo(BigDecimal value) {
            addCriterion("evectionAllowance <>", value, "evectionAllowance");
            return (Criteria) this;
        }

        public Criteria andEvectionAllowanceGreaterThan(BigDecimal value) {
            addCriterion("evectionAllowance >", value, "evectionAllowance");
            return (Criteria) this;
        }

        public Criteria andEvectionAllowanceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("evectionAllowance >=", value, "evectionAllowance");
            return (Criteria) this;
        }

        public Criteria andEvectionAllowanceLessThan(BigDecimal value) {
            addCriterion("evectionAllowance <", value, "evectionAllowance");
            return (Criteria) this;
        }

        public Criteria andEvectionAllowanceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("evectionAllowance <=", value, "evectionAllowance");
            return (Criteria) this;
        }

        public Criteria andEvectionAllowanceIn(List<BigDecimal> values) {
            addCriterion("evectionAllowance in", values, "evectionAllowance");
            return (Criteria) this;
        }

        public Criteria andEvectionAllowanceNotIn(List<BigDecimal> values) {
            addCriterion("evectionAllowance not in", values, "evectionAllowance");
            return (Criteria) this;
        }

        public Criteria andEvectionAllowanceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("evectionAllowance between", value1, value2, "evectionAllowance");
            return (Criteria) this;
        }

        public Criteria andEvectionAllowanceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("evectionAllowance not between", value1, value2, "evectionAllowance");
            return (Criteria) this;
        }

        public Criteria andAttendanceStatusIsNull() {
            addCriterion("attendanceStatus is null");
            return (Criteria) this;
        }

        public Criteria andAttendanceStatusIsNotNull() {
            addCriterion("attendanceStatus is not null");
            return (Criteria) this;
        }

        public Criteria andAttendanceStatusEqualTo(Integer value) {
            addCriterion("attendanceStatus =", value, "attendanceStatus");
            return (Criteria) this;
        }

        public Criteria andAttendanceStatusNotEqualTo(Integer value) {
            addCriterion("attendanceStatus <>", value, "attendanceStatus");
            return (Criteria) this;
        }

        public Criteria andAttendanceStatusGreaterThan(Integer value) {
            addCriterion("attendanceStatus >", value, "attendanceStatus");
            return (Criteria) this;
        }

        public Criteria andAttendanceStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("attendanceStatus >=", value, "attendanceStatus");
            return (Criteria) this;
        }

        public Criteria andAttendanceStatusLessThan(Integer value) {
            addCriterion("attendanceStatus <", value, "attendanceStatus");
            return (Criteria) this;
        }

        public Criteria andAttendanceStatusLessThanOrEqualTo(Integer value) {
            addCriterion("attendanceStatus <=", value, "attendanceStatus");
            return (Criteria) this;
        }

        public Criteria andAttendanceStatusIn(List<Integer> values) {
            addCriterion("attendanceStatus in", values, "attendanceStatus");
            return (Criteria) this;
        }

        public Criteria andAttendanceStatusNotIn(List<Integer> values) {
            addCriterion("attendanceStatus not in", values, "attendanceStatus");
            return (Criteria) this;
        }

        public Criteria andAttendanceStatusBetween(Integer value1, Integer value2) {
            addCriterion("attendanceStatus between", value1, value2, "attendanceStatus");
            return (Criteria) this;
        }

        public Criteria andAttendanceStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("attendanceStatus not between", value1, value2, "attendanceStatus");
            return (Criteria) this;
        }

        public Criteria andWorkingDateIsNull() {
            addCriterion("workingDate is null");
            return (Criteria) this;
        }

        public Criteria andWorkingDateIsNotNull() {
            addCriterion("workingDate is not null");
            return (Criteria) this;
        }

        public Criteria andWorkingDateEqualTo(Date value) {
            addCriterionForJDBCDate("workingDate =", value, "workingDate");
            return (Criteria) this;
        }

        public Criteria andWorkingDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("workingDate <>", value, "workingDate");
            return (Criteria) this;
        }

        public Criteria andWorkingDateGreaterThan(Date value) {
            addCriterionForJDBCDate("workingDate >", value, "workingDate");
            return (Criteria) this;
        }

        public Criteria andWorkingDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("workingDate >=", value, "workingDate");
            return (Criteria) this;
        }

        public Criteria andWorkingDateLessThan(Date value) {
            addCriterionForJDBCDate("workingDate <", value, "workingDate");
            return (Criteria) this;
        }

        public Criteria andWorkingDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("workingDate <=", value, "workingDate");
            return (Criteria) this;
        }

        public Criteria andWorkingDateIn(List<Date> values) {
            addCriterionForJDBCDate("workingDate in", values, "workingDate");
            return (Criteria) this;
        }

        public Criteria andWorkingDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("workingDate not in", values, "workingDate");
            return (Criteria) this;
        }

        public Criteria andWorkingDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("workingDate between", value1, value2, "workingDate");
            return (Criteria) this;
        }

        public Criteria andWorkingDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("workingDate not between", value1, value2, "workingDate");
            return (Criteria) this;
        }

        public Criteria andPersonIdIsNull() {
            addCriterion("personId is null");
            return (Criteria) this;
        }

        public Criteria andPersonIdIsNotNull() {
            addCriterion("personId is not null");
            return (Criteria) this;
        }

        public Criteria andPersonIdEqualTo(Integer value) {
            addCriterion("personId =", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdNotEqualTo(Integer value) {
            addCriterion("personId <>", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdGreaterThan(Integer value) {
            addCriterion("personId >", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("personId >=", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdLessThan(Integer value) {
            addCriterion("personId <", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdLessThanOrEqualTo(Integer value) {
            addCriterion("personId <=", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdIn(List<Integer> values) {
            addCriterion("personId in", values, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdNotIn(List<Integer> values) {
            addCriterion("personId not in", values, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdBetween(Integer value1, Integer value2) {
            addCriterion("personId between", value1, value2, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdNotBetween(Integer value1, Integer value2) {
            addCriterion("personId not between", value1, value2, "personId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}