package cn.hrsys.pojo;

import java.util.Date;

public class Heterodyne {
    private Integer id;

    private Integer staffId;

    private Integer heterodyneType;

    private Integer departmentId;

    private Date date;

    private String post;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStaffId() {
        return staffId;
    }

    public void setStaffId(Integer staffId) {
        this.staffId = staffId;
    }

    public Integer getHeterodyneType() {
        return heterodyneType;
    }

    public void setHeterodyneType(Integer heterodyneType) {
        this.heterodyneType = heterodyneType;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post == null ? null : post.trim();
    }
}