package cn.hrsys.pojo;

import java.util.Date;

public class TbModel {
    private String modelID;

    private String modelName;

    private String modelURL;

    private String parent;

    private String ancentor;

    private Integer orderBy;

    private String relPage;

    private Integer deleted;

    private Integer menuType;

    private String remark;
    
    private Date createTime;

    private Integer createBy;

    private Date updateTime;

    private Integer updateBy;
    
    private String createByName;
    
    private String updateByName;
    
    
    
    
    public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Integer getCreateBy() {
		return createBy;
	}

	public void setCreateBy(Integer createBy) {
		this.createBy = createBy;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Integer getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(Integer updateBy) {
		this.updateBy = updateBy;
	}

	public String getCreateByName() {
		return createByName;
	}

	public void setCreateByName(String createByName) {
		this.createByName = createByName;
	}

	public String getUpdateByName() {
		return updateByName;
	}

	public void setUpdateByName(String updateByName) {
		this.updateByName = updateByName;
	}

	/**
     * 2018/8/26 高巍
     */
    private String parentName;
    
    
    private String ancentorName;
    
    
    
    public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public String getAncentorName() {
		return ancentorName;
	}

	public void setAncentorName(String ancentorName) {
		this.ancentorName = ancentorName;
	}

	/**
     * 2018/8/26 高巍
     */

    public String getModelID() {
        return modelID;
    }

    public void setModelID(String modelID) {
        this.modelID = modelID == null ? null : modelID.trim();
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName == null ? null : modelName.trim();
    }

    public String getModelURL() {
        return modelURL;
    }

    public void setModelURL(String modelURL) {
        this.modelURL = modelURL == null ? null : modelURL.trim();
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent == null ? null : parent.trim();
    }

    public String getAncentor() {
        return ancentor;
    }

    public void setAncentor(String ancentor) {
        this.ancentor = ancentor == null ? null : ancentor.trim();
    }

    public Integer getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(Integer orderBy) {
        this.orderBy = orderBy;
    }

    public String getRelPage() {
        return relPage;
    }

    public void setRelPage(String relPage) {
        this.relPage = relPage == null ? null : relPage.trim();
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public Integer getMenuType() {
        return menuType;
    }

    public void setMenuType(Integer menuType) {
        this.menuType = menuType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }
}