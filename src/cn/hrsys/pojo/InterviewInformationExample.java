package cn.hrsys.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class InterviewInformationExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public InterviewInformationExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andResumeIdIsNull() {
            addCriterion("resumeId is null");
            return (Criteria) this;
        }

        public Criteria andResumeIdIsNotNull() {
            addCriterion("resumeId is not null");
            return (Criteria) this;
        }

        public Criteria andResumeIdEqualTo(Integer value) {
            addCriterion("resumeId =", value, "resumeId");
            return (Criteria) this;
        }

        public Criteria andResumeIdNotEqualTo(Integer value) {
            addCriterion("resumeId <>", value, "resumeId");
            return (Criteria) this;
        }

        public Criteria andResumeIdGreaterThan(Integer value) {
            addCriterion("resumeId >", value, "resumeId");
            return (Criteria) this;
        }

        public Criteria andResumeIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("resumeId >=", value, "resumeId");
            return (Criteria) this;
        }

        public Criteria andResumeIdLessThan(Integer value) {
            addCriterion("resumeId <", value, "resumeId");
            return (Criteria) this;
        }

        public Criteria andResumeIdLessThanOrEqualTo(Integer value) {
            addCriterion("resumeId <=", value, "resumeId");
            return (Criteria) this;
        }

        public Criteria andResumeIdIn(List<Integer> values) {
            addCriterion("resumeId in", values, "resumeId");
            return (Criteria) this;
        }

        public Criteria andResumeIdNotIn(List<Integer> values) {
            addCriterion("resumeId not in", values, "resumeId");
            return (Criteria) this;
        }

        public Criteria andResumeIdBetween(Integer value1, Integer value2) {
            addCriterion("resumeId between", value1, value2, "resumeId");
            return (Criteria) this;
        }

        public Criteria andResumeIdNotBetween(Integer value1, Integer value2) {
            addCriterion("resumeId not between", value1, value2, "resumeId");
            return (Criteria) this;
        }

        public Criteria andInterviewByIsNull() {
            addCriterion("interviewBy is null");
            return (Criteria) this;
        }

        public Criteria andInterviewByIsNotNull() {
            addCriterion("interviewBy is not null");
            return (Criteria) this;
        }

        public Criteria andInterviewByEqualTo(String value) {
            addCriterion("interviewBy =", value, "interviewBy");
            return (Criteria) this;
        }

        public Criteria andInterviewByNotEqualTo(String value) {
            addCriterion("interviewBy <>", value, "interviewBy");
            return (Criteria) this;
        }

        public Criteria andInterviewByGreaterThan(String value) {
            addCriterion("interviewBy >", value, "interviewBy");
            return (Criteria) this;
        }

        public Criteria andInterviewByGreaterThanOrEqualTo(String value) {
            addCriterion("interviewBy >=", value, "interviewBy");
            return (Criteria) this;
        }

        public Criteria andInterviewByLessThan(String value) {
            addCriterion("interviewBy <", value, "interviewBy");
            return (Criteria) this;
        }

        public Criteria andInterviewByLessThanOrEqualTo(String value) {
            addCriterion("interviewBy <=", value, "interviewBy");
            return (Criteria) this;
        }

        public Criteria andInterviewByLike(String value) {
            addCriterion("interviewBy like", value, "interviewBy");
            return (Criteria) this;
        }

        public Criteria andInterviewByNotLike(String value) {
            addCriterion("interviewBy not like", value, "interviewBy");
            return (Criteria) this;
        }

        public Criteria andInterviewByIn(List<String> values) {
            addCriterion("interviewBy in", values, "interviewBy");
            return (Criteria) this;
        }

        public Criteria andInterviewByNotIn(List<String> values) {
            addCriterion("interviewBy not in", values, "interviewBy");
            return (Criteria) this;
        }

        public Criteria andInterviewByBetween(String value1, String value2) {
            addCriterion("interviewBy between", value1, value2, "interviewBy");
            return (Criteria) this;
        }

        public Criteria andInterviewByNotBetween(String value1, String value2) {
            addCriterion("interviewBy not between", value1, value2, "interviewBy");
            return (Criteria) this;
        }

        public Criteria andInterviewDateIsNull() {
            addCriterion("interviewDate is null");
            return (Criteria) this;
        }

        public Criteria andInterviewDateIsNotNull() {
            addCriterion("interviewDate is not null");
            return (Criteria) this;
        }

        public Criteria andInterviewDateEqualTo(Date value) {
            addCriterionForJDBCDate("interviewDate =", value, "interviewDate");
            return (Criteria) this;
        }

        public Criteria andInterviewDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("interviewDate <>", value, "interviewDate");
            return (Criteria) this;
        }

        public Criteria andInterviewDateGreaterThan(Date value) {
            addCriterionForJDBCDate("interviewDate >", value, "interviewDate");
            return (Criteria) this;
        }

        public Criteria andInterviewDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("interviewDate >=", value, "interviewDate");
            return (Criteria) this;
        }

        public Criteria andInterviewDateLessThan(Date value) {
            addCriterionForJDBCDate("interviewDate <", value, "interviewDate");
            return (Criteria) this;
        }

        public Criteria andInterviewDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("interviewDate <=", value, "interviewDate");
            return (Criteria) this;
        }

        public Criteria andInterviewDateIn(List<Date> values) {
            addCriterionForJDBCDate("interviewDate in", values, "interviewDate");
            return (Criteria) this;
        }

        public Criteria andInterviewDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("interviewDate not in", values, "interviewDate");
            return (Criteria) this;
        }

        public Criteria andInterviewDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("interviewDate between", value1, value2, "interviewDate");
            return (Criteria) this;
        }

        public Criteria andInterviewDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("interviewDate not between", value1, value2, "interviewDate");
            return (Criteria) this;
        }

        public Criteria andDescribeIsNull() {
            addCriterion("describe is null");
            return (Criteria) this;
        }

        public Criteria andDescribeIsNotNull() {
            addCriterion("describe is not null");
            return (Criteria) this;
        }

        public Criteria andDescribeEqualTo(String value) {
            addCriterion("describe =", value, "describe");
            return (Criteria) this;
        }

        public Criteria andDescribeNotEqualTo(String value) {
            addCriterion("describe <>", value, "describe");
            return (Criteria) this;
        }

        public Criteria andDescribeGreaterThan(String value) {
            addCriterion("describe >", value, "describe");
            return (Criteria) this;
        }

        public Criteria andDescribeGreaterThanOrEqualTo(String value) {
            addCriterion("describe >=", value, "describe");
            return (Criteria) this;
        }

        public Criteria andDescribeLessThan(String value) {
            addCriterion("describe <", value, "describe");
            return (Criteria) this;
        }

        public Criteria andDescribeLessThanOrEqualTo(String value) {
            addCriterion("describe <=", value, "describe");
            return (Criteria) this;
        }

        public Criteria andDescribeLike(String value) {
            addCriterion("describe like", value, "describe");
            return (Criteria) this;
        }

        public Criteria andDescribeNotLike(String value) {
            addCriterion("describe not like", value, "describe");
            return (Criteria) this;
        }

        public Criteria andDescribeIn(List<String> values) {
            addCriterion("describe in", values, "describe");
            return (Criteria) this;
        }

        public Criteria andDescribeNotIn(List<String> values) {
            addCriterion("describe not in", values, "describe");
            return (Criteria) this;
        }

        public Criteria andDescribeBetween(String value1, String value2) {
            addCriterion("describe between", value1, value2, "describe");
            return (Criteria) this;
        }

        public Criteria andDescribeNotBetween(String value1, String value2) {
            addCriterion("describe not between", value1, value2, "describe");
            return (Criteria) this;
        }

        public Criteria andInterviewTimeIsNull() {
            addCriterion("interviewTime is null");
            return (Criteria) this;
        }

        public Criteria andInterviewTimeIsNotNull() {
            addCriterion("interviewTime is not null");
            return (Criteria) this;
        }

        public Criteria andInterviewTimeEqualTo(Integer value) {
            addCriterion("interviewTime =", value, "interviewTime");
            return (Criteria) this;
        }

        public Criteria andInterviewTimeNotEqualTo(Integer value) {
            addCriterion("interviewTime <>", value, "interviewTime");
            return (Criteria) this;
        }

        public Criteria andInterviewTimeGreaterThan(Integer value) {
            addCriterion("interviewTime >", value, "interviewTime");
            return (Criteria) this;
        }

        public Criteria andInterviewTimeGreaterThanOrEqualTo(Integer value) {
            addCriterion("interviewTime >=", value, "interviewTime");
            return (Criteria) this;
        }

        public Criteria andInterviewTimeLessThan(Integer value) {
            addCriterion("interviewTime <", value, "interviewTime");
            return (Criteria) this;
        }

        public Criteria andInterviewTimeLessThanOrEqualTo(Integer value) {
            addCriterion("interviewTime <=", value, "interviewTime");
            return (Criteria) this;
        }

        public Criteria andInterviewTimeIn(List<Integer> values) {
            addCriterion("interviewTime in", values, "interviewTime");
            return (Criteria) this;
        }

        public Criteria andInterviewTimeNotIn(List<Integer> values) {
            addCriterion("interviewTime not in", values, "interviewTime");
            return (Criteria) this;
        }

        public Criteria andInterviewTimeBetween(Integer value1, Integer value2) {
            addCriterion("interviewTime between", value1, value2, "interviewTime");
            return (Criteria) this;
        }

        public Criteria andInterviewTimeNotBetween(Integer value1, Integer value2) {
            addCriterion("interviewTime not between", value1, value2, "interviewTime");
            return (Criteria) this;
        }

        public Criteria andWrittenLevelIsNull() {
            addCriterion("writtenLevel is null");
            return (Criteria) this;
        }

        public Criteria andWrittenLevelIsNotNull() {
            addCriterion("writtenLevel is not null");
            return (Criteria) this;
        }

        public Criteria andWrittenLevelEqualTo(String value) {
            addCriterion("writtenLevel =", value, "writtenLevel");
            return (Criteria) this;
        }

        public Criteria andWrittenLevelNotEqualTo(String value) {
            addCriterion("writtenLevel <>", value, "writtenLevel");
            return (Criteria) this;
        }

        public Criteria andWrittenLevelGreaterThan(String value) {
            addCriterion("writtenLevel >", value, "writtenLevel");
            return (Criteria) this;
        }

        public Criteria andWrittenLevelGreaterThanOrEqualTo(String value) {
            addCriterion("writtenLevel >=", value, "writtenLevel");
            return (Criteria) this;
        }

        public Criteria andWrittenLevelLessThan(String value) {
            addCriterion("writtenLevel <", value, "writtenLevel");
            return (Criteria) this;
        }

        public Criteria andWrittenLevelLessThanOrEqualTo(String value) {
            addCriterion("writtenLevel <=", value, "writtenLevel");
            return (Criteria) this;
        }

        public Criteria andWrittenLevelLike(String value) {
            addCriterion("writtenLevel like", value, "writtenLevel");
            return (Criteria) this;
        }

        public Criteria andWrittenLevelNotLike(String value) {
            addCriterion("writtenLevel not like", value, "writtenLevel");
            return (Criteria) this;
        }

        public Criteria andWrittenLevelIn(List<String> values) {
            addCriterion("writtenLevel in", values, "writtenLevel");
            return (Criteria) this;
        }

        public Criteria andWrittenLevelNotIn(List<String> values) {
            addCriterion("writtenLevel not in", values, "writtenLevel");
            return (Criteria) this;
        }

        public Criteria andWrittenLevelBetween(String value1, String value2) {
            addCriterion("writtenLevel between", value1, value2, "writtenLevel");
            return (Criteria) this;
        }

        public Criteria andWrittenLevelNotBetween(String value1, String value2) {
            addCriterion("writtenLevel not between", value1, value2, "writtenLevel");
            return (Criteria) this;
        }

        public Criteria andWrittenGradeIsNull() {
            addCriterion("writtenGrade is null");
            return (Criteria) this;
        }

        public Criteria andWrittenGradeIsNotNull() {
            addCriterion("writtenGrade is not null");
            return (Criteria) this;
        }

        public Criteria andWrittenGradeEqualTo(String value) {
            addCriterion("writtenGrade =", value, "writtenGrade");
            return (Criteria) this;
        }

        public Criteria andWrittenGradeNotEqualTo(String value) {
            addCriterion("writtenGrade <>", value, "writtenGrade");
            return (Criteria) this;
        }

        public Criteria andWrittenGradeGreaterThan(String value) {
            addCriterion("writtenGrade >", value, "writtenGrade");
            return (Criteria) this;
        }

        public Criteria andWrittenGradeGreaterThanOrEqualTo(String value) {
            addCriterion("writtenGrade >=", value, "writtenGrade");
            return (Criteria) this;
        }

        public Criteria andWrittenGradeLessThan(String value) {
            addCriterion("writtenGrade <", value, "writtenGrade");
            return (Criteria) this;
        }

        public Criteria andWrittenGradeLessThanOrEqualTo(String value) {
            addCriterion("writtenGrade <=", value, "writtenGrade");
            return (Criteria) this;
        }

        public Criteria andWrittenGradeLike(String value) {
            addCriterion("writtenGrade like", value, "writtenGrade");
            return (Criteria) this;
        }

        public Criteria andWrittenGradeNotLike(String value) {
            addCriterion("writtenGrade not like", value, "writtenGrade");
            return (Criteria) this;
        }

        public Criteria andWrittenGradeIn(List<String> values) {
            addCriterion("writtenGrade in", values, "writtenGrade");
            return (Criteria) this;
        }

        public Criteria andWrittenGradeNotIn(List<String> values) {
            addCriterion("writtenGrade not in", values, "writtenGrade");
            return (Criteria) this;
        }

        public Criteria andWrittenGradeBetween(String value1, String value2) {
            addCriterion("writtenGrade between", value1, value2, "writtenGrade");
            return (Criteria) this;
        }

        public Criteria andWrittenGradeNotBetween(String value1, String value2) {
            addCriterion("writtenGrade not between", value1, value2, "writtenGrade");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}