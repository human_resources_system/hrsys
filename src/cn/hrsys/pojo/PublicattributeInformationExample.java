package cn.hrsys.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class PublicattributeInformationExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PublicattributeInformationExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andClassCodeIsNull() {
            addCriterion("classCode is null");
            return (Criteria) this;
        }

        public Criteria andClassCodeIsNotNull() {
            addCriterion("classCode is not null");
            return (Criteria) this;
        }

        public Criteria andClassCodeEqualTo(String value) {
            addCriterion("classCode =", value, "classCode");
            return (Criteria) this;
        }

        public Criteria andClassCodeNotEqualTo(String value) {
            addCriterion("classCode <>", value, "classCode");
            return (Criteria) this;
        }

        public Criteria andClassCodeGreaterThan(String value) {
            addCriterion("classCode >", value, "classCode");
            return (Criteria) this;
        }

        public Criteria andClassCodeGreaterThanOrEqualTo(String value) {
            addCriterion("classCode >=", value, "classCode");
            return (Criteria) this;
        }

        public Criteria andClassCodeLessThan(String value) {
            addCriterion("classCode <", value, "classCode");
            return (Criteria) this;
        }

        public Criteria andClassCodeLessThanOrEqualTo(String value) {
            addCriterion("classCode <=", value, "classCode");
            return (Criteria) this;
        }

        public Criteria andClassCodeLike(String value) {
            addCriterion("classCode like", value, "classCode");
            return (Criteria) this;
        }

        public Criteria andClassCodeNotLike(String value) {
            addCriterion("classCode not like", value, "classCode");
            return (Criteria) this;
        }

        public Criteria andClassCodeIn(List<String> values) {
            addCriterion("classCode in", values, "classCode");
            return (Criteria) this;
        }

        public Criteria andClassCodeNotIn(List<String> values) {
            addCriterion("classCode not in", values, "classCode");
            return (Criteria) this;
        }

        public Criteria andClassCodeBetween(String value1, String value2) {
            addCriterion("classCode between", value1, value2, "classCode");
            return (Criteria) this;
        }

        public Criteria andClassCodeNotBetween(String value1, String value2) {
            addCriterion("classCode not between", value1, value2, "classCode");
            return (Criteria) this;
        }

        public Criteria andClassNameIsNull() {
            addCriterion("className is null");
            return (Criteria) this;
        }

        public Criteria andClassNameIsNotNull() {
            addCriterion("className is not null");
            return (Criteria) this;
        }

        public Criteria andClassNameEqualTo(String value) {
            addCriterion("className =", value, "className");
            return (Criteria) this;
        }

        public Criteria andClassNameNotEqualTo(String value) {
            addCriterion("className <>", value, "className");
            return (Criteria) this;
        }

        public Criteria andClassNameGreaterThan(String value) {
            addCriterion("className >", value, "className");
            return (Criteria) this;
        }

        public Criteria andClassNameGreaterThanOrEqualTo(String value) {
            addCriterion("className >=", value, "className");
            return (Criteria) this;
        }

        public Criteria andClassNameLessThan(String value) {
            addCriterion("className <", value, "className");
            return (Criteria) this;
        }

        public Criteria andClassNameLessThanOrEqualTo(String value) {
            addCriterion("className <=", value, "className");
            return (Criteria) this;
        }

        public Criteria andClassNameLike(String value) {
            addCriterion("className like", value, "className");
            return (Criteria) this;
        }

        public Criteria andClassNameNotLike(String value) {
            addCriterion("className not like", value, "className");
            return (Criteria) this;
        }

        public Criteria andClassNameIn(List<String> values) {
            addCriterion("className in", values, "className");
            return (Criteria) this;
        }

        public Criteria andClassNameNotIn(List<String> values) {
            addCriterion("className not in", values, "className");
            return (Criteria) this;
        }

        public Criteria andClassNameBetween(String value1, String value2) {
            addCriterion("className between", value1, value2, "className");
            return (Criteria) this;
        }

        public Criteria andClassNameNotBetween(String value1, String value2) {
            addCriterion("className not between", value1, value2, "className");
            return (Criteria) this;
        }

        public Criteria andClassvalueIdIsNull() {
            addCriterion("classvalueId is null");
            return (Criteria) this;
        }

        public Criteria andClassvalueIdIsNotNull() {
            addCriterion("classvalueId is not null");
            return (Criteria) this;
        }

        public Criteria andClassvalueIdEqualTo(Integer value) {
            addCriterion("classvalueId =", value, "classvalueId");
            return (Criteria) this;
        }

        public Criteria andClassvalueIdNotEqualTo(Integer value) {
            addCriterion("classvalueId <>", value, "classvalueId");
            return (Criteria) this;
        }

        public Criteria andClassvalueIdGreaterThan(Integer value) {
            addCriterion("classvalueId >", value, "classvalueId");
            return (Criteria) this;
        }

        public Criteria andClassvalueIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("classvalueId >=", value, "classvalueId");
            return (Criteria) this;
        }

        public Criteria andClassvalueIdLessThan(Integer value) {
            addCriterion("classvalueId <", value, "classvalueId");
            return (Criteria) this;
        }

        public Criteria andClassvalueIdLessThanOrEqualTo(Integer value) {
            addCriterion("classvalueId <=", value, "classvalueId");
            return (Criteria) this;
        }

        public Criteria andClassvalueIdIn(List<Integer> values) {
            addCriterion("classvalueId in", values, "classvalueId");
            return (Criteria) this;
        }

        public Criteria andClassvalueIdNotIn(List<Integer> values) {
            addCriterion("classvalueId not in", values, "classvalueId");
            return (Criteria) this;
        }

        public Criteria andClassvalueIdBetween(Integer value1, Integer value2) {
            addCriterion("classvalueId between", value1, value2, "classvalueId");
            return (Criteria) this;
        }

        public Criteria andClassvalueIdNotBetween(Integer value1, Integer value2) {
            addCriterion("classvalueId not between", value1, value2, "classvalueId");
            return (Criteria) this;
        }

        public Criteria andClassvalueNameIsNull() {
            addCriterion("classvalueName is null");
            return (Criteria) this;
        }

        public Criteria andClassvalueNameIsNotNull() {
            addCriterion("classvalueName is not null");
            return (Criteria) this;
        }

        public Criteria andClassvalueNameEqualTo(String value) {
            addCriterion("classvalueName =", value, "classvalueName");
            return (Criteria) this;
        }

        public Criteria andClassvalueNameNotEqualTo(String value) {
            addCriterion("classvalueName <>", value, "classvalueName");
            return (Criteria) this;
        }

        public Criteria andClassvalueNameGreaterThan(String value) {
            addCriterion("classvalueName >", value, "classvalueName");
            return (Criteria) this;
        }

        public Criteria andClassvalueNameGreaterThanOrEqualTo(String value) {
            addCriterion("classvalueName >=", value, "classvalueName");
            return (Criteria) this;
        }

        public Criteria andClassvalueNameLessThan(String value) {
            addCriterion("classvalueName <", value, "classvalueName");
            return (Criteria) this;
        }

        public Criteria andClassvalueNameLessThanOrEqualTo(String value) {
            addCriterion("classvalueName <=", value, "classvalueName");
            return (Criteria) this;
        }

        public Criteria andClassvalueNameLike(String value) {
            addCriterion("classvalueName like", value, "classvalueName");
            return (Criteria) this;
        }

        public Criteria andClassvalueNameNotLike(String value) {
            addCriterion("classvalueName not like", value, "classvalueName");
            return (Criteria) this;
        }

        public Criteria andClassvalueNameIn(List<String> values) {
            addCriterion("classvalueName in", values, "classvalueName");
            return (Criteria) this;
        }

        public Criteria andClassvalueNameNotIn(List<String> values) {
            addCriterion("classvalueName not in", values, "classvalueName");
            return (Criteria) this;
        }

        public Criteria andClassvalueNameBetween(String value1, String value2) {
            addCriterion("classvalueName between", value1, value2, "classvalueName");
            return (Criteria) this;
        }

        public Criteria andClassvalueNameNotBetween(String value1, String value2) {
            addCriterion("classvalueName not between", value1, value2, "classvalueName");
            return (Criteria) this;
        }

        public Criteria andCreationByIsNull() {
            addCriterion("creationBy is null");
            return (Criteria) this;
        }

        public Criteria andCreationByIsNotNull() {
            addCriterion("creationBy is not null");
            return (Criteria) this;
        }

        public Criteria andCreationByEqualTo(Integer value) {
            addCriterion("creationBy =", value, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByNotEqualTo(Integer value) {
            addCriterion("creationBy <>", value, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByGreaterThan(Integer value) {
            addCriterion("creationBy >", value, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByGreaterThanOrEqualTo(Integer value) {
            addCriterion("creationBy >=", value, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByLessThan(Integer value) {
            addCriterion("creationBy <", value, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByLessThanOrEqualTo(Integer value) {
            addCriterion("creationBy <=", value, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByIn(List<Integer> values) {
            addCriterion("creationBy in", values, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByNotIn(List<Integer> values) {
            addCriterion("creationBy not in", values, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByBetween(Integer value1, Integer value2) {
            addCriterion("creationBy between", value1, value2, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationByNotBetween(Integer value1, Integer value2) {
            addCriterion("creationBy not between", value1, value2, "creationBy");
            return (Criteria) this;
        }

        public Criteria andCreationDateIsNull() {
            addCriterion("creationDate is null");
            return (Criteria) this;
        }

        public Criteria andCreationDateIsNotNull() {
            addCriterion("creationDate is not null");
            return (Criteria) this;
        }

        public Criteria andCreationDateEqualTo(Date value) {
            addCriterionForJDBCDate("creationDate =", value, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("creationDate <>", value, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateGreaterThan(Date value) {
            addCriterionForJDBCDate("creationDate >", value, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("creationDate >=", value, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateLessThan(Date value) {
            addCriterionForJDBCDate("creationDate <", value, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("creationDate <=", value, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateIn(List<Date> values) {
            addCriterionForJDBCDate("creationDate in", values, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("creationDate not in", values, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("creationDate between", value1, value2, "creationDate");
            return (Criteria) this;
        }

        public Criteria andCreationDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("creationDate not between", value1, value2, "creationDate");
            return (Criteria) this;
        }

        public Criteria andModifyByIsNull() {
            addCriterion("modifyBy is null");
            return (Criteria) this;
        }

        public Criteria andModifyByIsNotNull() {
            addCriterion("modifyBy is not null");
            return (Criteria) this;
        }

        public Criteria andModifyByEqualTo(Integer value) {
            addCriterion("modifyBy =", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByNotEqualTo(Integer value) {
            addCriterion("modifyBy <>", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByGreaterThan(Integer value) {
            addCriterion("modifyBy >", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByGreaterThanOrEqualTo(Integer value) {
            addCriterion("modifyBy >=", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByLessThan(Integer value) {
            addCriterion("modifyBy <", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByLessThanOrEqualTo(Integer value) {
            addCriterion("modifyBy <=", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByIn(List<Integer> values) {
            addCriterion("modifyBy in", values, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByNotIn(List<Integer> values) {
            addCriterion("modifyBy not in", values, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByBetween(Integer value1, Integer value2) {
            addCriterion("modifyBy between", value1, value2, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByNotBetween(Integer value1, Integer value2) {
            addCriterion("modifyBy not between", value1, value2, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyDateIsNull() {
            addCriterion("modifyDate is null");
            return (Criteria) this;
        }

        public Criteria andModifyDateIsNotNull() {
            addCriterion("modifyDate is not null");
            return (Criteria) this;
        }

        public Criteria andModifyDateEqualTo(Date value) {
            addCriterionForJDBCDate("modifyDate =", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("modifyDate <>", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateGreaterThan(Date value) {
            addCriterionForJDBCDate("modifyDate >", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("modifyDate >=", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateLessThan(Date value) {
            addCriterionForJDBCDate("modifyDate <", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("modifyDate <=", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateIn(List<Date> values) {
            addCriterionForJDBCDate("modifyDate in", values, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("modifyDate not in", values, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("modifyDate between", value1, value2, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("modifyDate not between", value1, value2, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andEnabledDateIsNull() {
            addCriterion("enabledDate is null");
            return (Criteria) this;
        }

        public Criteria andEnabledDateIsNotNull() {
            addCriterion("enabledDate is not null");
            return (Criteria) this;
        }

        public Criteria andEnabledDateEqualTo(Date value) {
            addCriterionForJDBCDate("enabledDate =", value, "enabledDate");
            return (Criteria) this;
        }

        public Criteria andEnabledDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("enabledDate <>", value, "enabledDate");
            return (Criteria) this;
        }

        public Criteria andEnabledDateGreaterThan(Date value) {
            addCriterionForJDBCDate("enabledDate >", value, "enabledDate");
            return (Criteria) this;
        }

        public Criteria andEnabledDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("enabledDate >=", value, "enabledDate");
            return (Criteria) this;
        }

        public Criteria andEnabledDateLessThan(Date value) {
            addCriterionForJDBCDate("enabledDate <", value, "enabledDate");
            return (Criteria) this;
        }

        public Criteria andEnabledDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("enabledDate <=", value, "enabledDate");
            return (Criteria) this;
        }

        public Criteria andEnabledDateIn(List<Date> values) {
            addCriterionForJDBCDate("enabledDate in", values, "enabledDate");
            return (Criteria) this;
        }

        public Criteria andEnabledDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("enabledDate not in", values, "enabledDate");
            return (Criteria) this;
        }

        public Criteria andEnabledDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("enabledDate between", value1, value2, "enabledDate");
            return (Criteria) this;
        }

        public Criteria andEnabledDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("enabledDate not between", value1, value2, "enabledDate");
            return (Criteria) this;
        }

        public Criteria andLoseDateIsNull() {
            addCriterion("loseDate is null");
            return (Criteria) this;
        }

        public Criteria andLoseDateIsNotNull() {
            addCriterion("loseDate is not null");
            return (Criteria) this;
        }

        public Criteria andLoseDateEqualTo(Date value) {
            addCriterionForJDBCDate("loseDate =", value, "loseDate");
            return (Criteria) this;
        }

        public Criteria andLoseDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("loseDate <>", value, "loseDate");
            return (Criteria) this;
        }

        public Criteria andLoseDateGreaterThan(Date value) {
            addCriterionForJDBCDate("loseDate >", value, "loseDate");
            return (Criteria) this;
        }

        public Criteria andLoseDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("loseDate >=", value, "loseDate");
            return (Criteria) this;
        }

        public Criteria andLoseDateLessThan(Date value) {
            addCriterionForJDBCDate("loseDate <", value, "loseDate");
            return (Criteria) this;
        }

        public Criteria andLoseDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("loseDate <=", value, "loseDate");
            return (Criteria) this;
        }

        public Criteria andLoseDateIn(List<Date> values) {
            addCriterionForJDBCDate("loseDate in", values, "loseDate");
            return (Criteria) this;
        }

        public Criteria andLoseDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("loseDate not in", values, "loseDate");
            return (Criteria) this;
        }

        public Criteria andLoseDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("loseDate between", value1, value2, "loseDate");
            return (Criteria) this;
        }

        public Criteria andLoseDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("loseDate not between", value1, value2, "loseDate");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}