package cn.hrsys.pojo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class RewardstandardInformationExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public RewardstandardInformationExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andRewardCodeIsNull() {
            addCriterion("rewardCode is null");
            return (Criteria) this;
        }

        public Criteria andRewardCodeIsNotNull() {
            addCriterion("rewardCode is not null");
            return (Criteria) this;
        }

        public Criteria andRewardCodeEqualTo(String value) {
            addCriterion("rewardCode =", value, "rewardCode");
            return (Criteria) this;
        }

        public Criteria andRewardCodeNotEqualTo(String value) {
            addCriterion("rewardCode <>", value, "rewardCode");
            return (Criteria) this;
        }

        public Criteria andRewardCodeGreaterThan(String value) {
            addCriterion("rewardCode >", value, "rewardCode");
            return (Criteria) this;
        }

        public Criteria andRewardCodeGreaterThanOrEqualTo(String value) {
            addCriterion("rewardCode >=", value, "rewardCode");
            return (Criteria) this;
        }

        public Criteria andRewardCodeLessThan(String value) {
            addCriterion("rewardCode <", value, "rewardCode");
            return (Criteria) this;
        }

        public Criteria andRewardCodeLessThanOrEqualTo(String value) {
            addCriterion("rewardCode <=", value, "rewardCode");
            return (Criteria) this;
        }

        public Criteria andRewardCodeLike(String value) {
            addCriterion("rewardCode like", value, "rewardCode");
            return (Criteria) this;
        }

        public Criteria andRewardCodeNotLike(String value) {
            addCriterion("rewardCode not like", value, "rewardCode");
            return (Criteria) this;
        }

        public Criteria andRewardCodeIn(List<String> values) {
            addCriterion("rewardCode in", values, "rewardCode");
            return (Criteria) this;
        }

        public Criteria andRewardCodeNotIn(List<String> values) {
            addCriterion("rewardCode not in", values, "rewardCode");
            return (Criteria) this;
        }

        public Criteria andRewardCodeBetween(String value1, String value2) {
            addCriterion("rewardCode between", value1, value2, "rewardCode");
            return (Criteria) this;
        }

        public Criteria andRewardCodeNotBetween(String value1, String value2) {
            addCriterion("rewardCode not between", value1, value2, "rewardCode");
            return (Criteria) this;
        }

        public Criteria andRewardNameIsNull() {
            addCriterion("rewardName is null");
            return (Criteria) this;
        }

        public Criteria andRewardNameIsNotNull() {
            addCriterion("rewardName is not null");
            return (Criteria) this;
        }

        public Criteria andRewardNameEqualTo(String value) {
            addCriterion("rewardName =", value, "rewardName");
            return (Criteria) this;
        }

        public Criteria andRewardNameNotEqualTo(String value) {
            addCriterion("rewardName <>", value, "rewardName");
            return (Criteria) this;
        }

        public Criteria andRewardNameGreaterThan(String value) {
            addCriterion("rewardName >", value, "rewardName");
            return (Criteria) this;
        }

        public Criteria andRewardNameGreaterThanOrEqualTo(String value) {
            addCriterion("rewardName >=", value, "rewardName");
            return (Criteria) this;
        }

        public Criteria andRewardNameLessThan(String value) {
            addCriterion("rewardName <", value, "rewardName");
            return (Criteria) this;
        }

        public Criteria andRewardNameLessThanOrEqualTo(String value) {
            addCriterion("rewardName <=", value, "rewardName");
            return (Criteria) this;
        }

        public Criteria andRewardNameLike(String value) {
            addCriterion("rewardName like", value, "rewardName");
            return (Criteria) this;
        }

        public Criteria andRewardNameNotLike(String value) {
            addCriterion("rewardName not like", value, "rewardName");
            return (Criteria) this;
        }

        public Criteria andRewardNameIn(List<String> values) {
            addCriterion("rewardName in", values, "rewardName");
            return (Criteria) this;
        }

        public Criteria andRewardNameNotIn(List<String> values) {
            addCriterion("rewardName not in", values, "rewardName");
            return (Criteria) this;
        }

        public Criteria andRewardNameBetween(String value1, String value2) {
            addCriterion("rewardName between", value1, value2, "rewardName");
            return (Criteria) this;
        }

        public Criteria andRewardNameNotBetween(String value1, String value2) {
            addCriterion("rewardName not between", value1, value2, "rewardName");
            return (Criteria) this;
        }

        public Criteria andRewardTotalIsNull() {
            addCriterion("rewardTotal is null");
            return (Criteria) this;
        }

        public Criteria andRewardTotalIsNotNull() {
            addCriterion("rewardTotal is not null");
            return (Criteria) this;
        }

        public Criteria andRewardTotalEqualTo(BigDecimal value) {
            addCriterion("rewardTotal =", value, "rewardTotal");
            return (Criteria) this;
        }

        public Criteria andRewardTotalNotEqualTo(BigDecimal value) {
            addCriterion("rewardTotal <>", value, "rewardTotal");
            return (Criteria) this;
        }

        public Criteria andRewardTotalGreaterThan(BigDecimal value) {
            addCriterion("rewardTotal >", value, "rewardTotal");
            return (Criteria) this;
        }

        public Criteria andRewardTotalGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("rewardTotal >=", value, "rewardTotal");
            return (Criteria) this;
        }

        public Criteria andRewardTotalLessThan(BigDecimal value) {
            addCriterion("rewardTotal <", value, "rewardTotal");
            return (Criteria) this;
        }

        public Criteria andRewardTotalLessThanOrEqualTo(BigDecimal value) {
            addCriterion("rewardTotal <=", value, "rewardTotal");
            return (Criteria) this;
        }

        public Criteria andRewardTotalIn(List<BigDecimal> values) {
            addCriterion("rewardTotal in", values, "rewardTotal");
            return (Criteria) this;
        }

        public Criteria andRewardTotalNotIn(List<BigDecimal> values) {
            addCriterion("rewardTotal not in", values, "rewardTotal");
            return (Criteria) this;
        }

        public Criteria andRewardTotalBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("rewardTotal between", value1, value2, "rewardTotal");
            return (Criteria) this;
        }

        public Criteria andRewardTotalNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("rewardTotal not between", value1, value2, "rewardTotal");
            return (Criteria) this;
        }

        public Criteria andRewardDesignerIsNull() {
            addCriterion("rewardDesigner is null");
            return (Criteria) this;
        }

        public Criteria andRewardDesignerIsNotNull() {
            addCriterion("rewardDesigner is not null");
            return (Criteria) this;
        }

        public Criteria andRewardDesignerEqualTo(String value) {
            addCriterion("rewardDesigner =", value, "rewardDesigner");
            return (Criteria) this;
        }

        public Criteria andRewardDesignerNotEqualTo(String value) {
            addCriterion("rewardDesigner <>", value, "rewardDesigner");
            return (Criteria) this;
        }

        public Criteria andRewardDesignerGreaterThan(String value) {
            addCriterion("rewardDesigner >", value, "rewardDesigner");
            return (Criteria) this;
        }

        public Criteria andRewardDesignerGreaterThanOrEqualTo(String value) {
            addCriterion("rewardDesigner >=", value, "rewardDesigner");
            return (Criteria) this;
        }

        public Criteria andRewardDesignerLessThan(String value) {
            addCriterion("rewardDesigner <", value, "rewardDesigner");
            return (Criteria) this;
        }

        public Criteria andRewardDesignerLessThanOrEqualTo(String value) {
            addCriterion("rewardDesigner <=", value, "rewardDesigner");
            return (Criteria) this;
        }

        public Criteria andRewardDesignerLike(String value) {
            addCriterion("rewardDesigner like", value, "rewardDesigner");
            return (Criteria) this;
        }

        public Criteria andRewardDesignerNotLike(String value) {
            addCriterion("rewardDesigner not like", value, "rewardDesigner");
            return (Criteria) this;
        }

        public Criteria andRewardDesignerIn(List<String> values) {
            addCriterion("rewardDesigner in", values, "rewardDesigner");
            return (Criteria) this;
        }

        public Criteria andRewardDesignerNotIn(List<String> values) {
            addCriterion("rewardDesigner not in", values, "rewardDesigner");
            return (Criteria) this;
        }

        public Criteria andRewardDesignerBetween(String value1, String value2) {
            addCriterion("rewardDesigner between", value1, value2, "rewardDesigner");
            return (Criteria) this;
        }

        public Criteria andRewardDesignerNotBetween(String value1, String value2) {
            addCriterion("rewardDesigner not between", value1, value2, "rewardDesigner");
            return (Criteria) this;
        }

        public Criteria andRegisterByIsNull() {
            addCriterion("registerBy is null");
            return (Criteria) this;
        }

        public Criteria andRegisterByIsNotNull() {
            addCriterion("registerBy is not null");
            return (Criteria) this;
        }

        public Criteria andRegisterByEqualTo(Integer value) {
            addCriterion("registerBy =", value, "registerBy");
            return (Criteria) this;
        }

        public Criteria andRegisterByNotEqualTo(Integer value) {
            addCriterion("registerBy <>", value, "registerBy");
            return (Criteria) this;
        }

        public Criteria andRegisterByGreaterThan(Integer value) {
            addCriterion("registerBy >", value, "registerBy");
            return (Criteria) this;
        }

        public Criteria andRegisterByGreaterThanOrEqualTo(Integer value) {
            addCriterion("registerBy >=", value, "registerBy");
            return (Criteria) this;
        }

        public Criteria andRegisterByLessThan(Integer value) {
            addCriterion("registerBy <", value, "registerBy");
            return (Criteria) this;
        }

        public Criteria andRegisterByLessThanOrEqualTo(Integer value) {
            addCriterion("registerBy <=", value, "registerBy");
            return (Criteria) this;
        }

        public Criteria andRegisterByIn(List<Integer> values) {
            addCriterion("registerBy in", values, "registerBy");
            return (Criteria) this;
        }

        public Criteria andRegisterByNotIn(List<Integer> values) {
            addCriterion("registerBy not in", values, "registerBy");
            return (Criteria) this;
        }

        public Criteria andRegisterByBetween(Integer value1, Integer value2) {
            addCriterion("registerBy between", value1, value2, "registerBy");
            return (Criteria) this;
        }

        public Criteria andRegisterByNotBetween(Integer value1, Integer value2) {
            addCriterion("registerBy not between", value1, value2, "registerBy");
            return (Criteria) this;
        }

        public Criteria andRegisterDateIsNull() {
            addCriterion("registerDate is null");
            return (Criteria) this;
        }

        public Criteria andRegisterDateIsNotNull() {
            addCriterion("registerDate is not null");
            return (Criteria) this;
        }

        public Criteria andRegisterDateEqualTo(Date value) {
            addCriterionForJDBCDate("registerDate =", value, "registerDate");
            return (Criteria) this;
        }

        public Criteria andRegisterDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("registerDate <>", value, "registerDate");
            return (Criteria) this;
        }

        public Criteria andRegisterDateGreaterThan(Date value) {
            addCriterionForJDBCDate("registerDate >", value, "registerDate");
            return (Criteria) this;
        }

        public Criteria andRegisterDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("registerDate >=", value, "registerDate");
            return (Criteria) this;
        }

        public Criteria andRegisterDateLessThan(Date value) {
            addCriterionForJDBCDate("registerDate <", value, "registerDate");
            return (Criteria) this;
        }

        public Criteria andRegisterDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("registerDate <=", value, "registerDate");
            return (Criteria) this;
        }

        public Criteria andRegisterDateIn(List<Date> values) {
            addCriterionForJDBCDate("registerDate in", values, "registerDate");
            return (Criteria) this;
        }

        public Criteria andRegisterDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("registerDate not in", values, "registerDate");
            return (Criteria) this;
        }

        public Criteria andRegisterDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("registerDate between", value1, value2, "registerDate");
            return (Criteria) this;
        }

        public Criteria andRegisterDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("registerDate not between", value1, value2, "registerDate");
            return (Criteria) this;
        }

        public Criteria andModifyByIsNull() {
            addCriterion("modifyBy is null");
            return (Criteria) this;
        }

        public Criteria andModifyByIsNotNull() {
            addCriterion("modifyBy is not null");
            return (Criteria) this;
        }

        public Criteria andModifyByEqualTo(Integer value) {
            addCriterion("modifyBy =", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByNotEqualTo(Integer value) {
            addCriterion("modifyBy <>", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByGreaterThan(Integer value) {
            addCriterion("modifyBy >", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByGreaterThanOrEqualTo(Integer value) {
            addCriterion("modifyBy >=", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByLessThan(Integer value) {
            addCriterion("modifyBy <", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByLessThanOrEqualTo(Integer value) {
            addCriterion("modifyBy <=", value, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByIn(List<Integer> values) {
            addCriterion("modifyBy in", values, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByNotIn(List<Integer> values) {
            addCriterion("modifyBy not in", values, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByBetween(Integer value1, Integer value2) {
            addCriterion("modifyBy between", value1, value2, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyByNotBetween(Integer value1, Integer value2) {
            addCriterion("modifyBy not between", value1, value2, "modifyBy");
            return (Criteria) this;
        }

        public Criteria andModifyDateIsNull() {
            addCriterion("modifyDate is null");
            return (Criteria) this;
        }

        public Criteria andModifyDateIsNotNull() {
            addCriterion("modifyDate is not null");
            return (Criteria) this;
        }

        public Criteria andModifyDateEqualTo(Date value) {
            addCriterionForJDBCDate("modifyDate =", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("modifyDate <>", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateGreaterThan(Date value) {
            addCriterionForJDBCDate("modifyDate >", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("modifyDate >=", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateLessThan(Date value) {
            addCriterionForJDBCDate("modifyDate <", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("modifyDate <=", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateIn(List<Date> values) {
            addCriterionForJDBCDate("modifyDate in", values, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("modifyDate not in", values, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("modifyDate between", value1, value2, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("modifyDate not between", value1, value2, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andAuditStatusIsNull() {
            addCriterion("auditStatus is null");
            return (Criteria) this;
        }

        public Criteria andAuditStatusIsNotNull() {
            addCriterion("auditStatus is not null");
            return (Criteria) this;
        }

        public Criteria andAuditStatusEqualTo(Integer value) {
            addCriterion("auditStatus =", value, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusNotEqualTo(Integer value) {
            addCriterion("auditStatus <>", value, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusGreaterThan(Integer value) {
            addCriterion("auditStatus >", value, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("auditStatus >=", value, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusLessThan(Integer value) {
            addCriterion("auditStatus <", value, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusLessThanOrEqualTo(Integer value) {
            addCriterion("auditStatus <=", value, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusIn(List<Integer> values) {
            addCriterion("auditStatus in", values, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusNotIn(List<Integer> values) {
            addCriterion("auditStatus not in", values, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusBetween(Integer value1, Integer value2) {
            addCriterion("auditStatus between", value1, value2, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("auditStatus not between", value1, value2, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andModifyCauseIsNull() {
            addCriterion("modifyCause is null");
            return (Criteria) this;
        }

        public Criteria andModifyCauseIsNotNull() {
            addCriterion("modifyCause is not null");
            return (Criteria) this;
        }

        public Criteria andModifyCauseEqualTo(String value) {
            addCriterion("modifyCause =", value, "modifyCause");
            return (Criteria) this;
        }

        public Criteria andModifyCauseNotEqualTo(String value) {
            addCriterion("modifyCause <>", value, "modifyCause");
            return (Criteria) this;
        }

        public Criteria andModifyCauseGreaterThan(String value) {
            addCriterion("modifyCause >", value, "modifyCause");
            return (Criteria) this;
        }

        public Criteria andModifyCauseGreaterThanOrEqualTo(String value) {
            addCriterion("modifyCause >=", value, "modifyCause");
            return (Criteria) this;
        }

        public Criteria andModifyCauseLessThan(String value) {
            addCriterion("modifyCause <", value, "modifyCause");
            return (Criteria) this;
        }

        public Criteria andModifyCauseLessThanOrEqualTo(String value) {
            addCriterion("modifyCause <=", value, "modifyCause");
            return (Criteria) this;
        }

        public Criteria andModifyCauseLike(String value) {
            addCriterion("modifyCause like", value, "modifyCause");
            return (Criteria) this;
        }

        public Criteria andModifyCauseNotLike(String value) {
            addCriterion("modifyCause not like", value, "modifyCause");
            return (Criteria) this;
        }

        public Criteria andModifyCauseIn(List<String> values) {
            addCriterion("modifyCause in", values, "modifyCause");
            return (Criteria) this;
        }

        public Criteria andModifyCauseNotIn(List<String> values) {
            addCriterion("modifyCause not in", values, "modifyCause");
            return (Criteria) this;
        }

        public Criteria andModifyCauseBetween(String value1, String value2) {
            addCriterion("modifyCause between", value1, value2, "modifyCause");
            return (Criteria) this;
        }

        public Criteria andModifyCauseNotBetween(String value1, String value2) {
            addCriterion("modifyCause not between", value1, value2, "modifyCause");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}