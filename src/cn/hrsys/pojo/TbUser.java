package cn.hrsys.pojo;

import java.util.Date;

public class TbUser {
    private Integer id;

    private String userID;

    private String logUsername;

    private String logPassword;

    private String showUsername;

    private String userVia;

    private String role;

    private Integer status;

    private Integer deleted;

    private Integer modifyStatus;

    private Date createTime;

    private Date modifyTime;

    private String remark;
    
    private int createBy;
    
    private int modifyBy;
    
    private String createByName;
    
    private String modifyByName;
    
    
    
    
    public String getCreateByName() {
		return createByName;
	}

	public void setCreateByName(String createByName) {
		this.createByName = createByName;
	}

	public String getModifyByName() {
		return modifyByName;
	}

	public void setModifyByName(String modifyByName) {
		this.modifyByName = modifyByName;
	}

	public int getCreateBy() {
		return createBy;
	}

	public void setCreateBy(int createBy) {
		this.createBy = createBy;
	}

	public int getModifyBy() {
		return modifyBy;
	}

	public void setModifyBy(int modifyBy) {
		this.modifyBy = modifyBy;
	}

	//加一个角色名称属性--高巍
    private String roleName;
    
    public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
    //加一个角色名称属性--高巍

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID == null ? null : userID.trim();
    }

    public String getLogUsername() {
        return logUsername;
    }

    public void setLogUsername(String logUsername) {
        this.logUsername = logUsername == null ? null : logUsername.trim();
    }

    public String getLogPassword() {
        return logPassword;
    }

    public void setLogPassword(String logPassword) {
        this.logPassword = logPassword == null ? null : logPassword.trim();
    }

    public String getShowUsername() {
        return showUsername;
    }

    public void setShowUsername(String showUsername) {
        this.showUsername = showUsername == null ? null : showUsername.trim();
    }

    public String getUserVia() {
        return userVia;
    }

    public void setUserVia(String userVia) {
        this.userVia = userVia == null ? null : userVia.trim();
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role == null ? null : role.trim();
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public Integer getModifyStatus() {
        return modifyStatus;
    }

    public void setModifyStatus(Integer modifyStatus) {
        this.modifyStatus = modifyStatus;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

	@Override
	public String toString() {
		return "id=" + id + "&userID=" + userID + "&logUsername=" + logUsername + "&logPassword=" + logPassword
				+ "&showUsername=" + showUsername + "&userVia=" + userVia + "&role=" + role + "&status=" + status
				+ "&deleted=" + deleted + "&modifyStatus=" + modifyStatus + "&createTime=" + createTime + "&modifyTime="
				+ modifyTime + "&remark=" + remark + "&roleName=" + roleName;
	}
    
    
}