package cn.hrsys.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.hrsys.pojo.TbRole;

public interface TbRoleService {
	/**
     * 查询角色数据条数
     * @return
     */
    int count();
    
    
    /**
     * 分页条件查询角色列表
     * @param PageNum 页数
     * @param DataNum 每页显示的数据条数
     * @param roleName 角色名称
     * @param status 角色状态
     * @return
     */
    List<TbRole> getAllTbRoleList(int PageNum,int DataNum,String roleName);
    
    
    /**
     * 查询后的数据条数
     * @param roleName
     * @return
     */
    int getAllTbRoleListPage(String roleName);
    
    /**
     * 查询启用角色
     * @return
     */
    List<TbRole> getAllHadRoleList();
    
    /**
     * 查询改角色是否被使用
     * @param roleID
     * @return
     */
    TbRole getAllRoleEmploy(String roleID);
    
    
    /**
     * 根据id查询单个角色
     * @param id 角色id
     * @return
     */
    TbRole getAllTbRole(String id);
    
    /**
     * 添加角色
     * @param tbRole 角色类
     * @return
     */
    int getAddTbRole(TbRole tbRole);
    
    /**
     * 根据id修改单个角色
     * @param tbRole 角色类
     * @return
     */
    int getAmendTbRole(TbRole tbRole);
    
    /**
     * 根据id删除单个角色
     * @param id
     * @return
     */
    int getDeleteTbRole(String id);
}
