package cn.hrsys.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cn.hrsys.mapper.RoleModelMapper;
import cn.hrsys.pojo.RoleModel;
import cn.hrsys.service.RoleModelService;

@Service("roleModelService")
public class RoleModelServiceImpl implements RoleModelService{

	@Resource
	private RoleModelMapper roleModelMapper;
	@Override
	public int getAddRoleModel(RoleModel roleModel) {
		// TODO Auto-generated method stub
		return roleModelMapper.getAddRoleModel(roleModel);
	}

	@Override
	public int getDeleteRoleModel(RoleModel roleModel) {
		// TODO Auto-generated method stub
		return roleModelMapper.getDeleteRoleModel(roleModel);
	}

}
