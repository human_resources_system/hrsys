package cn.hrsys.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cn.hrsys.mapper.PublicattributeInformationMapper;
import cn.hrsys.pojo.PublicattributeInformation;
import cn.hrsys.service.PublicattributeInformationService;

@Service("publicService")
public class PublicattributeInformationServiceImpl implements PublicattributeInformationService{

	@Resource
	private PublicattributeInformationMapper publicattributeInformationMapper;
	@Override
	public List<PublicattributeInformation> getAllpublic() {
		// TODO Auto-generated method stub
		return publicattributeInformationMapper.getAllpublic();
	}

}
