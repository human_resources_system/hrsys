package cn.hrsys.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cn.hrsys.mapper.TbUserMapper;
import cn.hrsys.pojo.TbUser;
import cn.hrsys.service.TbUserService;

@Service("tbUserService")
public class TbUserServiceImpl implements TbUserService{

	@Resource
	private TbUserMapper tbUserMapper;
	@Override
	public int count() {
		// TODO Auto-generated method stub
		return tbUserMapper.count();
	}

	@Override
	public TbUser getLogin(String logUsername, String logPassword) {
		// TODO Auto-generated method stub
		return tbUserMapper.getLogin(logUsername, logPassword);
	}

	@Override
	public List<TbUser> getAllUserList(int PageNum, int DataNum, String showUsername, String role) {
		// TODO Auto-generated method stub
		PageNum=(PageNum-1)*DataNum;
		return tbUserMapper.getAllUserList(PageNum, DataNum, showUsername, role);
	}

	@Override
	public TbUser getAllUser(int id) {
		// TODO Auto-generated method stub
		return tbUserMapper.getAllUser(id);
	}

	@Override
	public int getAddUser(TbUser tbUser) {
		// TODO Auto-generated method stub
		return tbUserMapper.getAddUser(tbUser);
	}

	@Override
	public int getAmendUser(TbUser tbUser) {
		// TODO Auto-generated method stub
		return tbUserMapper.getAmendUser(tbUser);
	}

	@Override
	public int getDeleteUser(int id) {
		// TODO Auto-generated method stub
		return tbUserMapper.getDeleteUser(id);
	}

	@Override
	public int getInquirylogUsername(String logUsername) {
		// TODO Auto-generated method stub
		return tbUserMapper.getInquirylogUsername(logUsername);
	}

	@Override
	public int getAllExistlogUsername(String logUsername) {
		// TODO Auto-generated method stub
		return tbUserMapper.getAllExistlogUsername(logUsername);
	}

	@Override
	public int getAllUserListPage(String showUsername, String role) {
		// TODO Auto-generated method stub
		return tbUserMapper.getAllUserListPage(showUsername, role);
	}

}
