package cn.hrsys.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cn.hrsys.mapper.TbRoleMapper;
import cn.hrsys.pojo.TbRole;
import cn.hrsys.service.TbRoleService;

@Service("tbRoleService")
public class TbRoleServiceImpl implements TbRoleService{

	@Resource
	private TbRoleMapper tbRoleMapper;
	
	@Override
	public int count() {
		// TODO Auto-generated method stub
		return tbRoleMapper.count();
	}

	@Override
	public List<TbRole> getAllTbRoleList(int PageNum, int DataNum, String roleName) {
		// TODO Auto-generated method stub
		PageNum=(PageNum-1)*DataNum;
		return tbRoleMapper.getAllTbRoleList(PageNum, DataNum, roleName);
	}

	@Override
	public TbRole getAllTbRole(String id) {
		// TODO Auto-generated method stub
		return tbRoleMapper.getAllTbRole(id);
	}

	@Override
	public int getAddTbRole(TbRole tbRole) {
		// TODO Auto-generated method stub
		return tbRoleMapper.getAddTbRole(tbRole);
	}

	@Override
	public int getAmendTbRole(TbRole tbRole) {
		// TODO Auto-generated method stub
		return tbRoleMapper.getAmendTbRole(tbRole);
	}

	@Override
	public int getDeleteTbRole(String id) {
		// TODO Auto-generated method stub
		return tbRoleMapper.getDeleteTbRole(id);
	}

	@Override
	public TbRole getAllRoleEmploy(String roleID) {
		// TODO Auto-generated method stub
		return tbRoleMapper.getAllRoleEmploy(roleID);
	}

	@Override
	public List<TbRole> getAllHadRoleList() {
		// TODO Auto-generated method stub
		return tbRoleMapper.getAllHadRoleList();
	}

	@Override
	public int getAllTbRoleListPage(String roleName) {
		// TODO Auto-generated method stub
		return tbRoleMapper.getAllTbRoleListPage(roleName);
	}
	
}
