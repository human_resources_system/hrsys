package cn.hrsys.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cn.hrsys.mapper.AgentMapper;
import cn.hrsys.pojo.Agent;
import cn.hrsys.pojo.PublicattributeInformation;
import cn.hrsys.service.AgentService;

@Service("agentService")
public class AgentServiceImpl implements AgentService{

	@Resource
	private AgentMapper agentMapper;


	@Override
	public int getAmendAgent(Agent agent) {
		// TODO Auto-generated method stub
		return agentMapper.getAmendAgent(agent);
	}


	@Override
	public List<Agent> getAllAgentList(int PageNum, int DataNum, int type1, int type2, int type3, int type4, String name,
			Date date) {
		// TODO Auto-generated method stub
		PageNum=(PageNum-1)*DataNum;
		return agentMapper.getAllAgentList(PageNum, DataNum, type1, type2, type3, type4, name, date);
	}


	@Override
	public int getAllAgentListPage(int type1, int type2, int type3, int type4, String name, Date date) {
		// TODO Auto-generated method stub
		return agentMapper.getAllAgentListPage(type1, type2, type3, type4, name, date);
	}



}
