package cn.hrsys.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cn.hrsys.mapper.TbModelMapper;
import cn.hrsys.pojo.TbModel;
import cn.hrsys.service.TbModelService;

@Service("tbModelService")
public class TbModelServiceImpl implements TbModelService{

	@Resource
	private TbModelMapper tbModelMapper;
	@Override
	public List<TbModel> getAllTbModelList(int PageNum,int DataNum,String modelName) {
		// TODO Auto-generated method stub
		PageNum=(PageNum-1)*DataNum;
		return tbModelMapper.getAllTbModelList(PageNum,DataNum,modelName);
	}


	@Override
	public List<TbModel> getAllTbUserNotTbModelList(int PageNum, int DataNum, String roleID, String modelName) {
		// TODO Auto-generated method stub
		PageNum=(PageNum-1)*DataNum;
		return tbModelMapper.getAllTbUserNotTbModelList(PageNum, DataNum, roleID, modelName);
	}


	@Override
	public List<TbModel> getAllTbUserHadTbModelList(int PageNum, int DataNum, String roleID, String modelName) {
		// TODO Auto-generated method stub
		PageNum=(PageNum-1)*DataNum;
		return tbModelMapper.getAllTbUserHadTbModelList(PageNum, DataNum, roleID, modelName);
	}
	
	
	@Override
	public int getAddTbModel(TbModel tbModel) {
		// TODO Auto-generated method stub
		return tbModelMapper.getAddTbModel(tbModel);
	}

	@Override
	public int getAmendTbModel(TbModel tbModel) {
		// TODO Auto-generated method stub
		return tbModelMapper.getAmendTbModel(tbModel);
	}

	@Override
	public int getDeleteTbModel(String id) {
		// TODO Auto-generated method stub
		return tbModelMapper.getDeleteTbModel(id);
	}

	@Override
	public TbModel getAllmodelID(String modelID) {
		// TODO Auto-generated method stub
		return tbModelMapper.getAllmodelID(modelID);
	}

	@Override
	public int count() {
		// TODO Auto-generated method stub
		return tbModelMapper.count();
	}


	@Override
	public int notModelCount(String roleID,String modelName) {
		// TODO Auto-generated method stub
		return tbModelMapper.notModelCount(roleID,modelName);
	}


	@Override
	public int hadModelCount(String roleID,String modelName) {
		// TODO Auto-generated method stub
		return tbModelMapper.hadModelCount(roleID,modelName);
	}


	@Override
	public List<TbModel> getAllTbUserTbModelList(String roleID,int orderBy) {
		// TODO Auto-generated method stub
		return tbModelMapper.getAllTbUserTbModelList(roleID,orderBy);
	}


	@Override
	public int getAllUserAuthority3(String roleID, String modelName) {
		// TODO Auto-generated method stub
		return tbModelMapper.getAllUserAuthority3(roleID, modelName);
	}


	@Override
	public List<TbModel> getAllFatherAuth() {
		// TODO Auto-generated method stub
		return tbModelMapper.getAllFatherAuth();
	}


	@Override
	public List<TbModel> getAllAuthModule() {
		// TODO Auto-generated method stub
		return tbModelMapper.getAllAuthModule();
	}


	@Override
	public int getAllSonAuth(String parent) {
		// TODO Auto-generated method stub
		return tbModelMapper.getAllSonAuth(parent);
	}


	@Override
	public List<TbModel> getAllAuthority() {
		// TODO Auto-generated method stub
		return tbModelMapper.getAllAuthority();
	}


	@Override
	public int getAllTbModelListPage(String modelName) {
		// TODO Auto-generated method stub
		return tbModelMapper.getAllTbModelListPage(modelName);
	}


	

}
