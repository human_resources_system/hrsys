package cn.hrsys.service;

import org.apache.ibatis.annotations.Param;

import cn.hrsys.pojo.RoleModel;

public interface RoleModelService {
	/**
     * 给指定角色添加权限
     * @param roleID 角色id
     * @param modelID 权限id
     * @return
     */
    int getAddRoleModel(RoleModel roleModel);
    
    /**
     * 删除指定角色的权限
     * @param modelID
     * @return
     */
    int getDeleteRoleModel(RoleModel roleModel);
}
