package cn.hrsys.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.hrsys.pojo.TbUser;

public interface TbUserService {
	 /**
     * 查询用户数据条数
     * @return
     */
    int count();
    
    /**
     * 查询是否存在该用户并启用
     * @param logUsername
     * @return
     */
    int getInquirylogUsername(String logUsername);
    
    
    /**
     * 查询是否存在该用户名
     * @param logUsername
     * @return
     */
    int getAllExistlogUsername(String logUsername);
    
    
    /**
     * 用户登陆
     * @param logUsername 用户账号
     * @param logPassword 用户密码
     * @return
     */
    TbUser getLogin(@Param("logUsername")String logUsername,@Param("logPassword")String logPassword);
    
    /**
     * 分页显示用户列表
     * @param PageNum 页数
     * @param DataNum 每页显示的数据条数
     * @param showUsername 用户名称
     * @param role 角色id
     * @return
     */
    List<TbUser> getAllUserList(@Param("PageNum")int PageNum,@Param("DataNum")int DataNum,
    		@Param("showUsername")String showUsername,@Param("role")String role);
    
    
    /**
     * 查询后的页数
     * @param showUsername
     * @param role
     * @return
     */
    int getAllUserListPage(@Param("showUsername")String showUsername,@Param("role")String role);
    
    /**
     * 根据id查询单个用户信息
     * @param id
     * @return
     */
    TbUser getAllUser(int id);
    
    /**
     * 添加用户
     * @param tbUser 用户类
     * @return
     */
    int getAddUser(TbUser tbUser);
    
    /**
     * 根据id修改单个用户
     * @param tbUser 用户表
     * @return
     */
    int getAmendUser(TbUser tbUser);
    
    /**
     * 根据id删除单个用户
     * @param id
     * @return
     */
    int getDeleteUser(int id);
}
