package cn.hrsys.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.hrsys.pojo.TbModel;

public interface TbModelService {
	
	
	/**
     * 查询数据条数
     * @return
     */
    int count();
    
    
    /**
     * 查询用户没有的权限条数
     * @return
     */
    int notModelCount(String roleID,String modelName);
    
    
    /**
     * 查询用户有的权限条数
     * @return
     */
    int hadModelCount(String roleID,String modelName);
    
    
	/**
     * 查询权限
     * @return
     */
    List<TbModel> getAllTbModelList(int PageNum,int DataNum,
    		String modelName);
    
    /**
     * 查询后权限数据条数
     * @return
     */
    int getAllTbModelListPage(String modelName);
    
    /**
     * 查询所有权限
     * @return
     */
    List<TbModel> getAllAuthority();
    
    /**
     * 查询用户没有的权限
     * @param modelID 用户id
     * @return
     */
    List<TbModel> getAllTbUserNotTbModelList(int PageNum,int DataNum,String roleID,String modelName);
    
    /**
     * 查询用户有的权限
     * @param modelID 用户id
     * @return
     */
    List<TbModel> getAllTbUserHadTbModelList(int PageNum,int DataNum,String roleID,String modelName);
    
    
    /**
     * 查询用户权限
     * @param roleID
     * @return
     */
    List<TbModel> getAllTbUserTbModelList(String roleID,int orderBy);
    
    
    
    /**
     * 查询有没有子级权限
     * @param parent
     * @return
     */
    int getAllSonAuth(String parent);
    
    /**
     * 查询用户的三级权限
     * @param roleID
     * @param modelName
     * @return
     */
    int getAllUserAuthority3(String roleID,
    		String modelName);
    
    
    /**
     * 查询所有父级权限
     * @return
     */
    List<TbModel> getAllFatherAuth();
    
    
    /**
     * 查询权限模块
     * @return
     */
    List<TbModel> getAllAuthModule();
    
    /**
     * 查询是否存在相同id
     * @param modelID
     * @return
     */
    TbModel getAllmodelID(String modelID);
    
    /**
     * 添加权限
     * @param tbModel 权限类
     * @return
     */
    int getAddTbModel(TbModel tbModel);
    
    /**
     * 跟俊id修改单个权限
     * @param tbModel 权限修改
     * @return
     */
    int getAmendTbModel(TbModel tbModel);
    
    /**
     * 根据id删除单个权限
     * @param id
     * @return
     */
    int getDeleteTbModel(String id);
}
