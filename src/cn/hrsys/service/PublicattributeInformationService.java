package cn.hrsys.service;

import java.util.List;

import cn.hrsys.pojo.PublicattributeInformation;

public interface PublicattributeInformationService {
	/**
     * 查询代办类型
     * @return
     */
    List<PublicattributeInformation> getAllpublic();
}
