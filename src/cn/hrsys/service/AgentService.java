package cn.hrsys.service;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.hrsys.pojo.Agent;
import cn.hrsys.pojo.PublicattributeInformation;

public interface AgentService {
	/**
     * 分页查询待办类型类
     * @param PageNum 
     * @param DataNum
     * @param type 待办类型
     * @return
     */
	List<Agent> getAllAgentList(@Param("PageNum")int PageNum,@Param("DataNum")int DataNum,
    		@Param("1type")int type1,@Param("2type")int type2,@Param("3type")int type3,@Param("type4")int type4,
    		@Param("name")String name,@Param("date")Date date);
	
	
	/**
     * 查询待办类型类条数
     * @param type1
     * @param type2
     * @param type3
     * @param type4
     * @param name
     * @param date
     * @return
     */
    int getAllAgentListPage(@Param("type1")int type1,@Param("type2")int type2,@Param("type3")int type3,@Param("type4")int type4,
    		@Param("name")String name,@Param("date")Date date);
    
    /**
     * 根据id修改待办状态
     * @param id 待办id
     * @return
     */
    int getAmendAgent(Agent agent);
}
