package cn.hrsys.mapper;

import cn.hrsys.pojo.MonthStatistics;
import cn.hrsys.pojo.MonthStatisticsExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
/**
 * 月信息统计接口类
 * @author 11388
 *
 */
public interface MonthStatisticsMapper {
    int countByExample(MonthStatisticsExample example);

    int deleteByExample(MonthStatisticsExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(MonthStatistics record);

    int insertSelective(MonthStatistics record);

    List<MonthStatistics> selectByExample(MonthStatisticsExample example);

    MonthStatistics selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") MonthStatistics record, @Param("example") MonthStatisticsExample example);

    int updateByExample(@Param("record") MonthStatistics record, @Param("example") MonthStatisticsExample example);

    int updateByPrimaryKeySelective(MonthStatistics record);

    int updateByPrimaryKey(MonthStatistics record);
    
    
    /**
     * 查询月信息统计总记录数
     * @return
     */
    int count();
    
    /**
     * 查询月信息统计并分页显示
     * @param PageNum 当前页数
     * @param DataNum	
     * @param name
     * @param departmentMountId
     * @param month
     * @return
     */
    List<MonthStatistics> getAllMonthStatisticsInfo(@Param("PageNum")Integer PageNum,
    		@Param("DataNum")Integer DataNum,
    		@Param("name")String name,
    		@Param("departmentMountId")int departmentMountId,
    		@Param("month")String month);

    
}