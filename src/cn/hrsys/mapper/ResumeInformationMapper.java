package cn.hrsys.mapper;

import cn.hrsys.pojo.ResumeInformation;
import cn.hrsys.pojo.ResumeInformationExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ResumeInformationMapper {
    int countByExample(ResumeInformationExample example);

    int deleteByExample(ResumeInformationExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(ResumeInformation record);

    int insertSelective(ResumeInformation record);

    List<ResumeInformation> selectByExample(ResumeInformationExample example);

    ResumeInformation selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") ResumeInformation record, @Param("example") ResumeInformationExample example);

    int updateByExample(@Param("record") ResumeInformation record, @Param("example") ResumeInformationExample example);

    int updateByPrimaryKeySelective(ResumeInformation record);

    int updateByPrimaryKey(ResumeInformation record);
}