package cn.hrsys.mapper;

import cn.hrsys.pojo.AttendanceInformation;
import cn.hrsys.pojo.AttendanceInformationExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
/**
 * 考勤信息管理接口类
 * @author 11388
 *
 */
public interface AttendanceInformationMapper {
    int countByExample(AttendanceInformationExample example);

    int deleteByExample(AttendanceInformationExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(AttendanceInformation record);

    int insertSelective(AttendanceInformation record);

    List<AttendanceInformation> selectByExample(AttendanceInformationExample example);

    AttendanceInformation selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") AttendanceInformation record, @Param("example") AttendanceInformationExample example);

    int updateByExample(@Param("record") AttendanceInformation record, @Param("example") AttendanceInformationExample example);

    int updateByPrimaryKeySelective(AttendanceInformation record);

    int updateByPrimaryKey(AttendanceInformation record);
    
    
    /**
     * 查询考勤管理信息总记录数
     * @return
     */
    int count();
    
    /**
     * 查询考勤信息、出差信息、加班信息、请假信息并分页显示
     */
    List<AttendanceInformation> getAllAttendanceInformationInfo(@Param("PageNum")Integer PageNum,
    		@Param("DataNum")Integer DataNum,
    		@Param("name")String name,
    		@Param("departmentMountId")int departmentMountId,
    		@Param("attendanceStatus")int attendanceStatus,
    		@Param("workingDate")String workingDate);

    
    
}