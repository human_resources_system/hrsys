package cn.hrsys.mapper;

import cn.hrsys.pojo.PublicattributeInformation;
import cn.hrsys.pojo.PublicattributeInformationExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PublicattributeInformationMapper {
    int countByExample(PublicattributeInformationExample example);

    int deleteByExample(PublicattributeInformationExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(PublicattributeInformation record);

    int insertSelective(PublicattributeInformation record);

    List<PublicattributeInformation> selectByExample(PublicattributeInformationExample example);

    PublicattributeInformation selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") PublicattributeInformation record, @Param("example") PublicattributeInformationExample example);

    int updateByExample(@Param("record") PublicattributeInformation record, @Param("example") PublicattributeInformationExample example);

    int updateByPrimaryKeySelective(PublicattributeInformation record);

    int updateByPrimaryKey(PublicattributeInformation record);
    
    
    /**
     * 查询代办类型
     * @return
     */
    List<PublicattributeInformation> getAllpublic();
}