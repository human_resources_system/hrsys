package cn.hrsys.mapper;

import cn.hrsys.pojo.SubjectInformation;
import cn.hrsys.pojo.SubjectInformationExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SubjectInformationMapper {
    int countByExample(SubjectInformationExample example);

    int deleteByExample(SubjectInformationExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(SubjectInformation record);

    int insertSelective(SubjectInformation record);

    List<SubjectInformation> selectByExample(SubjectInformationExample example);

    SubjectInformation selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") SubjectInformation record, @Param("example") SubjectInformationExample example);

    int updateByExample(@Param("record") SubjectInformation record, @Param("example") SubjectInformationExample example);

    int updateByPrimaryKeySelective(SubjectInformation record);

    int updateByPrimaryKey(SubjectInformation record);
}