package cn.hrsys.mapper;

import cn.hrsys.pojo.RewardstandardInformation;
import cn.hrsys.pojo.RewardstandardInformationExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
/**
 * 薪酬标准接口类
 * @author 11388
 *
 */
public interface RewardstandardInformationMapper {
	
	
	
    int countByExample(RewardstandardInformationExample example);

    int deleteByExample(RewardstandardInformationExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(RewardstandardInformation record);

    int insertSelective(RewardstandardInformation record);

    List<RewardstandardInformation> selectByExample(RewardstandardInformationExample example);

    RewardstandardInformation selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") RewardstandardInformation record, @Param("example") RewardstandardInformationExample example);

    int updateByExample(@Param("record") RewardstandardInformation record, @Param("example") RewardstandardInformationExample example);

    int updateByPrimaryKeySelective(RewardstandardInformation record);

    int updateByPrimaryKey(RewardstandardInformation record);
    
    
    /**
     * 查询薪酬标准信息总记录数
     */
    int count();
    
    /**
     * 查询所有薪酬标准信息并分页显示
     * @return
     */
    List<RewardstandardInformation> getAllRewardstandardInfo(@Param("PageNum")String PageNum,@Param("DataNum")String DataNum,@Param("rewardCode")String rewardCode,@Param("auditStatus")int auditStatus,@Param("registerDate") String registerDate);
    
    /**
     * 新增薪酬标准信息
     */
    int insertAllRewardstandardInfo(RewardstandardInformation standard);
    
    /**
     * 根据id修改薪酬标准信息
     * @param record
     * @return
     */
    int updateRewardstandardInfoById(RewardstandardInformation standard);
    
    /**
     * 根据id删除薪酬标准信息
     * @param id
     * @return
     */
    int deleteRewardstandardInfoById(int id);
    
    /**
     * 根据id查看一条薪酬标准信息
     * @param id
     * @return
     */
    RewardstandardInformation getRewardstandardInfoById(int id);

    
}