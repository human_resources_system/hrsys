package cn.hrsys.mapper;

import cn.hrsys.pojo.Agent;
import cn.hrsys.pojo.AgentExample;
import cn.hrsys.pojo.PublicattributeInformation;

import java.util.Date;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface AgentMapper {
    int countByExample(AgentExample example);

    int deleteByExample(AgentExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Agent record);

    int insertSelective(Agent record);

    List<Agent> selectByExample(AgentExample example);

    Agent selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Agent record, @Param("example") AgentExample example);

    int updateByExample(@Param("record") Agent record, @Param("example") AgentExample example);

    int updateByPrimaryKeySelective(Agent record);

    int updateByPrimaryKey(Agent record);
    
    /**
     * 分页查询待办类型类
     * @param PageNum 
     * @param DataNum
     * @param type 待办类型
     * @return
     */
    List<Agent> getAllAgentList(@Param("PageNum")int PageNum,@Param("DataNum")int DataNum,
    		@Param("type1")int type1,@Param("type2")int type2,@Param("type3")int type3,@Param("type4")int type4,
    		@Param("name")String name,@Param("date")Date date);
    
    /**
     * 查询待办类型类条数
     * @param type1
     * @param type2
     * @param type3
     * @param type4
     * @param name
     * @param date
     * @return
     */
    int getAllAgentListPage(@Param("type1")int type1,@Param("type2")int type2,@Param("type3")int type3,@Param("type4")int type4,
    		@Param("name")String name,@Param("date")Date date);
    
    /**
     * 根据id修改待办状态
     * @param id 待办id
     * @return
     */
    int getAmendAgent(Agent agent);
    
    
}