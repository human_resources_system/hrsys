package cn.hrsys.mapper;

import cn.hrsys.pojo.TbOperationLog;
import cn.hrsys.pojo.TbOperationLogExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TbOperationLogMapper {
    int countByExample(TbOperationLogExample example);

    int deleteByExample(TbOperationLogExample example);

    int deleteByPrimaryKey(Long logID);

    int insert(TbOperationLog record);

    int insertSelective(TbOperationLog record);

    List<TbOperationLog> selectByExample(TbOperationLogExample example);

    TbOperationLog selectByPrimaryKey(Long logID);

    int updateByExampleSelective(@Param("record") TbOperationLog record, @Param("example") TbOperationLogExample example);

    int updateByExample(@Param("record") TbOperationLog record, @Param("example") TbOperationLogExample example);

    int updateByPrimaryKeySelective(TbOperationLog record);

    int updateByPrimaryKey(TbOperationLog record);
    
    
    /**
     * 分页显示日志类
     * @param PageNum 页数
     * @param DataNum 每页显示条数
     * @param showUsername 用户名称
     */
    List<TbOperationLog> getAllTbOperationLogList(@Param("PageNum")int PageNum,@Param("DataNum")int DataNum,
    		@Param("showUsername")String showUsername);

}