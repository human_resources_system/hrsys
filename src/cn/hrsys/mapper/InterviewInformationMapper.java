package cn.hrsys.mapper;

import cn.hrsys.pojo.InterviewInformation;
import cn.hrsys.pojo.InterviewInformationExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface InterviewInformationMapper {
    int countByExample(InterviewInformationExample example);

    int deleteByExample(InterviewInformationExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(InterviewInformation record);

    int insertSelective(InterviewInformation record);

    List<InterviewInformation> selectByExample(InterviewInformationExample example);

    InterviewInformation selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") InterviewInformation record, @Param("example") InterviewInformationExample example);

    int updateByExample(@Param("record") InterviewInformation record, @Param("example") InterviewInformationExample example);

    int updateByPrimaryKeySelective(InterviewInformation record);

    int updateByPrimaryKey(InterviewInformation record);
}