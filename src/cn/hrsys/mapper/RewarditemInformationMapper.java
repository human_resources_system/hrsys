package cn.hrsys.mapper;

import cn.hrsys.pojo.RewarditemInformation;
import cn.hrsys.pojo.RewarditemInformationExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface RewarditemInformationMapper {
    int countByExample(RewarditemInformationExample example);

    int deleteByExample(RewarditemInformationExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(RewarditemInformation record);

    int insertSelective(RewarditemInformation record);

    List<RewarditemInformation> selectByExample(RewarditemInformationExample example);

    RewarditemInformation selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") RewarditemInformation record, @Param("example") RewarditemInformationExample example);

    int updateByExample(@Param("record") RewarditemInformation record, @Param("example") RewarditemInformationExample example);

    int updateByPrimaryKeySelective(RewarditemInformation record);

    int updateByPrimaryKey(RewarditemInformation record);
}