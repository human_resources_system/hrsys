package cn.hrsys.mapper;

import cn.hrsys.pojo.RewardgrantInformation;
import cn.hrsys.pojo.RewardgrantInformationExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
/**
 * 薪酬发放接口类
 * @author 11388
 *
 */
public interface RewardgrantInformationMapper {
    int countByExample(RewardgrantInformationExample example);

    int deleteByExample(RewardgrantInformationExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(RewardgrantInformation record);

    int insertSelective(RewardgrantInformation record);

    List<RewardgrantInformation> selectByExample(RewardgrantInformationExample example);

    RewardgrantInformation selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") RewardgrantInformation record, @Param("example") RewardgrantInformationExample example);

    int updateByExample(@Param("record") RewardgrantInformation record, @Param("example") RewardgrantInformationExample example);

    int updateByPrimaryKeySelective(RewardgrantInformation record);

    int updateByPrimaryKey(RewardgrantInformation record);
    
    
    /**
     * 查询薪酬信息总记录数
     * @return
     */
    int count();
    
    /**
     * 查看所有薪酬单信息
     * @param PageNum
     * @param DataNum
     * @return
     */
    List<RewardgrantInformation> getAllRewardgrantInformationInfo(@Param("PageNum")Integer PageNum,
    		@Param("DataNum")Integer DataNum);
    
    /**
     * 新增薪酬单信息
     * @param grant
     * @return
     */
    int insertRewardgrantInformationInfo(RewardgrantInformation grant);
    
    /**
     * 根据id修改薪酬单信息
     * @param grant
     * @return
     */
    int updateRewardgrantInformationInfoById(RewardgrantInformation grant);
    /**
     * 根据id删除薪酬单信息
     * @param id
     * @return
     */
    int deleteRewardgrantInformationInfoById(int id);

    
}