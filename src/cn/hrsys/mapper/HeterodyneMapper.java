package cn.hrsys.mapper;

import cn.hrsys.pojo.Heterodyne;
import cn.hrsys.pojo.HeterodyneExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface HeterodyneMapper {
    int countByExample(HeterodyneExample example);

    int deleteByExample(HeterodyneExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Heterodyne record);

    int insertSelective(Heterodyne record);

    List<Heterodyne> selectByExample(HeterodyneExample example);

    Heterodyne selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Heterodyne record, @Param("example") HeterodyneExample example);

    int updateByExample(@Param("record") Heterodyne record, @Param("example") HeterodyneExample example);

    int updateByPrimaryKeySelective(Heterodyne record);

    int updateByPrimaryKey(Heterodyne record);
}