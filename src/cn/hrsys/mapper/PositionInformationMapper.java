package cn.hrsys.mapper;

import cn.hrsys.pojo.PositionInformation;
import cn.hrsys.pojo.PositionInformationExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PositionInformationMapper {
    int countByExample(PositionInformationExample example);

    int deleteByExample(PositionInformationExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(PositionInformation record);

    int insertSelective(PositionInformation record);

    List<PositionInformation> selectByExample(PositionInformationExample example);

    PositionInformation selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") PositionInformation record, @Param("example") PositionInformationExample example);

    int updateByExample(@Param("record") PositionInformation record, @Param("example") PositionInformationExample example);

    int updateByPrimaryKeySelective(PositionInformation record);

    int updateByPrimaryKey(PositionInformation record);
}