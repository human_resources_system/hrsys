package cn.hrsys.mapper;

import cn.hrsys.pojo.RoleModel;
import cn.hrsys.pojo.RoleModelExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface RoleModelMapper {
    int countByExample(RoleModelExample example);

    int deleteByExample(RoleModelExample example);

    int deleteByPrimaryKey(String roleID);

    int insert(RoleModel record);

    int insertSelective(RoleModel record);

    List<RoleModel> selectByExample(RoleModelExample example);

    RoleModel selectByPrimaryKey(String roleID);

    int updateByExampleSelective(@Param("record") RoleModel record, @Param("example") RoleModelExample example);

    int updateByExample(@Param("record") RoleModel record, @Param("example") RoleModelExample example);

    int updateByPrimaryKeySelective(RoleModel record);

    int updateByPrimaryKey(RoleModel record);
    
    
    /**
     * 给指定角色添加权限
     * @param roleID 角色id
     * @param modelID 权限id
     * @return
     */
    int getAddRoleModel(RoleModel roleModel);
    
    /**
     * 删除指定角色的权限
     * @param modelID
     * @return
     */
    int getDeleteRoleModel(RoleModel roleModel);
}