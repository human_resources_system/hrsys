package cn.hrsys.mapper;

import cn.hrsys.pojo.DepartmentInformation;
import cn.hrsys.pojo.DepartmentInformationExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface DepartmentInformationMapper {
   
    
     
    /**
     * 查询父级部门
     * @return
     */
    List<DepartmentInformation> getAllDepar();
}