package cn.hrsys.mapper;

import cn.hrsys.pojo.RecruitInformation;
import cn.hrsys.pojo.RecruitInformationExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface RecruitInformationMapper {
    int countByExample(RecruitInformationExample example);

    int deleteByExample(RecruitInformationExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(RecruitInformation record);

    int insertSelective(RecruitInformation record);

    List<RecruitInformation> selectByExample(RecruitInformationExample example);

    RecruitInformation selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") RecruitInformation record, @Param("example") RecruitInformationExample example);

    int updateByExample(@Param("record") RecruitInformation record, @Param("example") RecruitInformationExample example);

    int updateByPrimaryKeySelective(RecruitInformation record);

    int updateByPrimaryKey(RecruitInformation record);
    
    
    /**
     * 查询招聘信息并显示出来
     * @param PageNum 页数
     * @param DataNum 每页显示条数
     * @param recName 职位名字
     * @param recClass 职位分类
     * @param recCoeckDate 登记日期
     * @param recExpirationDate 截止日期
     * @return List  招聘信息集合
     */
     
    List<RecruitInformation>  getAllRecruitInformation(@Param("PageNum")Integer PageNum,@Param("DataNum")Integer DataNum,@Param("recName") String recName, @Param("recClass") String recClass,@Param("recCoeckDate")String recCoeckDate, @Param("recExpirationDate") String recExpirationDate);

    /**
     * 新增招聘信息
     * @param recruitInformation  招聘实体
     * @return  1  0 
     */
    int getAddRecruitInformation(RecruitInformation recruitInformation);
    
    /**
     * 修改招聘信息
     * @param recruitInformation
     * @return
     */
    int getUpdateRecruitInformation(RecruitInformation recruitInformation);
    
    /**
     * 删除招聘信息
     * @param recruitInformation
     * @return
     */
    int getDeteleRecruitInformation(Integer id);

    /**
     * 查询总条数
     * @return
     */
    int count();
    
}