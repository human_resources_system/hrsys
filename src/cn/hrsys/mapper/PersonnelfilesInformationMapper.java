package cn.hrsys.mapper;

import cn.hrsys.pojo.PersonnelfilesInformation;
import cn.hrsys.pojo.PersonnelfilesInformationExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
/**
 * 人事档案信息接口类
 * @author 11388
 *
 */
public interface PersonnelfilesInformationMapper {
    int countByExample(PersonnelfilesInformationExample example);

    int deleteByExample(PersonnelfilesInformationExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(PersonnelfilesInformation record);

    int insertSelective(PersonnelfilesInformation record);

    List<PersonnelfilesInformation> selectByExample(PersonnelfilesInformationExample example);

    PersonnelfilesInformation selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") PersonnelfilesInformation record, @Param("example") PersonnelfilesInformationExample example);

    int updateByExample(@Param("record") PersonnelfilesInformation record, @Param("example") PersonnelfilesInformationExample example);

    int updateByPrimaryKeySelective(PersonnelfilesInformation record);

    int updateByPrimaryKey(PersonnelfilesInformation record);
    
    /**
     * 查询人事档案信息总记录数
     */
    int count();
    
    /**
     * 查询所有人事档案信息，并分页显示
     * @param PageNum
     * @param DataNum
     * @param personnelCode
     * @param auditStatus
     * @param filesStatus
     * @param registerDate
     * @return
     */
    List<PersonnelfilesInformation> getAllPersonnelfilesInformationInfo(@Param("PageNum")Integer PageNum,
    		@Param("DataNum")Integer DataNum,
    		@Param("personnelCode")String personnelCode,
    		@Param("auditStatus")int auditStatus,
    		@Param("filesStatus")int filesStatus,
    		@Param("registerDate")String registerDate);
    
    /**
     * 新增人事档案信息
     * @param person
     * @return
     */
    int insertPersonnelfilesInformationInfo(PersonnelfilesInformation person);
    
    /**
     * 根据id修改人事档案信息
     * @param person
     * @return
     */
    int updatePersonnelfilesInformationInfoById(PersonnelfilesInformation person);
    
    /**
     * 根据id删除人事档案信息
     * @param id
     * @return
     */
    int deletePersonnelfilesInformationInfoById(int id);
    
    /**
     * 根据id恢复人事档案信息
     * @param id
     * @return
     */
    int recoverPersonnelfilesInformationInfoById(int id);

    
    
}