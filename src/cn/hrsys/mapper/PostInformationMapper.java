package cn.hrsys.mapper;

import cn.hrsys.pojo.PostInformation;
import cn.hrsys.pojo.PostInformationExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PostInformationMapper {
    int countByExample(PostInformationExample example);

    int deleteByExample(PostInformationExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(PostInformation record);

    int insertSelective(PostInformation record);

    List<PostInformation> selectByExample(PostInformationExample example);

    PostInformation selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") PostInformation record, @Param("example") PostInformationExample example);

    int updateByExample(@Param("record") PostInformation record, @Param("example") PostInformationExample example);

    int updateByPrimaryKeySelective(PostInformation record);

    int updateByPrimaryKey(PostInformation record);
}