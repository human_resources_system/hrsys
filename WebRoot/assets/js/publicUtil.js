
var path="/hrSYS";
$(function(){
	checkLogin();
});
//验证是否登录
function checkLogin(){
	//获取cookie值
	var cookieValue=$.cookie("tbUser");
	//判断cookie值,为空则提示未登录并跳转登录页面
	if(""==cookieValue ||null==cookieValue ||undefined==cookieValue){
		alert("用户未登录！请登录");
		window.location.href ="/hrSYS/";
	}else{
		//显示用户信息到页面中
		$(".tbshowUsername").html(getCookie("tbUser","showUsername"));
		$("#tbuserVia").attr("src",getCookie("tbUser","userVia"));
		$(".tbroleName").html(getCookie("tbUser","roleName"));
	}
}

//用户注销
$(".userlogout").click(function(){
	$.cookie("tbUser","",{path: '/'});
	window.location.href ="/hrSYS/";
});

//获取cookie值 cookieKeyName:键名称，cookieValueName:值名称
function getCookie(cookieKeyName,cookieValueName){
	//获取cookie值
	var cookieKeyValue=$.cookie(cookieKeyName);
	//判断cookie值是否为空，为空则提示未登录，返回登录页面
	if(undefined!=cookieKeyValue||null!=cookieKeyValue||""!=cookieKeyValue){
		//将cookie值进行分割成数组
		var cookieArray=cookieKeyValue.split("&");
		for (var i = 0; i < cookieArray.length; i++) {
			var array = cookieArray[i].split("=");
			//将key和value分开
			var valueName = array[0].replace(/\s+/g, "");
			var Value=array[1];
			//对比值名称，对应则返回
			if(cookieValueName==valueName){
				if (cookieKeyValue != "") {
					//进行 URI 解码
	                return decodeURI(Value);
	            } else {
	                return "";
	            }
			}
		}
	}else{
		alert("用户未登录！请登录");
		window.location.href ="/hrSYS/";
	}
}
//查询路径上的数据 getPath("id") 
function getPath(name) { 
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i"); 
	var r = window.location.search.substr(1).match(reg); 
	if (r != null) return unescape(r[2]); return null; 
	}
