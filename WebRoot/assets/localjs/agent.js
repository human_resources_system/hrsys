/**
 * 
 */

function addValue(ele) {
	if (ele.getAttribute("href") == "agen/agencyList.html") {
		var role = getCookie("tbUser", "role");
		Id = ele.getAttribute("id");
		$("#" + Id).attr("href", "agen/agencyList.html?role=" + role);
	}
}

$("#agentnextPage").click(function() {
	var PageNum = $("#PageNum").html();
	var Page = $("#Page").html();
	if (PageNum == Page) {
		alert("已经是尾页了哦！");
	} else {
		agentList(parseInt(PageNum) + 1);
	}

});

$("#agentprevPage").click(function() {
	var PageNum = $("#PageNum").html();
	if (PageNum == 1) {
		alert("已经是首页了哦！");
	} else if (PageNum == 0) {
		alert("没有上一页了！");
		roleList(1);
	} else {
		agentList(parseInt(PageNum) - 1);
	}
});

$("#agenthomePage").click(function() {
	agentList(1);
});

$("#agentendPage").click(function() {
	var Page = $("#Page").html();
	agentList(Page);
});

$("#selectagent").click(function() {
	agentList(1);
});


function agentList(PageNum) {
	var agentname = $("#agentname").val();
	var agenttype = $("#agenttype").val();
	var agentdate = $("#date").val();
	var UserRole = getCookie("tbUser", "role");
	$.ajax({
		type : "GET",
		url : "agentList",
		dataType : "json",
		data : {
			"agentname" : agentname,
			"agenttype" : agenttype,
			"agentdate" : agentdate,
			"UserRole" : UserRole,
			"PageNum" : PageNum
		},
		success : function(data) {
			var pageStr = '';
			if (data != '') {
				for (var js in data) {
					var dates = new Date(data[js].date);
					var date = dates.Format("yyyy-MM-dd");
					pageStr += '<tr>' +
						'<td>' + data[js].typeName + '</td>' +
						'<td>' + data[js].name + '</td>' +
						'<td>' + date + '</td>' +
						'<td>' +
						'<a><img src="assets/icon_9m8sb4cx6z/update.png" style="width: 15px;cursor:pointer;" onclick="agentItem('+
						"'"+data[js].type+"'"+
						",'"+data[js].agentId+"'"+
						')" title="处理"/></a>' +
						'</td>' +
						'</tr>';
				}
				$("#agentlist").html(pageStr);
				$("#PageNum").html(PageNum);
				agentListPage(agentname, agenttype, agentdate, UserRole);
			} else {
				pageStr += '<tr><td colspan="4">没有该数据哦~~</td></tr>';
				$("#agentlist").html(pageStr);
				$("#agentname").val("");
				$("#agenttype").val("");
				$("#date").val("");
				$("#PageNum").html(0);
				$("#Page").html(0);
			}
		},
		error : function() {
			alert("服务器请求失败");
		}
	});
}



function agentListPage(agentname, agenttype, agentdate, UserRole) {
	$.ajax({
		type : "GET",
		url : "agentListPage",
		dataType : "json",
		data : {
			"agentname" : agentname,
			"agenttype" : agenttype,
			"agentdate" : agentdate,
			"UserRole" : UserRole
		},
		success : function(data) {
			$("#Page").html(data);
		},
		error : function() {
			alert("服务器请求失败");
		}
	});
}


function agentItem(type,itemId){
	if(type==1){
		alert(itemId);
	}else if(type==2){
		alert(itemId);
	}else if(type==3){
		alert(itemId);
	}else if(type==4){
		alert(itemId);
	}
}