/**
 *  权限模块
 */
     
    /***************************权限管理*******************************/
         $("#nextPagemodel").click(function(){
        	var PageNum=$("#PageNum").html();
        	var Page=$("#Page").html();
        	if(PageNum==Page){
        		alert("已经是尾页了哦！"); 
        	}else{
        		authorityList(parseInt(PageNum)+1);
        	}
        	
         });
         
         $("#prevPagemodel").click(function(){
        	 var PageNum=$("#PageNum").html();
        	 if(PageNum==1){
        		 alert("已经是首页了哦！"); 
        	 }else if(PageNum==0){
        		 alert("没有上一页了！"); 
        		 authorityList(1);
        	 }else{
        		 authorityList(parseInt(PageNum)-1);
        	 } 
         });
         
         $("#homePagemodel").click(function(){
        	 authorityList(1);
         });
         
         $("#endPagemodel").click(function(){
         	var Page=$("#Page").html();
         	authorityList(Page);
         });
         
         $("#selectmdoel").click(function(){
          	authorityList(1);
          });

     function authorityList(PageNum){
	   var modelname=$("#modelname").val();
	   $.ajax({
			type:"GET",
			url:"authorityList",
			dataType:"json",
			data:{
				"modelname":modelname,
				"PageNum":PageNum
			},
	   		success: function (data){
	   				var pageStr='';
	   			if(data !=''){
	   				for(var js in data){
	   					var date = new Date(data[js].updateTime);
 	   					var updateTime=date.Format("yyyy-MM-dd");
 	   				    var date1 = new Date(data[js].createTime);
	   					var createTime=date1.Format("yyyy-MM-dd");
	   					pageStr+='<tr>';
	   					if(data[js].modelName!=undefined){
	   						pageStr+='<td>'+data[js].modelName+'</td>';
	   					}else{
	   						pageStr+='<td></td>';
	   					}
	   					
	   					if(data[js].parentName !=undefined){
	   						pageStr+='<td>'+data[js].parentName+'</td>';
	   					}else{
	   						pageStr+='<td></td>';
	   					}
	   					
	   					if(data[js].ancentorName !=undefined){
	   						pageStr+='<td>'+data[js].ancentorName+'</td>';
	   					}else{
	   						pageStr+='<td></td>';
	   					}
	   					if(updateTime=='NaN-aN-aN'){
	   						pageStr+='<td>'+createTime+'</td>';
	   					}else{
	   						pageStr+='<td>'+updateTime+'</td>';
	   					}
	   					if(data[js].updateByName==null){
	   						pageStr+='<td>'+data[js].createByName+'</td>';
	   					}else{
	   						pageStr+='<td>'+data[js].updateByName+'</td>';
	   					}
	   					pageStr+='<td>'+
	   	                    '<a><img src="assets/icon_9m8sb4cx6z/update.png"  style="width: 15px;cursor:pointer;" onclick="updateModel('+
	   					"'"+data[js].modelID+"'"+
	   					')" title="修改"/></a>'+
	   	                    '<a><img src="assets/icon_9m8sb4cx6z/delete.png" style="width: 15px;cursor:pointer;" onclick="deleteModel('+
	   					"'"+data[js].modelID+"'"+
	   					')"  title="删除"/></a>'+  
	   	                    '</td></tr>';
	   				}
	   				$("#hrauthorityList").html(pageStr);
	   				$("#PageNum").html(PageNum);
	   				authorityListPage(modelname);
	   			}else{
	   				pageStr+='<tr><td colspan="6">没有该数据哦~~</td></tr>';
	   				$("#hrauthorityList").html(pageStr);
	   				$("#modelname").val("");
	   				$("#PageNum").html(0);
	   				$("#Page").html(0);
	   			}
	   		},error:function(){
		          alert("服务器请求失败"); 
	        }
		});
	}
     
     
     function authorityListPage(modelname){
  	   $.ajax({
  			type:"GET",
  			url:"authorityListPage",
  			dataType:"json",
  			data:{
  				"modelname":modelname
  			},
  	   		success: function (data){
  	   				$("#Page").html(data);
  	   		},error:function(){
  		          alert("服务器请求失败"); 
  	        }
  		});
  	}
     
     
     $("#addModel").click(function(){
    	 var authorityManag="添加权限";
  	   var userRoleId=getCookie("tbUser", "role");
  	   $.ajax({
  			type:"GET",
  			url:"userAuthorityManag",
  			dataType:"json",
  			data:{
  				"authorityManag":authorityManag,
  				"userRoleId":userRoleId
  			},
  	   		success: function (data){
  	   				if(data.Authority=="exist"){
  	   				window.location.href="auth/authorityAdd.html";
  	   				}else{
  	   					alert("对不起，您没有访问权限！");
  	   				}
  	   		},error:function(){
  		          alert("服务器请求失败"); 
  	        }
  		});
     });
     
     
     $("#saveModel").click(function(){
 	    var modelName=$("#modelName").val(); 
 	    var modelUrl=$("#modelUrl").val(); 
 	    var farModel=$("#farModel").val(); 
 	    var Module=$("#Module").val(); 
 	    var remark=$("#remark").val(); 
 	   var yr=getCookie("tbUser", "id");
 	    $.ajax({
 			type:"GET",
 			url:"AddModel",
 			dataType:"json",
 			data:{
 				"modelName":modelName,
 				"modelUrl":modelUrl,
 				"farModel":farModel,
 				"Module":Module,
 				"remark":remark,
 				"yr":yr
 			},
 	   		success: function (data){
 	   			if(data.addModel=="exist"){
 	   				alert("添加成功！");
 	   				window.location.href="auth/authorityList.html";
   	   				}else{
   	   			    alert("添加失败！");
   	   				}
 	   		},error:function(){
 		          alert("服务器请求失败"); 
 	        }
 		});
 	    
  });
  
  
  $("#returnModel").click(function(){
 	 window.location.href="auth/authorityList.html";
  });
     
     
     function updateModel(modelID){
    	 var authorityManag="修改权限";
    	   var userRoleId=getCookie("tbUser", "role");
    	   $.ajax({
    			type:"GET",
    			url:"userAuthorityManag",
    			dataType:"json",
    			data:{
    				"authorityManag":authorityManag,
    				"userRoleId":userRoleId
    			},
    	   		success: function (data){
    	   				if(data.Authority=="exist"){
    	   				window.location.href="auth/authorityUpdate.html?modelID="+modelID;
    	   				}else{
    	   					alert("对不起，您没有访问权限！");
    	   				}
    	   		},error:function(){
    		          alert("服务器请求失败"); 
    	        }
    		});
     }
     
         $("#upsaveModel").click(function(){
        	 var modelName=$("#upmodelName").val();
        	 var modelUrl=$("#upmodelUrl").val();
        	 var remark=$("#upremark").val();
        	 var modelId=$("#modelId").html();
        	 var mod=getCookie("tbUser", "id");
        	 $.ajax({
      			type:"GET",
      			url:"UpdateModel",
      			dataType:"json",
      			data:{
      				"modelId":modelId,
      				"modelName":modelName,
      				"modelUrl":modelUrl,
      				"remark":remark,
      				"mod":mod
      			},
      	   		success: function (data){
      	   			if(data.UpdateModel=="exist"){
      	   				alert("修改成功！");
      	   				window.location.href="auth/authorityList.html";
        	   				}else{
        	   			    alert("修改失败！");
        	   				}
      	   		},error:function(){
      		          alert("服务器请求失败"); 
      	        }
      		});
         });
         
         
         function deleteModel(modelID){
        	 var authorityManag="删除权限";
      	   var userRoleId=getCookie("tbUser", "role");
      	   $.ajax({
      			type:"GET",
      			url:"userAuthorityManag",
      			dataType:"json",
      			data:{
      				"authorityManag":authorityManag,
      				"userRoleId":userRoleId
      			},
      	   		success: function (data){
      	   				if(data.Authority=="exist"){
      	   				if(confirm("确认删除这个权限？")){
         	           		$.ajax({
         	              			type:"GET",
         	              			url:"deleteModel",
         	              			dataType:"json",
         	              			data:{
         	              				"modelID":modelID
         	              			},
         	              	   		success: function (data){
         	              	   				if(data.deleteModel=="exist"){
         	              	   				   alert("删除成功！");
         	              	   				authorityList(1);
         	              	   				}else{
         	              	   				   alert("删除失败！");
         	              	   				}
         	              	   		},error:function(){
         	              		          alert("服务器请求失败"); 
         	              	        }
         	              		});
         	           	}
      	   				}else{
      	   					alert("对不起，您没有访问权限！");
      	   				}
      	   		},error:function(){
      		          alert("服务器请求失败"); 
      	        }
      		});
         }
     
     
    /***************************用户未有权限*******************************/
     $("#nextPagenotmodel").click(function(){
     	var notModelPageNum=$("#notModelPageNum").html();
     	var notModelPage=$("#notModelPage").html();
     	if(notModelPageNum==notModelPage){
     		alert("已经是尾页了哦！"); 
     	}else{
     		notauthorityList(parseInt(notModelPageNum)+1);
     	}
     	
      });
      
      $("#prevPagenotmodel").click(function(){
     	 var notModelPageNum=$("#notModelPageNum").html();
     	 if(notModelPageNum==1){
     		 alert("已经是首页了哦！"); 
     	 }else if(notModelPageNum==0){
     		alert("已经没有上一页了！");
     		notauthorityList(1);
     	 }else{
     		notauthorityList(parseInt(notModelPageNum)-1);
     	 } 
      });
      
      $("#homePagenotmodel").click(function(){
    	  notauthorityList(1);
      });
      
      $("#endPagenotmodel").click(function(){
      	var notModelPage=$("#notModelPage").html();
      	notauthorityList(notModelPage);
      });
      
      $("#selectnotmdoel").click(function(){
    	  notauthorityList(1);
       });
     
     
     
     function notauthorityList(PageNum){
  	   var notmodelname=$("#notmodelname").val();
  	   var roleId=$("#roleId").html();
  	   $.ajax({
  			type:"GET",
  			url:"notauthorityList",
  			dataType:"json",
  			data:{
  				"notmodelname":notmodelname,
  				"roleId":roleId,
  				"PageNum":PageNum
  			},
  	   		success: function (data){
  	   				var pageStr='';
  	   			if(data !=''){
  	   				for(var js in data){
  	   					pageStr+='<tr>';
  	   					if(data[js].modelName!=undefined){
  	   						pageStr+='<td>'+data[js].modelName+'</td>';
  	   					}else{
  	   						pageStr+='<td></td>';
  	   					}
  	   					
  	   					if(data[js].parentName !=undefined){
  	   						pageStr+='<td>'+data[js].parentName+'</td>';
  	   					}else{
  	   						pageStr+='<td></td>';
  	   					}
  	   					
  	   					if(data[js].ancentorName !=undefined){
  	   						pageStr+='<td>'+data[js].ancentorName+'</td>';
  	   					}else{
  	   						pageStr+='<td></td>';
  	   					}
  	   					pageStr+='<td>'+
  	   	                    '<a style="cursor:pointer;" onclick="addauthority('+
  	   					"'"+data[js].modelID+"'"+
  	   	                    ')"><img src="assets/icon_9m8sb4cx6z/add.png" style="width: 15px;"  title="添加"/></a>';
  	   				}
  	   				$("#notauthorityList").html(pageStr);
  	   				$("#notModelPageNum").html(PageNum);
  	   			    notModelCount(roleId,notmodelname);
	   			    hadModelCount(roleId,null);
  	   			}else{
  	   				pageStr+='<tr><td colspan="4">没有该数据哦~~</td></tr>';
  	   				$("#notauthorityList").html(pageStr);
  	   			    notModelCount(roleId,notmodelname);
  	   			    hadModelCount(roleId,null);
  	   			    $("#notModelPageNum").html(0);
  	   				$("#notmodelname").val("");
  	   			}
  	   		},error:function(){
  		          alert("not服务器请求失败"); 
  	        }
  		});
  	}
     
     function notModelCount(roleId,notmodelname){
     	$.ajax({
    			type:"GET",
    			url:"notModelCount",
    			dataType:"json",
    			data:{
    				"roleId":roleId,
    				"notmodelname":notmodelname
    			},
    	   		success: function (data){
    	   		$("#notModelPage").html(data);
    	   		},error:function(){
    		          alert("服务器请求失败"); 
    	        }
    		});
	      }
     
     
     
     
     
     
     
     
     
     
     /***************************用户已有权限*******************************/
     
     
     $("#nextPagehadmodel").click(function(){
      	var hadModelPageNum=$("#hadModelPageNum").html();
      	var hadModelPage=$("#hadModelPage").html();
      	if(hadModelPageNum==hadModelPage){
      		alert("已经是尾页了哦！"); 
      	}else{
      		hadauthorityList(parseInt(hadModelPageNum)+1);
      	}
      	
       });
       
       $("#prevPagehadmodel").click(function(){
      	 var hadModelPageNum=$("#hadModelPageNum").html();
      	 if(hadModelPageNum==1){
      		 alert("已经是首页了哦！"); 
      	 }else if(hadModelPageNum==0){
      		alert("已经没有上一页了！");
      		hadauthorityList(1);
      	 }else{
      		hadauthorityList(parseInt(hadModelPageNum)-1);
      	 } 
       });
       
       $("#homePagehadmodel").click(function(){
    	   hadauthorityList(1);
       });
       
       $("#endPagehadmodel").click(function(){
       	var hadModelPage=$("#hadModelPage").html();
       	hadauthorityList(hadModelPage);
       });
       
       $("#selecthadmdoel").click(function(){
    	   hadauthorityList(1);
        });
      
      
      
      function hadauthorityList(PageNum){
   	   var hadmodelname=$("#hadmodelname").val();
   	   var roleId=$("#roleId").html();
   	   $.ajax({
   			type:"GET",
   			url:"hadauthorityList",
   			dataType:"json",
   			data:{
   				"hadmodelname":hadmodelname,
   				"roleId":roleId,
   				"PageNum":PageNum
   			},
   	   		success: function (data){
   	   				var pageStr='';
   	   			if(data !=''){
   	   				for(var js in data){
   	   					pageStr+='<tr>';
   	   					if(data[js].modelName!=undefined){
   	   						pageStr+='<td>'+data[js].modelName+'</td>';
   	   					}else{
   	   						pageStr+='<td></td>';
   	   					}
   	   					
   	   					if(data[js].parentName !=undefined){
   	   						pageStr+='<td>'+data[js].parentName+'</td>';
   	   					}else{
   	   						pageStr+='<td></td>';
   	   					}
   	   					
   	   					if(data[js].ancentorName !=undefined){
   	   						pageStr+='<td>'+data[js].ancentorName+'</td>';
   	   					}else{
   	   						pageStr+='<td></td>';
   	   					}
   	   				pageStr+='<td>'+
 	                    '<a style="cursor:pointer;" onclick="minusauthority('+
 					"'"+data[js].modelID+"'"+
 	                    ')"><img src="assets/icon_9m8sb4cx6z/minus.png" style="width: 15px;"  title="删除"/></a>';
   	   				}
   	   				$("#hadauthorityList").html(pageStr);
   	   				$("#hadModelPageNum").html(PageNum);
   	   			    notModelCount(roleId,null);
   			        hadModelCount(roleId,hadmodelname);
   	   			}else{
   	   				pageStr+='<tr><td colspan="4">没有该数据哦~~</td></tr>';
   	   				$("#hadauthorityList").html(pageStr);
   	   			    notModelCount(roleId,null);
   	   			    hadModelCount(roleId,hadmodelname);
   	   			     $("#hadModelPageNum").html(0);
   	   				$("#hadmodelname").val("");
   	   			}
   	   		},error:function(){
   		          alert("had服务器请求失败"); 
   	        }
   		});
   	}
      
      
      function hadModelCount(roleId,hadmodelname){
      	$.ajax({
     			type:"GET",
     			url:"hadModelCount",
     			dataType:"json",
     			data:{
     				"roleId":roleId,
     				"hadmodelname":hadmodelname
     			},
     	   		success: function (data){
     	   		$("#hadModelPage").html(data);
     	   		},error:function(){
     		          alert("服务器请求失败"); 
     	        }
     		});
	      }

      
      
      
      /***************************用户添加权限*******************************/
      
        function addauthority(modelId){
        	var roleId=$("#roleId").html();
        	var hadModelPage=$("#hadModelPage").html();
        	$.ajax({
       			type:"GET",
       			url:"addauthority",
       			dataType:"json",
       			data:{
       				"roleId":roleId,
       				"modelId":modelId
       			},
       	   		success: function (data){
       	   			if(data.add=="success"){
       	   			notauthorityList(1);
       	   			if(hadModelPage=="0"){
       	   			hadauthorityList(1);
       	   			}else{
       	   			hadauthorityList(hadModelPage);
       	   			}
       	   			}else{
       	   			  alert("添加失败！"); 
       	   			}
       	   		},error:function(){
       		          alert("这里服务器请求失败"); 
       	        }
       		});
	      }
        
        
        $("#AddAllAuth").click(function(){
        	var roleId=$("#roleId").html();
        	var hadModelPage=$("#hadModelPage").html();
        	$.ajax({
       			type:"GET",
       			url:"AddAllAuth",
       			dataType:"json",
       			data:{
       				"roleId":roleId
       			},
       	   		success: function (data){
       	   			if(data.add=="exist"){
       	   			notauthorityList(1);
       	   			if(hadModelPage=="0"){
       	   			hadauthorityList(1);
       	   			}else{
       	   			hadauthorityList(hadModelPage);	
       	   			}
       	   			}else{
       	   			  alert("添加失败！"); 
       	   			}
       	   		},error:function(){
       		          alert("服务器请求失败"); 
       	        }
       		});
        });
        
        
        
      /***************************用户删除权限*******************************/
        
        function minusauthority(modelId){
        	var roleId=$("#roleId").html();
        	var hadModelPage=$("#hadModelPage").html();
        	$.ajax({
       			type:"GET",
       			url:"minusauthority",
       			dataType:"json",
       			data:{
       				"roleId":roleId,
       				"modelId":modelId
       			},
       	   		success: function (data){
       	   			if(data.minus=="success"){
       	   			notauthorityList(1);
       	   			hadauthorityList(hadModelPage);
       	   			}else{
       	   			  alert("删除失败！"); 
       	   			}
       	   		},error:function(){
       		          alert("服务器请求失败"); 
       	        }
       		});
	      }
        
        
        $("#DeleteAllAuth").click(function(){
        	var roleId=$("#roleId").html();
        	var hadModelPage=$("#hadModelPage").html();
        	$.ajax({
       			type:"GET",
       			url:"DeleteAllAuth",
       			dataType:"json",
       			data:{
       				"roleId":roleId
       			},
       	   		success: function (data){
       	   			if(data.dele=="exist"){
       	   			notauthorityList(1);
       	   			hadauthorityList(hadModelPage);
       	   			}else{
       	   			  alert("删除失败！"); 
       	   			}
       	   		},error:function(){
       		          alert("服务器请求失败"); 
       	        }
       		});
        });
        
        
        
        
     