/**
 *  角色模块
 */
     
     
     function mOver(ele,Status){
    	 if(Status==1){
    		 ele.setAttribute("class","btn btn-warning");
    		 ele.setAttribute("value", "禁用");
    	 }else if(Status==2){
    		 ele.setAttribute("class","btn btn-success");
    		 ele.setAttribute("value", "启用");
    	 }  
    }
     
     function mOut(ele,Status){
    	 if(Status==2){
    		 ele.setAttribute("class","btn btn-warning");
    		 ele.setAttribute("value", "禁用");
    	 }else if(Status==1){
    		 ele.setAttribute("class","btn btn-success");
    		 ele.setAttribute("value", "启用");
    	 } 
	 }
     
     function status(roleId,Status){
    	 $.ajax({
 			type:"GET",
 			url:"amendStatus",
 			dataType:"json",
 			data:{
 				"roleId":roleId,
 				"Status":Status
 			},
 	   		success: function (data){
 	   				if(data.amend=="success"){
 	   				roleList(1);
 	   				}else{
 	   					alert("该角色已被使用，无法禁用！");
 	   				}
 	   		},error:function(){
 		          alert("服务器请求失败"); 
 	        }
 		});
	}
     
     
     
     
     $("#rolenextPage").click(function(){
     	var PageNum=$("#PageNum").html();
     	var Page=$("#Page").html();
     	if(PageNum==Page){
     		alert("已经是尾页了哦！"); 
     	}else{
     		roleList(parseInt(PageNum)+1);
     	}
     	
      });
      
      $("#roleprevPage").click(function(){
     	 var PageNum=$("#PageNum").html();
     	 if(PageNum==1){
     		 alert("已经是首页了哦！"); 
     	 }else if(PageNum==0){
    		 alert("没有上一页了！"); 
    		 roleList(1);
    	 }else{
     		roleList(parseInt(PageNum)-1);
     	 } 
      });
      
      $("#rolehomePage").click(function(){
    	  roleList(1);
      });
      
      $("#roleendPage").click(function(){
      	var Page=$("#Page").html();
      	roleList(Page);
      });
      
      $("#selectrole").click(function(){
    	  roleList(1);
      });
     
     
     function roleList(PageNum){
    	 var rolename=$("#rolename").val();
    	 $.ajax({
 			type:"GET",
 			url:"roleList",
 			dataType:"json",
 			data:{
 				"rolename":rolename,
 				"PageNum":PageNum
 			},
 	   		success: function (data){
 	   				var pageStr='';
 	   			if(data !=''){
 	   				for(var js in data){
 	   					var date = new Date(data[js].updateTime);
 	   					var updateTime=date.Format("yyyy-MM-dd");
 	   				    var date1 = new Date(data[js].createTime);
	   					var createTime=date1.Format("yyyy-MM-dd");
 	   					pageStr+="<tr>" +
 	   							'<td>'+data[js].roleName+'</td>';
 	   					if(updateTime!='NaN-aN-aN'){
 	   						pageStr+='<td>'+updateTime+'</td>';
 	   					}else{
 	   					pageStr+='<td>'+createTime+'</td>';
 	   					}
 	   					if(data[js].updateByName==null){
 	   					pageStr+='<td>'+data[js].createByName+'</td>';
 	   					}else{
 	   					pageStr+='<td>'+data[js].updateByName+'</td>';
 	   					}
 	   					if(1==data[js].showStatus){
 	   						pageStr+='<td><input type="button" onmouseover="mOver(this,'+
 	   							"'1'"+
 	   						')" onmouseout="mOut(this,'+
 	   							"'1'"+
 	   							')" onclick="status('+
 	   						"'"+data[js].roleID+"','"+
 	   					     data[js].showStatus+"')" +
 	   						'" class="btn btn-success" value="开启"/></td>';
 	   					}else if(2==data[js].showStatus){
 	   						pageStr+='<td><input type="button" onmouseover="mOver(this,'+
	   							"'2'"+
	   						')" onmouseout="mOut(this,'+
	   							"'2'"+
	   							')" onclick="status('+
	   						"'"+data[js].roleID+"','"+
	   					     data[js].showStatus+"')" +
	   						'" class="btn btn-warning" value="禁用"/></td>';
 	   					}
 	   					pageStr+='<td><a ><img src="assets/icon_9m8sb4cx6z/all.png"  style="width: 15px;cursor:pointer;" onclick="allRole('+
 	   						"'"+data[js].roleID+"'"+
 	   						')" title="查看"/></a>'+
                         '<a ><img src="assets/icon_9m8sb4cx6z/delete.png" style="width: 15px;cursor:pointer;" onclick="deleteRole('+
 	   						"'"+data[js].roleID+"'"+
 	   						')"  title="删除"/></a>'+
                      
                   '</td></tr>';
 	   				}
 	   				$("#hrrolelist").html(pageStr);
 	   				$("#PageNum").html(PageNum);
 	   			     roleListPage(rolename);
 	   			}else{
 	   				pageStr+='<tr><td colspan="4">没有该数据哦~~</td></tr>';
 	   				$("#hrrolelist").html(pageStr);
 	   			 	$("#rolename").val("");
 	   			    $("#PageNum").html(0);
 	   			    $("#Page").html(0);
 	   			}
 	   		},error:function(){
 		          alert("服务器请求失败"); 
 	        }
 		});
	  }
     
     
     function roleListPage(rolename){
    	 $.ajax({
 			type:"GET",
 			url:"roleListPage",
 			dataType:"json",
 			data:{
 				"rolename":rolename
 			},
 	   		success: function (data){
 	   				$("#Page").html(data);
 	   		},error:function(){
 		          alert("服务器请求失败"); 
 	        }
 		});
	  }
     
     
     
     $("#addRole").click(function(){
    	 var authorityManag="添加角色";
  	   var userRoleId=getCookie("tbUser", "role");
  	   $.ajax({
  			type:"GET",
  			url:"userAuthorityManag",
  			dataType:"json",
  			data:{
  				"authorityManag":authorityManag,
  				"userRoleId":userRoleId
  			},
  	   		success: function (data){
  	   				if(data.Authority=="exist"){
  	   				window.location.href="role/roleAdd.html";
  	   				}else{
  	   					alert("对不起，您没有访问权限！");
  	   				}
  	   		},error:function(){
  		          alert("服务器请求失败"); 
  	        }
  		});
 	   				
     });
     
     $("#saverole").click(function (){
    	   var rolename=$("#rolename").val();
    	   var roleremark=$("#roleremark").val();
    	   var yr=getCookie("tbUser", "id");
    	   $.ajax({
     			type:"GET",
     			url:"addRole",
     			dataType:"json",
     			data:{
     				"rolename":rolename,
     				"roleremark":roleremark,
     				"yr":yr
     			},
     	   		success: function (data){
     	   				if(data.addRole=="exist"){
     	   				window.location.href="role/roleList.html";
     	   			    alert("添加成功！");
     	   				}else{
     	   					alert("添加失败！");
     	   				}
     	   		},error:function(){
     		          alert("服务器请求失败"); 
     	        }
     		});
     });
     
     $("#returnrole").click(function (){
    	 window.location.href="role/roleList.html";
     });
     
     function allRole(roleId){
    	  var authorityManag="添加角色";
    	   var userRoleId=getCookie("tbUser", "role");
    	   $.ajax({
    			type:"GET",
    			url:"userAuthorityManag",
    			dataType:"json",
    			data:{
    				"authorityManag":authorityManag,
    				"userRoleId":userRoleId
    			},
    	   		success: function (data){
    	   				if(data.Authority=="exist"){
    	   				window.location.href="role/roleView.html?roleId="+roleId;
    	   				}else{
    	   					alert("对不起，您没有访问权限！");
    	   				}
    	   		},error:function(){
    		          alert("服务器请求失败"); 
    	        }
    		});
    	 
     }
     
     
     function deleteRole(roleId) {
    	 var authorityManag="删除角色";
  	   var userRoleId=getCookie("tbUser", "role");
  	   $.ajax({
  			type:"GET",
  			url:"userAuthorityManag",
  			dataType:"json",
  			data:{
  				"authorityManag":authorityManag,
  				"userRoleId":userRoleId
  			},
  	   		success: function (data){
  	   				if(data.Authority=="exist"){
  	   				if(confirm("确认删除这个角色？")){
     	           		$.ajax({
     	              			type:"GET",
     	              			url:"deleteRole",
     	              			dataType:"json",
     	              			data:{
     	              				"roleId":roleId
     	              			},
     	              	   		success: function (data){
     	              	   				if(data.deleteRole=="exist"){
     	              	   				   alert("删除成功！");
     	              	   				   roleList(1);
     	              	   				}else{
     	              	   				   alert("删除失败！");
     	              	   				}
     	              	   		},error:function(){
     	              		          alert("服务器请求失败"); 
     	              	        }
     	              		});
     	           	}
  	   				}else{
  	   					alert("对不起，您没有访问权限！");
  	   				}
  	   		},error:function(){
  		          alert("服务器请求失败"); 
  	        }
  		});
    }
     
     
     
     
     
