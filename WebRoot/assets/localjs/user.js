/**
 * 用户管理
 * 
 */

         $("#nextPage").click(function(){
        	var PageNum=$("#PageNum").html();
        	var Page=$("#Page").html();
        	if(PageNum==Page){
        		alert("已经是尾页了哦！"); 
        	}else{
        		userList(parseInt(PageNum)+1);
        	}
        	
         });
         
         $("#prevPage").click(function(){
        	 var PageNum=$("#PageNum").html();
        	 if(PageNum==1){
        		 alert("已经是首页了哦！"); 
        	 }else if(PageNum==0){
        		 alert("没有上一页了！"); 
        		 userList(1);
        	 }else{
        		 userList(parseInt(PageNum)-1);
        	 } 
         });
         
         $("#homePage").click(function(){
        	 userList(1);
         });
         
         $("#endPage").click(function(){
         	var Page=$("#Page").html();
         	userList(Page);
         });
         
         $("#btn_query").click(function(){
        	 userList(1);
         });
         
         function amenduserstatus(id,status){
        	 $.ajax({
      			type:"GET",
      			url:"amenduserstatus",
      			dataType:"json",
      			data:{
      				"id":id,
      				"status":status
      			},
      	   		success: function (data){
      	   				if(data.amend=="success"){
      	   				userList(1);
      	   				}else{
      	   					alert("失败");
      	   				}
      	   		},error:function(){
      		          alert("服务器请求失败"); 
      	        }
      		});
         }


   function userList(PageNum){
	   var username=$("#username").val();
	   var userrole=$("#userrole").val(); 
	   $.ajax({
			type:"GET",
			url:"userList",
			dataType:"json",
			data:{
				"username":username,
				"userrole":userrole,
				"PageNum":PageNum
			},
	   		success: function (data){
	   				var pageStr='';
	   			if(data !=''){
	   				for(var js in data){
	   					var date = new Date(data[js].modifyTime);
	   					var modifyTime=date.Format("yyyy-MM-dd");
	   					var date1 = new Date(data[js].createTime);
	   					var createTime=date1.Format("yyyy-MM-dd");
	   					pageStr+="<tr>" +
	   							'<td>'+data[js].showUsername+'</td>'+
	   							'<td>'+data[js].roleName+'</td>';
	   					if(data[js].modifyByName==null){
	   						pageStr+='<td>'+data[js].createByName+'</td>';
	   					}else{
	   						pageStr+='<td>'+data[js].modifyByName+'</td>';
	   					}
	   					if(modifyTime!='NaN-aN-aN'){
	   						pageStr+='<td>'+modifyTime+'</td>';
	   					}else{
	   						pageStr+='<td>'+createTime+'</td>';
	   					}	
	   					if(1==data[js].status){
	   						pageStr+='<td><input type="button" onmouseover="mOver(this,'+
	   							"'1'"+
	   						')" onmouseout="mOut(this,'+
	   							"'1'"+
	   							')" onclick="amenduserstatus('+
	   						"'"+data[js].id+"','"+
	   					     data[js].status+"')" +
	   						'" class="btn btn-success" value="开启"/></td>';
	   					}else if(2==data[js].status){
	   						pageStr+='<td><input type="button" onmouseover="mOver(this,'+
   							"'2'"+
   						')" onmouseout="mOut(this,'+
   							"'2'"+
   							')" onclick="amenduserstatus('+
   						"'"+data[js].id+"','"+
   					     data[js].status+"')" +
   						'" class="btn btn-warning" value="禁用"/></td>';
	   					}
	   					pageStr+='<td><input type="button" onclick="authorityadmin('+
	   					"'"+data[js].role+"'"+
	   					')" class="btn btn-primary" value="权限管理"/></td>'+
	   							'<td>'+
                        '<a ><img src="assets/icon_9m8sb4cx6z/all.png"  style="width: 15px;cursor:pointer;" onclick="AllTbUser('+
	   							"'"+data[js].id+"'"+
	   							')"  title="查看"/></a>'+
                        '<a ><img src="assets/icon_9m8sb4cx6z/update.png" style="width: 15px;cursor:pointer;" onclick="updateUser('+
	   							"'"+data[js].id+"'"+
	   							')"   title="修改"/></a>'+
                        '<a ><img src="assets/icon_9m8sb4cx6z/delete.png" style="width: 15px;cursor:pointer;" onclick="deleteUser('+
	   							"'"+data[js].id+"'"+
	   							')"  title="删除"/></a>'+
                     
                  '</td></tr>';
	   				}
	   				$("#hruserlist").html(pageStr);
	   				$("#PageNum").html(PageNum);
	   				userListPage(username,userrole);
	   			}else{
	   				pageStr+='<tr><td colspan="6">没有该数据哦~~</td></tr>';
	   				$("#hruserlist").html(pageStr);
	   				$("#username").val("");
	   			    $("#userrole").val(""); 
	   			    $("#PageNum").html(0);
	   			    $("#Page").html(0);
	   			}
	   		},error:function(){
		          alert("服务器请求失败"); 
	        }
		});
	}
   
   
   function userListPage(username,userrole){
	   $.ajax({
			type:"GET",
			url:"userListPage",
			dataType:"json",
			data:{
				"username":username,
				"userrole":userrole
			},
	   		success: function (data){
	   				$("#Page").html(data);
	   		},error:function(){
		          alert("服务器请求失败"); 
	        }
		});
	}
   
   
   
   Date.prototype.Format = function(fmt)   
   { 
   //author:wangweizhen
     var o = {   
       "M+" : this.getMonth()+1,                 //月份   
       "d+" : this.getDate(),                    //日
       "h+" : this.getHours(),                   //小时   
       "m+" : this.getMinutes(),                 //分   
       "s+" : this.getSeconds(),                 //秒   
       "q+" : Math.floor((this.getMonth()+3)/3), //季度   
       "S"  : this.getMilliseconds()             //毫秒   
     };   
     if(/(y+)/.test(fmt))   
       fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));   
     for(var k in o)   
       if(new RegExp("("+ k +")").test(fmt))   
     fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));   
     return fmt;   
   };
   
   
   
   
   
   
   function authorityadmin(roleId) {
	   var authorityManag="用户权限管理";
	   var userRoleId=getCookie("tbUser", "role");
	   $.ajax({
 			type:"GET",
 			url:"userAuthorityManag",
 			dataType:"json",
 			data:{
 				"authorityManag":authorityManag,
 				"userRoleId":userRoleId
 			},
 	   		success: function (data){
 	   				if(data.Authority=="exist"){
 	   				window.location.href="user/userAuthority.html?roleId="+roleId;
 	   				}else{
 	   					alert("对不起，您没有访问权限！");
 	   				}
 	   		},error:function(){
 		          alert("服务器请求失败"); 
 	        }
 		});
 
   }
   
   
   
   
   
   $("#addUser").click(function(){
	   var authorityManag="添加用户";
	   var userRoleId=getCookie("tbUser", "role");
	   $.ajax({
			type:"GET",
			url:"userAuthorityManag",
			dataType:"json",
			data:{
				"authorityManag":authorityManag,
				"userRoleId":userRoleId
			},
	   		success: function (data){
	   				if(data.Authority=="exist"){
	   				window.location.href="user/userAdd.html";
	   				}else{
	   					alert("对不起，您没有访问权限！");
	   				}
	   		},error:function(){
		          alert("服务器请求失败"); 
	        }
		});
   });
             
   
           function username(){
        	   var username=$("#username").val();
        	   $.ajax({
       			type:"GET",
       			url:"allExistlogName",
       			dataType:"json",
       			data:{
       				"username":username
       			},
       	   		success: function (data){
       	   				if(data.logName=="exist"){
       	   				   $("#accerror").html("该账号已存在！");
       	   				   $("#username").val("");
       	   				}else{
       	   				$("#accerror").html("");
       	   				}
       	   		},error:function(){
       		          alert("服务器请求失败"); 
       	        }
       		});
           }
           
           
           
           function pwdlength(){
        	   var pwdlength=document.getElementById("password").value.length;
        	   if(pwdlength>6&&pwdlength<20){
        		   $("#pwd").html("");
        	   }else{
        		   $("#pwd").html("注意：密码长度要大于6位，小于20位");
        		   $("#password").val("");
        	   }
           }
           
           function uppwdlength(){
        	   var pwdlength=document.getElementById("uppassword").value.length;
        	   if(pwdlength>6&&pwdlength<20&&pwd!=oglpwd){
        		   $("#pwd").html("");
        	   }else{
        		   $("#pwd").html("注意：密码长度要大于6位，小于20位");
        		   $("#password").val("");
        	   }
           }
   
   
           function pwdAgr(){
        	   var password=$("#password").val();
        	   var passwords=$("#passwords").val();
        	   if(password!=passwords){
        		   $("#pwds").html("两次输入密码不一样！");
        	   }else{
        		   $("#pwds").html("");
        	   }
           }
           
           function uppwdAgr(){
        	   var password=$("#uppassword").val();
        	   var passwords=$("#uppasswords").val();
        	   if(password!=passwords){
        		   $("#pwds").html("两次输入密码不一样！");
        	   }else{
        		   $("#pwds").html("");
        	   }
           }
   
           $("#saveuser").click(function(){
        	   var showusername=$("#showusername").val();
        	   var username=$("#username").val();
        	   var password=$("#password").val();
        	   var passwords=$("#passwords").val();
        	   var roleId=$("#roleId").val();
        	   var via=$("#via").val();
        	   var remark=$("#remark").val();
        	   var pwd=$("#pwd").html();
        	   var pwds=$("#pwds").html();
        	   var yr=getCookie("tbUser", "id");
        	   if(showusername!=""&& username!=""&& password!=""&& passwords!=""&&
        			   roleId!=""&&via!=""&& remark!=""&&pwd==""&&pwds==""){
        		   $.ajaxFileUpload({
        		        url:path+'/AddUser',//服务器端请求地址
        		        type:'post',
        		        secureuri: false,//是否需要安全协议，一般设置为false
        		        fileElementId: 'via',//文件上传域的ID
        		        contentType : "application/json; charset=utf-8",
        		        dataType:'json', //返回值类型 一般设置为json
              			data:{
              				"showusername":showusername,
              				"username":username,
              				"passwords":passwords,
              				"roleId":roleId,
              				"remark":remark,
              				"yr":yr
              			},
              	   		success: function (data){
              	   				if(data.AddUser=="exist"){
              	   					alert("添加成功！");
              	   				window.location.href="user/userList.html";
              	   				}else{
              	   				    alert("添加失败！");
              	   				}
              	   		},error:function(){
              		          alert("服务器请求失败"); 
              	        }
              		});
        	   }else if(pwd!=""||pwds!=""){
        		   alert("填写信息错误！");
        	   }else{
        		   alert("请填写完整信息！");
        	   }
           });
           
           
           $("#upsaveuser").click(function(){
        	   var id=$("#userid").html();
        	   var showusername=$("#upshowusername").val();
        	   var username=$("#upusername").val();
        	   var password=$("#uppassword").val();
        	   var passwords=$("#uppasswords").val();
        	   var roleId=$("#uproleId").val();
        	   var remark=$("#upremark").val();
        	   var pwd=$("#pwd").html();
        	   var pwds=$("#pwds").html();
        	   var mod=getCookie("tbUser", "id");
        	   if(showusername!=""&&username!=""&&password!=""&&passwords!=""&&
        			   roleId!=""&&remark!=""&&pwd==""&&pwds==""){
        		   $.ajaxFileUpload({
       		        url:path+'/UpdateUser',//服务器端请求地址
       		        type:'post',
       		        secureuri: false,//是否需要安全协议，一般设置为false
       		        fileElementId: 'upvia',//文件上传域的ID
       		        contentType : "application/json; charset=utf-8",
       		        dataType:'json', //返回值类型 一般设置为json
             			data:{
             				"id":id,
             				"showusername":showusername,
             				"passwords":passwords,
             				"roleId":roleId,
             				"remark":remark,
             				"mod":mod
             			},
             	   		success: function (data){
             	   				if(data.UpdateUser=="exist"){
             	   					alert("修改成功！");
             	   				window.location.href="user/userList.html";
             	   				}else{
             	   				    alert("修改失败！");
             	   				}
             	   		},error:function(){
             		          alert("服务器请求失败"); 
             	        }
             		});
        	   }else if(pwd!=""||pwds!=""){
        		   alert("填写信息错误！");
        	   }
        	   else{
        		   alert("请填写完整信息！");
        	   }
           });
           
           
           $("#returnuser").click(function(){
        	   window.location.href="user/userList.html";
           });
   
       function updateUser(id){
    	   var authorityManag="修改用户";
    	   var userRoleId=getCookie("tbUser", "role");
    	   $.ajax({
   			type:"GET",
   			url:"userAuthorityManag",
   			dataType:"json",
   			data:{
   				"authorityManag":authorityManag,
   				"userRoleId":userRoleId
   			},
   	   		success: function (data){
   	   				if(data.Authority=="exist"){
   	   				window.location.href="user/userUpdate.html?id="+id;
   	   				}else{
   	   					alert("对不起，您没有访问权限！");
   	   				}
   	   		},error:function(){
   		          alert("服务器请求失败"); 
   	        }
   		});  
       }
   
        function AllTbUser(id) {
        	var authorityManag="查看用户";
     	   var userRoleId=getCookie("tbUser", "role");
     	   $.ajax({
    			type:"GET",
    			url:"userAuthorityManag",
    			dataType:"json",
    			data:{
    				"authorityManag":authorityManag,
    				"userRoleId":userRoleId
    			},
    	   		success: function (data){
    	   				if(data.Authority=="exist"){
    	   					window.location.href="user/userView.html?id="+id;
    	   				}else{
    	   					alert("对不起，您没有访问权限！");
    	   				}
    	   		},error:function(){
    		          alert("服务器请求失败"); 
    	        }
    		});  
	    }
        
        function deleteUser(id) {
        	var authorityManag="删除用户";
      	   var userRoleId=getCookie("tbUser", "role");
      	   $.ajax({
     			type:"GET",
     			url:"userAuthorityManag",
     			dataType:"json",
     			data:{
     				"authorityManag":authorityManag,
     				"userRoleId":userRoleId
     			},
     	   		success: function (data){
     	   				if(data.Authority=="exist"){
     	   				if(confirm("确认删除这个用户？")){
     	           		$.ajax({
     	              			type:"GET",
     	              			url:"deleteUser",
     	              			dataType:"json",
     	              			data:{
     	              				"id":id
     	              			},
     	              	   		success: function (data){
     	              	   				if(data.deleteUser=="exist"){
     	              	   				   alert("删除成功！");
     	              	   				   userList(1);
     	              	   				}else{
     	              	   				   alert("删除失败！");
     	              	   				}
     	              	   		},error:function(){
     	              		          alert("服务器请求失败"); 
     	              	        }
     	              		});
     	           	}
     	   				}else{
     	   					alert("对不起，您没有访问权限！");
     	   				}
     	   		},error:function(){
     		          alert("服务器请求失败"); 
     	        }
     		});
        	
        	
	    }
   
        
         function selUser(){
        	 var id=getCookie("tbUser", "id");
        	 window.location.href="user/userViewMe.html?id="+id;
         }
         
         $("#returnuserMe").click(function(){
        	 var logUsername=getCookie("tbUser", "logUsername");
        	 var logPassword=getCookie("tbUser", "logPassword");
        	 window.location.href="main/index.html?logUsername="+logUsername+"&logPassword="+logPassword;
         });
         
         var logUsername=getCookie("tbUser", "logUsername");
    	 var logPassword=getCookie("tbUser", "logPassword");
          $("#index").attr("href","main/index.html?logUsername="+logUsername+"&logPassword="+logPassword);
         
          
          function upUser(){
        	  var id=getCookie("tbUser", "id");
        	  window.location.href="user/userUpdateMe.html?id="+id;
          }
          
          $("#upsaveuserMe").click(function(){ 
        	 var id=$("#userid").html();
      	   var showusername=$("#upshowusername").val();
      	   var password=$("#uppassword").val();
      	   var passwords=$("#uppasswords").val();
      	   var via=$("#upviame").val();
      	   var remark=$("#upremark").val();
      	   var pwd=$("#pwd").html();
      	   var pwds=$("#pwds").html();
      	 var logUsername=getCookie("tbUser", "logUsername");
       	 var logPassword=getCookie("tbUser", "logPassword");
      	 if(showusername!=""&&password!=""&&passwords!=""&&remark!=""&&pwd==""&&pwds==""){
      		$.ajaxFileUpload({
   		        url:path+'/UpdateUserMe',//服务器端请求地址
   		        type:'post',
   		        secureuri: false,//是否需要安全协议，一般设置为false
   		        fileElementId: 'upviame',//文件上传域的ID
   		        contentType : "application/json; charset=utf-8",
   		        dataType:'json', //返回值类型 一般设置为json
         			data:{
       				"id":id,
       				"showusername":showusername,
       				"passwords":passwords,
       				"remark":remark
       			},
       	   		success: function (data){
       	   				if(data.UpdateUser=="exist"){
       	   					alert("修改成功！");
       	        	 window.location.href="main/index.html?logUsername="+logUsername+"&logPassword="+logPassword;
       	   				}else{
       	   				    alert("修改失败！");
       	   				}
       	   		},error:function(){
       		          alert("服务器请求失败"); 
       	        }
       		});
  	   }else if(pwd!=""||pwds!=""){
  		   alert("填写信息错误！");
  	   }
  	   else{
  		   alert("请填写完整信息！");
  	   }
          });
   
   
   