<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<div style="margin: 10px;">
            	<!-- 导航条  -->
            	<ul  class="breadcrumb">
            		<li><a href="index.html">首页</a><i class="en-arrow-right7"></i></li>
            		<li></i><a href="#">系统设置</a><i class="en-arrow-right7"></i></li>
            		<li><i class="en-arrow-right7"></i>添加权限</li>
            	</ul>
			</div>   
			<ul class="row back">
				<li class="col-md-3">
              		<span>权限名称:</span>
              		<input type="text" name="" id="modelName" class="select"/>
              </li>
			</ul>
			
			<ul class="row back">
				<li class="col-md-3">
              		<span>URL:</span>
              		<input type="text" style="margin-left: 25px;" name="" id="modelUrl" class="select"/>
              </li>
			</ul>
			
			<ul class="row back">
				<li class="col-md-3">
              		<span>父级权限:</span>
              		<select class="select" id="farModel">
              		<option value="falset"></option>
              		<c:forEach items="${FarAuthlist}" var="far">
              		   <option value="${far.modelID}">${far.modelName}</option>
              		</c:forEach>
              		</select>
              </li>
			</ul>
			
			<ul class="row back">
				<li class="col-md-3">
              		<span>权限模块:</span>
              		<select class="select" id="Module">
              		<option value="falset"></option>
              		   <c:forEach items="${AuthModulelist}" var="modu">
              		   <option value="${modu.modelID}">${modu.modelName}</option>
              		   </c:forEach>
              		</select>
              </li>
			</ul>
			<ul class="row back">             	
              	<li class="col-md-12">
					<span style="float: left;">权限备注:</span>
              		<textarea name="" id="remark" rows="8" cols="100%"></textarea>
              	</li>
              </ul>
              <ul  >
              	<li>
					<input type="button"  class="btn btn-primary bunntleft" name="" id="saveModel" value="保存" />
				</li>
				<li>
					<input type="button"  class="btn btn-primary bunnt" name="" id="returnModel" value="返回" />
				</li>
              </ul>
<%@include file="footer.jsp"%>