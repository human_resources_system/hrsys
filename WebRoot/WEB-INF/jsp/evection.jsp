<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<div style="margin: 10px;">
            	<!-- 导航条  -->
            	<ul  class="breadcrumb">
            		<li><a href="index.html">首页</a><i class="en-arrow-right7"></i></li>
            		<li></i><a href="#">人事管理</a><i class="en-arrow-right7"></i></li>
            		<li></i><a href="clockingin.html">考勤信息管理</a><i class="en-arrow-right7"></i></li>
            		<li><i class="en-arrow-right7"></i> 出差记录</li>
            	</ul>
			</div>
			

            <div >
					<ul class="toubu">
							<li>
								<span>姓名：</span>
								<input type="text" class="test" />					
							</li>
							<li>
								<span>所属部门：</span>
								<select class="test">
									<option value="0">----请选择----</option>
								</select>					
							</li>
							<li>								
								<center>
										<span>工作日期：</span>
										<input type="text" class="input test" id="pickdate" />
								</center>	
								
							</li>
							<li>
								<input type="button" class="btn btn-primary" name="" id="" value="查询" />
							</li>
					</ul>
		    </div>
                
      
                
                
        <div class="ta">
            <table class="providerTable" cellpadding="0" cellspacing="0">
                <tbody>
                <tr class="firstTr">
                    <th width="10%">档案编码</th>
                    <th width="10%">员工姓名</th>
                    <th width="10%">所属部门</th>
                    <th width="10%">职位</th>
                    <th width="10%">职称</th>
                    <th width="10%">出差开始时间</th>
                    <th width="10%">出差天数</th>
                    <th width="10%">出差原因</th>
                    <th width="10%">考勤状态</th>
                    <th width="10%">工作日期</th>
                </tr>
                
                <tr>
                    <td>BDQN0201</td>
                    <td>张三</td>
                    <td>产品设计中心</td>
                    <td>技术培训</td>
                    <td>高级顾问</td>
                    <td>2018-08-20</td>
                    <td>5</td>
                    <td>外出学习</td>
                    <td>出差中</td>
                    <td>2017-09-01</td>
                </tr>
                <tr>
                    <td>BDQN0203</td>
                    <td>张三</td>
                    <td>产品设计中心</td>
                    <td>技术培训</td>
                    <td>高级顾问</td>
                    <td>2018-08-20</td>
                    <td>5</td>
                    <td>外出学习</td>
                    <td>请假中</td>
                    <td>2017-09-01</td>
                </tr>
                <tr>
                    <td>BDQN0203</td>
                    <td>张三</td>
                    <td>产品设计中心</td>
                    <td>技术培训</td>
                    <td>高级顾问</td>
                    <td>2018-08-20</td>
                    <td>5</td>
                    <td>外出学习</td>
                    <td>请假中</td>
                    <td>2017-09-01</td>
                </tr>
            </tbody>
        </table>               
     </div>
<%@include file="footer.jsp"%>