<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<div style="margin: 10px;">
            	<!-- 导航条  -->
            	<ul  class="breadcrumb">
            		<li><a href="index.html">首页</a><i class="en-arrow-right7"></i></li>
            		<li></i><a href="#">系统设置</a><i class="en-arrow-right7"></i></li>
            		<li><i class="en-arrow-right7"></i>查看用户</li>
            	</ul>
			</div>   
			
			<div style="width: 700px;">

				<ul class="row resumetext">
				<li class=" col-md-3">
              	    <label>
              			用户名称:           			
              		</label>
              		<span class="rstext">${tbUser.showUsername}</span>
              </li>
            </ul>
            
            <ul class="row resumetext">
				<li class=" col-md-3">
              	    <label>
              			用户账号:          			
              		</label>
              		<span class="rstext">${tbUser.logUsername}</span>
              </li>
            </ul>
            
            <ul class="row resumetext">
				<li class=" col-md-3">
              	    <label>
              			用户角色:          			
              		</label>
              		<span class="rstext">${tbUser.roleName}</span>
              </li>
            </ul>
            
            <ul class="row resumetext">
				<li class=" col-md-3">
              	    <label>
              			用户头像:          			
              		</label>
              		<span class="rstext"><img src="${tbUser.userVia}" style="height: 50px;width: 50px;"/></span>
              </li>
            </ul>
            
            <ul class="row resumetext">
				<li class=" col-md-3">
              	    <label>
              			用户备注:          			
              		</label>
              		<span class="rstext">${tbUser.remark}</span>
              </li>
            </ul>

              <ul  >
				<li>
					<input type="button"  class="btn btn-primary bunnt" name="" id="returnuser" value="返回" />
				</li>
              </ul>
			</div>
<%@include file="footer.jsp"%>