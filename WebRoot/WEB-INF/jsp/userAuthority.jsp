<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<div style="margin: 10px;">
            	<!-- 导航条  -->
            	<ul  class="breadcrumb">
            		<li><a href="index.html">首页</a><i class="en-arrow-right7"></i></li>
            		<li></i><a href="#">系统设置</a><i class="en-arrow-right7"></i></li>
            		<li><i class="en-arrow-right7"></i> 用户权限</li>
            	</ul>
			</div>
			<span id="roleId" hidden="hidden">${roleId}</span>
			
			<div >
					<ul class="toubu">
						
						    <li>
							用户未拥有权限
							</li>
							<li>								
								<span>权限名称：</span>
								<input type="text" class="test" id="notmodelname"/>
							</li>
							<li>
								<input type="button" class="btn btn-primary" name="" id="selectnotmdoel" value="查询"/>
							</li>
							<li>
								<input type="button" class="btn btn-primary" name="" id="AddAllAuth" value="添加所有权限"/>
							</li>
							<li>
								<input type="button" class="btn btn-primary" name="" id="DeleteAllAuth" value="删除所有权限"/>
							</li>
					</ul>
		    </div>
                
        <div class="ta">
            <table class="providerTable" cellpadding="0" cellspacing="0">
                <tbody>
                <tr class="firstTr">
                    <th width="30%">权限名称</th>
                    <th width="30%">父级权限</th>
                    <th width="30%">所属模块</th>
                    <th width="10%">操作</th>
                </tr>
                </tbody>
                <tbody id="notauthorityList">
                <c:forEach items="${notmodellist}" var="notmodel">
                <tr>
                    <td>${notmodel.modelName}</td>
                    <td>${notmodel.parentName}</td>
                    <td>${notmodel.ancentorName}</td>
                    <td>
                            <a style="cursor:pointer;" onclick="addauthority('${notmodel.modelID}')"><img src="assets/icon_9m8sb4cx6z/add.png" style="width: 15px;"  title="添加"/></a>
                    </td>
                </tr>
                </c:forEach>
            </tbody>
        </table>               
     </div> 
     
     <div class="text-center">
        
<!--MvcPager v3.0.1 for ASP.NET MVC 4.0+ © 2009-2015 Webdiyer (http://www.webdiyer.com)-->
<ul class="pagination" data-currentpage="2" data-firstpage="/mvcpager/demos/applycss/" data-invalidpageerrmsg="页索引无效" data-outrangeerrmsg="页索引超出范围" data-pagecount="80" data-pageparameter="id" data-pagerid="Webdiyer.MvcPager" data-urlformat="/mvcpager/demos/applycss/__id__/" id="bootstrappager">
	    <li>
		    <a href="#" id="homePagenotmodel">首页</a>
		</li>
		<li><a href="#" id="prevPagenotmodel">上页</a>	
		</li>
		<li>
			<a href="#"><span id="notModelPageNum">${notModelPageNum}</span>/<span id="notModelPage">${notModelPage}</span></a>
		</li>
		<li>
			<a href="#" id="nextPagenotmodel">下页</a>
		</li>
		<li>
			<a href="#" id="endPagenotmodel">尾页</a>
		</li>
</ul>
<!--MvcPager v3.0.1 for ASP.NET MVC 4.0+ © 2009-2015 Webdiyer (http://www.webdiyer.com)-->

</div> 

<hr />

<div >
					<ul class="toubu">
						<li>
							用户已拥有权限
							</li>
							<li>								
								<span>权限名称：</span>
								<input type="text" class="test" id="hadmodelname"/>
							</li>
							<li>
								<input type="button" class="btn btn-primary" name="" id="selecthadmdoel" value="查询"/>
							</li>
					</ul>
		    </div>
                
      
                
                
        <div class="ta">
            <table class="providerTable" cellpadding="0" cellspacing="0">
                <tbody>
                <tr class="firstTr">
                    <th width="30%">权限名称</th>
                    <th width="30%">父级权限</th>
                    <th width="30%">所属模块</th>
                    <th width="10%">操作</th>
                </tr>
                </tbody>
                <tbody id="hadauthorityList">
                <c:forEach items="${hadmodellist}" var="hadmodel">
                <tr>
                    <td>${hadmodel.modelName}</td>
                    <td>${hadmodel.parentName}</td>
                    <td>${hadmodel.ancentorName}</td>
                    <td>
                            <a style="cursor:pointer;" onclick="minusauthority('${hadmodel.modelID}')"><img src="assets/icon_9m8sb4cx6z/minus.png" style="width: 15px;"  title="删除"/></a>
                    </td>
                </tr>
                </c:forEach>
            </tbody>
        </table>               
     </div> 
     
     <div class="text-center">
        
<!--MvcPager v3.0.1 for ASP.NET MVC 4.0+ © 2009-2015 Webdiyer (http://www.webdiyer.com)-->
<ul class="pagination" data-currentpage="2" data-firstpage="/mvcpager/demos/applycss/" data-invalidpageerrmsg="页索引无效" data-outrangeerrmsg="页索引超出范围" data-pagecount="80" data-pageparameter="id" data-pagerid="Webdiyer.MvcPager" data-urlformat="/mvcpager/demos/applycss/__id__/" id="bootstrappager">
	    <li>
		    <a href="#" id="homePagehadmodel">首页</a>
		</li>
		<li><a href="#" id="prevPagehadmodel">上页</a>	
		</li>
		<li>
			<a href="#"><span id="hadModelPageNum">${hadModelPageNum}</span>/<span id="hadModelPage">${hadModelPage}</span></a>
		</li>
		<li>
			<a href="#" id="nextPagehadmodel">下页</a>
		</li>
		<li>
			<a href="#" id="endPagehadmodel">尾页</a>
		</li>
</ul>
<!--MvcPager v3.0.1 for ASP.NET MVC 4.0+ © 2009-2015 Webdiyer (http://www.webdiyer.com)-->

</div>
<%@include file="footer.jsp"%>