<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<div style="margin: 10px;">
            	<!-- 导航条  -->
            	<ul  class="breadcrumb">
            		<li><a href="index.html">首页</a><i class="en-arrow-right7"></i></li>
            		<li></i><a href="#">人事管理</a><i class="en-arrow-right7"></i></li>
            		<li><i class="en-arrow-right7"></i> 考勤信息管理</li>
            	</ul>
			</div>
			

            <div >
					<ul class="toubu">
							<li>
								<span>姓名：</span>
								<input type="text" class="test" />					
							</li>
							<li>
								<span>所属部门：</span>
								<select class="test">
									<option value="0">----请选择----</option>
								</select>					
							</li>
							<li>
								<span>考勤状态：</span>
								<select class="test">
									<option value="0">----请选择----</option>
								</select>					
							</li>
							<li>								
								<center>
										<span>工作日期：</span>
										<input type="text" class="input test" id="pickdate" />
								</center>	
								
							</li>
							<li>
								<input type="button" class="btn btn-primary" name="" id="" value="查询" />
							</li>
							<li>
								<input type="button" class="btn btn-danger" onclick="skipmonth()" name="" id="" value="月信息统计" />
								
							</li>
							<li>
								<input type="button" class="btn btn-danger" onclick="skipevection()" name="" id="" value="出差记录" />
								
							</li>
							<li>
								<input type="button" class="btn btn-danger" onclick="skipleave()" name="" id="" value="请假记录" />
								
							</li>
							<li>
								<input type="button" class="btn btn-danger" onclick="skipovertime()" name="" id="" value="加班记录" />
								
							</li>
							
					</ul>
		    </div>
                
      
                
                
        <div class="ta">
            <table class="providerTable" cellpadding="0" cellspacing="0">
                <tbody>
                <tr class="firstTr">
                    <th width="10%">档案编码</th>
                    <th width="10%">员工姓名</th>
                    <th width="10%">所属部门</th>
                    <th width="10%">职位</th>
                    <th width="10%">职称</th>
                    <th width="10%">出勤时间</th>
                    <th width="10%">退勤时间</th>
                    <th width="10%">考勤状态</th>
                    <th width="10%">工作日期</th>
                </tr>
                
                <tr>
                    <td>BDQN0201</td>
                    <td>张三</td>
                    <td>产品设计中心</td>
                    <td>技术培训</td>
                    <td>高级顾问</td>
                    <td>9：00</td>
                    <td>18：00</td>
                    <td>正常出勤</td>
                    <td>2017-09-01</td>
                </tr>
                <tr>
                    <td>BDQN0203</td>
                    <td>张三</td>
                    <td>产品设计中心</td>
                    <td>技术培训</td>
                    <td>高级顾问</td>
                    <td>9：00</td>
                    <td>18：00</td>
                    <td>正常出勤</td>
                    <td>2017-09-02</td>
                </tr>
                <tr>
                    <td>BDQN0203</td>
                    <td>张三</td>
                    <td>产品设计中心</td>
                    <td>技术培训</td>
                    <td>高级顾问</td>
                    <td>9：00</td>
                    <td>18：00</td>
                    <td>正常出勤</td>
                    <td>2017-09-03</td>
                </tr>
            </tbody>
        </table>               
     </div>     
                
                
        <div class="text-center">
        
<!--MvcPager v3.0.1 for ASP.NET MVC 4.0+ © 2009-2015 Webdiyer (http://www.webdiyer.com)-->
<ul class="pagination" data-currentpage="2" data-firstpage="/mvcpager/demos/applycss/" data-invalidpageerrmsg="页索引无效" data-outrangeerrmsg="页索引超出范围" data-pagecount="80" data-pageparameter="id" data-pagerid="Webdiyer.MvcPager" data-urlformat="/mvcpager/demos/applycss/__id__/" id="bootstrappager">
	    <li>
		    <a href="/mvcpager/demos/applycss/">首页</a></li><li><a href="/mvcpager/demos/applycss/">上页</a>	
		</li>
		<li>
			<a href="/mvcpager/demos/applycss/">1</a>
		</li>
		<li >
			<a href="#">2</a>
		</li>
		<li>
			<a href="/mvcpager/demos/applycss/3/">3</a>
		</li>
		<li>
			<a href="/mvcpager/demos/applycss/4/">4</a>
		</li>
		<li>
			<a href="/mvcpager/demos/applycss/5/">5</a>
		</li>
		<li>
			<a href="/mvcpager/demos/applycss/6/">6</a>
		</li>
		<li>
			<a href="/mvcpager/demos/applycss/7/">7</a>
		</li>
		<li>
			<a href="/mvcpager/demos/applycss/8/">8</a>
		</li>
		<li>
			<a href="/mvcpager/demos/applycss/9/">9</a>
		</li>
		<li>
			<a href="/mvcpager/demos/applycss/10/">10</a>
		</li>
		<li>
			<a href="/mvcpager/demos/applycss/11/">...</a>			
		</li>
		<li>
			<a href="/mvcpager/demos/applycss/3/">下页</a>
		</li>
		<li>
			<a href="/mvcpager/demos/applycss/80/">尾页</a>
		</li>
</ul>
<!--MvcPager v3.0.1 for ASP.NET MVC 4.0+ © 2009-2015 Webdiyer (http://www.webdiyer.com)-->

</div>               	
                	</div>
               </div>
<%@include file="footer.jsp"%>
