<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<div style="margin: 10px;">
            	<!-- 导航条  -->
            	<ul  class="breadcrumb">
            		<li><a href="index.html">首页</a><i class="en-arrow-right7"></i></li>
            		<li></i><a href="#">系统设置</a><i class="en-arrow-right7"></i></li>
            		<li><i class="en-arrow-right7"></i>修改用户信息</li>
            	</ul>
			</div>   
			<ul class="row back">
				<li class="col-md-5">
              		<span>用户名称:</span><span id="userid" hidden="hidden">${tbUser.id}</span>
              		<input type="text" name="" value="${tbUser.showUsername}"  id="upshowusername" class="select"/>
              </li>
			</ul>
			<ul class="row back">
				<li class="col-md-5">
              		<span>用户账号:</span>
              		<input type="text" name="" value="${tbUser.logUsername}"  id="upusername" class="select" readonly/>
              </li>
			</ul>
			<ul class="row back">
				<li class="col-md-5">
              		<span>用户密码:</span><span id="oglpwd" hidden="hidden">${tbUser.logPassword}</span>
              		<input type="password" name="" onBlur="uppwdlength()" id="uppassword" class="select"/>
              		&nbsp;<span id="pwd">注意：密码长度要大于6位，小于20位</span>
              </li>
              
			</ul>
			<ul class="row back">
				<li class="col-md-5">
              		<span>确认密码:</span>
              		<input type="password" name=""  onBlur="uppwdAgr()" id="uppasswords" class="select"/>
              		&nbsp;<span id="pwds"></span>
              </li>
			</ul>
			<ul class="row back">
				<li class="col-md-5">
              		<span>用户角色:</span>
              		<input type="text" name="" value="${tbUser.roleName}"  class="select"  readonly/>
              </li>
			</ul>
			<ul class="row back">
				<li class="col-md-5">
              		<span>用户头像:</span>
              		<img id="userviaMe" alt="用户头像" src="${tbUser.userVia}" style="height: 70px;width: 70px;">
              		<div class="file-box btn btn-primary">
                    <input type="file" id="upviame" name="upviame" class="file-btn"/>
                                                        上传文件
                    </div>
              </li>
			</ul>
			<ul class="row back">             	
              	<li class="col-md-12">
					<span style="float: left;">用户备注:</span>
              		<textarea name="" id="upremark" rows="8" cols="100%">${tbUser.remark}</textarea>
              	</li>
              </ul>
              <ul  >
              	<li>
					<input type="button"  class="btn btn-primary bunntleft" name="" id="upsaveuserMe" value="保存" />
				</li>
				<li>
					<input type="button"  class="btn btn-primary bunnt" name="" id="returnuserMe" value="返回" />
				</li>
              </ul>
<%@include file="footer.jsp"%>