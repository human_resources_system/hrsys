<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
	
<head>
<base href="<%=basePath%>">
<meta charset="UTF-8">
<link href="assets/css/styles.css" rel='stylesheet' type='text/css' />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--webfonts-->
		<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic|Oswald:400,300,700' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,300,700,800' rel='stylesheet' type='text/css'>
<!--//webfonts-->
</head>
<body class="login-07">
<div >
		<div class="seventh-login">
			<h3>${error}</h3>
			<form class="seven" action="main/index.html" method="post">
				<ul>
					<li>
						<input type="text"   placeholder="请输入账号" name="logUsername" required/>
						<div class="clear"> </div>
					</li> 
					<li>
						<input type="password"   placeholder="请输入密码" name="logPassword" required/>
						<div class="clear"> </div>
					</li>
					<li>
					<input type="submit"  value="LOG IN" > 
					</li>
				</ul>
			</form>
		</div>
	</div>
</body>
</html>
