<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="author" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<title>Neuron HTML CSS Template</title>

<link rel="stylesheet" href="assets/css/bootstrap.min.css">


<!-- Main css -->
<link rel="stylesheet" href="assets/css/style.css">

</head>
<body>


<!-- Home Section -->

<section id="home" class="main-home parallax-section">
     <div class="overlay"></div>
     <div id="particles-js"></div>
     <div class="container">
          <div class="row">

               <div class="col-md-12 col-sm-12">
                    <h1>人力资源管理系统</h1>
                    <h4 style="font-size: 20px;">Human Resources Management System.</h4>
                    <a href="main/Login.html" class="smoothScroll btn btn-default">Login</a>
               </div>

          </div>
     </div>
</section>


<script src="assets/js/particles.min.js"></script>
<script src="assets/js/apps.js"></script>



</body>
</html>
