<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<div style="margin: 10px;">
            	<!-- 导航条  -->
            	<ul  class="breadcrumb">
            		<li><a href="index.html">首页</a><i class="en-arrow-right7"></i></li>
            		<li></i><a href="#">系统设置</a><i class="en-arrow-right7"></i></li>
            		<li><i class="en-arrow-right7"></i>角色管理</li>
            	</ul>
			</div>
            <div >
					<ul class="toubu">
							<li>
								<span>角色名称：</span>
								<input type="text" class="test" id="rolename"/>					
							</li>
							<li>
								<input type="button" class="btn btn-primary" name="" id="selectrole" value="查询"/>
							</li>
							<li>
								<input type="button" class="btn btn-danger" name="" id="addRole" value="添加角色"/>
							</li>
					</ul>
		    </div>
                
      
                
                
        <div class="ta">
            <table class="providerTable" cellpadding="0" cellspacing="0">
                <tbody>
                <tr class="firstTr">
                    <th width="30%">角色名称</th>
                    <th width="20%">更新时间</th>
                    <th width="20%">更新人</th>
                    <th width="15%">角色状态</th>
                    <th width="15%">操作</th>
                    
                </tr>
                </tbody>
                <tbody id="hrrolelist">
                <c:forEach items="${rolelist}" var="role">
                <tr>
                    <td>${role.roleName}</td>
                    <c:choose>
                    <c:when test="${role.updateTime==null}">
                    <td><fn:formatDate value='${role.createTime}' pattern='yyyy-MM-dd' /></td>
                    </c:when>
                    <c:otherwise>
                    <td><fn:formatDate value='${role.updateTime}' pattern='yyyy-MM-dd' /></td>
                    </c:otherwise>
                    </c:choose>
                    <c:choose>
                    <c:when test="${role.updateByName==null}">
                    <td>${role.createByName}</td>
                    </c:when>
                    <c:otherwise>
                    <td>${role.updateByName}</td>
                    </c:otherwise>
                    </c:choose>
                    <c:if test="${role.showStatus==1}">
                    <td><input type="button" onmouseover="mOver(this,'1')" onmouseout="mOut(this,'1')" onclick="status('${role.roleID}','${role.showStatus}')" class="btn btn-success" value="启用"/></td>
                    </c:if>
                    <c:if test="${role.showStatus==2}">
                    <td><input type="button" onmouseover="mOver(this,'2')" onmouseout="mOut(this,'2')" onclick="status('${role.roleID}','${role.showStatus}')" class="btn btn-warning" value="禁用"/></td>
                    </c:if>
                    <td>
                        <a href="#"><img src="assets/icon_9m8sb4cx6z/all.png"  style="width: 15px;"  onclick="allRole('${role.roleID}')"  title="查看"/></a>
                            <a href="#"><img src="assets/icon_9m8sb4cx6z/delete.png" style="width: 15px;" onclick="deleteRole('${role.roleID}')"  title="删除"/></a>
                       
                    </td>
                </tr>
                </c:forEach>
            </tbody>
        </table>               
     </div> 
     <div class="text-center">
        
<!--MvcPager v3.0.1 for ASP.NET MVC 4.0+ © 2009-2015 Webdiyer (http://www.webdiyer.com)-->
<ul class="pagination" data-currentpage="2" data-firstpage="/mvcpager/demos/applycss/" data-invalidpageerrmsg="页索引无效" data-outrangeerrmsg="页索引超出范围" data-pagecount="80" data-pageparameter="id" data-pagerid="Webdiyer.MvcPager" data-urlformat="/mvcpager/demos/applycss/__id__/" id="bootstrappager">
	    <li>
		    <a href="#" id="rolehomePage">首页</a>
		</li>
		<li><a href="#" id="roleprevPage">上页</a>	
		</li>
		<li>
			<a href="#"><span id="PageNum">${PageNum}</span>/<span id="Page">${Page}</span></a>
		</li>
		<li>
			<a href="#" id="rolenextPage">下页</a>
		</li>
		<li>
			<a href="#" id="roleendPage">尾页</a>
		</li>
</ul>
<!--MvcPager v3.0.1 for ASP.NET MVC 4.0+ © 2009-2015 Webdiyer (http://www.webdiyer.com)-->

</div>               	
                	</div>
               </div> 
<%@include file="footer.jsp"%>