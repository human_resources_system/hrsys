<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<div style="margin: 10px;">
            	<!-- 导航条  -->
            	<ul  class="breadcrumb">
            		<li><a href="index.html">首页</a><i class="en-arrow-right7"></i></li>
            		<li></i><a href="#">系统设置</a><i class="en-arrow-right7"></i></li>
            		<li><i class="en-arrow-right7"></i>修改权限</li>
            	</ul>
			</div>   
			<ul class="row back">
				<li class="col-md-3">
              		<span>权限名称:</span><span id="modelId" hidden="hidden">${tbModel.modelID}</span>
              		<input type="text" name="" value="${tbModel.modelName}" id="upmodelName" class="select"/>
              </li>
			</ul>
			
			<ul class="row back">
				<li class="col-md-3">
              		<span>URL:</span>
              		<c:choose>
              		<c:when test="${tbModel.modelURL==null}">
              		<input type="text" style="margin-left: 25px;" value="${tbModel.modelURL}" name=""  id="upmodelUrl" class="select" readonly/>
              		</c:when>
              		<c:otherwise>
              		<input type="text" style="margin-left: 25px;" value="${tbModel.modelURL}" name=""  id="upmodelUrl" class="select"/>
              		</c:otherwise>
              		</c:choose>
              		
              </li>
			</ul>
			
			<ul class="row back">
				<li class="col-md-3">
              		<span>父级权限:</span>
              		<input type="text" value="${tbModel.parentName}" name="" id=""  class="select" readonly/>
              </li>
			</ul>
			
			<ul class="row back">
				<li class="col-md-3">
              		<span>模块标识:</span>
              		<input type="text" value="${tbModel.ancentorName}" name="" id="" class="select" readonly/>
              </li>
			</ul>
			<ul class="row back">             	
              	<li class="col-md-12">
					<span style="float: left;">模块备注:</span>
              		<textarea name="" id="upremark" rows="8" cols="100%">${tbModel.remark}</textarea>
              	</li>
              </ul>
              <ul  >
              	<li>
					<input type="button"  class="btn btn-primary bunntleft" name="" id="upsaveModel" value="保存" />
				</li>
				<li>
					<input type="button"  class="btn btn-primary bunnt" name="" id="returnModel" value="返回" />
				</li>
              </ul>
<%@include file="footer.jsp"%>