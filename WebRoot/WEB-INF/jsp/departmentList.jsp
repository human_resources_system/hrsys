<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
     <div class="tree well" class="col-lg-12">
					    <ul id="rootUL" class="col-lg-3">
							
					    </ul> 
					    <div class="col-lg-7">
                            <!-- Start col-lg-12 -->
                            <div class="panel panel-default toggle" id="spr_1">
                                <!-- Start .panel -->
                                <div class="panel-body">
                                    <form class="form-horizontal" role="form">
                                        <div class="form-group">
                                            <label class="col-lg-2 col-md-2 col-sm-12 control-label">部门编码：</label>
                                            <div class="col-md-5 col-sm-12">
                                                <input class="form-control" id="mask-phone" type="text" disabled="disabled">
                                            </div>
                                        </div>
                                        <!-- End .form-group  -->
                                        
                                        <div class="form-group">
                                            <label class="col-md-2  col-sm-12 control-label">部门简称：</label>
                                            <div class="col-md-5 col-sm-12">
                                                <input class="form-control" id="mask-phoneExt" type="text" disabled="disabled">
                                            </div>
                                        </div>
                                        <!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-md-2  col-sm-12 control-label">部门全称：</label>
                                            <div class="col-md-5 col-sm-12">
                                                <input class="form-control" id="mask-phoneInt" type="text" disabled="disabled">
                                            </div>
                                        </div>
                                        <!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-md-2  col-sm-12 control-label">所属部门：</label>
                                            <div class="col-md-5 col-sm-12">
                                                <input class="form-control" id="mask-date" type="text" disabled="disabled">
                                            </div>
                                        </div>
                                        <!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-md-2  col-sm-12 control-label">部门地址：</label>
                                            <div class="col-md-7 col-sm-12">
                                                <input class="form-control" id="mask-ssn" type="text" disabled="disabled">
                                            </div>
                                        </div>
                                        <!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-md-2  col-sm-12 control-label">简介：</label>
                                            <div class="col-md-7 col-sm-12">
                                                <textarea class="form-control" disabled="disabled">111111111111111</textarea>
                                            </div>
                                        </div>
                                        <!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-md-2  col-sm-12 control-label">备注：</label>
                                            <div class="col-md-7 col-sm-12">
                                                <textarea class="form-control" disabled="disabled">111111111111111</textarea>
                                            </div>
                                        </div>
                                        <!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-md-2  col-sm-12 control-label">是否启用：</label>
                                            <div class="">
                                                <input  type="checkbox"/>
                                                <!--<span class="help-block">99%</span>-->
                                            </div>
                                        </div>
                                        <!-- End .form-group  -->
                                    </form>
                                    <div class="form-group text-center">
                                            <div class="">
                                            	<a href="dapartmentAdd.html" class="btn btn-default btn-lg active" role="button" id="infoAdd" disabled="disabled" role="button">新增部门</a>&nbsp;&nbsp;&nbsp;&nbsp;
                                            	<a href="departmentUpdate.html" class="btn btn-default btn-lg active" role="button" id="infoUpdate"  disabled="disabled">修改部门</a>&nbsp;&nbsp;&nbsp;&nbsp;
                                            	<button class="btn btn-default btn-lg active" id="infoDel"  disabled="disabled">删除部门</button>
                                                <!--<span class="help-block">99%</span>-->
                                            </div>
                                        </div>
                                </div>
                            </div>
                            <!-- End .panel -->
                        </div>
					</div>
<%@include file="footer.jsp"%>