<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<div style="margin: 10px;">
            	<!-- 导航条  -->
            	<ul  class="breadcrumb">
            		<li><a href="index.html">首页</a><i class="en-arrow-right7"></i></li>
            		<li></i><a href="#">系统设置</a><i class="en-arrow-right7"></i></li>
            		<li><i class="en-arrow-right7"></i>查看角色信息</li>
            	</ul>
			</div>   
			
			<div style="width: 700px;">
				
				<ul class="row resumetext">
				<li class=" col-md-3">
              	    <label>
              			角色名称:           			
              		</label>
              		<span class="rstext">${tbRole.roleName}</span>
              </li>
            </ul>
            
            <ul class="row resumetext">
				<li class=" col-md-3">
              	    <label>
              			角色备注:           			
              		</label>
              		<span class="rstext">${tbRole.roleRemark}</span>
              </li>
            </ul>

              <ul  >
				<li>
					<input type="button"  class="btn btn-primary bunnt" name="" id="returnrole" value="返回" />
				</li>
              </ul>
			</div>
<%@include file="footer.jsp"%>