<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<div style="margin: 10px;">
            	<!-- 导航条  -->
            	<ul  class="breadcrumb">
            		<li><a href="index.html">首页</a><i class="en-arrow-right7"></i></li>
            		<li></i><a href="#">系统设置</a><i class="en-arrow-right7"></i></li>
            		<li><i class="en-arrow-right7"></i> 用户管理</li>
            	</ul>
			</div>
			

            <div >
					<ul class="toubu">
							<li>
								<span>用户名称：</span>
								<input type="text" class="test" id="username"/>					
							</li>
							<li>								
								<span>用户角色：</span>
								<input type="text" class="test" id="userrole"/>
							</li>
							<li>
								<input type="button" class="btn btn-primary" name="" id="btn_query" value="查询"/>
							</li>
							<li>
								<input type="button" class="btn btn-danger"  name="" id="addUser" value="添加用户"/>
							</li>
					</ul>
		    </div>
                
                
        <div class="ta">
            <table class="providerTable" cellpadding="0" cellspacing="0">
                <tbody>
                <tr class="firstTr">
                    <th width="15%">用户名称</th>
                    <th width="15%">用户角色</th>
                    <th width="15%">更新人</th>
                    <th width="15%">更新日期</th>
                    <th width="13%">用户状态</th>
                    <th width="13">权限管理</th>
                    <th width="14%">操作</th>
                    
                </tr>
                </tbody>
                <tbody id="hruserlist">
                <c:forEach items="${userlist}" var="user">
                <tr>
                    <td>${user.showUsername}</td>
                    <td>${user.roleName}</td>
                    <c:choose>
                    <c:when test="${user.modifyByName==null}">
                    <td>${user.createByName}</td>
                    </c:when>
                    <c:otherwise>
                    <td>${user.modifyByName}</td>
                    </c:otherwise>
                    </c:choose>
                    <c:choose>
                    <c:when test="${user.modifyTime==null}">
                    <td><fn:formatDate value='${user.createTime}' pattern='yyyy-MM-dd' /></td>
                    </c:when>
                    <c:otherwise>
                    <td><fn:formatDate value='${user.modifyTime}' pattern='yyyy-MM-dd' /></td>
                    </c:otherwise>
                    </c:choose>
                    <c:if test="${user.status == 1}">
                    <td><input type="button" onmouseover="mOver(this,'1')" onmouseout="mOut(this,'1')" onclick="amenduserstatus('${user.id}','${user.status}')" class="btn btn-success" value="启用"/></td>
                    </c:if>
                    <c:if test="${user.status == 2}">
                    <td ><input type="button" onmouseover="mOver(this,'2')" onmouseout="mOut(this,'2')" onclick="amenduserstatus('${user.id}','${user.status}')" class="btn btn-warning" value="禁用"/></td>
                    </c:if>
                    <td><input type="button" onclick="authorityadmin('${user.role}')" class="btn btn-primary"  id="authorityManag" value="用户权限管理"/></td>
                    <td>
                        <a href="#"><img src="assets/icon_9m8sb4cx6z/all.png"  style="width: 15px;" onclick="AllTbUser('${user.id}')"   title="查看"/></a>
                          <a href="#"><img src="assets/icon_9m8sb4cx6z/update.png" style="width: 15px;"  onclick="updateUser('${user.id}')"  title="修改"/></a>
                            <a href="#"><img src="assets/icon_9m8sb4cx6z/delete.png" style="width: 15px;"  onclick="deleteUser('${user.id}')" title="删除"/></a>
                       
                    </td>
                </tr>
                </c:forEach>
            </tbody>
        </table>               
     </div>     
                
                
        <div class="text-center">
        
<!--MvcPager v3.0.1 for ASP.NET MVC 4.0+ © 2009-2015 Webdiyer (http://www.webdiyer.com)-->
<ul class="pagination" data-currentpage="2" data-firstpage="/mvcpager/demos/applycss/" data-invalidpageerrmsg="页索引无效" data-outrangeerrmsg="页索引超出范围" data-pagecount="80" data-pageparameter="id" data-pagerid="Webdiyer.MvcPager" data-urlformat="/mvcpager/demos/applycss/__id__/" id="bootstrappager">
	    <li>
		    <a href="#" id="homePage">首页</a>
		</li>
		<li><a href="#" id="prevPage">上页</a>	
		</li>
		<li>
			<a href="#"><span id="PageNum">${PageNum}</span>/<span id="Page">${Page}</span></a>
		</li>
		<li>
			<a href="#" id="nextPage">下页</a>
		</li>
		<li>
			<a href="#" id="endPage">尾页</a>
		</li>
</ul>
<!--MvcPager v3.0.1 for ASP.NET MVC 4.0+ © 2009-2015 Webdiyer (http://www.webdiyer.com)-->

</div>               	
                	</div>
               </div> 
<%@include file="footer.jsp"%>