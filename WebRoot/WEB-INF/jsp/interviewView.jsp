<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<div style="margin: 10px;">
            	<!-- 导航条  -->
            	<ul  class="breadcrumb">
            		<li><a href="index.html">首页</a><i class="en-arrow-right7"></i></li>
            		<li></i><a href="#">招聘管理</a><i class="en-arrow-right7"></i></li>
            		<li></i><a href="#">面试管理</a><i class="en-arrow-right7"></i></li>
            		<li><i class="en-arrow-right7"></i>面试状态查看</li>
            	</ul>
			</div>
			
					<ul class="row resumetext">
				<li class=" col-md-3">
              	    <label>
              			所属部门:           			
              		</label>
              		<span class="rstext">财务部</span>
              	</li>
              	
              	<li class=" col-md-3">
              	    <label>
              			应聘职位:           			
              		</label>
              			<span>会计员</span>
              	</li>
              	   
              	<li class="col-md-3">
              		<label>职位编码:</label>
              		<span>GASDW</span>
              	</li>
              	
              	<li class="col-md-3">
              		<label>职位分类:</label>
              		<span>技术</span>
              	</li>
            </ul>
            
            <ul class="row resumetext">
            	<li class="col-md-3">
              		<label>
              			姓名:
              			&nbsp;&nbsp;
              			&nbsp;&nbsp;&nbsp; 			
              		</label>
              		<span>张三</span>
              	</li>
              	
              	<li class=" col-md-3">
              	    <label>
              			性别: 
              			&nbsp;&nbsp;
              			&nbsp;&nbsp;&nbsp;            			
              		</label>
              		<span>男</span>
              	</li>
              	
              	<li class="  col-md-3">
              		<label >招聘类型:</label>
              		<span>校园招聘</span>
              	</li>
            	
            	<li class="col-md-3">
              		<label>
              			邮箱:
              			&nbsp;&nbsp;
              			&nbsp;&nbsp;&nbsp;          			
              		</label>
              		<span>773623455@qq.com</span>
              </li>           	
            </ul>
            
      
            
            
            <ul class="row resumetext">
            	
            	<li class="col-md-3">
              		<label>
              			联系电话:          			 			
              		</label>
              		<span>15241686495</span>
              </li>
              
            	<li class="col-md-3">
              		<label>
              			政治面貌:		
              		</label>
              		<span>党员</span>
              	</li>
              	
              	<li class=" col-md-3">
              	    <label>
              			身份证号:     			
              		</label>
              		<span>544848455486154848</span>
              	</li>
              	
          
            	<li class="col-md-3">
              		<label>
              			学历:
              			&nbsp;&nbsp;
              			&nbsp;&nbsp;&nbsp;          			
              		</label>
              		<span>本科</span>
              </li>           	
            </ul>
            
            
             <ul class="row resumetext">
            	<li class="col-md-3">
              		<label>
              			专业:	
              			&nbsp;&nbsp;
              			&nbsp;&nbsp;&nbsp;
              		</label>
              		<span>金融</span>
              	</li>
              	
              	    	<li class="  col-md-3">
              		<label >毕业院校:</label>
              		<span>广州岭南金融学院</span>
              	</li>
            	
              	
              	
              	<li class="  col-md-3">
              		<label >资薪要求:</label>
              		<span>6000</span>
              		<label>(月薪)</label>
              	</li>
            	
            	<li class="col-md-3">
              		<label>
              			是否在职:         			
              		</label>
              		<span>是</span>
              </li>           	
            </ul>
            
            
             <ul class="row resumetext">  
             	
             	<li class=" col-md-3">
              	    <label>
              			工作经验:     			
              		</label>
              		<span>9</span>
              	</li>
            	<li class="col-md-3">
              		<label >
              			是否应届生:         			
              		</label>
              		<span>否</span>
              </li>    
                                   
              <li class="col-md-3">
              		<label>登记时间:</label>
              		<span>2018-08-17</span>
              	</li>
            </ul>
            
        <ul class="row resumetext">

              	<li class="col-md-6">
              		<label>
              			户口所在地:          			 			
              		</label>
              		<span>奥术大师大奥所大多</span>
              	</li>
            	
            	   	<li class="col-md-6">
              		<label>
              			现住址:          			 			
              		</label>
              		<span>广州市天河区</span>
              	</li>
            </ul>
            
            
             <ul class="row resumetext">             	
              	<li class="col-md-12">
					<span style="float: left;">个人履历:</span>
              		<textarea name="" rows="8" cols="100%"></textarea>
              	</li>
              </ul>
              
               <ul class="row resumetext">             	
              	<li class="col-md-12">
              		<span>简历附件:</span>
              		<div class="file-box btn btn-primary">
                    <input type="file" class="file-btn"/>
                                                                  上传文件
                    </div>
                  	  
              	</li>
              </ul>
              
              <ul class="row resumetext">     
              
              	
              	<li class="col-md-3">
              		<label>推荐人:</label>
              		<span>人事小张</span>
              	</li>
              	
              	<li class="col-md-3">
              		<label>推荐时间: &nbsp;&nbsp;&nbsp;</label>
              		<span>2017-8-17</span>
              	</li>		
              </ul>
              
            <ul class="row resumetext">             	
              	<li class="col-md-12">
					<span style="float: left;">推荐意见:</span>
					
              		<textarea name="" rows="3" cols="100%"></textarea>
              	</li>
            </ul>
            
 <div class="interview">
 	<span>----------面试结果---------------------------------------------------------------------------------------------------------------------------</span>
 </div>           
            
            <ul class="row resumetext">
            	<li class=" col-md-4">
            	<label>面试状态:</label>
              		<span>一面</span>
				</li>              	
              	<li class=" col-md-4">
              	    <label>笔试等级:</label>
              		<span>B</span>
              	</li>
              	

            	<li class="col-md-4">
              		<label>笔试成绩:</label>
              		<span>100</span>
              </li>           	
            </ul>
            
            
            <ul class="row resumetext">
            	<li class="col-md-3">
              		<label>一面面试人:</label>
              		<span>招聘小王</span>
              	</li>
              	
              	<li class=" col-md-3">
              	   <label>面试时间:</label>
              		<span>2018-8-22</span>
              	</li>
              	

            		<li class="col-md-3">
              		<label>二面面试人:</label>
              		<span></span>
              	</li>
              	
              	<li class=" col-md-3">
              	   <label>面试时间:</label>
              		<span></span>
              	</li>         	
            </ul>
            <ul class="row resumetext">             	
              	<li class="col-md-6">
					<span style="float: left;">一面评价:</span>
              		<textarea name="" rows="3" cols="60%"></textarea>
              	</li>
              	
              		<li class="col-md-6">
					<span style="float: left;">二面评价:</span>
              		<textarea name="" rows="3" cols="60%"></textarea>
              	</li>
              </ul>
            
                <ul  >
              	<li>
					<input type="button"  class="btn btn-primary bunntleft" name="" id="" value="保存" />
				</li>
				<li>
					<input type="button"  class="btn btn-primary bunnt" name="" id="" value="返回" />
				</li>
              </ul>
<%@include file="footer.jsp"%>