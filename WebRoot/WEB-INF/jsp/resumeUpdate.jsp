<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<div style="margin: 10px;">
            	<!-- 导航条  -->
            	<ul  class="breadcrumb">
            		<li><a href="index.html">首页</a><i class="en-arrow-right7"></i></li>
            		<li></i><a href="#">招聘管理</a><i class="en-arrow-right7"></i></li>
            		<li></i><a href="#">简历管理</a><i class="en-arrow-right7"></i></li>
            		<li><i class="en-arrow-right7"></i>修改简历</li>
            	</ul>
			</div>
			
			<ul class="row resumetext">
				<li class=" col-md-3">
              	    <span>
              			所属部门:           			
              		</span>
              		<select class="select">
              			<option>--请选择--</option>
              		</select> 
              	</li>
              	
              	<li class=" col-md-3">
              	    <span>
              			应聘职位:           			
              		</span>
              		<select class="select">
              			<option>--请选择--</option>
              		</select> 
              	</li>
              	   
              	<li class="col-md-3">
              		<span>职位编码:</span>
              		<input type="text" name="" id="" class="select" readonly/>
              	</li>
              	
              	<li class="col-md-3">
              		<span>职位分类:</span>
              		<input type="text" name="" id="" class="select" readonly/>
              	</li>
            </ul>
            
            <ul class="row resumetext">
            	<li class="col-md-3">
              		<span>
              			姓名:
              			&nbsp;&nbsp;
              			&nbsp;&nbsp;&nbsp; 			
              		</span>
              		<input type="text" name="" id="" class="select" />
              	</li>
              	
              	<li class=" col-md-3">
              	    <span>
              			性别: 
              			&nbsp;&nbsp;
              			&nbsp;&nbsp;&nbsp;            			
              		</span>
              		<select class="select">
              			<option>--请选择--</option>
              			<option>男</option>
              			<option>女</option>
              		</select> 
              	</li>
              	
              	<li class="  col-md-3">
              		<span >招聘类型:</span>
              		<select class="select">
              			<option>--请选择--</option>
              			<option>--校园招聘--</option>
              			<option>--社会招聘--</option>
              		</select> 
              	</li>
            	
            	<li class="col-md-3">
              		<span>
              			邮箱:
              			&nbsp;&nbsp;
              			&nbsp;&nbsp;&nbsp;          			
              		</span>
              		<input type="text" name="" id="" class="select"  />
              </li>           	
            </ul>
            
      
            
            
            <ul class="row resumetext">
            	
            	<li class="col-md-3">
              		<span>
              			联系电话:          			 			
              		</span>
              		<input type="text" name="" id="" class="select" />
              </li>
              
            	<li class="col-md-3">
              		<span>
              			政治面貌:		
              		</span>
              		<input type="text" name="" id="" class="select" />
              	</li>
              	
              	<li class=" col-md-3">
              	    <span>
              			身份证号:     			
              		</span>
              		<input type="text" name="" id="" class="select" />
              	</li>
              	
          
            	<li class="col-md-3">
              		<span>
              			学历:
              			&nbsp;&nbsp;
              			&nbsp;&nbsp;&nbsp;          			
              		</span>
              		<select class="select">
              			<option>--请选择--</option>
              			<option>--校园招聘--</option>
              			<option>--社会招聘--</option>
              		</select> 
              </li>           	
            </ul>
            
            
             <ul class="row resumetext">
            	<li class="col-md-3">
              		<span>
              			专业:	
              			&nbsp;&nbsp;
              			&nbsp;&nbsp;&nbsp;
              		</span>
              		<input type="text" name="" id="" class="select" />
              	</li>
              	
              	    	<li class="  col-md-3">
              		<span >毕业院校:</span>
              		<input type="text" name="" id="" class="select" />
              	</li>
            	
              	
              	
              	<li class="  col-md-3">
              		<span >资薪要求:</span>
              		<input type="text" name="" id="" class="select" />
              		<span>(月薪)</span>
              	</li>
            	
            	<li class="col-md-3">
              		<span>
              			是否在职:         			
              		</span>
              		<select class="select">
              			<option>--请选择--</option>
              			<option>--是--</option>
              			<option>--否--</option>
              		</select> 
              </li>           	
            </ul>
            
            
             <ul class="row resumetext">  
             	
             	<li class=" col-md-3">
              	    <span>
              			工作经验:     			
              		</span>
              		<input type="text" name="" id="" class="select" />
              	</li>
            	<li class="col-md-3">
              		<span >
              			是否应届生:         			
              		</span>
              		<select class="select">
              			<option>--请选择--</option>
              			<option>--是--</option>
              			<option>--否--</option>
              		</select> 
              </li>    
                                   
              <li class="col-md-3">
              		<span>登记时间:</span>
              		<input type="text" class="input select" id="date" />
              	</li>
            </ul>
            
        <ul class="row resumetext">

              	<li class="col-md-6">
              		<span>
              			户口所在地:          			 			
              		</span>
              		<input type="text" name="" id="" class="select" style="width: 70%;" />
              	</li>
            	
            	   	<li class="col-md-6">
              		<span>
              			现住址:          			 			
              		</span>
              		<input type="text" name="" id="" class="select" style="width: 70%;"/>
              	</li>
            </ul>
            
            
             <ul class="row resumetext">             	
              	<li class="col-md-12">
					<span style="float: left;">个人履历:</span>
              		<textarea name="" rows="8" cols="100%"></textarea>
              	</li>
              </ul>
              
               <ul class="row resumetext">             	
              	<li class="col-md-12">
              		<span>简历附件:</span>
              		<div class="file-box btn btn-primary">
                    <input type="file" class="file-btn"/>
                                                                  上传文件
                    </div>
                  	  
              	</li>
              </ul>
              
              <ul class="row resumetext">     
              	<li class="col-md-3">
              		<span>是否推荐面试:</span>
              		<select class="select">
              			<option>--请选择--</option>
              			<option>--是--</option>
              			<option>--否--</option>
              		</select> 
              	</li>
              	
              	<li class="col-md-3">
              		<span>推荐人:</span>
              		<input type="text" name="" id="" class="select" readonly/>
              	</li>
              	
              	<li class="col-md-3">
              		<span>推荐时间: &nbsp;&nbsp;&nbsp;</span>
              		<input type="text" name="" id="" class="select" readonly/>
              	</li>		
              </ul>
              
            <ul class="row resumetext">             	
              	<li class="col-md-12">
					<span style="float: left;">推荐意见:</span>
					
              		<textarea name="" rows="3" cols="100%"></textarea>
              	</li>
            </ul> 
            
             <ul  >
              	<li>
					<input type="button"  class="btn btn-primary bunntleft" name="" id="" value="保存" />
				</li>
				<li>
					<input type="button"  class="btn btn-primary bunnt" name="" id="" value="返回" />
				</li>
            </ul>
<%@include file="footer.jsp"%>