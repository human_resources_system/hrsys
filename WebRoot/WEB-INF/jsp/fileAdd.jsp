<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<div style="margin: 10px;">
            	<!-- 导航条  -->
            	<ul class="breadcrumb">
            		<li><a href="index.html">首页</a><i class="en-arrow-right7"></i></li>
            		<li><a href="#">人事管理</a><i class="en-arrow-right7"></i></li>
            		<li><a href="file.html">人事档案管理</a><i class="en-arrow-right7"></i></li>
            		<li><i class="en-arrow-right7"></i>人事档案登记</li>
            	</ul>
			</div>
			
		 		<div class="qu">
		 			<ul>
		 				<li>
		 					<span>档案编码:</span>
              		      <input type="text" name="" class="select">
		 				</li>
		 				<li>
		 					<span>所属部门:</span>
		              		<select class="select">
		              			<option>--请选择--</option>
		              			<option>--校园招聘--</option>
		              			<option>--社会招聘--</option>
		              		</select> 		 					
		 				</li>
		 				<li>		 					
		 					<span>档案状态:</span>
		              		<select class="select">
		              			<option>--请选择--</option>
		              			<option>--校园招聘--</option>
		              			<option>--社会招聘--</option>
		              		</select> 	
		 				</li>
		 				<li>	 					
		              		<span>职位名称:</span>
		              		<select class="select">
		              			<option>--请选择--</option>          			
		              		</select> 	 					
		 				</li>
		 			
			                <div class="file-box quphoto">
			                    <input type="file" name="photo" onchange="photoAll()" id="photo" class="file-btn">
			                    <img alt="个人照片" id="photos" style="height: 115px;width: 120px;"  src="assets/icon_9m8sb4cx6z/emppty.png">
			                    </div>
			                
		 					<li>
		 					<span>职位编码:</span>
              		      <input type="text" name="" class="select">
		 				</li>
		 				<li>
		 					<span>
		              			性别: 
		              			&nbsp;&nbsp;
		              			&nbsp;&nbsp;&nbsp;            			
		              		</span>
		              		<select class="select">
		              			<option>--请选择--</option>
		              			<option>男</option>
		              			<option>女</option>
		              		</select> 
		 					
		 				</li>
		 				
		 				<li>
		 					<span>
		              			姓名:
		              			&nbsp;&nbsp;
		              			&nbsp;&nbsp;&nbsp; 			
		              		</span>
		              		<input type="text" id="Filename" name="" class="select">
		 				</li>
		 				<li>		 					
		              		<span>
		              			邮箱:
		              			&nbsp;&nbsp;
		              			&nbsp;&nbsp;&nbsp;          			
		              		</span>
		              		<input type="text" name="" class="select">
		 				
		 				</li>
		 				<li>
		 					<span>
              					住址:
              					&nbsp;&nbsp;
		              			&nbsp;&nbsp;&nbsp;  
              				</span>
              				<input type="text" name="" class="select" style="width: 670px;">
		 				</li>
		 				<li>
		 					<span>
		              			联系电话:          			 			
		              		</span>
		              		<input type="text" name="" id="" class="select">
		 					
		 				</li>
		 				<li>
		 					<span>
		              			身份证号:     			
		              		</span>
		              		<input type="text" name="" class="select">
		 				</li>
		 				<li>
		 					<span>
		              			户口地址:     			
		              		</span>
		              		<input type="text" name="" class="select">
		 				</li>
		 				<li>
		 					<span>
		              			政治面貌:		
		              		</span>
		              		<input type="text" name="" class="select">				
		 				</li>
		 				
		 				<li class="gaga">
		 				
		 				</li>
		 				<li>
		 					<span>
		              			毕业院校:		
		              		</span>
		              		<input type="text" name="" class="select">
		 				</li>
		 				<li>
		 					<span>
		              			专业:		
		              			&nbsp;&nbsp;
		              			&nbsp;&nbsp;&nbsp;
		              		</span>
		              		<input type="text" name="" class="select">
		 				</li>
		 				<li>
		 					<span>
		              			学历:
		              			&nbsp;&nbsp;
		              			&nbsp;&nbsp;&nbsp;          			
		              		</span>
		              		<select class="select">
		              			<option>--请选择--</option>
		              			<option>--校园招聘--</option>
		              			<option>--社会招聘--</option>
		              		</select> 
		 				</li>
		 				<li>
		 					<span>酬薪标准:</span>
		 					<select class="select">
		              			<option>--请选择--</option>
		              			<option>--校园招聘--</option>
		              			<option>--社会招聘--</option>
		              		</select> 
		 				</li>
		 				<li>
		 					<span>
		              			开户行:		
		              			&nbsp;&nbsp;
		              		</span>
		              		<input type="text" name="" class="select">
		 				</li>
		 				<li>
		 					<span>
		              		银行卡号:
		              		</span>
		              		<input type="text" name="" class="select">
		 				</li>
		 				<li>
		 					<span>
		              		社保卡号:
		              		</span>
		              		<input type="text" name="" class="select">
		 				</li>
		 				<li>
		 					<span>
		              		登记人:
		              		&nbsp;&nbsp;&nbsp;
		              		</span>
		              		<input type="text" name="" class="select" readonly="">
		 				</li>
		 				<li>
		 						<span>建档时间:</span>
              		<input type="text" class="input select" id="date">
		 				</li>
		 				<li class="gaga"></li>
		 				<li>				
							<span style="float: left;">个人履历:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
		              		<textarea name="" rows="6" cols="100%"></textarea>
		 				</li>
		 				<li>				
							<span style="float: left;">家庭关系信息:</span>
		              		<textarea name="" rows="4" cols="100%"></textarea>
		 				</li>
		 					<li>				
							<span style="float: left;">
								备注:
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								
							</span>
		              		<textarea name="" rows="2" cols="100%"></textarea>
		 				</li>
		 				<li class="gaga"></li>
		 				<li>
		 						<span>简历附件:</span>
			              		<div class="file-box btn btn-primary">
			                    <input type="file" name="resume" onchange="resumeAll()" id="resume" class="file-btn">
			                                                                  上传文件
			                    </div>
			                    <a href="#" class="btn" id="res"></a>
			      
		 				</li>
		 				<li class="gaga"></li>
		 				
		 			</ul>
		 		</div>
		 		<ul >
              	<li class="bynnt3ge bynnt3gelert">
					<input type="button"  class="btn btn-primary" name="" id="" value="提交审核" />
				</li>
				<li class="bynnt3ge">
					<input type="button"  class="btn btn-primary" name="" id="saveFile" value="保存" />
				</li>
				<li class="bynnt3ge">
					<input type="button"  class="btn btn-primary" name="" id="" value="返回" />
				</li>
              </ul>
<%@include file="footer.jsp"%>