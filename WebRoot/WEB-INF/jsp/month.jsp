<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<div style="margin: 10px;">
            	<!-- 导航条  -->
            	<ul  class="breadcrumb">
            		<li><a href="index.html">首页</a><i class="en-arrow-right7"></i></li>
            		<li></i><a href="#">人事管理</a><i class="en-arrow-right7"></i></li>
            		<li></i><a href="clockingin.html">考勤信息管理</a><i class="en-arrow-right7"></i></li>
            		<li><i class="en-arrow-right7"></i> 考勤信息统计</li>
            	</ul>
			</div>
			

            <div >
					<ul class="toubu">
							<li>
								<span>姓名：</span>
								<input type="text" class="test" />					
							</li>
							<li>
								<span>所属部门：</span>
								<select class="test">
									<option value="0">----请选择----</option>
								</select>					
							</li>
							<li>								
									<label for="search_begindate">选取月份：</label>
									<input type="text" class="input test" id="search_begindate"  />
							</li>
							<li>
								<input type="button" class="btn btn-primary" name="" id="" value="查询" />
							</li>
					</ul>
		    </div>
                
      
                
                
        <div class="ta">
            <table class="providerTable" cellpadding="0" cellspacing="0">
                <tbody>
                <tr class="firstTr">
                    <th width="7.1%">档案编码</th>
                    <th width="7.1%">员工姓名</th>
                    <th width="7.1%">所属部门</th>
                    <th width="7.1%">职位</th>
                    <th width="7.1%">职称</th>
                    <th width="7.1%">迟到早退次数</th>
                    <th width="7.1%">迟到早退应扣薪水</th>
                    <th width="7.1%">请假天数</th>
                    <th width="7.1%">请假应扣薪水</th>
                    <th width="7.1%">加班总工时</th>
                    <th width="7.1%">加班补贴</th>
                    <th width="7.1%">出差天数</th>
                    <th width="7.1%">出差补贴</th>
                    <th width="7.1%">月总工时</th>
                </tr>
                
                <tr>
                    <td>BDQN0201</td>
                    <td>张三</td>
                    <td>产品设计中心</td>
                    <td>技术培训</td>
                    <td>高级顾问</td>
                    <td>9：00</td>
                    <td>18：00</td>
                    <td>正常出勤</td>
                    <td>2017-09-01</td>
                    <td>BDQN0201</td>
                    <td>张三</td>
                    <td>产品设计中心</td>
                    <td>技术培训</td>
                    <td>高级顾问</td>
                </tr>
                <tr>
                    <td>BDQN0201</td>
                    <td>张三</td>
                    <td>产品设计中心</td>
                    <td>技术培训</td>
                    <td>高级顾问</td>
                    <td>9：00</td>
                    <td>18：00</td>
                    <td>正常出勤</td>
                    <td>2017-09-01</td>
                    <td>BDQN0201</td>
                    <td>张三</td>
                    <td>产品设计中心</td>
                    <td>技术培训</td>
                    <td>高级顾问</td>
                </tr>
                <tr>
                    <td>BDQN0201</td>
                    <td>张三</td>
                    <td>产品设计中心</td>
                    <td>技术培训</td>
                    <td>高级顾问</td>
                    <td>9：00</td>
                    <td>18：00</td>
                    <td>正常出勤</td>
                    <td>2017-09-01</td>
                    <td>BDQN0201</td>
                    <td>张三</td>
                    <td>产品设计中心</td>
                    <td>技术培训</td>
                    <td>高级顾问</td>
                </tr>
            </tbody>
        </table>               
     </div>
<%@include file="footer.jsp"%>