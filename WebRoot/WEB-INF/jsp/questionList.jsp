<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<div style="margin: 10px;">
            	<!-- 导航条  -->
            	<ul  class="breadcrumb">
            		<li></i><a href="index.html">系统设置</a><i class="en-arrow-right7"></i></li>
            		<li><i class="question.html"></i> <a href="questionList.html">题库管理设置</a></li>
            	</ul>
			</div>
			

            <div >
					<ul class="toubu">
							<li>
								<span>试题标题：</span>
								<input type="text" class="test" />					
							</li>
							<li>
								<span>试题类型：</span>
								<select id="city" class="select">
						        <option selected="" value="-- 请选择 --">-- 请选择 --</option>
						        <option value="JAVA开发">JAVA开发</option>
						        <option value="C++开发">C++开发</option>
						        <option value="C#开发">C#开发</option>
						        <option value="PHP开发">PHP开发</option>
						        <option value="软件测试">软件测试</option>
						      </select>
							</li>
							<li>								
								<center>
										<span>所属部门：</span>
										<select id="city" class="select">
										    <option selected="" value="-- 请选择 --">-- 请选择 --</option>
										    <option value="质量管理部">质量管理部</option>
										    <option value="产品设计中心">产品设计中心</option>
										    <option value="平台研发中心">平台研发中心</option>
										    <option value="产品管理部">产品管理部</option>
										    <option value="区域市场发展一部">区域市场发展一部</option>
										    <option value="区域市场发展二部">区域市场发展二部</option>
										    <option value="区域市场发展三部">区域市场发展三部</option>
										    <option value="区域市场发展四部">区域市场发展四部</option>
										    <option value="人力资源中心">人力资源中心</option>
										    <option value="财务部">财务部</option>
										</select>
								</center>	
								
							</li>
							<li>
								
								<center>
										<span>是否启用：</span>
										  <select id="city" class="select">
									        <option selected="" value="-- 请选择 --">-- 请选择 --</option>
									        <option value="是">是</option>
									        <option value="否">否</option>
									      </select>
								</center>					
							</li>
							<li>
								<input type="button" class="btn btn-primary" name="" id="" value="查询" />
							</li>
							<li>
								<a href="QuestionAdd.html">
								<input type="button" class="btn btn-danger" name="" id="" value="新增试题" /></a>
							</li>
								
					</ul>
		    </div>
                
      
                
                
        <div class="ta">
            <table class="providerTable" cellpadding="0" cellspacing="0">
                <tbody>
                <tr class="firstTr">
                    <th width="10%">试题标题</th>
                    <th width="10%">试题类型</th>
                    <th width="10%">试题级别</th>
                    <th width="10%">所属部门</th>
                    <th width="10%">创建人</th>
                    <th width="10%">出题时间</th>
                    <th width="10%">是否启用</th>
                    <th width="30%">操作</th>
                    
                </tr>
                <tr>
                    <td>java</td>
                    <td>java高级</td>
                    <td>A</td>
                    <td>开发部</td>
                    <td>二狗</td>
                    <td>2015-11-12</td>
                    <td>是</td>
                    <td>
                        <a href="QuestionView.html"><img src="assets/icon_9m8sb4cx6z/all.png"  style="width: 15px;"  title="查看"/></a>
                          <a href="QuestionUptate.html"><img src="assets/icon_9m8sb4cx6z/update.png" style="width: 15px;"  title="修改"/></a>
                            <a href="providerView.html"><img src="assets/icon_9m8sb4cx6z/delete.png" style="width: 15px;"  title="删除"/></a>
                       
                    </td>
                </tr>
                <tr>
                    <td>c++</td>
                    <td>c++中级</td>
                    <td>B</td>
                    <td>开发部</td>
                    <td>大蛇</td>
                    <td>2015-11-12</td>
                    <td>否</td>
                    <td>
                        <a href="providerView.html"><img src="assets/icon_9m8sb4cx6z/all.png"  style="width: 15px;"  title="查看"/></a>
                          <a href="providerView.html"><img src="assets/icon_9m8sb4cx6z/update.png" style="width: 15px;"  title="修改"/></a>
                            <a href="providerView.html"><img src="assets/icon_9m8sb4cx6z/delete.png" style="width: 15px;"  title="删除"/></a>
                    </td>
                </tr>
            </tbody>
        </table>               
     </div>     
                
                
        <div class="text-center">
        
<!--MvcPager v3.0.1 for ASP.NET MVC 4.0+ © 2009-2015 Webdiyer (http://www.webdiyer.com)-->
<ul class="pagination" data-currentpage="2" data-firstpage="/mvcpager/demos/applycss/" data-invalidpageerrmsg="页索引无效" data-outrangeerrmsg="页索引超出范围" data-pagecount="80" data-pageparameter="id" data-pagerid="Webdiyer.MvcPager" data-urlformat="/mvcpager/demos/applycss/__id__/" id="bootstrappager">
	    <li>
		    <a href="/mvcpager/demos/applycss/">首页</a></li><li><a href="/mvcpager/demos/applycss/">上页</a>	
		</li>
		<li>
			<a href="/mvcpager/demos/applycss/">1</a>
		</li>
		<li >
			<a href="#">2</a>
		</li>
		<li>
			<a href="/mvcpager/demos/applycss/3/">3</a>
		</li>
		<li>
			<a href="/mvcpager/demos/applycss/4/">4</a>
		</li>
		<li>
			<a href="/mvcpager/demos/applycss/5/">5</a>
		</li>
		<li>
			<a href="/mvcpager/demos/applycss/6/">6</a>
		</li>
		<li>
			<a href="/mvcpager/demos/applycss/7/">7</a>
		</li>
		<li>
			<a href="/mvcpager/demos/applycss/8/">8</a>
		</li>
		<li>
			<a href="/mvcpager/demos/applycss/9/">9</a>
		</li>
		<li>
			<a href="/mvcpager/demos/applycss/10/">10</a>
		</li>
		<li>
			<a href="/mvcpager/demos/applycss/11/">...</a>			
		</li>
		<li>
			<a href="/mvcpager/demos/applycss/3/">下页</a>
		</li>
		<li>
			<a href="/mvcpager/demos/applycss/80/">尾页</a>
		</li>
</ul>
<!--MvcPager v3.0.1 for ASP.NET MVC 4.0+ © 2009-2015 Webdiyer (http://www.webdiyer.com)-->

</div>               	
                	</div>
               </div>
<%@include file="footer.jsp"%>