<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fn"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html lang="en">
    <head>
    <base href="<%=basePath%>">
       <meta charset="utf-8">
        <title>Forms</title>
        <!-- Mobile specific metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <!-- Force IE9 to render in normal mode -->
        <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
        <meta name="author" content="SuggeElson" />
        <meta name="description" content=""
        />
        <meta name="keywords" content=""
        />
        <meta name="application-name" content="sprFlat admin template" />
        <!-- Import google fonts - Heading first/ text second -->
        <link rel='stylesheet' type='text/css'/>
        <!--[if lt IE 9]>

<![endif]-->
        <!-- Css files -->
        <!-- Icons -->
        <link href="assets/css/icons.css" rel="stylesheet" />
        <!-- jQueryUI -->
        <link href="assets/css/sprflat-theme/jquery.ui.all.css" rel="stylesheet" />
        <!-- Bootstrap stylesheets (included template modifications) -->
        <link href="assets/css/bootstrap.css" rel="stylesheet" />
        <!-- Plugins stylesheets (all plugin custom css) -->
        <link href="assets/css/plugins.css" rel="stylesheet" />
        <!-- Main stylesheets (template main css file) -->
        <link href="assets/css/main.css" rel="stylesheet" />
        <!-- Custom stylesheets ( Put your own changes here ) -->
        <link href="assets/css/custom.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="assets/css/style2.css"/>
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css"/>
        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/img/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/img/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/img/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/img/ico/apple-touch-icon-57-precomposed.png">
        <link rel="icon" href="assets/img/ico/favicon.ico" type="image/png">
        <!-- Windows8 touch icon ( http://www.buildmypinnedsite.com/ )-->
        <meta name="msapplication-TileColor" content="#3399cc" />
       
		<!--
        	作者：offline
        	时间：2018-08-15
        	描述：页面基本样式  开始
        -->
        <link href="assets/css/ceshi.css" rel="stylesheet"/>
        <link href="assets/csszi/zj1.css" rel="stylesheet"/>
         <link rel="stylesheet" type="text/css" href="assets/csszi/zj2.css"/>
         <!--
         	作者：offline
         	时间：2018-08-15
         	描述：页面基本样式    结束
         -->
         <!--
         	作者：offline
         	时间：2018-08-15
         	描述：时间格式样式  开始
         -->
         <link rel="stylesheet" type="text/css" href="assets/time/datedropper.css">
         <link rel="stylesheet" type="text/css" href="assets/time/timedropper.min.css">
       	<!--
           	作者：offline
           	时间：2018-08-15
           	描述：时间格式样式 结束
           -->
           
           <style type="text/css">
        	#sidebar .sidebar-inner #sideNav li .nav.sub li a{
        		padding-left: 50px;
        	}
        	.tree li>span:hover{
        		background-color: lightblue;
        	}
        </style>
        
       
    </head>
    <body>
        <!-- Start #header -->
        <div id="header">
            <div class="container-fluid">
                <div class="navbar">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="#">
                            <i class="im-windows8 text-logo-element animated bounceIn"></i><span class="text-logo">HR</span><span class="text-slogan">SYS</span> 
                        </a>
                    </div>
                    <nav class="top-nav" role="navigation">
                        <ul class="nav navbar-nav pull-left">
                            <li id="toggle-sidebar-li">
                                <a href="#" id="toggle-sidebar"><i class="en-arrow-left2"></i>
                        </a>
                            </li>
                            <li>
                                <a href="#" class="full-screen"><i class="fa-fullscreen"></i></a>
                            </li>
                            
                            
                        </ul>
                        <ul class="nav navbar-nav pull-right">
                            
                            
                            <li class="dropdown">
                                <a href="#" data-toggle="dropdown" style="width: 270px;text-align: left;">
                                    <img id="tbuserVia" class="user-avatar" src="" alt=""><span class="tbshowUsername"></span>--<span class="tbroleName"></span></a>
                                <ul class="dropdown-menu right" role="menu" style="width: 270px;text-align: left;">
                                    <li><a href="#" onclick="selUser()"><i class="st-user"></i>个人信息</a>
                                    </li>
                                    <li><a href="#" onclick="upUser()"><i class="st-settings"></i>修改资料</a>
                                    </li>
                                    <li><a href="#" class="userlogout"><i class="im-exit"></i>退出登录</a>
                                    </li>
                                </ul>
                            </li>
                            
                        </ul>
                    </nav>
                </div>
                <!-- Start #header-area -->
                
                <!-- End #header-area -->
            </div>
            <!-- Start .header-inner -->
        </div>
        <!-- End #header -->
        <!-- Start #sidebar -->
        <div id="sidebar">
            <!-- Start .sidebar-inner -->
            <div class="sidebar-inner">
                <!-- Start #sideNav -->
                <ul id="sideNav" class="nav nav-pills nav-stacked">
                    <li class="top-search">
                        <form>
                            <input type="text" name="search" placeholder="搜索 ...">
                            <button type="submit"><i class="ec-search s20"></i>
                            </button>
                        </form>
                    </li>
                    <li><a href="main/index.html" id="index">首页 <i class=""></i></a>
                    </li>
                    <c:forEach items="${fatherauthorityList}" var="fatlist">
                    <li>
                        <c:if test="${fatlist.modelName=='我的工作平台'}">
                        <a href="#">${fatlist.modelName}<i class="im-paragraph-justify"></i></a>
                        </c:if>
                        <c:if test="${fatlist.modelName=='系统设置'}">
                        <a href="#">${fatlist.modelName}<i class="ec-cog"></i></a>
                        </c:if>
                        <c:if test="${fatlist.modelName=='人事管理'}">
                        <a href="#">${fatlist.modelName}<i class="im-users2"></i></a>
                        </c:if>
                        <c:if test="${fatlist.modelName=='薪酬管理'}">
                        <a href="#">${fatlist.modelName}<i class="im-coin"></i></a>
                        </c:if>
                        <c:if test="${fatlist.modelName=='招聘管理'}">
                        <a href="#">${fatlist.modelName}<i class="fa-flag-checkered"></i></a>
                        </c:if>
                        <c:if test="${fatlist.modelName=='数据分析'}">
                        <a href="#">${fatlist.modelName}<i class="im-bars"></i></a>
                        </c:if>
                        <ul class="nav sub">
                        <c:forEach items="${sonauthorityList}" var="sonlist">
                             <c:if test="${fatlist.modelID==sonlist.parent}">
                            <li><a href="${sonlist.modelURL}" id="${sonlist.relPage}${sonlist.menuType}" onmouseover="addValue(this)">${sonlist.modelName}</a>
                            </li> 
                            </c:if> 
                            </c:forEach>                         
                        </ul>
                    </li>
                    </c:forEach>
                </ul>
                <!-- End #sideNav -->
                <!-- Start .sidebar-panel -->
                <div class="sidebar-panel">
                    <h4 class="sidebar-panel-title"><i class="im-fire"></i>Human Resources</h4>
                    
                </div>
                <!-- End .sidebar-panel -->
            </div>
            <!-- End .sidebar-inner -->
        </div>
        <!-- End #sidebar -->
        <!-- Start #right-sidebar -->
        <div id="right-sidebar" class="hide-sidebar">
            <!-- Start .sidebar-inner -->
            <div class="sidebar-inner">
                <div class="sidebar-panel mt0">
                    <div class="sidebar-panel-content fullwidth pt0">
                        <div class="chat-user-list">
                            <form class="form-horizontal chat-search" role="form">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="搜索账号...">
                                    <button type="submit"><i class="ec-search s16"></i>
                                    </button>
                                </div>
                                <!-- End .form-group  -->
                            </form>
                            <ul class="chat-ui bsAccordion">
                                <li>
                                    <a>历史账号 <i class="en-arrow-down5"></i></a>
                                    <ul class="in">
                                        <li>
                                            <a href="#" class="chat-name">
                                                Chad Engle
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="chat-name">
                                                Anthony Lagoon</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="chat-box">
                            <h5>Chad Engle</h5>
                            <a id="close-user-chat" href="#" class="btn btn-xs btn-primary"><i class="en-arrow-left4"></i></a>
                            <ul class="chat-ui chat-messages">
                                <li class="chat-user">
                                    <p class="avatar">
                                        <img src="assets/img/avatars/49.jpg" alt="@chadengle">
                                    </p>
                                    <p class="chat-name">Chad Engle <span class="chat-time">15 seconds ago</span>
                                    </p>
                                    <span class="status online"><i class="en-dot"></i></span>
                                    <p class="chat-txt">Hello Sugge check out the last order.</p>
                                </li>
                                <li class="chat-me">
                                    <p class="avatar">
                                        <img src="assets/img/avatars/48.jpg" alt="SuggeElson">
                                    </p>
                                    <p class="chat-name">SuggeElson <span class="chat-time">10 seconds ago</span>
                                    </p>
                                    <span class="status online"><i class="en-dot"></i></span>
                                    <p class="chat-txt">Ok i will check it out.</p>
                                </li>
                                <li class="chat-user">
                                    <p class="avatar">
                                        <img src="assets/img/avatars/49.jpg" alt="@chadengle">
                                    </p>
                                    <p class="chat-name">Chad Engle <span class="chat-time">now</span>
                                    </p>
                                    <span class="status online"><i class="en-dot"></i></span>
                                    <p class="chat-txt">Thank you, have a nice day</p>
                                </li>
                            </ul>
                            <div class="chat-write">
                                <form action="#" class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <textarea name="sendmsg" id="sendMsg" class="form-control elastic" rows="1"></textarea>
                                        <a role="button" class="btn" id="attach_photo_btn">
                                            <i class="fa-picture s20"></i> 
                                        </a>
                                        <input type="file" name="attach_photo" id="attach_photo">
                                    </div>
                                    <!-- End .form-group  -->
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End .sidebar-inner -->
        </div>
        <!-- End #right-sidebar -->
        <!-- Start #content -->
        <div id="content" class="full-page overLap">