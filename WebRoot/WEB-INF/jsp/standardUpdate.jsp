<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<div style="margin: 10px;">
            	<!-- 导航条  -->
            	<ul  class="breadcrumb">
            		<li><a href="index.html">首页</a><i class="en-arrow-right7"></i></li>
            		<li></i><a href="#">薪酬管理</a><i class="en-arrow-right7"></i></li>
            		<li></i><a href="standard.html">薪酬标准管理</a><i class="en-arrow-right7"></i></li>
            		<li><i class="en-arrow-right7"></i> 标准修改</li>
            	</ul>
            	
              <ul class="row back">
              	<li class=" col-md-3">
              		<span >
              			薪酬标准编号:         			
              		</span>
              		<input type="text" name="" id="" class="select" />
              	</li>
              	
              	<li class="  col-md-3">
              		<span >薪酬标准名称:</span>
              		<input type="text" name="" id="" class="select" />
              		
              	</li>
              	
              	
              	<li class="col-md-3">
              		<span>薪酬总额:</span>
              		<input type="text" name="" id="" class="select" />
              	</li>
              	
              		<li class="  col-md-3">
              		<span >制定人:</span>
              		<input type="text" name="" id="" class="select" />
              	</li>
              </ul>
              
            <ul class="row back">     
              	<li class="col-md-3">
              		<span>
              			登记人:
              			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
              		</span>
              		<input type="text" name="" id="" class="select" readonly/>
              	</li>     	
              	
              	<li class="col-md-3">
              		<span>
              			登记时间: 
              			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              		</span>
              		<input type="text" name="" id="" class="select" readonly/>
              	</li>  	        		
              </ul>
              
                <ul class="row back">             	
              	
					   <div class="ta">
            <table class="providerTable" cellpadding="0" cellspacing="0">
                <tbody>
                <tr class="firstTr">
                    <th width="10%">序号</th>
                    <th width="40%">薪酬项目名称</th>
                    <th width="50%">金额</th>
                </tr>
                
                <tr>
                    <td>1</td>
                    <td>基本工资</td>
                    <td><input type="number" class="select2"></td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>绩效奖金</td>
                    <td><input type="number" class="select2"></td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>交通补助</td>
                    <td><input type="number" class="select2"></td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>通讯补助</td>
                    <td><input type="number" class="select2"></td>
                </tr>
                <tr>
                    <td>5</td>
                    <td>餐补</td>
                    <td><input type="number" class="select2"></td>
                </tr>
                <tr>
                    <td>6</td>
                    <td>住房补助</td>
                    <td><input type="number" class="select2"></td>
                </tr>
                <tr>
                    <td>7</td>
                    <td>出差补助</td>
                    <td><input type="number" class="select2"></td>
                </tr>
                <tr>
                    <td>8</td>
                    <td>加班补助</td>
                    <td><input type="number" class="select2"></td>
                </tr>
            </tbody>
        </table>               
     </div>     
   </ul>
              
              <ul>
              	<li class="bynnt3ge bynnt3gelert">
					<input type="button"  class="btn btn-primary" name="" id="" value="修改提交" />
				</li>
				<li class="bynnt3ge">
					<input type="button"  class="btn btn-primary" name="" id="" value="返回" />
				</li>
              </ul>
			</div>
<%@include file="footer.jsp"%>