<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<div style="margin: 10px;">
            	<!-- 导航条  -->
            	<ul  class="breadcrumb">
            		<li><a href="index.html">首页</a><i class="en-arrow-right7"></i></li>
            		<li></i><a href="#">招聘管理</a><i class="en-arrow-right7"></i></li>
            		<li></i><a href="#">职位发布管理</a><i class="en-arrow-right7"></i></li>
            		<li><i class="en-arrow-right7"></i> 查看招聘信息</li>
            	</ul>
            	
              <ul class="row back">
              	<li class=" col-md-3 ">
              		<label >
              			部门:&nbsp;&nbsp;
              			&nbsp;&nbsp;&nbsp;
              			&nbsp;             			
              		</label>
              			<span>财政部</span>
              		
              	</li>
              	
              	<li class="  col-md-3">
              		<label >招聘类型:</label>
              		<span>校园招聘</span>
              	</li>
              	
              	
              	<li class="col-md-3">
              		<label>招聘人数:</label>
              		<span>5</span>
              	</li>
              	
              		<li class="  col-md-3">
              		<label >职位名称:</label>
              		<span>会计员</span>
              	</li>
              </ul>
              
            <ul class="row back">     
              	<li class="col-md-3">
              		<label>职位编码:</label>
              		<span>ADSADW</span>
              	</li>
              	
              	<li class="col-md-3">
              		<label>职位分类:</label>
              		<span>技术</span>
              	</li>
              	
              	<li class="col-md-3">
              		<label>登记人: &nbsp;&nbsp;&nbsp;</label>
              		<span>小正</span>
              	</li>
              	
              	<li class="col-md-3">
              		<label>登记时间:</label>
              		<span>2018-06-21</span>
              	</li>
              	
              	        		
              </ul>
              
              <ul class="row back">
              	<li class="col-md-3">
              		<label>截止时间:</label>
              		<span>2018-06-21</span>
              	</li>
              </ul>
              
             <ul class="row back">             	
              	<li class="col-md-12">
					<label style="float: left;">职位描述:</label>
              		<textarea name="" rows="8" cols="100%" readonly></textarea>
              	</li>
              </ul>
              
                <ul class="row back">             	
              	<li class="col-md-12">
					<label style="float: left;">招聘要求:</label>
              		<textarea name="" rows="8" cols="100%" readonly></textarea>
              	</li>
              </ul>
              
              <ul  >
              	<li>
					<input type="button"  class="btn btn-primary bunntleft" name="" id="" value="保存" />
				</li>
				<li>
					<input type="button"  class="btn btn-primary bunnt" name="" id="" value="返回" />
				</li>
              </ul>
			</div>
<%@include file="footer.jsp"%>