<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!-- Important javascript libs(put in all pages) -->



        
        <script>
        window.jQuery || document.write('<script src="assets/js/libs/jquery-2.1.1.min.js">\x3C/script>')
        </script>
        <script src="assets/js/jquery-ui.js"></script>
        
        <!-- Bootstrap plugins -->
        <script src="assets/js/bootstrap/bootstrap.js"></script>
        <!-- Core plugins ( not remove ever) -->
        <!-- Handle responsive view functions -->
        <script src="assets/js/jRespond.min.js"></script>
        <!-- Custom scroll for sidebars,tables and etc. -->
        <script src="assets/plugins/core/slimscroll/jquery.slimscroll.min.js"></script>
        <script src="assets/plugins/core/quicksearch/jquery.quicksearch.js"></script>
        <!-- Bootbox confirm dialog for reset postion on panels -->
        <script src="assets/plugins/forms/icheck/jquery.icheck.js"></script>
        <script src="assets/plugins/misc/highlight/highlight.pack.js"></script>
        <script src="assets/plugins/misc/countTo/jquery.countTo.js"></script>
        <script src="assets/js/jquery.sprFlat.js"></script>
        <script src="assets/js/jquery.cookie.js"></script>
        <script src="assets/js/publicUtil.js"></script>
        <script src="assets/js/app.js"></script>
        <script src="assets/js/ajaxfileupload.js"></script>
        <script src="assets/js/test.js"></script>
        <!-- 高巍 -->
        <script src="assets/localjs/user.js"></script>
        <script src="assets/localjs/model.js"></script>
        <script src="assets/localjs/role.js"></script>
        <script src="assets/localjs/agent.js"></script>
        <!-- 高巍 -->
        
         <script type="text/javascript">
        $(function () {
            $('.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', '关闭');
            function openLi(){
            	$('#infoAdd').removeAttr("disabled");
            	$('#infoAdd').removeClass("btn-default");
            	$('#infoAdd').addClass("btn-primary");
                $("#infoDel").removeAttr("disabled");
            	$('#infoDel').removeClass("btn-default");
            	$('#infoDel').addClass("btn-info");
                $('#infoUpdate').removeAttr("disabled");
            	$('#infoUpdate').removeClass("btn-default");
            	$('#infoUpdate').addClass("btn-warning");
            }
            function closeLi(){
            	$("#infoAdd").attr("disabled","disabled");
            	$('#infoAdd').removeClass("btn-primary");
            	$('#infoAdd').addClass("btn-default");
                $("#infoDel").attr("disabled","disabled");
            	$('#infoDel').removeClass("btn-info");
            	$('#infoDel').addClass("btn-default");
                $("#infoUpdate").attr("disabled","disabled");
            	$('#infoUpdate').removeClass("btn-warning");
            	$('#infoUpdate').addClass("btn-default");
            }
            $('.tree li.parent_li > span').on('click', function (e) {
                var children = $(this).parent('li.parent_li').find(' > ul > li');
                if (children.is(":visible")) {
                    children.hide('fast');
                    $(this).attr('title', '展开').find(' > i').addClass('icon-plus-sign').removeClass('icon-minus-sign');
                    openLi();
                } else {
                    children.show('fast');
                    $(this).attr('title', '关闭').find(' > i').addClass('icon-minus-sign').removeClass('icon-plus-sign');
                    closeLi();
                }
                e.stopPropagation();
            });
            $('.tree li').on('click', function (e) {
                $("#infoDel").removeAttr("disabled");
            	$('#infoDel').removeClass("btn-default");
            	$('#infoDel').addClass("btn-info");
                $('#infoUpdate').removeAttr("disabled");
            	$('#infoUpdate').removeClass("btn-default");
            	$('#infoUpdate').addClass("btn-warning");
            });
        });
    </script>
        <!--
        	作者：offline
        	时间：2018-08-15
        	描述：时间格式JS   开始
        -->   		
			<script src="assets/time/datedropper.min.js"></script>
			<script src="assets/time/timedropper.min.js"></script>
			<script>
			$("#pickdate").dateDropper({
				animate: false,
				format: 'Y-m-d',
				maxYear: '2020'
			});
			
			
			$("#date").dateDropper({
				animate: false,
				format: 'Y-m-d',
				maxYear: '2020'
			});
			</script>		
		<!--
        	作者：offline
        	时间：2018-08-15
        	描述：时间格式JS  结束
        -->
