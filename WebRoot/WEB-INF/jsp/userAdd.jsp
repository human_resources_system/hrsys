<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<div style="margin: 10px;">
            	<!-- 导航条  -->
            	<ul  class="breadcrumb">
            		<li><a href="index.html">首页</a><i class="en-arrow-right7"></i></li>
            		<li></i><a href="#">系统设置</a><i class="en-arrow-right7"></i></li>
            		<li><i class="en-arrow-right7"></i>添加用户</li>
            	</ul>
			</div>   
			<ul class="row back">
				<li class="col-md-5">
              		<span>用户名称:</span>
              		<input type="text" name=""  id="showusername" class="select"/>
              </li>
			</ul>
			<ul class="row back">
				<li class="col-md-5">
              		<span>用户账号:</span>
              		<input type="text" name="" onblur="username()" id="username" class="select"/>
              		&nbsp;<span id="accerror"></span>
              </li>
			</ul>
			<ul class="row back">
				<li class="col-md-5">
              		<span>用户密码:</span>
              		<input type="password" name="" onBlur="pwdlength()" id="password" class="select"/>
              		&nbsp;<span id="pwd">注意：密码长度要大于6位，小于20位</span>
              </li>
              
			</ul>
			<ul class="row back">
				<li class="col-md-5">
              		<span>确认密码:</span>
              		<input type="password" name=""  onBlur="pwdAgr()" id="passwords" class="select"/>
              		&nbsp;<span id="pwds" ></span>
              </li>
			</ul>
			<ul class="row back">
				<li class="col-md-5">
              		<span>用户角色:</span>
              		<select class="select" id="roleId">
              		<c:forEach items="${rolelist}" var="role">
              			<option value="${role.roleID}">${role.roleName}</option>
              		</c:forEach>
              		</select>
              </li>
			</ul>
			<ul class="row back">
				<li class="col-md-5">
              		<span>用户头像:</span>
              		<div class="file-box btn btn-primary">
                    <input type="file" id="via" name="via" class="file-btn"/>
                                   上传文件
                    </div>
              </li>
			</ul>
			<ul class="row back">             	
              	<li class="col-md-12">
					<span style="float: left;">用户备注:</span>
              		<textarea name="" rows="8" cols="100%" id="remark"></textarea>
              	</li>
              </ul>
              <ul  >
              	<li>
					<input type="button"  class="btn btn-primary bunntleft" name="" id="saveuser" value="保存" />
				</li>
				<li>
					<input type="button"  class="btn btn-primary bunnt" name="" id="returnuser" value="返回" />
				</li>
              </ul>
<%@include file="footer.jsp"%>