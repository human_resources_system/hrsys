<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<div style="margin: 10px;">
            	<!-- 导航条  -->
            	<ul  class="breadcrumb">
            		<li><a href="index.html">首页</a><i class="en-arrow-right7"></i></li>
            		<li></i><a href="#">我的工作平台</a><i class="en-arrow-right7"></i></li>
            		<li><i class="en-arrow-right7"></i> 待办管理</li>
            	</ul>
			</div>
			

            <div >
					<ul class="toubu">
							<li>
								<span>任务名称：</span>
								<input type="text" id="agentname" class="test" />					
							</li>
							<li>								
								<span>待办类型：</span>
								<select class="select" id="agenttype">
								<option value="0">--请选择--</option>
              		<c:forEach items="${publist}" var="pub">
              			<option value="${pub.classvalueId}">${pub.classvalueName}</option>
              		</c:forEach>
              		</select>
							</li>
							<li>
								<center>
										<span>提交日期：</span>
										<input type="text"  class="input test" id="date" />
								</center>					
							</li>
							<li>
								<input type="button" class="btn btn-primary" name="" id="selectagent" value="查询"/>
							</li>
					</ul>
		    </div>
                
      
                
                
        <div class="ta">
            <table class="providerTable" cellpadding="0" cellspacing="0">
                <tbody>
                <tr class="firstTr">
                    <th width="20%">待办类型</th>
                    <th width="35%">待办任务名称</th>
                    <th width="35%">提交日期</th>
                    <th width="10%">操作</th>
                    
                </tr>
                </tbody>
                <tbody id="agentlist">
                <c:forEach items="${agentlist}" var="agent">
                <tr>
                    <td>${agent.typeName}</td>
                    <td>${agent.name}</td>
                    <td><fn:formatDate value='${agent.date}' pattern='yyyy-MM-dd' /></td>
                    <td>
                         <a href="#"><img src="assets/icon_9m8sb4cx6z/update.png" style="width: 15px;" onclick="agentItem('${agent.type}','${agent.agentId}')" title="处理"/></a>
                    </td>
                </tr>
                </c:forEach>
            </tbody>
        </table>               
     </div>     
                
                
        <div class="text-center">
        
<!--MvcPager v3.0.1 for ASP.NET MVC 4.0+ © 2009-2015 Webdiyer (http://www.webdiyer.com)-->
<ul class="pagination" data-currentpage="2" data-firstpage="/mvcpager/demos/applycss/" data-invalidpageerrmsg="页索引无效" data-outrangeerrmsg="页索引超出范围" data-pagecount="80" data-pageparameter="id" data-pagerid="Webdiyer.MvcPager" data-urlformat="/mvcpager/demos/applycss/__id__/" id="bootstrappager">
	    <li>
		    <a href="#" id="agenthomePage">首页</a>
		</li>
		<li><a href="#" id="agentprevPage">上页</a>	
		</li>
		<li>
			<a href="#"><span id="PageNum">${PageNum}</span>/<span id="Page">${Page}</span></a>
		</li>
		<li>
			<a href="#" id="agentnextPage">下页</a>
		</li>
		<li>
			<a href="#" id="agentendPage">尾页</a>
		</li>
</ul>
<!--MvcPager v3.0.1 for ASP.NET MVC 4.0+ © 2009-2015 Webdiyer (http://www.webdiyer.com)-->

</div>               	
                	</div>
               </div>      
<%@include file="footer.jsp"%>
