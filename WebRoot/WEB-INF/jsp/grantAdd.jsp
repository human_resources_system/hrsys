<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<div style="margin: 10px;">
            	<!-- 导航条  -->
            	<ul  class="breadcrumb">
            		<li><a href="index.html">首页</a><i class="en-arrow-right7"></i></li>
            		<li></i><a href="#">薪酬管理</a><i class="en-arrow-right7"></i></li>
            		<li></i><a href="grant.html">薪酬发放管理</a><i class="en-arrow-right7"></i></li>
            		<li><i class="en-arrow-right7"></i> 发放登记</li>
            	</ul>
            	
               <div class="ta">
        	
        	<div class="tada">
        		<label>薪酬单编号:</label>
        		<span>HS1000000003</span>
        	</div>
        	<div class="tada">
        		<label>机构:</label>
        		<span>集团（总裁办）/产品设计中心</span>
        	</div>
   			<div class="tada">
   					<label style="margin-right:20px;">本机构人数:<span style="margin-left:5px; font-weight: 500;">3</span></label>
   					<label style="margin-right:20px;">基本薪酬总额:<span style="margin-left:5px; font-weight: 500;">14,200.00</span></label>
   					<label style="margin-right:20px;">实发金额:<span style="margin-left:5px; font-weight: 500;">3</span></label>
   			</div>
   			<div class="tada">
   			   <div style="margin-right: 10px; float: left;">
					<input type="button"  class="btn btn-primary" name="" id="" value="保存" />
				</div>
				<div style="margin-right: 10px;  float: left;">
					<input type="button"  class="btn btn-primary" name="" id="" value="提交审核" />
				</div>
				<div style="margin-right: 10px;  float: left;">
					<input type="button"  class="btn btn-primary" name="" id="" value="返回" />
				</div>
   				<div style="float: right; margin-top: 10px;">
   					<label style="margin-right:20px;">登记人:<span style="margin-left:5px; font-weight: 500;">2013-9-9 12:00:00</span></label>
   				<label style="margin-right:20px;">登记时间:<span style="margin-left:5px; font-weight: 500;">2013-9-9 12:00:00</span></label>
   			   </div>
   			</div>
   			
   			
   			<div class="tada" style="margin-top:53px;">
			    <table class="providerTable" cellpadding="0" cellspacing="0">
                <tbody>
                <tr class="firstTr">
                    <th width="8.33%">序号</th>
                    <th width="8.33%">档案编码</th>
                    <th width="8.33%">姓名</th>
                    <th width="8.33%">基本工资</th>
                    <th width="8.33%">绩效工资</th>
                    <th width="8.33%">交通补助</th>
                    <th width="8.33%">通讯补助</th>
                    <th width="8.33%">餐补</th>
                    <th width="8.33%">住房补助</th>
                    <th width="8.33%">出差补助</th>
                    <th width="8.33%">加班补助</th>
                    <th width="8.33%">应扣金额</th>                   
                </tr>
                
                <tr>
                 	<td>1</td>
                 	<td>BDQN1112</td>	
                 	<td>张三</td>
                 	<td>1000.00</td>
                 	<td><input type="number" style="text-align: center;width: 80px;" value="1000.00"/></td>
                 	<td>100.00</td>
                 	<td>100.00</td>
                 	<td>100.00</td>
                 	<td>500.00</td>
                 	<td><input type="number" style="text-align: center;width: 80px;" value="200.00"/></td>
                 	<td><input type="number" style="text-align: center;width: 80px;" value="300.00"/></td>
                 	<td><input type="number" style="text-align: center;width: 80px;" value="400.00"/></td>
                </tr>
                 <tr>
                 	<td>1</td>
                 	<td>BDQN1112</td>	
                 	<td>张三</td>
                 	<td>1000.00</td>
                 	<td><input type="number" style="text-align: center;width: 80px;" value="1000.00"/></td>
                 	<td>100.00</td>
                 	<td>100.00</td>
                 	<td>100.00</td>
                 	<td>500.00</td>
                 	<td><input type="number" style="text-align: center;width: 80px;" value="200.00"/></td>
                 	<td><input type="number" style="text-align: center;width: 80px;" value="300.00"/></td>
                 	<td><input type="number" style="text-align: center;width: 80px;" value="400.00"/></td>
                </tr>
                <tr>
                 	<td>1</td>
                 	<td>BDQN1112</td>	
                 	<td>张三</td>
                 	<td>1000.00</td>
                 	<td><input type="number" style="text-align: center;width: 80px;" value="1000.00"/></td>
                 	<td>100.00</td>
                 	<td>100.00</td>
                 	<td>100.00</td>
                 	<td>500.00</td>
                 	<td><input type="number" style="text-align: center;width: 80px;" value="200.00"/></td>
                 	<td><input type="number" style="text-align: center;width: 80px;" value="300.00"/></td>
                 	<td><input type="number" style="text-align: center;width: 80px;" value="400.00"/></td>
                </tr>
                <tr>
                 	<td>1</td>
                 	<td>BDQN1112</td>	
                 	<td>张三</td>
                 	<td>1000.00</td>
                 	<td><input type="number" style="text-align: center;width: 80px;" value="1000.00"/></td>
                 	<td>100.00</td>
                 	<td>100.00</td>
                 	<td>100.00</td>
                 	<td>500.00</td>
                 	<td><input type="number" style="text-align: center;width: 80px;" value="200.00"/></td>
                 	<td><input type="number" style="text-align: center;width: 80px;" value="300.00"/></td>
                 	<td><input type="number" style="text-align: center;width: 80px;" value="400.00"/></td>
                </tr>
                
                
            </tbody>
        </table>
<%@include file="footer.jsp"%>