<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<div style="margin: 10px;">
            	<!-- 导航条  -->
            	<ul  class="breadcrumb">
            		<li></i><a href="index.html">系统设置</a><i class="en-arrow-right7"></i></li>
            		<li><i class="en-arrow-right7"></i> <a href="remunerationList.html">薪酬项目设置</a></li>
            	</ul>
			</div>
        <div class="ta">
            <table class="providerTable" cellpadding="0" cellspacing="0">
                <tbody>
                	<ul class="toubu">
                		<li>
                			<span>薪酬名称：</span>
							<input type="text" class="test" />	
                		</li>
                		<li>
                			<span>薪酬编号：</span>
							<input type="text" class="test" />	
                		</li>
                		<li>
                			<span>创建人：</span>
							<input type="text" class="test" />	
                		</li>
							<li>
								<center>
										<span>是否启用：</span>
										  <select id="city" class="select">
									        <option selected="" value="-- 请选择 --">-- 请选择 --</option>
									        <option value="是">是</option>
									        <option value="否">否</option>
									      </select>
								</center>					
							</li>
                		<li>
							<a href="RemunerationAdd.html">
							<input type="button" class="btn btn-danger" name="" id="" value="新增薪酬"/></a>
						</li>
                	</ul>
				
                <tr class="firstTr">
                    <th width="10%">薪酬编号</th>
                    <th width="10%">薪酬名称</th>
                    <th width="20%">薪酬描述</th>
                    <th width="10%">创建时间</th>
                    <th width="10%">创建人</th>
                    <th width="10%">是否启用</th>
                    <th width="30%">操作</th>
                </tr>
                <tr>
                    <td>001</td>
                    <td>北京市粮油总公司</td>
                    <td>很好的东西</td>
                    <td>2015-11-12</td>
                    <td>二狗</td>
                    <td>是</td>
                    <td>
                        <a href="remunerationView.html"><img src="assets/icon_9m8sb4cx6z/all.png"  style="width: 15px;"  title="查看"/></a>
                          <a href="remunerationUpdate.html"><img src="assets/icon_9m8sb4cx6z/update.png" style="width: 15px;"  title="修改"/></a>
                            <a href="providerView.html"><img src="assets/icon_9m8sb4cx6z/delete.png" style="width: 15px;"  title="删除"/></a>
                       	
                    </td>
                </tr>
                <tr>
                    <td>002</td>
                    <td>湖南市粮油总公司</td>
                    <td>很好的东西</td>
                    <td>2015-11-12</td>
                    <td>二狗子</td>
                    <td>否</td>
                    <td>
                        <a href="providerView.html"><img src="assets/icon_9m8sb4cx6z/all.png"  style="width: 15px;"  title="查看"/></a>
                          <a href="providerView.html"><img src="assets/icon_9m8sb4cx6z/update.png" style="width: 15px;"  title="修改"/></a>
                            <a href="providerView.html"><img src="assets/icon_9m8sb4cx6z/delete.png" style="width: 15px;"  title="删除"/></a>
                    </td>
                </tr>
            </tbody>
        </table>               
     </div>
<%@include file="footer.jsp"%>