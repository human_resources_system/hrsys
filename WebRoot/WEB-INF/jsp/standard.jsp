<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<div style="margin: 10px;">
            	<!-- 导航条  -->
            	<ul  class="breadcrumb">
            		<li><a href="index.html">首页</a><i class="en-arrow-right7"></i></li>
            		<li></i><a href="#">薪酬管理</a><i class="en-arrow-right7"></i></li>
            		<li><i class="en-arrow-right7"></i> 薪酬标准管理</li>
            	</ul>
			</div>
			

            <div >
					<ul class="toubu">
							<li>
								<span>薪酬标准编号：</span>
								<input type="text" class="test" />					
							</li>
							<li>
								<span>审核状态：</span>
								<input type="text" class="test"/>					
							</li>
							<li>								
								<center>
										<span>登记日期：</span>
										<input type="text" class="input test" id="pickdate" />
										<span>至</span>
										<input type="text" class="input test" id="date" />
								</center>	
								
							</li>
							
							<li>
								<input type="button" class="btn btn-primary" name="" id="" value="查询" />
							</li>
							<li>
								<input type="button" class="btn btn-danger" name="" id="" value="标准登记" />
								
							</li>
					</ul>
		    </div>
                
      
                
                
        <div class="ta">
            <table class="providerTable" cellpadding="0" cellspacing="0">
                <tbody>
                <tr class="firstTr">
                    <th width="15%">薪酬标准编码</th>
                    <th width="15%">薪酬标准名称</th>
                    <th width="10%">审核状态</th>
                    <th width="10%">登记时间</th>
                    <th width="30%">操作</th>
                    
                </tr>
                
                <tr>
                    <td>1000001</td>
                    <td>高级软件工程师</td>
                    <td>起草</td>
                    <td>2013-9-9</td>              
                    <td>
                        <a href="standardView.html"><img src="assets/icon_9m8sb4cx6z/查看.png"  style="width: 20px;"  title="查看"/></a>
                          <a href="standardUpdate.html"><img src="assets/icon_9m8sb4cx6z/修改 .png" style="width: 20px;"  title="修改"/></a>
                            <a href="providerView.html"><img src="assets/icon_9m8sb4cx6z/删除.png" style="width: 20px;"  title="删除"/></a>
                    </td>
                </tr>
                 <tr>
                    <td>1000001</td>
                    <td>高级软件工程师</td>
                    <td>起草</td>
                    <td>2013-9-9</td>              
                    <td>
                        <a href="standardView.html"><img src="assets/icon_9m8sb4cx6z/查看.png"  style="width: 20px;"  title="查看"/></a>
                          <a href="standardUpdate.html"><img src="assets/icon_9m8sb4cx6z/修改 .png" style="width: 20px;"  title="修改"/></a>
                            <a href="providerView.html"><img src="assets/icon_9m8sb4cx6z/删除.png" style="width: 20px;"  title="删除"/></a>
                    </td>
                </tr>
            </tbody>
        </table>               
     </div>     
                
                
        <div class="text-center">
        
<!--MvcPager v3.0.1 for ASP.NET MVC 4.0+ © 2009-2015 Webdiyer (http://www.webdiyer.com)-->
<ul class="pagination" data-currentpage="2" data-firstpage="/mvcpager/demos/applycss/" data-invalidpageerrmsg="页索引无效" data-outrangeerrmsg="页索引超出范围" data-pagecount="80" data-pageparameter="id" data-pagerid="Webdiyer.MvcPager" data-urlformat="/mvcpager/demos/applycss/__id__/" id="bootstrappager">
	    <li>
		    <a href="/mvcpager/demos/applycss/">首页</a></li><li><a href="/mvcpager/demos/applycss/">上页</a>	
		</li>
		<li>
			<a href="/mvcpager/demos/applycss/">1</a>
		</li>
		<li >
			<a href="#">2</a>
		</li>
		<li>
			<a href="/mvcpager/demos/applycss/3/">3</a>
		</li>
		<li>
			<a href="/mvcpager/demos/applycss/4/">4</a>
		</li>
		<li>
			<a href="/mvcpager/demos/applycss/5/">5</a>
		</li>
		<li>
			<a href="/mvcpager/demos/applycss/6/">6</a>
		</li>
		<li>
			<a href="/mvcpager/demos/applycss/7/">7</a>
		</li>
		<li>
			<a href="/mvcpager/demos/applycss/8/">8</a>
		</li>
		<li>
			<a href="/mvcpager/demos/applycss/9/">9</a>
		</li>
		<li>
			<a href="/mvcpager/demos/applycss/10/">10</a>
		</li>
		<li>
			<a href="/mvcpager/demos/applycss/11/">...</a>			
		</li>
		<li>
			<a href="/mvcpager/demos/applycss/3/">下页</a>
		</li>
		<li>
			<a href="/mvcpager/demos/applycss/80/">尾页</a>
		</li>
</ul>
<!--MvcPager v3.0.1 for ASP.NET MVC 4.0+ © 2009-2015 Webdiyer (http://www.webdiyer.com)-->

</div>               	
                	</div>
               </div>
<%@include file="footer.jsp"%>