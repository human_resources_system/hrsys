<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fn" %>
<%@include file="header.jsp"%>
<div style="margin: 10px;">
            	<!-- 导航条  -->
            	<ul  class="breadcrumb">
            		<li><a href="index.html">首页</a><i class="en-arrow-right7"></i></li>
            		<li></i><a href="#">薪酬管理</a><i class="en-arrow-right7"></i></li>
            		<li><i class="en-arrow-right7"></i> 薪酬发放管理</li>
            	</ul>
			</div>
			

            <div >
					<ul class="toubu">						
							<li>
								<input type="button" class="btn btn-primary" name="" id="btn_addGrantForm" value="新增" />
							</li>
							<li>
								<input type="button" class="btn btn-danger" name="" id="" value="删除" />				
							</li>
							<li>
								<input type="button" class="btn btn-primary" name="" id="" value="保存" />				
							</li>
							<li>
								<input type="button" class="btn btn-primary" name="" id="" value="取消" />				
							</li>
					</ul>
		    </div>
                
      
                
                
        <div class="tada">
            <table class="providerTable" cellpadding="0" cellspacing="0">
                <tbody>
                <tr class="firstTr">
                    <th width="5%">选择</th>
                    <th width="15%">薪酬单编号</th>
                    <th width="5%">薪酬次数</th>
                    <th width="5">总人数</th>
                    <th width="20%">基本薪酬总额</th>
                    <th width="20%">实发金额</th>
                    <th width="20%">发放时间</th>
                </tr>
                </tbody>
                <tbody id="grantForm">
                	
            		</tbody>
        </table>               
     </div>   
                     
        <div class="ta">
        	<div class="tada">
        		<label>薪酬单编号:</label>
        		<span id="rewardbillCode">HS1000000003</span>
        	</div>
   			<div class="tada">
   					<label style="margin-right:20px;">薪酬次数:<span id="" style="margin-left:5px; font-weight: 500;">3</span></label>
   					<label style="margin-right:20px;">总人数:<span style="margin-left:5px; font-weight: 500;">4</span></label>
   					<label style="margin-right:20px;">基本薪酬总额:<span style="margin-left:5px; font-weight: 500;">14,200.00</span></label>
   					<label style="margin-right:20px;">实发金额:<span style="margin-left:5px; font-weight: 500;">3</span></label>
   			</div>
   			<div style="float: right; margin-top: 10px;">
   				<label style="margin-right:20px;">上次发薪时间:<span style="margin-left:5px; font-weight: 500;">2013-9-9 12:00:00</span></label>
   			</div>
   			<div class="tada">
			    <table class="providerTable" cellpadding="0" cellspacing="0">
                <tbody>
                <tr class="firstTr">
                    <th width="10%">序号</th>
                    <th width="15%">I级机构名称</th>
                    <th width="15%">II级机构名称</th>
                    <th width="10">人数</th>
                    <th width="25%">基本薪酬金额(元)</th>
                    <th width="25%">操作</th>                   
                </tr>
                
                <tr>
                 	<td>1</td>
                 	<td >集团(总裁办)</td>	
                 	<td >产品设计中心</td>
                 	<td >2</td>
                 	<td >6,600.00</td>
                 	<td >
                 		<a href="grantAdd.html">登记</a>
                 	</td>	
                </tr>
                  <tr>
                 	<td>1</td>
                 	<td >集团(总裁办)</td>	
                 	<td >产品设计中心</td>
                 	<td >2</td>
                 	<td >6,600.00</td>
                 	<td >
                 		<a href="grantCheck.html">审核</a>
                 	</td>	
                </tr>
                
            </tbody>
        </table>       
        </div>
     </div> 
<%@include file="footer.jsp"%>