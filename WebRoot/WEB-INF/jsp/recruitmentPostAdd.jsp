<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<div style="margin: 10px;">
            	<!-- 导航条  -->
            	<ul  class="breadcrumb">
            		<li><a href="index.html">首页</a><i class="en-arrow-right7"></i></li>
            		<li></i><a href="#">招聘管理</a><i class="en-arrow-right7"></i></li>
            		<li></i><a href="#">职位发布管理</a><i class="en-arrow-right7"></i></li>
            		<li><i class="en-arrow-right7"></i> 发布招聘信息</li>
            	</ul>
            	
              <ul class="row back">
              	<li class=" col-md-3">
              		<span >
              			部门:&nbsp;&nbsp;
              			&nbsp;&nbsp;&nbsp;
              			&nbsp;             			
              		</span>
              		<select class="select">
              			<option>--请选择--</option>
              		</select> 
              	</li>
              	
              	<li class="  col-md-3">
              		<span >招聘类型:</span>
              		<select class="select">
              			<option>--请选择--</option>
              			<option>--校园招聘--</option>
              			<option>--社会招聘--</option>
              		</select> 
              	</li>
              	
              	
              	<li class="col-md-3">
              		<span>招聘人数:</span>
              		<input type="number" name="" id="" class="select" />
              	</li>
              	
              		<li class="  col-md-3">
              		<span >职位名称:</span>
              		<select class="select">
              			<option>--请选择--</option>          			
              		</select> 
              	</li>
              </ul>
              
            <ul class="row back">     
              	<li class="col-md-3">
              		<span>职位编码:</span>
              		<input type="text" name="" id="" class="select" readonly/>
              	</li>
              	
              	<li class="col-md-3">
              		<span>职位分类:</span>
              		<input type="text" name="" id="" class="select" readonly/>
              	</li>
              	
              	<li class="col-md-3">
              		<span>登记人: &nbsp;&nbsp;&nbsp;</span>
              		<input type="text" name="" id="" class="select" readonly/>
              	</li>
              	
              	<li class="col-md-3">
              		<span>登记时间:</span>
              		<input type="text" name="" id="" class="select" readonly/>
              	</li>
              	
              	        		
              </ul>
              
              <ul class="row back">
              	<li class="col-md-3">
              		<span>截止时间:</span>
              		<input type="text" class="input select" id="date" />
              	</li>
              </ul>
              
             <ul class="row back">             	
              	<li class="col-md-12">
					<span style="float: left;">职位描述:</span>
              		<textarea name="" rows="8" cols="100%"></textarea>
              	</li>
              </ul>
              
                <ul class="row back">             	
              	<li class="col-md-12">
					<span style="float: left;">招聘要求:</span>
              		<textarea name="" rows="8" cols="100%"></textarea>
              	</li>
              </ul>
              
              <ul  >
              	<li>
					<input type="button"  class="btn btn-primary bunntleft" name="" id="" value="保存" />
				</li>
				<li>
					<input type="button"  class="btn btn-primary bunnt" name="" id="" value="返回" />
				</li>
              </ul>
			</div>
<%@include file="footer.jsp"%>