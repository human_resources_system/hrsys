<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<div style="margin: 10px;">
                 <ul  class="breadcrumb">
            		<li><a href="index.html">首页</a><i class="en-arrow-right7"></i></li>
            		<li></i><a href="#">系统设置</a><i class="en-arrow-right7"></i></li>
            		<li><i class="en-arrow-right7"></i> 权限管理</li>
            	</ul>
			</div>
			
			<div >
					<ul class="toubu">
							<li>								
								<span>权限名称：</span>
								<input type="text" class="test"  id="modelname"/>
							</li>
							<li>
								<input type="button" class="btn btn-primary" name="" id="selectmdoel" value="查询"/>
							</li>
							<li>
								<input type="button" class="btn btn-danger" name="" id="addModel" value="添加权限"/>
							</li>
					</ul>
		    </div>
                
      
                
                
        <div class="ta">
            <table class="providerTable" cellpadding="0" cellspacing="0">
                <tbody>
                <tr class="firstTr">
                    <th width="18%">权限名称</th>
                    <th width="18%">父级权限</th>
                    <th width="18%">所属板块</th>
                    <th width="18%">更新时间</th>
                    <th width="18%">更新人</th>
                    <th width="10%">操作</th>
                </tr>
                 </tbody>
                  <tbody id="hrauthorityList">
                  <c:forEach items="${modellist}" var="model">
                <tr>
                    <td>${model.modelName}</td>
                    <td>${model.parentName}</td>
                    <td>${model.ancentorName}</td>
                    <c:choose>
                    <c:when test="${model.updateTime==null}">
                    <td><fn:formatDate value='${model.createTime}' pattern='yyyy-MM-dd' /></td>
                    </c:when>
                    <c:otherwise>
                    <td><fn:formatDate value='${model.updateTime}' pattern='yyyy-MM-dd' /></td>
                    </c:otherwise>
                    </c:choose>
                    <c:choose>
                    <c:when test="${model.updateByName==null}">
                    <td>${model.createByName}</td>
                    </c:when>
                    <c:otherwise>
                    <td>${model.updateByName}</td>
                    </c:otherwise>
                    </c:choose>
                    <td>
                          <a href="#"><img src="assets/icon_9m8sb4cx6z/update.png" style="width: 15px;"  onclick="updateModel('${model.modelID}')" title="修改"/></a>
                            <a href="#"><img src="assets/icon_9m8sb4cx6z/delete.png" style="width: 15px;" onclick="deleteModel('${model.modelID}')"  title="删除"/></a>
                       
                    </td>
                </tr>
                </c:forEach>
            </tbody>
        </table>               
     </div> 
     
     <div class="text-center">
        
<!--MvcPager v3.0.1 for ASP.NET MVC 4.0+ © 2009-2015 Webdiyer (http://www.webdiyer.com)-->
<ul class="pagination" data-currentpage="2" data-firstpage="/mvcpager/demos/applycss/" data-invalidpageerrmsg="页索引无效" data-outrangeerrmsg="页索引超出范围" data-pagecount="80" data-pageparameter="id" data-pagerid="Webdiyer.MvcPager" data-urlformat="/mvcpager/demos/applycss/__id__/" id="bootstrappager">
	    <li>
		    <a href="#" id="homePagemodel">首页</a>
		</li>
		<li><a href="#" id="prevPagemodel">上页</a>	
		</li>
		<li>
			<a href="#"><span id="PageNum">${PageNum}</span>/<span id="Page">${Page}</span></a>
		</li>
		<li>
			<a href="#" id="nextPagemodel">下页</a>
		</li>
		<li>
			<a href="#" id="endPagemodel">尾页</a>
		</li>
</ul>
<!--MvcPager v3.0.1 for ASP.NET MVC 4.0+ © 2009-2015 Webdiyer (http://www.webdiyer.com)-->
</div> 
<%@include file="footer.jsp"%>