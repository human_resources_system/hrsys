package cn.hrsys.service.impl;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.hrsys.pojo.Agent;
import cn.hrsys.pojo.PublicattributeInformation;
import cn.hrsys.service.AgentService;

public class AgentServiceImplTest {
	ApplicationContext ctx=new ClassPathXmlApplicationContext("applicationContext.xml");
	AgentService agentService=ctx.getBean("agentService",AgentService.class);
	@Test
	public void testGetAllAgentList() {
		List<Agent> list=agentService.getAllAgentList(1,7,0,2,3,4,"人",null);
		//int i=agentService.getAllAgentListPage(1,2,0,0,null,null);
		for(Agent a:list){
			System.out.println(a.getName());
		}
		//System.out.println(i);
	}
	

	@Test
	public void testGetAmendAgent() {
		Agent a=new Agent();
		a.setId(1);
		a.setType(2);
		int i=agentService.getAmendAgent(a);
		System.out.println(i);
	}

}
