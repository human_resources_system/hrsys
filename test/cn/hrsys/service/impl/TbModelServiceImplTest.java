package cn.hrsys.service.impl;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.hrsys.pojo.TbModel;
import cn.hrsys.service.TbModelService;
import cn.hrsys.util.AddUtil;

public class TbModelServiceImplTest {
	ApplicationContext ctx=new ClassPathXmlApplicationContext("applicationContext.xml");
	TbModelService tbModelService=ctx.getBean("tbModelService",TbModelService.class);

	
	AddUtil au=new AddUtil();
	
	
	
	@Test
	public void testCount(){
	}


	@Test
	public void testGetAllTbModelList() {
		 List<TbModel> list=tbModelService.getAllAuthority();
		 for(TbModel tm:list){
			 System.out.println(tm.getModelID());
		 }
	}

	@Test
	public void testGetAllTbUserNotTbModelList() {
		List<TbModel> list=tbModelService.getAllTbModelList(1,40,"用户");
		int i=tbModelService.getAllTbModelListPage("用户");
		 for(TbModel tm:list){
			 System.out.println(tm.getModelName());
		 }
		 System.out.println(i);
	}

	

	@Test
	public void testGetAllTbUserHadTbModelList() {
		int i=tbModelService.getAllUserAuthority3("f", "权限管理");
		 System.out.println(i);
	}

	@Test
	public void testGetAddTbModel() {
		TbModel tm=new TbModel();
			TbModel tbmodel=tbModelService.getAllmodelID(au.getRandomString(17));
			if(tbmodel==null){
				tm.setModelID(au.getRandomString(17));
				tm.setModelURL("analyse/");
				tm.setRelPage("analyse");
				int s=tbModelService.getAddTbModel(tm);
			}
		
	}

	@Test
	public void testGetAmendTbModel() {
		TbModel tm=new TbModel();
		tm.setModelID("asda");
		tm.setModelName("dasdasdsssssss");
		tm.setModelURL("dasdasdfasfd");
		int i=tbModelService.getAmendTbModel(tm);
		System.out.println(i);
	}

	@Test
	public void testGetDeleteTbModel() {
		int i=tbModelService.getDeleteTbModel("asda");
		System.out.println(i);
	}
	
	@Test
	public void testGetAllmodelID(){
		TbModel tm=tbModelService.getAllmodelID("asd");
		System.out.println(tm.getModelName());
	}
	

}
