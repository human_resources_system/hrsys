package cn.hrsys.service.impl;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.hrsys.pojo.TbRole;
import cn.hrsys.service.TbRoleService;

public class TbRoleServiceImplTest {
	ApplicationContext ctx=new ClassPathXmlApplicationContext("applicationContext.xml");
	TbRoleService tbRoleService=ctx.getBean("tbRoleService",TbRoleService.class);
	@Test
	public void testCount() {
		int i=tbRoleService.count();
		System.out.println(i);
	}

	@Test
	public void testGetAllTbRoleList() {
		List<TbRole> list=tbRoleService.getAllTbRoleList(1, 100, "e");
		int i=tbRoleService.getAllTbRoleListPage("e");
		for(TbRole tr:list){
			System.out.println(tr.getClassvalueName());
		}
		System.out.println(i);
	}
	
	
	@Test
	public void testGetAllRole(){
		TbRole tr=tbRoleService.getAllRoleEmploy("m");
		if(tr!=null){
			System.out.println("使用中");
		}else{
			System.out.println("未使用");
		}
	}

	@Test
	public void testGetAllTbRole() {
		TbRole tr=tbRoleService.getAllTbRole("qwe");
		System.out.println(tr.getRoleName());
	}

	@Test
	public void testGetAddTbRole() {
		TbRole tr=new TbRole();
		tr.setRoleID("fgh");
		tr.setRoleName("班主任");
		int i=tbRoleService.getAddTbRole(tr);
		System.out.println(i);
	}

	@Test
	public void testGetAmendTbRole() {
		TbRole tr=new TbRole();
		tr.setRoleID("fgh");
		tr.setRoleName("傻掉");
		int i=tbRoleService.getAmendTbRole(tr);
		System.out.println(i);
	}

	@Test
	public void testGetDeleteTbRole() {
		int i=tbRoleService.getDeleteTbRole("fgh");
		System.out.println(i);
	}

}
