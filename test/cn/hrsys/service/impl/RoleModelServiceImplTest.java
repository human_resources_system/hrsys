package cn.hrsys.service.impl;

import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.hrsys.pojo.RoleModel;
import cn.hrsys.service.RoleModelService;

public class RoleModelServiceImplTest {
	ApplicationContext ctx=new ClassPathXmlApplicationContext("applicationContext.xml");
	RoleModelService roleModelService=ctx.getBean("roleModelService",RoleModelService.class);
	@Test
	public void testGetAddRoleModel() {
		RoleModel rm=new RoleModel();
		rm.setRoleID("c");
		rm.setModelID("0BU7oOsxo0E1E6uEl");
		int i=roleModelService.getAddRoleModel(rm);
		System.out.println(i);
	}

	@Test
	public void testGetDeleteRoleModel() {
		RoleModel rm=new RoleModel();
		rm.setRoleID("c");
		rm.setModelID("1qRx5HMWRaKcxH8EQ");
		int i=roleModelService.getDeleteRoleModel(rm);
		System.out.println(i);
	}

}
