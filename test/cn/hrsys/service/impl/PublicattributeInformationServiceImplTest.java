package cn.hrsys.service.impl;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.hrsys.pojo.PublicattributeInformation;
import cn.hrsys.service.PublicattributeInformationService;

public class PublicattributeInformationServiceImplTest {

	ApplicationContext ctx=new ClassPathXmlApplicationContext("applicationContext.xml");
	
	PublicattributeInformationService publicService=ctx.getBean("publicService",PublicattributeInformationService.class);
	
	@Test
	public void testGetAllpublic() {
		List<PublicattributeInformation> list =publicService.getAllpublic();
		for(PublicattributeInformation a:list){
			System.out.println(a.getClassvalueName());
		}
	}

}
