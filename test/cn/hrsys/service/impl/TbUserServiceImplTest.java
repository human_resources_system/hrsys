package cn.hrsys.service.impl;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.hrsys.pojo.TbUser;
import cn.hrsys.service.TbUserService;

public class TbUserServiceImplTest {
	ApplicationContext ctx=new ClassPathXmlApplicationContext("applicationContext.xml");
	TbUserService tbUserService=ctx.getBean("tbUserService",TbUserService.class);
	@Test
	public void testCount() {
		int i =tbUserService.count();
		System.out.println(i);
	}
	
	
	@Test
	public void testGetInquirylogUsername() {
		int i =tbUserService.getInquirylogUsername("123");
		System.out.println(i);
	}

	@Test
	public void testGetLogin() {
		TbUser tu=tbUserService.getLogin("gw", "123");
		System.out.println(tu);
	}

	@Test
	public void testGetAllUserList() {
		List<TbUser> list=tbUserService.getAllUserList(1, 40, "军","b");
		for(TbUser tu:list){
			System.out.println(tu.getCreateByName());
		}
		
	}

	@Test
	public void testGetAllUser() {
		TbUser tu=tbUserService.getAllUser(1);
		System.out.println(tu.getRoleName());
	}

	@Test
	public void testGetAddUser() {
		TbUser tu=new TbUser();
		tu.setUserID("sdas");
		tu.setLogUsername("asdasd");
		tu.setLogPassword("wwwqrqs");
		tu.setShowUsername("qhjs");
		int i=tbUserService.getAddUser(tu);
		System.out.println(i);
	}

	@Test
	public void testGetAmendUser() {
		TbUser tu=new TbUser();
		tu.setId(2);
		tu.setUserID("sdas");
		tu.setLogUsername("asdasd");
		tu.setLogPassword("qweqweqw");
		tu.setShowUsername("qhjs");
		int i=tbUserService.getAmendUser(tu);
		System.out.println(i);
	}

	@Test
	public void testGetDeleteUser() {
		int i=tbUserService.getDeleteUser(2);
		System.out.println(i);
	}

}
